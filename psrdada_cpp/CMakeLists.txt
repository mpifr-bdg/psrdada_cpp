include_directories(${CMAKE_SOURCE_DIR})

# Collect all include and source files from th current directory
file(GLOB PSRDADA_CPP_INC "${CMAKE_CURRENT_SOURCE_DIR}/*.cuh" "${CMAKE_CURRENT_SOURCE_DIR}/*.hpp")
file(GLOB PSRDADA_CPP_SRC "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cu")
add_library(${CMAKE_PROJECT_NAME} ${PSRDADA_CPP_SRC})
target_link_libraries(${CMAKE_PROJECT_NAME} PUBLIC ${DEPENDENCY_LIBRARIES})
set(PSRDADA_CPP_LIBRARIES ${CMAKE_PROJECT_NAME} ${DEPENDENCY_LIBRARIES})

# ----------------------------- #
# -- Build DADA tools / apps -- #
# ----------------------------- #
set(DADA_TOOLS_CLI
    junkdb
    syncdb
    file_to_dada
    dbnull
    dbreset
    dbdisk
    diskdb
    dbsplit
    zerodb
    dbdiskleap
    dbdisk_multithread
)
# Install the DADA tools using the DADA_TOOLS_NAMES list
foreach(EXEC_NAME ${DADA_TOOLS_CLI})
    set(EXEC_SRC "cli/${EXEC_NAME}.cpp")
    add_executable(${EXEC_NAME} ${EXEC_SRC})
    target_link_libraries(${EXEC_NAME} PUBLIC ${PSRDADA_CPP_LIBRARIES})
    install(TARGETS ${EXEC_NAME} DESTINATION bin)
endforeach()

# ------------------------------------------ #
# -- Install testing tools (dep to gtest) -- #
# ------------------------------------------ #
if(ENABLE_TESTING)
    set(PSRDADA_CPP_TESTING_INC
        testing_tools/clients.hpp
        testing_tools/streams.hpp
        testing_tools/benchmark_tools.hpp
        testing_tools/tools.cuh)
    if(ENABLE_BENCHMARK)
        list(APPEND ${PSRDADA_CPP_TESTING_INC} testing_tools/benchmark_tools.hpp)
    endif(ENABLE_BENCHMARK)
    install(FILES ${PSRDADA_CPP_TESTING_INC} DESTINATION include/psrdada_cpp/testing_tools)
    add_subdirectory(test)
endif()

# ---------------------------------- #
# -- Build and install submodules -- #
# ---------------------------------- #
if(BUILD_SUBMODULES)
  add_subdirectory(common)
  add_subdirectory(gated_spectrometer)
  add_subdirectory(fft_spectrometer)
  add_subdirectory(rs_spectrometer)
  add_subdirectory(merger)
endif()

if (BUILD_DADAFLOW)
  add_subdirectory(dadaflow)
endif()

# --------------------------------- #
# -- Build and install benchmark -- #
# --------------------------------- #
# Add benchmark binaries here to get executed with 'make -C <path-to-build> run_benchmark'
if(ENABLE_BENCHMARK AND BUILD_SUBMODULES)
    add_custom_target(run_benchmark
        COMMAND gspectrometer_benchmark --benchmark_counters_tabular=true
        COMMAND merger_benchmark --benchmark_counters_tabular=true
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
endif()

# --------------------------------------------------------------------- #
# Install targets: lib, bin and include - Also export the cmake project #
# --------------------------------------------------------------------- #
install (TARGETS ${CMAKE_PROJECT_NAME}
    EXPORT ${CMAKE_PROJECT_NAME}Targets
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    INCLUDES DESTINATION include/)
install(FILES ${PSRDADA_CPP_INC} DESTINATION include/psrdada_cpp)

# Generate the config file for find_package()
include(CMakePackageConfigHelpers)
set(GENEREATED_DIR "${CMAKE_BINARY_DIR}/generated" CACHE INTERNAL "")
set(CMAKE_FILES_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/${CMAKE_PROJECT_NAME}")
set(VERSION_FILE "${GENEREATED_DIR}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake")

# Use CMake generator to create version and config files
write_basic_package_version_file(${VERSION_FILE} VERSION ${PROJECT_VERSION} COMPATIBILITY AnyNewerVersion)
set(CONFIG_FILE "${GENEREATED_DIR}/${CMAKE_PROJECT_NAME}Config.cmake")
configure_package_config_file("${CMAKE_SOURCE_DIR}/cmake/${CMAKE_PROJECT_NAME}Config.cmake.in" "${CONFIG_FILE}" INSTALL_DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})

# Create cmake folder and export the cmake project
# install(TARGETS psrdada_cpp EXPORT psrdada_cppTargets)
install(EXPORT ${CMAKE_PROJECT_NAME}Targets
    FILE ${CMAKE_PROJECT_NAME}Targets.cmake
    NAMESPACE ${CMAKE_PROJECT_NAME}::
    DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})

# Install the generated files in the cmale folder
install(FILES ${VERSION_FILE} ${CONFIG_FILE}
  COMPONENT "${PROJECT_NAME}"
  DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})
