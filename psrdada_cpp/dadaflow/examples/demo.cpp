#include "psrdada_cpp/dadaflow/pipelines/Demo.hpp"

int main()
{
    using namespace psrdada_cpp;
    DemoPipeline<int> pipeline;
    Graph& graph = pipeline.graph();
    graph.show_dependencies();
    graph.show_topological_order();
    GraphRunner::run(&graph);
    std::cout << "----- Statistics -----\n";
    graph.show_statistics();
    graph.make_dot_graph();
    return 0;
}
