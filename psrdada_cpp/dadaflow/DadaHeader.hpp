#pragma once
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <string>
#include <optional>

namespace psrdada_cpp
{

/**
 * @brief Exception raised in the event of a requested
 *        key not being in the DADA header
 * 
 */
class InvalidHeaderKey: public std::runtime_error
{
  public:
    explicit InvalidHeaderKey(const std::string& key)
        : std::runtime_error("Key not found: " + key)
    {}
};
  
/**
 * @brief      A helper class for managing DADA headers
 *
 * @detail     DADA headers are composed of ASCII key-value
 *             pairs stored in a single char array.
 */
class DadaHeader
{
  public:
    using MapIterator =
        std::map<std::string, std::string, std::less<>>::const_iterator;

    DadaHeader()                             = default;
    DadaHeader(DadaHeader const&)            = default;
    DadaHeader& operator=(DadaHeader const&) = default;
    DadaHeader(DadaHeader&&)                 = default;
    DadaHeader& operator=(DadaHeader&&)      = default;
    ~DadaHeader()                            = default;

    /**
     * @brief Read the key-value pairs from an ascii format header
     *
     * @tparam Container The container type being read. Must be both
     *                   iterable and contain char data.
     *
     * @param container The container being read
     */
    template <typename Container>
        requires(std::is_same_v<typename Container::value_type, char> &&
                 Iterable<Container>)
    void read_from(Container const& container);

    /**
     * @brief Read the key-value pairs from an ascii format header
     *
     * @param bytes A psrdada_cpp RawBytes object
     */
    void read_from(RawBytes const& bytes);

    /**
     * @brief Read the key-value pairs from an ascii format header
     *
     * @param data A raw pointer to a char* buffer containing the header data
     * @param size The size of the buffer in bytes
     */
    void read_from(const char* data, std::size_t size);

    /**
     * @brief      Get a value from the header
     *
     * @param      key   An ASCII key (the name of the value to get)
     *
     * @tparam     T     The data type of the parameter to be read
     *
     * @return     The value corresponding to the given key
     */
    template <typename T>
    [[nodiscard]] T get(std::string_view key) const;

    /**
     * @brief      Get an optional value from the header
     *
     * @param      key   An ASCII key (the name of the value to get)
     *
     * @tparam     T     The data type of the parameter to be read
     *
     * @return     The value corresponding to the given key (optional)
     */
    template <typename T>
    [[nodiscard]] std::optional<T> get_optional(std::string_view key) const;

    /**
     * @brief      Get a vector of values from the header
     *
     * @param      key         An ASCII key (the name of the value to get)
     * @param      delimeter   The delimiter string that separates values
     *
     * @tparam     T     The data type of the parameters to be read
     *
     * @return     The value corresponding to the given key
     */
    template <typename T>
    [[nodiscard]] std::vector<T> get(std::string_view key,
                                     std::string_view delimiter) const;

    /**
     * @brief      Get an optional vector of values from the header
     *
     * @param      key         An ASCII key (the name of the value to get)
     * @param      delimeter   The delimiter string that separates values
     *
     * @tparam     T     The data type of the parameters to be read
     *
     * @return     The value corresponding to the given key (optional)
     */
    template <typename T>
    [[nodiscard]] std::optional<std::vector<T>> get_optional(std::string_view key,
                                                             std::string_view delimiter) const;

    /**
     * @brief      Set a value in the header
     *
     * @param      key    An ASCII key (the name of the value to be set)
     * @param[in]  value  The value to set
     *
     * @tparam     T      The type of value being set
     */
    template <typename T>
    void set(std::string_view key, T const& value);

    /**
     * @brief      Set a value in the header
     *
     * @param      key    An ASCII key (the name of the value to be set)
     * @param[in]  value  The value to set
     *
     * @note explicit specialisation for string to avoid it being treated like
     *       other STL containers.
     */
    void set(std::string_view key, std::string const& value);

    /**
     * @brief      Set a value in the header from a container
     *
     * @param      key    An ASCII key (the name of the value to be set)
     * @param[in]  values  The values to set
     *
     * @tparam     T      The type of value being set
     */
    template <template <typename, typename> class Container,
              typename T,
              typename A>
    void set(std::string_view key,
             Container<T, A> const& values,
             std::size_t precision = 15);

    /**
     * @brief      Clear all key-value pairs
     */
    void purge();

    /**
     * @brief An iterator to the start of the underlying map of key-value pairs
     *
     * @note This may be deprecated in future as it allows manipulation of
     *       the encapsulated key-value data.
     *
     * @return MapIterator
     */
    [[nodiscard]] MapIterator begin() const;

    /**
     * @brief An iterator to the end of the underlying map of key-value pairs
     *
     * @note This may be deprecated in future as it allows manipulation of
     *       the encapsulated key-value data.
     *
     * @return MapIterator
     */
    [[nodiscard]] MapIterator end() const;

    /**
     * @brief A const iterator to the start of the underlying map of key-value
     * pairs
     *
     * @note This may be deprecated in future as it allows manipulation of
     *       the encapsulated key-value data.
     *
     * @return MapIterator
     */
    [[nodiscard]] MapIterator cbegin() const;

    /**
     * @brief A const iterator to the end of the underlying map of key-value
     * pairs
     *
     * @note This may be deprecated in future as it allows manipulation of
     *       the encapsulated key-value data.
     *
     * @return MapIterator
     */
    [[nodiscard]] MapIterator cend() const;

    /**
     * @brief Check if a given key exists in the header
     * 
     * @param key The key to check
     * @return true The key is in the header
     * @return false They key is not in the header
     */
    [[nodiscard]] bool has_key(std::string_view key) const;

    /**
     * @brief Convert the header to a vector of chars
     *
     * @param compact Specify compact packing. Setting to false will pack the
     * header with extra whitespace and column alignment to make the header more
     * human readable. Default is compact packing.
     *
     * @return std::vector<char>
     */
    [[nodiscard]] std::vector<char> to_vector(bool compact = true) const;

  private:
    std::map<std::string, std::string, std::less<>> _header_map;

    template <typename T>
    std::string to_string(T const& value) const;

    std::string to_string(std::string_view value) const;

    void parse_line(std::string_view line);

    std::string_view trim(std::string_view str);

    void read_from_impl(std::istringstream& stream);

    std::string_view safe_lookup(std::string_view key) const;

    template <typename T>
    T convert_token(std::string_view token) const;
};

} // namespace psrdada_cpp

#include "psrdada_cpp/dadaflow/detail/DadaHeader.cpp"
