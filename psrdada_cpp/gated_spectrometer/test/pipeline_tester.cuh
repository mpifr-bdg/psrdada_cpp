#pragma once

#include <gtest/gtest.h>
#include <string>
#include <vector>
#include <numeric>

#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/dada_db.hpp"
#include "psrdada_cpp/testing_tools/streams.hpp"
#include "psrdada_cpp/testing_tools/tools.cuh"
#include "psrdada_cpp/gated_spectrometer/pipeline.cuh"

namespace psrdada_cpp {
namespace gated_spectrometer {
namespace test {


const std::size_t spead_size = 4096; // number of samples per spead heap

/**
 * @brief struct containing all necessary test parameters
 *
 */
struct test_config
{
    std::size_t fft_length;
    std::size_t naccumulate;
    std::size_t nbits;
    std::size_t nheaps_block;
    std::string msg;

    /**
     * @brief Determines values and return the actual pipe_config struct
     *          used to configure the gated_spectrometer::Pipeline
     *
     * @return pipe_config
     */
    pipe_config config()
    {
        pipe_config ip;
        ip.nbits = nbits;
        ip.fft_length = fft_length;
        ip.naccumulate = naccumulate;
        ip.selectedBit = 0;
        ip.selectedSideChannel = 0;
        ip.nSideChannels = 1;
        ip.speadHeapSize = spead_size * nbits / 8;
        ip.dadaBufferLayout.initialize(
            nheaps_block * (ip.speadHeapSize + ip.nSideChannels * 8),
            ip.speadHeapSize,
            ip.nSideChannels);
        return ip;
    }

    /**
     * @brief Computes and returns the number of input blocks required to produce a output block
     *
     * @param npol number of polarization
     * @return std::size_t
     */
    std::size_t nblocks(std::size_t npol=1)
    {
        std::size_t blocks = fft_length * naccumulate / (spead_size * nheaps_block);
        if(blocks == 0){ return 1;} // Catch the case: 1 in buffer -> N out buffer (N > 0)
        return blocks*npol;
    }

    std::size_t nspectra(std::size_t npol=1)
    {
        std::size_t nspec = nheaps_block * spead_size / (fft_length * naccumulate * npol);
        if(nspec == 0){ return 1;}
        return nspec;
    }

    std::size_t nchannels()
    {
        return fft_length / 2 + 1;
    }
};


/**
 * @brief Tester class for testing the gated_spectrometer::Pipeline
 *
 */
class PipelineTester : public ::testing::TestWithParam<test_config>
{
public:
    /**
     * @brief Construct a new Pipeline Tester object
     *
     */
    PipelineTester();

    /**
     * @brief Destroy the Pipeline Tester object
     *
     */
    ~PipelineTester();

    /**
     * @brief Test by injecting a DC offset to gated_spectrometer::Pipeline::operator()
     *
     * @tparam InputType - either SinglePolarizationInput or DualPolarizationInput
     * @tparam OutputType - either GatedPowerSpectrumOutput or GatedFullStokesOutput
     * @param num_obuffers Number of output buffers to produce
     */
    template<typename InputType, typename OutputType>
    void test_with_dc_offset(std::size_t num_obuffers=16);

    /**
     * @brief Test by injecting a delta impulse to gated_spectrometer::Pipeline::operator()
     *
     * @param off entire input block has the noise diode on or off, defaults to false
     *
     * ToDo: Support full stokes mode
     */
    void test_with_delta_impulse(bool off=false);

    /**
     * @brief Tests the sidechannel items for full Stokes mode
     *
     *  ToDo: Test is not generalized yet. Needs adjustment for setting sidechannel items
     */
    void test_sidechannel_items_stokes();

    /**
     * @brief Tests the sidechannel items for full dual pol mode
     *
     *  ToDo: Test is not generalized yet. Needs adjustment for setting sidechannel items
     */
    void test_sidechannel_items_dual();

    /**
     * @brief Test by injecting a waveform (sine) to gated_spectrometer::Pipeline::operator()
     *
     * ToDo: Implement the method
     */
    void test_detect_peak(){}

private:
    test_config _config;
    pipe_config _pipe_config;
    std::size_t obuffer_idx = 0;

    testing_tools::DummyStream<float> consumer;
    std::unique_ptr<RawBytes> raw_header;
    std::unique_ptr<RawBytes> raw_data;
    std::vector<char> header;
    std::vector<char> idata;
    uint64_t* sidechannel_items;
};

// --------------- //
// Implementations //
// --------------- //

PipelineTester::PipelineTester()
    : ::testing::TestWithParam<test_config>(),
    _config(GetParam())
{
    BOOST_LOG_TRIVIAL(debug) << "Constructing PipelineTester object";
    _pipe_config = _config.config();
    header.resize(4096);
    idata.resize(_pipe_config.dadaBufferLayout.getBufferSize());

    raw_data.reset(new RawBytes(reinterpret_cast<char *>(idata.data()), idata.size(), idata.size()));
    raw_header.reset(new RawBytes(reinterpret_cast<char *>(header.data()), header.size(), header.size()));
    sidechannel_items = reinterpret_cast<uint64_t*>(raw_data->ptr()
        + _config.nheaps_block * _pipe_config.speadHeapSize);
}


PipelineTester::~PipelineTester()
{
    BOOST_LOG_TRIVIAL(debug) << "Destroying PipelineTester object";
}


template<typename InputType, typename OutputType>
void PipelineTester::test_with_dc_offset(std::size_t num_obuffers)
{
    // Setup the environment
    std::size_t expected_dc = 0;
    std::size_t npol = 1;
    if(std::is_same<InputType, DualPolarizationInput>::value){npol = 2;}
    std::size_t nblocks = _config.nblocks(npol);
    std::size_t calls = 2 + (nblocks * (num_obuffers+1)); // Calls to Pipeline::operator() to generate desired num_obuffers

    // Initialize test object
    gated_spectrometer::Pipeline<decltype(consumer), InputType, OutputType>
        test_object(_pipe_config, consumer);
    EXPECT_NO_THROW(test_object.init(*raw_header));

    ASSERT_EQ(test_object.nblocks(), nblocks);
    // Write data to pipeline and evaluate the DC output
    for(std::size_t i = 0; i < calls; i++)
    {
        // Increment the DC offset by i/nblocks whenever a new oblock starts
        if(i % nblocks == 0)
        {
            testing_tools::fill_value(idata.data(),
                _pipe_config.dadaBufferLayout.sizeOfData(),
                _config.nbits, int8_t(i/nblocks));
        }
        ASSERT_FALSE(test_object(*raw_data));

        // New output buffer received
        if(consumer.call_count() == obuffer_idx + 1)
        {
            expected_dc = _pipe_config.fft_length
                * _pipe_config.fft_length
                * _pipe_config.naccumulate * npol
                * obuffer_idx * obuffer_idx;
            // Iterate over output spectra - catches case 1 input -> N outputs
            for(std::size_t ii = 0; ii < consumer.data_size(); ii+=(_config.nchannels()*2 + 8) * npol * npol)
            {
                for(std::size_t iii = 0; iii < _config.nchannels() * 2 * npol * npol; iii++)
                {
                    // Check the DC bin of spectra - except the first obuffer (DC offset is 0)
                    if(iii % (_config.nchannels() * 2 * npol) == 0 && obuffer_idx != 0)
                    {
                        EXPECT_NEAR(consumer.item(ii + iii)/expected_dc, 1, 0.00001)
                            << "Unexpected value in buffer " << obuffer_idx << " in DC bin " << ii + iii;
                    }
                    // Otherwise it should be zero
                    else
                    {
                        ASSERT_NEAR(consumer.item(ii + iii), 0, 0.00001)
                            << "Unexpected value in buffer " << obuffer_idx << " with fft-bin " << ii + iii;
                    }
                }
            }
            // consumer.save_data("tmp/test_buffer" + std::to_string(obuffer_idx) + ".dat");
            obuffer_idx = consumer.call_count();
        }
    }
    ASSERT_EQ(num_obuffers, consumer.call_count());
}


void PipelineTester::test_with_delta_impulse(bool off)
{
    // Setup the environment
    std::size_t calls = 2 + (_config.nblocks() * 9);
    float power_on = 0; float power_off=0;

    // Set noise diode
    for (size_t ii = 0; ii < _config.nheaps_block; ii++)
    {
        SET_BIT(sidechannel_items[ii], off);
    }

    // Initialize test object
    gated_spectrometer::Pipeline<decltype(consumer), SinglePolarizationInput, GatedPowerSpectrumOutput>
        test_object(_pipe_config, consumer);
    EXPECT_NO_THROW(test_object.init(*raw_header));

    ASSERT_EQ(test_object.nblocks(), _config.nblocks());
    // Write data to pipeline and validate the output spectra
    for(std::size_t i = 0; i < calls; i++){
        // Only one input buffer contains the delta impulse - Position 120 is the start of a 8,10, and 12-bit value
        if(i % _config.nblocks() == 0)
            testing_tools::set_value(idata.data(), int8_t(i/_config.nblocks()), _config.nbits, 120);
        else
            testing_tools::set_value(idata.data(), 0, _config.nbits, 120);

        ASSERT_FALSE(test_object(*raw_data));
        // New output buffer received
        if(consumer.call_count() == obuffer_idx + 1)
        {
            (off) ? power_on = obuffer_idx * obuffer_idx : power_off = obuffer_idx * obuffer_idx;
            // Check that every FFT bin contains the delta impulse power
            for(std::size_t ii = 0; ii < _config.nchannels(); ii++)
            {
                ASSERT_NEAR(consumer.item(ii), power_on, power_on * 0.00001)
                    << "Unexpected value in noise-on-buffer with index " << ii;
                ASSERT_NEAR(consumer.item(_config.nchannels() + ii), power_off, power_off * 0.00001)
                    << "Unexpected value in noise-off-buffer with index " << ii;
            }
            obuffer_idx = consumer.call_count();
        }
    }
}


void PipelineTester::test_sidechannel_items_stokes()
{
    // Initialize test object
    std::size_t calls = 2 + (_config.nblocks() * 2);
    gated_spectrometer::Pipeline<decltype(consumer), DualPolarizationInput, GatedFullStokesOutput>
        test_object(_pipe_config, consumer);
    EXPECT_NO_THROW(test_object.init(*raw_header));
    // fill sidechannel items
    for (size_t i = 0; i < _config.nheaps_block; i+=4)
    {
        sidechannel_items[i] = 0uL;
        sidechannel_items[i+1] = 0uL;
        sidechannel_items[i+2] = 0uL;
        sidechannel_items[i+3] = 0uL;
        SET_BIT(sidechannel_items[i], 0);
        SET_BIT(sidechannel_items[i + 1], 0);
    }
    sidechannel_items[0] |= (23UL) << 32;
    sidechannel_items[1] |= (3UL)  << 32;
    sidechannel_items[2] |= (27UL) << 32;
    sidechannel_items[3] |= (7UL)  << 32;

    for(std::size_t i = 0; i < calls; i++)
    {
        ASSERT_FALSE(test_object(*raw_data));
        // New output buffer received
        if(consumer.call_count() == obuffer_idx + 1)
        {
            ASSERT_EQ(consumer.data_size(), (_config.nchannels() + 4) * 2 * 4 * _config.nspectra(2)); // 4 Spectra (IQUV), 2 Gates, 16 bytes SCI/Spectra
            for(std::size_t ii = 0; ii < 4; ii++)
            {
                std::size_t idx = 8 * _config.nchannels() + ii * sizeof(size_t); // Calculated in float not in bytes/char
                uint64_t* sci = reinterpret_cast<uint64_t*>(consumer.data_ptr(idx));
                EXPECT_EQ(sci[0], _config.nheaps_block * spead_size / 2) << "G0, S" << ii; // expect samples from two polarizations
                EXPECT_EQ(sci[2], _config.nheaps_block * spead_size / 2) << "G1, S" << ii;
                // Correct number of overflowed samples
                EXPECT_EQ(sci[1], uint64_t(27 + 7)) << "G0, S" << ii;
                EXPECT_EQ(sci[3], uint64_t(23 + 3)) << "G1, S" << ii;  // First heap has 23+3 and bit set, thus G1
            }
            obuffer_idx = consumer.call_count();
        }
    }
}


void PipelineTester::test_sidechannel_items_dual()
{
    // Initialize test object
    std::size_t calls = 2 + (_config.nblocks() * 2);
    gated_spectrometer::Pipeline<decltype(consumer), DualPolarizationInput, GatedFullStokesOutput>
        test_object(_pipe_config, consumer);
    EXPECT_NO_THROW(test_object.init(*raw_header));
    // fill sidechannel items
    for (size_t i = 0; i < _config.nheaps_block; i+=2)
    {
        sidechannel_items[i] = 0;
        sidechannel_items[i+1] = 0;
        SET_BIT(sidechannel_items[i], 0);
    }
    sidechannel_items[0] |= (23UL) << 32;
    sidechannel_items[1] |= (27UL) << 32;

    for(std::size_t i = 0; i < calls; i++)
    {
        ASSERT_FALSE(test_object(*raw_data));
        // New output buffer received
        if(consumer.call_count() == obuffer_idx + 1)
        {
            ASSERT_EQ(consumer.data_size(), (_config.nchannels() + 4) * 4 * _config.nspectra()); // 4 Spectra (IQUV), 2 Gates, 16 bytes SCI/Spectra
            uint64_t* sci = reinterpret_cast<uint64_t*>(consumer.data_ptr(2 * _config.nchannels()));
            EXPECT_EQ(sci[0], _config.nheaps_block * spead_size / 2) << "G0, S"; // expect samples from two polarizations
            EXPECT_EQ(sci[2], _config.nheaps_block * spead_size / 2) << "G1, S";
            // Correct number of overflowed samples
            EXPECT_EQ(sci[1], 27uL) << "G0, S";
            EXPECT_EQ(sci[3], 23uL) << "G1, S";  // First heap has 23+3 and bit set, thus G1

            obuffer_idx = consumer.call_count();
        }
    }
}

}
}
}