#pragma once
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <memory>

namespace psrdada_cpp
{

template <typename FormatterCallback>
struct GenericHeaderFormatter {
    GenericHeaderFormatter(FormatterCallback&& callback)
        : _callback(std::move(callback))
    {
    }

    template <typename HeaderStream, typename... Args>
    void operator()(HeaderStream& stream, Args&&... data)
    {
        std::vector<char> header = _callback(std::forward<Args>(data)...);
        RawBytes& bytes          = stream.next();
        if(header.size() > bytes.total_bytes()) {
            throw std::runtime_error(
                "Header is to big to write to DADA buffer");
        }
        std::fill(bytes.ptr(), bytes.ptr() + bytes.total_bytes(), 0);
        std::copy(header.begin(), header.end(), bytes.ptr());
        bytes.used_bytes(bytes.total_bytes());
        stream.release();
    }

    FormatterCallback _callback;
};

/** -- Usage example --
GenericHeaderFormatter formatter([](auto const& a, auto const& b) {
    typename DadaHeader::ValueTypes values;
    values["HDR_VERSION"] = std::string{"1.0"};
    values["DATA_TYPE"] = std::string{"INT32"};
    return values;
});
*/

class GenericDataFormatter
{
  public:

    ~GenericDataFormatter(){};

    template <typename DataStream, typename DataType>
        requires Iterable<DataType>
    void operator()(DataStream& stream, DataType const& data)
    {
        using ElementType              = typename DataType::value_type;
        const std::size_t element_size = sizeof(ElementType);

        auto it = data.begin();
        while(it != data.end()) {
            if(!stream.has_block()) {
                stream.next(); // Move to the next block if no current block
            }

            RawBytes& block = stream.current_block();
            std::size_t remaining_bytes =
                block.total_bytes() - block.used_bytes();

            // Ensure block alignment is correct for the element size
            validate_block_alignment(remaining_bytes, element_size);

            // Calculate the number of elements that can fit in the current
            // block
            std::size_t block_elems = remaining_bytes / element_size;
            std::size_t data_elems  = std::distance(it, data.end());

            // Copy the appropriate number of elements into the block
            it = copy_data_to_block<decltype(it), ElementType>(it,
                                                               block,
                                                               block_elems,
                                                               data_elems,
                                                               element_size);

            // If the block is filled, mark it as full and release it
            if(data_elems >= block_elems) {
                block.used_bytes(block.total_bytes());
                stream.release();
            }
        }
    }

  private:
    void validate_block_alignment(std::size_t remaining_bytes,
                                  std::size_t element_size) const
    {
        if(remaining_bytes % element_size != 0) {
            throw std::runtime_error(
                "Buffer has invalid size for remaining elements");
        }
    }

    template <typename Iterator, typename ElementType>
    Iterator copy_data_to_block(Iterator it,
                                RawBytes& block,
                                std::size_t block_elems,
                                std::size_t data_elems,
                                std::size_t element_size)
    {
        ElementType* block_ptr =
            reinterpret_cast<ElementType*>(block.ptr() + block.used_bytes());
        if(data_elems >= block_elems) {
            // Fill the block with the maximum elements that fit
            std::copy(it, it + block_elems, block_ptr);
            it += block_elems;
        } else {
            // Copy the remaining data if it's smaller than the block capacity
            std::copy(it, it + data_elems, block_ptr);
            block.used_bytes(block.used_bytes() + data_elems * element_size);
            it += data_elems; // We are done after copying all data
        }
        return it;
    }
};

template <typename HeaderFormatter, typename DataFormatter>
class AsyncDadaSink
{
  public:
    AsyncDadaSink(key_t key,
                  MultiLog& log,
                  HeaderFormatter&& header_formatter,
                  DataFormatter&& data_formatter)
        : _client(std::make_unique<DadaWriteClient>(key, log)),
          _header_formatter(std::move(header_formatter)),
          _data_formatter(std::move(data_formatter))
    {
    }

    AsyncDadaSink(AsyncDadaSink const&)            = delete;
    AsyncDadaSink& operator=(AsyncDadaSink const&) = delete;
    AsyncDadaSink(AsyncDadaSink&&)                 = default;

    template <typename... DataPtrs>
    void operator()(DataPtrs&&... data_ptrs)
    {
        if(_first_call) {
            _header_formatter(_client->header_stream(),
                              *(std::forward<DataPtrs>(data_ptrs))...);
            _first_call = false;
        }
        _data_formatter(_client->data_stream(),
                        *(std::forward<DataPtrs>(data_ptrs))...);
    }

    DadaWriteClient& client()
    {
        return *_client;
    }

  protected:
    std::unique_ptr<DadaWriteClient> _client;
    HeaderFormatter _header_formatter;
    DataFormatter _data_formatter;
    bool _first_call = true;
};

} // namespace psrdada_cpp
