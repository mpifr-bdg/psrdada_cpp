#pragma once
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/ResourcePool.hpp"
#include "psrdada_cpp/dadaflow/Graph.hpp"
#include "psrdada_cpp/dadaflow/DadaAutoGen.hpp"
#include "psrdada_cpp/dadaflow/DadaHeader.hpp"
#include "psrdada_cpp/dadaflow/processors/Transposer.hpp"
#include "psrdada_cpp/dadaflow/processors/AsyncDadaSink.hpp"
#include "psrdada_cpp/dadaflow/processors/AsyncDadaSource.hpp"
#include "psrdada_cpp/dadaflow/vector_traits.cuh"
#include <chrono>
#include <memory>

namespace psrdada_cpp {

/**
 * @brief The type of the data captured by MKRECV from the RFSoC PFB
 * 
 * @tparam Container The container type that will hold the data.
 * 
 * @details The data is stored in TAPFT order.
 */
template <typename Container>
using RFSoCPFBData = DescribedData<Container,
                                   TimeDimensionN<0>,      // T - N
                                   AntennaDimension,       // A - 1     -  interchangable order
                                   PolarisationDimension,  // P - 2     -  interchangable order
                                   FrequencyDimension,     // F - 2048  -  interchangable order
                                   TimeDimensionN<1>>;     // T - 512 (entire heap)


/**
 * @brief The RFSoC data post transpose
 * 
 * @tparam Container The container type that will hold the data.
 * 
 * @details The data is stored in ATTFP order.
 */
template <typename Container>
using RFSoCPFBDataTranspose = DescribedData<Container,
                                   TimeDimensionN<0>,    
                                   TimeDimensionN<1>,
                                   AntennaDimension,
                                   FrequencyDimension,
                                   PolarisationDimension>; 


std::string format_dspsr_epoch(const Timestamp::EpochType& timestamp) {
    std::time_t time = std::chrono::system_clock::to_time_t(timestamp);
    std::stringstream epoch;
    epoch << std::put_time(std::gmtime(&time), "%Y-%m-%d-%H:%M:%S");
    return epoch.str();
}


class RFSoC2TAFPPipeline
{
public:
    using TAPFTVoltage = RFSoCPFBData<std::span<char2>>;
    using TTAFPVoltage = RFSoCPFBDataTranspose<std::vector<char2>>;
    using AllocatorType = ResourcePool<TTAFPVoltage>;
    using TransposerType = Transposer<TAPFTVoltage, TTAFPVoltage>;

    RFSoC2TAFPPipeline(key_t input_key, key_t output_key, std::size_t nthreads)
    : _reader_log("RFSoC2TAFPReader") 
    , _writer_log("RFSoC2TAFPWriter")
    {
        auto& dada_source = _graph.add_node(
            AsyncDadaSource(input_key, _reader_log,
                DadaAutoGen<TAPFTVoltage>(
                    [](MetadataMap& metadata, DadaHeader const& header)
                    {
                        metadata["TELESCOPE"] = header.get<std::string>("TELESCOPE");
                        metadata["INSTRUMENT"] = header.get<std::string>("INSTRUMENT");
                        metadata["RECEIVER"] = header.get<std::string>("RECEIVER");
                        metadata["SOURCE"] = header.get<std::string>("SOURCE");
                        metadata["RA"] = header.get<std::string>("RA");
                        metadata["DEC"] = header.get<std::string>("DEC");
                        metadata["MODE"] = header.get<std::string>("MODE");
                        metadata["CALFREQ"] = header.get<std::string>("CALFREQ");
                    }
                )), "DADA reader");
        
        auto& transposer = _graph.add_node(TransposerType(SafeResourcePool<TTAFPVoltage>::create(3), nthreads), "Transposer");

        AsyncDadaSink async_sink{
            output_key,
            _writer_log,
            GenericHeaderFormatter([](TTAFPVoltage const& data) -> std::vector<char> {
                auto const& metadata = data.metadata();
                DadaHeader header;
                header.set("HDR_VERSION", "1.0");
                header.set("HDR_SIZE",    "4096");
                header.set("UTC_START",   format_dspsr_epoch(data.timestamp().epoch()));
                header.set("PICOSECONDS", static_cast<std::size_t>(data.timestamp().offset().count()));
                header.set("OBS_OFFSET",  "0");
                header.set("SOURCE",      safe_metadata_get<std::string>(metadata, "SOURCE"));
                header.set("CALFREQ",     safe_metadata_get<std::string>(metadata, "CALFREQ"));
                header.set("MODE",        safe_metadata_get<std::string>(metadata, "MODE"));
                header.set("RA",          safe_metadata_get<std::string>(metadata, "RA"));
                header.set("DEC",         safe_metadata_get<std::string>(metadata, "DEC"));
                header.set("TELESCOPE",   safe_metadata_get<std::string>(metadata, "TELESCOPE"));
                header.set("INSTRUMENT",  safe_metadata_get<std::string>(metadata, "INSTRUMENT"));
                header.set("RECEIVER",    safe_metadata_get<std::string>(metadata, "RECEIVER"));
                header.set("FREQ",        data.centre_frequency().value() / 1e6); // Convert to MHz
                header.set("BW",          data.bandwidth().value() / 1e6); // Convert to MHz
                header.set("TSAMP",       data.TimeDimensionN<1>::timestep().count() * 1e6); // Convert to us
                header.set("NBIT",        sizeof(value_type_t<TTAFPVoltage::value_type>) * 8);
                header.set("NDIM",        vector_size_v<TTAFPVoltage::value_type>);
                header.set("NPOL",        data.npol());
                header.set("NCHAN",       data.nchannels());
                header.set("RESOLUTION",  "1");
                header.set("DSB",         "1");
                return header.to_vector();
            }),
            GenericDataFormatter()};

        auto& dada_sink = _graph.add_node<
            decltype(async_sink),
            invocation_traits<
                decltype(async_sink), std::shared_ptr<TTAFPVoltage> 
            >
        > (std::move(async_sink), "DADA writer");

        _graph.connect<0, 0>(dada_source, transposer, "src->transpose", 3);
        _graph.connect<0, 0>(transposer, dada_sink, "transpose->sink", 3);
        _graph.prepare();

    }

    Graph& graph() { return _graph; }

private:
    MultiLog _reader_log;
    MultiLog _writer_log;
    Graph _graph;

};

}