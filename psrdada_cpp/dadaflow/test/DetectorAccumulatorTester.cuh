#pragma once
#include "psrdada_cpp/dadaflow/processors/DetectorAccumulator.cuh"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/ResourcePool.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/FixedWidth.hpp"
#include "psrdada_cpp/dadaflow/StokesVector.cuh"

#include <thrust/fill.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace psrdada_cpp {
namespace test {

class DetectorAccumulatorTester : public ::testing::Test
{
public:
    using InputType = DescribedData<thrust::device_vector<float2>, PolarisationDimension, TimeDimension, FrequencyDimension>;
    using OutputType = DescribedData<thrust::device_vector<float>, PolarisationDimension, TimeDimension, FrequencyDimension>;
    using AllocatorType = ResourcePool<OutputType>;

    DetectorAccumulatorTester(){}

    ~DetectorAccumulatorTester(){}

    void SetUp() override {};

    void TearDown() override {};

    std::shared_ptr<InputType> make_input(
        std::size_t npol, std::size_t nsamples, 
        std::size_t nchannels) const;

    void correctness_test_stokes(
        std::size_t npol,
        std::size_t nsamples,
        std::size_t nchannels,
        std::size_t naccumulate,
        std::size_t ncalls
    ) const;

    void correctness_test_dualpoln(
        std::size_t npol,
        std::size_t nsamples,
        std::size_t nchannels,
        std::size_t naccumulate,
        std::size_t ncalls
    ) const;

    std::vector<double> cpu_stokes_sum(InputType const& input, std::size_t input_offset, std::size_t naccumulate) const;
    std::vector<double> cpu_dualpoln_sum(InputType const& input, std::size_t input_offset, std::size_t naccumulate) const;
};

} // namespace test
} // namespace psrdada_cpp