#pragma once
#include "psrdada_cpp/dadaflow/units.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <chrono>
#include <type_traits>

using namespace boost::units;
using namespace boost::units::si;
using namespace boost::units::astronomical;
using namespace std::chrono;

/**
 *   When specifiying dimensions, both a Base and its Derived cannot be used
 *   together as template arguments, e.g. the following two classes are
 *   incompatible as template arguments to described vector.
 *
 *   class A {};
 *   class B: public A {};
 *
 *   To resolve this another class must be derived:
 *
 *   class C: public A{};
 *
 *   Now C and B can be used together.
 *
 *   Alternatively for any Dimension that is repeated we can generate a template
 *   class to help disambiguate, e.g.:
 *
 *   template <int ordinal>
 *   class MultiA: public A {};
 *
 *   Now we may use MultiA<0>, MultiA<1>, ... etc. as template arguments.
 *
 *   The CREATE_REPEATABLE_DIMENSION macro provides a helper to do this for
 *   abitrary dimensions.
 */

/**
 * @brief Create templated subclasses of given dimension type.
 *
 * @note Used to make dimensions repeatable.
 */
#define CREATE_REPEATABLE_DIMENSION(ClassName)  \
    template <int N>                            \
    class ClassName##N: public ClassName        \
    {                                           \
      public:                                   \
        using BaseType = ClassName;             \
        using ClassName::ClassName;             \
        using ClassName::describe;              \
        void from(ClassName##N<N> const& other) \
        {                                       \
            ClassName::from(other);             \
        }                                       \
    }

namespace psrdada_cpp
{

/**
 * @brief A safe wrapper for validity checked metadata
 *
 * @tparam MetadataType The type of the stored metadata
 */
template <typename MetadataType>
struct MetaDataWrapper {
    /**
     * @brief Set the metadata.
     *
     * @param metadata The metadata value.
     *
     * @details Implicitly marks the metadata as valid.
     */
    void set(MetadataType const& metadata)
    {
        _metadata = metadata;
        _stale    = false;
    }

    /**
     * @brief Set the metadata for a vector type.
     *
     * @tparam T The value type of the metadata vector.
     * @param metadata The metadata vector to set.
     * @param expected_size The expected size of the metadata.
     *
     * @details The expected size is passed to allow for a check
     *          before setting. Implicitly marks the metadata as valid.
     */
    template <typename T = MetadataType>
    typename std::enable_if<
        std::is_same<T, std::vector<typename T::value_type>>::value>::type
    set(MetadataType const& metadata, std::size_t expected_size)
    {
        if(metadata.size() != expected_size) {
            throw std::runtime_error(
                "Attempted to set metadata with invalid size");
        }
        _metadata = metadata;
        _stale    = false;
    }

    /**
     * @brief Get a const reference to the metadata
     *
     * @return MetadataType const&
     */
    MetadataType const& get() const
    {
        if(_stale) {
            throw std::runtime_error("Stale metadata was requested");
        }
        return _metadata;
    }

    /**
     * @brief Mark the metadata as stale.
     *
     * @note Stale metadata will raise an exception on a get call.
     */
    void mark_stale() { _stale = true; }

    /**
     * @brief Check if the metadata is stale.
     *
     * @return true   Metadata is stale.
     * @return false  Metadata is valid.
     */
    bool is_stale() const { return _stale; }

    MetadataType _metadata;
    bool _stale = true;
};

template <typename MetadataType>
std::ostream& operator<<(std::ostream& os,
                         MetaDataWrapper<MetadataType> const& obj)
{
    if(obj.is_stale()) {
        os << "(unset)";
    } else {
        os << obj.get();
    }
    return os;
}

/**
 * @brief A class for wrapping timestamps
 */
class Timestamp
{
  public:
    using EpochType  = time_point<system_clock, seconds>;
    using OffsetType = duration<long double, pico>; // picoseconds

    /**
     * @brief Construct a new Timestamp object
     *
     */
    Timestamp() {}

    /**
     * @brief Constructs a new Timestamp object with a specified epoch time and
     * offset.
     *
     * @tparam EpochRep     The representation type for the epoch duration
     * (e.g., int64_t, double).
     * @tparam EpochPeriod  The period (or ratio) for the epoch duration (e.g.,
     * seconds, milliseconds).
     * @tparam OffsetRep    The representation type for the offset duration.
     * @tparam OffsetPeriod The period (or ratio) for the offset duration.
     *
     * @param epoch   A time point representing the epoch, which is a duration
     * since the system clock's epoch. It defines the base point in time for
     * this Timestamp.
     * @param offset  A duration that specifies an offset from the epoch.
     *
     * @details The constructor initializes the Timestamp object by converting
     * the provided epoch to a time point with a precision of seconds and offset
     * in picoseconds.
     */
    template <typename EpochRep,
              typename EpochPeriod,
              typename OffsetRep,
              typename OffsetPeriod>
    Timestamp(time_point<system_clock, duration<EpochRep, EpochPeriod>> epoch,
              duration<OffsetRep, OffsetPeriod> offset)
        : _epoch(time_point<system_clock, seconds>(epoch.time_since_epoch())),
          _offset(duration_cast<OffsetType>(offset))
    {
    }

    /**
     * @brief Construct a new Timestamp object from a UNIX epoch
     *
     * @param unix_epoch A unix epoch in long double precision
     *
     * @note Long double usually offers 18 decimal digits of precision. This is
     *       best case 10 nanoseconds precision.
     */
    Timestamp(long double unix_epoch)
    {
        auto whole_seconds =
            seconds(static_cast<long long>(std::floor(unix_epoch)));
        auto fractional_seconds =
            duration<long double>(unix_epoch - std::floor(unix_epoch));
        _offset =
            duration_cast<duration<long double, pico>>(fractional_seconds);
        _epoch = time_point<system_clock, seconds>(whole_seconds);
    }

    /**
     * @brief Test timestamp equality.
     *
     * @param other The timestamp to compare against.
     * @return true
     * @return false
     *
     * @details timestamps are evaluated with a picosecond tolerance.
     */
    bool operator==(Timestamp const& other) const
    {
        OffsetType tolerance =
            std::chrono::duration<long double, std::pico>(1.0);
        return (_epoch == other._epoch) &&
               (std::abs((_offset - other._offset).count()) <
                tolerance.count());
    }

    /**
     * @brief Get the epoch.
     *
     * @return EpochType
     */
    EpochType epoch() const { return _epoch; }

    /**
     * @brief Get the offset.
     *
     * @return OffsetType
     */
    OffsetType offset() const { return _offset; }

    /**
     * @brief Add a duration to the timestamp.
     *
     * @tparam Rep The representation type for the duration.
     * @tparam Period The period (or ratio) for the duration.
     * @param duration_ The duration to add to the timestamp
     * @return Timestamp
     *
     * @details This overload will not change the epoch, only the offset.
     */
    template <typename Rep, typename Period>
    Timestamp operator+(duration<Rep, Period> duration_) const
    {
        auto duration_in_picoseconds =
            duration_cast<duration<long double, pico>>(duration_);
        OffsetType new_offset = _offset + duration_in_picoseconds;
        return Timestamp(_epoch, new_offset);
    }

    /**
     * @brief Increment the timestamp by a duration.
     *
     * @tparam Rep The representation type for the duration.
     * @tparam Period The period (or ratio) for the duration.
     * @param duration_ The duration to add to the timestamp
     * @return Timestamp
     *
     * @details This overload will not change the epoch, only the offset.
     */
    template <typename Rep, typename Period>
    Timestamp& operator+=(duration<Rep, Period> duration_)
    {
        auto duration_in_picoseconds =
            duration_cast<duration<long double, pico>>(duration_);
        _offset += duration_in_picoseconds;
        return *this;
    }

    /**
     * @brief Convert the timestamp to a Modified Julian Date (MJD).
     *
     * @return long double The MJD.
     */
    long double mjd() const
    {
        // MJD epoch is Unix time at MJD 0 (Nov 17, 1858): 2400000.5
        // Julian Days corresponds to 2440587.5 Unix time days
        constexpr long double unix_to_mjd_offset = 2400000.5l;

        // Extract seconds since Unix epoch from _epoch
        auto unix_time_seconds = _epoch.time_since_epoch().count();

        // Convert picoseconds offset to seconds
        long double offset_seconds = _offset.count() * 1e-12l;

        // Calculate Julian Date from Unix time and offset
        long double jd =
            (unix_time_seconds + offset_seconds) / 86400.0l + 2440587.5l;

        // Convert Julian Date to MJD
        long double mjd = jd - unix_to_mjd_offset;

        return mjd;
    }

    static Timestamp from_mjd(long double mjd) {
        const long double mjd_epoch_offset = 40587.0;  // Days from MJD to Unix epoch
        const long double seconds_per_day = 86400.0;   // Seconds in a day
        if (mjd < 0) {
            throw std::invalid_argument("MJD cannot be negative.");
        }
        long double unix_time = (mjd - mjd_epoch_offset) * seconds_per_day;
        return Timestamp(unix_time);
    }

    long double unix_epoch() const {
        // Extract the whole seconds since Unix epoch from _epoch
        long double unix_time_seconds = static_cast<long double>(_epoch.time_since_epoch().count());

        // Convert the offset from picoseconds to seconds
        long double offset_seconds = _offset.count() * 1e-12L;

        // Combine epoch seconds with fractional offset
        return unix_time_seconds + offset_seconds;
    }

  private:
    EpochType _epoch;
    OffsetType _offset;
};

// * This is a quick hack as no template is actually needed. Implementation
// * should be compiled in a separate file to avoid multiple definitions.
template <typename N = bool>
std::ostream& operator<<(std::ostream& stream, Timestamp const& timestamp)
{
    stream << timestamp.epoch() << " + " << timestamp.offset().count()
           << " picoseconds";
    return stream;
}

/**
 * @brief A common base class for dimensions
 *
 * @note This class cannot be explicitly constructed from
 *       application code
 */
class BaseDimension
{
  public:
    /**
     * @brief Construct a new BaseDimension object and set
     *        the extent of the dimension to zero.
     *
     * @note Overrides of the methods of this class should only
     *       be documented if they have unexpected behaviour.
     */
    BaseDimension(): _extent(0) {};
    virtual ~BaseDimension(){};
    
    /**
     * @brief Get a short name to describe the dimension
     *
     * @return const char*
     */
    virtual const char* short_name() const = 0;

    /**
     * @brief Get a long name to describe the dimension
     *
     * @return const char*
     */
    virtual const char* long_name() const = 0;

    /**
     * @brief Return a description string with the parameters of the dimension
     *
     * @return std::string
     */
    virtual std::string describe() const = 0;

    /**
     * @brief Copy description information from another instance of this
     * dimension
     */
    void from(BaseDimension const&) {}

    /**
     * @brief A callback that is invoked when the dimension is resized
     *
     * @param size
     */
    virtual void resize_dimension(std::size_t size) { _extent = size; };

    /**
     * @brief Return the extent of the dimension
     *
     * @return std::size_t
     */
    std::size_t extent() const { return _extent; }

  protected:
    std::size_t _extent;
};

/**
 * @brief A common base for time-like dimensions.
 */
class BaseTimeDimension: public BaseDimension
{
  public:
    using TimestepType = duration<long double>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "T"; }
    const char* long_name() const override { return "Time"; }

    /**
     * @brief Return the number of time samples
     *
     * @return std::size_t
     *
     * @details Repeated dimensions of the same type will be aggregated
     */
    std::size_t nsamples() const { return extent(); }

    /**
     * @brief Set the time resolution of the data
     *
     * @param dt The time resolution
     */
    void timestep(TimestepType const& dt) { _timestep.set(dt); }

    /**
     * @brief Get the time resolution of the data
     *
     * @return TimestepType
     */
    TimestepType const& timestep() const { return _timestep.get(); }

    TimestepType timeduration() const { return _timestep.get() * nsamples(); }

    virtual std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Nsamples: " << nsamples() << "\n";
        stream << std::setprecision(15);
        stream << "  Time resolution: " << _timestep << "\n";
        return stream.str();
    }

    void from(BaseTimeDimension const& other) { _timestep = other._timestep; }

  protected:
    MetaDataWrapper<TimestepType> _timestep;
};

using TimeDimension = BaseTimeDimension;
CREATE_REPEATABLE_DIMENSION(TimeDimension);

/**
 * @brief A common base for frequency-like dimensions.
 */
class BaseFrequencyDimension: public BaseDimension
{
  public:
    using FrequencyType = quantity<frequency>;

    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "F"; }
    const char* long_name() const override { return "Frequency"; }

    /**
     * @brief Return the number of frequency channels.
     *
     * @return std::size_t
     *
     * @details Repeated dimensions of the same type will be aggregated.
     */
    std::size_t nchannels() const { return extent(); }

    /**
     * @brief Return the frequency of a given channel.
     *
     * @param idx The index of the channel.
     * @return FrequencyType
     */
    virtual FrequencyType channel_frequency(std::size_t idx) const = 0;

    virtual std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Nchannels: " << nchannels() << "\n";
        return stream.str();
    }
};

/**
 * @brief A class for representing contiguous frequency channels
 *
 * @note This currently assumes that the band is critically sampled
 *       such that frequency channels do not overlap.
 */
class ContiguousFrequencyDimension: public BaseFrequencyDimension
{
  public:
    using BaseFrequencyDimension::BaseFrequencyDimension;

    const char* long_name() const override { return "ContiguousFrequency"; }

    /**
     * @brief Get the frequency of the first channel.
     *
     * @return FrequencyType const&
     */
    FrequencyType const& channel_zero_freq() const { return _fch0.get(); }

    /**
     * @brief Set the frequency of the first channel.
     *
     * @param fch0 The frequency of the first channel.
     */
    void channel_zero_freq(FrequencyType const& fch0) { _fch0.set(fch0); }

    /**
     * @brief Get the bandwidth of the frequency channels.
     *
     * @return FrequencyType const&
     */
    FrequencyType const& channel_bandwidth() const { return _chbw.get(); }

    /**
     * @brief Set the bandwidth of the frequency channels.
     *
     * @param chbw The channel bandwidth.
     */
    void channel_bandwidth(FrequencyType const& chbw) { _chbw.set(chbw); }

    /**
     * @brief Compute the full bandwidth of the dimension.
     *
     * @return FrequencyType
     *
     * @note Assumes critical sampling.
     */
    FrequencyType bandwidth() const
    {
        return channel_bandwidth() * static_cast<double>(nchannels());
    }

    /**
     * @brief Compute the nominal centre frequency of the dimension.
     *
     * @return FrequencyType
     *
     * @note Assumes critical sampling and calculates the exact centre half 
     *       way between the middle of the top channel and the middle of the 
     *       bottom channel.
     */
    FrequencyType centre_frequency() const
    {
        // Go half a channel down to the bottom of the bottom channel 
        // then add half the complete bandwidth
        return (channel_zero_freq() - channel_bandwidth() / 2.0) + (bandwidth() / 2.0);
    }

    /**
     * @brief Get the frequency of the given channel.
     *
     * @param idx The frequency channel index.
     * @return FrequencyType
     *
     * @note Assumes critical sampling.
     */
    FrequencyType channel_frequency(std::size_t idx) const override
    {
        return channel_zero_freq() +
               static_cast<double>(idx) * channel_bandwidth();
    }

    virtual std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Nchannels: " << nchannels() << "\n";
        stream << std::setprecision(15);
        stream << "  Channel 0 frequency: " << _fch0 << "\n";
        stream << "  Channel bandwidth: " << _chbw << "\n";
        return stream.str();
    }

    void from(ContiguousFrequencyDimension const& other)
    {
        _fch0 = other._fch0;
        _chbw = other._chbw;
    }

  private:
    MetaDataWrapper<FrequencyType> _fch0;
    MetaDataWrapper<FrequencyType> _chbw;
};

using FrequencyDimension = ContiguousFrequencyDimension;

CREATE_REPEATABLE_DIMENSION(ContiguousFrequencyDimension);

template <int N>
using FrequencyDimensionN = ContiguousFrequencyDimensionN<N>;

/**
 * @brief A class for representing a non-contiguous frequency dimension.
 */
class NonContiguousFrequencyDimension: public BaseFrequencyDimension
{
  public:
    using FrequenciesType = std::vector<FrequencyType>;
    using BaseFrequencyDimension::BaseFrequencyDimension;

    const char* long_name() const override { return "NonContiguousFrequency"; }

    /**
     * @brief Get the frequencies of all channels.
     *
     * @return FrequenciesType const&
     */
    FrequenciesType const& frequencies() const { return _frequencies.get(); }

    /**
     * @brief Set the frequencies of all channels.
     *
     * @param freqs
     */
    void frequencies(FrequenciesType const& freqs)
    {
        _frequencies.set(freqs, nchannels());
    }

    /**
     * @brief Get the frequency of the given channel.
     *
     * @param idx The frequency channel index.
     * @return FrequencyType
     */
    FrequencyType channel_frequency(std::size_t idx) const override
    {
        if(idx >= frequencies().size()) {
            throw std::runtime_error(
                "Index is out-of-bounds for frequency array");
        }
        return frequencies()[idx];
    }

    virtual std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Nchannels: " << nchannels() << "\n";
        stream << std::setprecision(15);
        stream << "  Channel frequencies: " << _frequencies << "\n";
        return stream.str();
    }

    void resize_dimension(std::size_t size) override
    {
        // Resising this type will invalidate the frequencies
        // metadata.
        BaseFrequencyDimension::resize_dimension(size);
        if(!_frequencies.is_stale()) {
            if(_frequencies.get().size() != nchannels()) {
                _frequencies.mark_stale();
            }
        }
    };

    void from(NonContiguousFrequencyDimension const& other)
    {
        if(!other._frequencies.is_stale()) {
            _frequencies.set(other._frequencies.get(), nchannels());
        }
    }

  private:
    MetaDataWrapper<FrequenciesType> _frequencies;
};

CREATE_REPEATABLE_DIMENSION(NonContiguousFrequencyDimension);

/**
 * @brief A base class for representing multibeam data.
 *
 */
class BaseBeamDimension: public BaseDimension
{
  public:
    using BeamsType = std::vector<std::string>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "B"; }
    const char* long_name() const override { return "Beam"; }

    /**
     * @brief Return the number of beams
     *
     * @return std::size_t
     *
     * @details Repeated dimensions of the same type will be aggregated
     */
    std::size_t nbeams() const { return extent(); }

    /**
     * @brief Get the list of beam descriptions
     *
     * @return BeamsType const&
     */
    BeamsType const& beams() const { return _beams.get(); }

    /**
     * @brief Set the list of beam descriptions
     *
     * @param beams
     */
    void beams(BeamsType const& descs) { _beams.set(descs, nbeams()); }

    void resize_dimension(std::size_t size) override
    {
        BaseDimension::resize_dimension(size);
        if(!_beams.is_stale()) {
            if(_beams.get().size() != nbeams()) {
                _beams.mark_stale();
            }
        }
    };

    std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Nbeams: " << nbeams() << "\n";
        stream << "  Beams: " << _beams << "\n";
        return stream.str();
    }

    void from(BaseBeamDimension const& other)
    {
        if(!other._beams.is_stale()) {
            _beams.set(other._beams.get(), nbeams());
        }
    }

  private:
    MetaDataWrapper<BeamsType> _beams;
};

/**
 * @brief A class for wrapping multi-beam data.
 *
 */
using BeamDimension = BaseBeamDimension;

/**
 * @brief A base class for representing multi-antenna data.
 *
 */
class BaseAntennaDimension: public BaseDimension
{
  public:
    using AntennasType = std::vector<std::string>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "A"; }
    const char* long_name() const override { return "Antenna"; }

    /**
     * @brief Return the number of antennas
     *
     * @return std::size_t
     *
     * @details Repeated dimensions of the same type will be aggregated
     */
    std::size_t nantennas() const { return extent(); }

    /**
     * @brief Get the list of antenna descriptions
     *
     * @return AntennasType const&
     */
    AntennasType const& antennas() const { return _antennas.get(); }

    /**
     * @brief Set the list of antenna descriptions
     *
     * @param antennas
     */
    void antennas(AntennasType const& descs)
    {
        _antennas.set(descs, nantennas());
    }

    void resize_dimension(std::size_t size) override
    {
        BaseDimension::resize_dimension(size);
        if(!_antennas.is_stale()) {
            if(_antennas.get().size() != nantennas()) {
                _antennas.mark_stale();
            }
        }
    };

    std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Nantennas: " << nantennas() << "\n";
        stream << "  Antennas: " << _antennas << "\n";
        return stream.str();
    }

    void from(BaseAntennaDimension const& other)
    {
        if(!other._antennas.is_stale()) {
            _antennas.set(other._antennas.get(), nantennas());
        }
    }

  private:
    MetaDataWrapper<AntennasType> _antennas;
};

/**
 * @brief A class for wrapping antenna data.
 *
 */
using AntennaDimension = BaseAntennaDimension;

/**
 * @brief A base class for representing polarisation-like data.
 *
 * @note polarisation information may be embedded (like complex valued data)
 *       on the actual data type stored in a DescribedData container. This
 *       class is intended for times when that is not the case.
 */
class BasePolarisationDimension: public BaseDimension
{
  public:
    using PolarisationsType = std::vector<std::string>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "P"; }
    const char* long_name() const override { return "Polarisation"; }

    /**
     * @brief Get the number of polarisations.
     *
     * @return std::size_t
     */
    virtual std::size_t npol() const { return extent(); }

    /**
     * @brief Set the polarisations.
     *
     * @param pols A vector of polarisation names
     */
    void polarisations(PolarisationsType const& pols)
    {
        _pols.set(pols, npol());
    }

    /**
     * @brief Get the polarisations.
     *
     * @return PolarisationsType const&
     */
    PolarisationsType const& polarisations() const { return _pols.get(); }

    std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Npolarisations: " << npol() << "\n";
        stream << "  Polarisations: " << _pols << "\n";
        return stream.str();
    }

    void resize_dimension(std::size_t size) override
    {
        BaseDimension::resize_dimension(size);
        if(!_pols.is_stale()) {
            if(_pols.get().size() != npol()) {
                _pols.mark_stale();
            }
        }
    };

    void from(BasePolarisationDimension const& other)
    {
        if(!other._pols.is_stale()) {
            _pols.set(other._pols.get(), npol());
        }
    }

  private:
    MetaDataWrapper<PolarisationsType> _pols;
};

/**
 * @brief A class for wrapping polarisation data.
 *
 */
using PolarisationDimension = BasePolarisationDimension;
CREATE_REPEATABLE_DIMENSION(BasePolarisationDimension);

template <int N>
using PolarisationDimensionN = BasePolarisationDimensionN<N>;
/**
 * @brief A base class for wrapping dedispersered data.
 *
 */
class BaseDispersionDimension: public BaseDimension
{
  public:
    using DispersionMeasuresType = std::vector<dispersion_measure>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "D"; }
    const char* long_name() const override { return "Dispersion"; }

    /**
     * @brief Return the number of DMs
     *
     * @return std::size_t
     */
    std::size_t ndms() const { return extent(); }

    /**
     * @brief Get the list of DMs
     *
     * @return DispersionMeasuresType const&
     */
    DispersionMeasuresType const& dms() const { return _dms.get(); }

    /**
     * @brief Set the list of DMs
     *
     * @param dms
     */
    void dms(DispersionMeasuresType const& dms) { _dms.set(dms, ndms()); }

    void resize_dimension(std::size_t size) override
    {
        BaseDimension::resize_dimension(size);
        if(!_dms.is_stale()) {
            if(_dms.get().size() != ndms()) {
                _dms.mark_stale();
            }
        }
    };

    std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  NDMs: " << ndms() << "\n";
        stream << std::setprecision(15);
        stream << "  DMs: " << _dms << "\n";
        return stream.str();
    }

    void from(BaseDispersionDimension const& other)
    {
        if(!other._dms.is_stale()) {
            _dms.set(other._dms.get(), ndms());
        }
    }

  private:
    MetaDataWrapper<DispersionMeasuresType> _dms;
};

/**
 * @brief A class for wrapping dedispersed data.
 *
 */
using DispersionDimension = BaseDispersionDimension;

/**
 * @brief A base class for wrapping gated data.
 *
 */
class BaseGateDimension: public BaseDimension
{
  public:
    using GatesType = std::vector<std::string>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "G"; }
    const char* long_name() const override { return "Gate"; }

    /**
     * @brief Return the number of gates
     *
     * @return std::size_t
     */
    virtual std::size_t ngates() const { return extent(); }

    void gates(GatesType const& gates) { _gates.set(gates, ngates()); }

    GatesType const& gates() const { return _gates.get(); }

    std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Ngates: " << ngates() << "\n";
        stream << "  Gates: " << _gates << "\n";
        return stream.str();
    }

    void resize_dimension(std::size_t size) override
    {
        BaseDimension::resize_dimension(size);
        if(!_gates.is_stale()) {
            if(_gates.get().size() != ngates()) {
                _gates.mark_stale();
            }
        }
    };

    void from(BaseGateDimension const& other)
    {
        if(!other._gates.is_stale()) {
            _gates.set(other._gates.get(), ngates());
        }
    }

  private:
    MetaDataWrapper<GatesType> _gates;
};

/**
 * @brief A class for wrapping gated data.
 *
 */
using GateDimension = BaseGateDimension;


class BaselineDimension : protected BaseDimension
{
public:
    using BaselinesType = std::vector<std::string>;
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "B";}
    const char* long_name() const override { return "Baseline";}
    /**
     * @brief Return the number of antennas
     *
     * @return std::size_t
     *
     * @details Repeated dimensions of the same type will be aggregated
     */
    std::size_t nbaselines() const { return extent(); }

    /**
     * @brief Get the list of antenna descriptions
     *
     * @return BaselinesType const&
     */
    BaselinesType const& baselines() const { return _baselines.get(); }

    /**
     * @brief Set the list of antenna descriptions
     *
     * @param antennas
     */
    void baselines(BaselinesType const& descs)
    {
        _baselines.set(descs, nbaselines());
    }

    void resize_dimension(std::size_t size) override
    {
        BaseDimension::resize_dimension(size);
        if(!_baselines.is_stale()) {
            if(_baselines.get().size() != nbaselines()) {
                _baselines.mark_stale();
            }
        }
    };

    std::string describe() const override
    {
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  NBaselines: " << nbaselines() << "\n";
        // stream << "  Baseline: " << _baselines << "\n";
        return stream.str();
    }

    void from(BaselineDimension const& other)
    {
        if(!other._baselines.is_stale()) {
            _baselines.set(other._baselines.get(), nbaselines());
        }
    }

  private:
    MetaDataWrapper<BaselinesType> _baselines;
};

CREATE_REPEATABLE_DIMENSION(BaselineDimension);


} // namespace psrdada_cpp