#include <gtest/gtest.h>
#include <boost/shared_ptr.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/utility/setup/console.hpp>

#include "psrdada_cpp/cli_utils.hpp"

typedef boost::log::sinks::synchronous_sink<boost::log::sinks::text_ostream_backend> sink_t;

namespace psrdada_cpp {
namespace test {

TEST(SetLogLevel, UPPER)
{
    std::stringstream ss;

    // Set up a logging sink with the stringstream as the target
    auto backend = boost::make_shared<boost::log::sinks::text_ostream_backend>();
    backend->add_stream(boost::shared_ptr<std::ostream>(&ss, boost::null_deleter()));
    backend->auto_flush(true);

    auto sink = boost::make_shared<sink_t>(backend);
    boost::log::core::get()->add_sink(sink);

    // Set log level
    set_log_level("INfO");

    BOOST_LOG_TRIVIAL(debug) << "Should not be in log";
    EXPECT_EQ(ss.str().size(), 0);  // No debug messages

    BOOST_LOG_TRIVIAL(info) << "Should be in log";
    EXPECT_GT(ss.str().size(), 0); // Info messages appear in the log

    // Remove the sink to clean up
    boost::log::core::get()->remove_sink(sink);
    init_log_level();
}

TEST(SetLogLevel, ThrowOnUnknownLevel)
{
    EXPECT_THROW(set_log_level("unknown_log_level"), std::runtime_error);
}


}
}
