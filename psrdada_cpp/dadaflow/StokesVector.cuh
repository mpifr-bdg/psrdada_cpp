#pragma once

#include "cuComplex.h"
#include "psrdada_cpp/dadaflow/vector_traits.cuh"

namespace psrdada_cpp
{

__host__ __device__ static __inline__ float cuCmagf(float2 x)
{
    return x.x * x.x + x.y * x.y;
}

struct StokesI {
    static constexpr const char* name() { return "I"; };
    __host__ __device__ static inline float calculate(cuFloatComplex const& p0,
                                                      cuFloatComplex const& p1)
    {
        return cuCmagf(p0) + cuCmagf(p1);
    }
};

struct StokesQ {
    static constexpr const char* name() { return "Q"; };
    __host__ __device__ static inline float calculate(cuFloatComplex const& p0,
                                                      cuFloatComplex const& p1)
    {
        return cuCmagf(p0) - cuCmagf(p1);
    }
};
struct StokesU {
    static constexpr const char* name() { return "U"; };
    __host__ __device__ static inline float calculate(cuFloatComplex const& p0,
                                                      cuFloatComplex const& p1)
    {
        return 2 * cuCrealf(cuCmulf(p0, cuConjf(p1)));
    }
};
struct StokesV {
    static constexpr const char* name() { return "V"; };
    __host__ __device__ static inline float calculate(cuFloatComplex const& p0,
                                                      cuFloatComplex const& p1)
    {
        return 2 * cuCimagf(cuCmulf(p0, cuConjf(p1)));
    }
};

using I = StokesI;
using Q = StokesQ;
using U = StokesU;
using V = StokesV;

/**
 * @brief A type wrapper for stokes-vector-like data
 *
 * @tparam StorageType        The storage type for each stokes parameter, e.g.
 * float, int8_t, short etc.
 * @tparam StokesParameters   The stokes parameter types that are being
 * represented, must be StokesI, StokesQ, StokesU or StokesV (or their aliases
 * I, Q, U and V)
 *
 * @details Can store the stokes parameters in any order for between 1 and 4
 * stokes parameters. The StorageType and the number of stokes parameters will
 * determine the base class. For example StokesVector<float, I, Q> will inherit
 * from float2 (and hence behave like a float2) and StokesVector<signed char, Q,
 * V, I> will inherit from char2. Functions that take a the type of the
 * baseclass
 * as an argument will also be able to take instances of the corresponding
 * stokes type. As the stokes parameter types are only stored as part of the
 * type and not as a member, extracting them requires template metaprogramming.
 */
 template <typename StorageType, typename... StokesParameters>
 struct StokesVector : vector_type_t<StorageType, sizeof...(StokesParameters)> {

     template <typename StokesParameterT>
     __host__ __device__ constexpr StorageType& get() {
         constexpr std::size_t index = index_of_v<StokesParameterT, StokesParameters...>;
         if constexpr (index == 0) {
             return this->x;
         } else if constexpr (index == 1) {
             return this->y;
         } else if constexpr (index == 2) {
             return this->z;
         } else if constexpr (index == 3) {
             return this->w;
         } else {
            []<bool flag = false>()
                {static_assert(flag, "StokesParameterT is not part of this StokesVector");}();
         }
     }

     template <typename StokesParameterT>
     __host__ __device__ constexpr StorageType const & get() const {
         constexpr std::size_t index = index_of_v<StokesParameterT, StokesParameters...>;
         if constexpr (index == 0) {
             return this->x;
         } else if constexpr (index == 1) {
             return this->y;
         } else if constexpr (index == 2) {
             return this->z;
         } else if constexpr (index == 3) {
             return this->w;
         } else {
            []<bool flag = false>()
                {static_assert(flag, "StokesParameterT is not part of this StokesVector");}();
         }
     }
 };

/**
 * @brief Calculate the Stokes parameters from a pair of complex polarisations
 *
 * @tparam The storage type for each stokes parameter, e.g.
 * float, int8_t, short etc.
 * @tparam The stokes parameter types that are being
 * represented, must be StokesI, StokesQ, StokesU or StokesV (or their aliases
 * I, Q, U and V)
 * @param p0 data of polarisation channel 0
 * @param p1 data of polarisation channel 1
 *
 * @details Currently it is assumed that the stokes parameters are in a
 * Cartesian basis. An increasing phase convention is used. The input
 * polarisations must be complex valued. Real data must first be Hilbert
 * transformed or Fourier transformed before this function can be applied.
 */
template <typename StorageType, typename... StokesParameters>
__host__ __device__ auto calculate_stokes(cuFloatComplex const& p0,
                                          cuFloatComplex const& p1)
{
    return StokesVector<StorageType, StokesParameters...>{
        StokesParameters::calculate(p0, p1)...};
}

/**
 * @brief Create a name string from a Stokes type
 *
 * @tparam StokesParameters The stokes parameters
 * @return std::vector<std::string>
 *
 * @details Returns a string of the form "IQUV", "QU",
 *          "I", etc.
 */
template <typename... StokesParameters>
std::vector<std::string> get_stokes_names()
{
    return {StokesParameters::name()...};
}

template <typename StokesToMask, typename StokesType>
struct stokes_mask;

template <typename StokesToMask, typename T, typename... S>
struct stokes_mask<StokesToMask, StokesVector<T, S...>> {
    using type                  = StokesVector<T, S...>;
    static constexpr type value = {
        static_cast<T>(std::is_same_v<StokesToMask, S>)...};
};

template <typename T>
struct is_stokes: std::false_type {
};

template <typename T, typename... S>
struct is_stokes<StokesVector<T, S...>>: std::true_type {
};

template <typename T>
static constexpr bool is_stokes_v = is_stokes<T>::value;

template <typename T>
concept StokesLike = is_stokes_v<T>;

template <typename StokesVectorT, typename StokesParamT>
struct stokes_contains;

template <typename T, typename... StokesParamsT, typename StokesParamT>
struct stokes_contains<StokesVector<T, StokesParamsT...>, StokesParamT> {
    static constexpr bool value = (std::is_same_v<StokesParamsT, StokesParamT> || ...);
};

// Helper variable template for easier usage
template <typename StokesVectorT, typename StokesParamT>
inline constexpr bool stokes_contains_v = stokes_contains<StokesVectorT, StokesParamT>::value;

} // namespace psrdada_cpp