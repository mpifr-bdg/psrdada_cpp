#pragma once
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/common.hpp"
#include <tuple>
#include <algorithm>

namespace psrdada_cpp {

/**
 * @brief Processor for mutliplexing a data stream
 * 
 * @tparam N The number of multiplexer outputs
 * @tparam T The type of the multiplexer data
 * 
 * @details No allocator is required for the multiplexer as it 
 *          copies the shared_ptr instances, not the underlying
 *          data.
 */
 // ! This should be updated to take any type and not wrap in a shared ptr
template <int N, typename T>
class Multiplexer
{
public:
    using InputPtr = std::shared_ptr<T>;
    using OutputPtr = std::shared_ptr<T>;
    using Output = decltype(replicate_tuple<N>(OutputPtr(nullptr)));

    Output operator()(InputPtr input) {
      return replicate_tuple<N>(input);
    }
};

} // namespace psrdada_cpp