#pragma once
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cuda_utils.hpp"

#include <cuda.h>

namespace psrdada_cpp
{

/**
 * @brief A RAII wrapper for cudaStream_t.
 *
 * @details This is an RAII cudaStream_t wrapper to allow for adding
 *          cuda streams as class members without having to write move
 *          constructors and destructors that explicitly support them.
 *
 * @note Classes that use these streams are not copyable.
 */
class CudaStream
{
  public:
    /**
     * @brief Construct a new CudaStream object.
     *
     */
    CudaStream() { CUDA_ERROR_CHECK(cudaStreamCreate(&_stream)); }

    /**
     * @brief Destroy the CudaStream object.
     *
     */
    ~CudaStream()
    {
        if(_stream) {
            CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
        }
    }

    CudaStream(const CudaStream&)            = delete;
    CudaStream& operator=(const CudaStream&) = delete;

    /**
     * @brief Move construct a new CudaStream object.
     *
     * @param other A CudaStream r-value.
     */
    CudaStream(CudaStream&& other) noexcept: _stream(other._stream)
    {
        other._stream = nullptr; // Nullify the moved-from object's stream
    }

    /**
     * @brief Move assign this CudaStream object.
     *
     * @param other The moved-from object.
     * @return CudaStream&
     */
    CudaStream& operator=(CudaStream&& other) noexcept
    {
        if(this != &other) {
            // Destroy the current stream if it exists
            if(_stream) {
                CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
            }
            // Transfer ownership of the stream
            _stream       = other._stream;
            other._stream = nullptr;
        }
        return *this;
    }

    /**
     * @brief Get the managed cudaStream_t instance.
     *
     * @return const cudaStream_t&
     */
    const cudaStream_t& stream() const { return _stream; }

    /**
     * @brief Synchronizes the cudaStream_t with the host
     * 
     */
    void synchronize()
    {
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
    }

  private:
    cudaStream_t _stream = nullptr;
};

} // namespace psrdada_cpp