#pragma once
#include <atomic>
#include <chrono>
#include <ctime>
#include <cuda/std/span>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <optional>
#include <span>
#include <string>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <tuple>
#include <type_traits>
#include <utility>
#include <any>
#include <unordered_map>
#include <string_view>

#if defined(__GNUC__) || defined(__clang__)
    #include <cstdlib>
    #include <cxxabi.h>

template <typename T>
std::string getTypeName()
{
    int status = 0;
    std::unique_ptr<char, void (*)(void*)> res{
        abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status),
        std::free};
    return (status == 0) ? res.get() : typeid(T).name();
}

#else // defined(__GNUC__) || defined(__clang__)

template <typename T>
std::string getTypeName()
{
    return typeid(T).name();
}

#endif // defined(__GNUC__) || defined(__clang__)

namespace psrdada_cpp
{

/**
 * @brief Generic vector ostream formatter
 *
 * @tparam T The value type of the vector
 * @tparam A The allocator type of the vector
 * @param stream The ostream to write to.
 * @param container The vector instance to write
 * @return std::ostream&
 */
template <typename T, typename A>
std::ostream& operator<<(std::ostream& stream,
                         std::vector<T, A> const& container)
{
    const std::size_t n_to_show = 25;
    if(container.size() > 0) {
        auto it = container.begin();
        for(size_t i = 0; i < n_to_show && it != container.end(); ++i, ++it) {
            stream << *it
                   << (i < (n_to_show - 1) && std::next(it) != container.end()
                           ? ", "
                           : "");
        }
        if(container.size() > n_to_show) {
            stream << "...";
        }
    }
    return stream;
}

template <typename Tuple, std::size_t... Is>
void print_tuple(const Tuple& tuple, std::index_sequence<Is...>)
{
    ((std::cout << (Is == 0 ? "" : ", ") << *std::get<Is>(tuple)), ...);
    std::cout << "\n";
}

template <typename... Args>
void print_tuple(const std::tuple<Args...>& tuple)
{
    print_tuple(tuple, std::make_index_sequence<sizeof...(Args)>());
}

template <typename T, std::size_t... I>
auto ptr_vec_to_ref_tuple_impl(const std::vector<T>& v,
                               std::index_sequence<I...>)
{
    return std::make_tuple(std::ref(*v[I])...);
}

template <std::size_t N, typename T>
auto ptr_vec_to_ref_tuple(const std::vector<T>& v)
{
    if(v.size() < N) {
        throw std::runtime_error(
            "Cannot convert a vector of size < N to a tuple of size N");
    }
    return ptr_vec_to_ref_tuple_impl<T>(v, std::make_index_sequence<N>{});
}

template <typename Tuple, typename Func>
void for_each_in_tuple(Tuple&& t, Func&& f)
{
    std::apply(
        [&f](auto&&... args) {
            (void)std::initializer_list<int>{
                (f(std::forward<decltype(args)>(args)), 0)...};
        },
        std::forward<Tuple>(t));
}

template <typename T>
T safe_metadata_get(auto const& metadata, std::string key)
{
    try {
        return std::any_cast<T>(metadata.at(key));
    } catch(const std::out_of_range&) {
        std::cout << "Key " << key << "not found in the map" << std::endl;
        throw;
    } catch(const std::bad_any_cast&) {
        std::cout << "Bad cast on key" << key << std::endl;
        throw;
    }
}

// Helpers to check if a given type is in a
// variadic list of types
template <typename D, typename... Ds>
struct contains;

template <typename D>
struct contains<D>: std::false_type {
};

template <typename D, typename Head, typename... Tail>
struct contains<D, Head, Tail...>: std::conditional_t<std::is_same_v<D, Head>,
                                                      std::true_type,
                                                      contains<D, Tail...>> {
};

// Trait to determine the index of a type in a variadic template parameter pack
template <typename T, typename... List>
struct index_of;

// Specialization when the first type in the list matches T
template <typename T, typename... Rest>
struct index_of<T, T, Rest...>: std::integral_constant<std::size_t, 0> {
};

// Specialization for when the first type does not match T
template <typename T, typename First, typename... Rest>
struct index_of<T, First, Rest...>
    : std::integral_constant<std::size_t, 1 + index_of<T, Rest...>::value> {
};

// Trait to determine the index of a type in a variadic template parameter pack
template <typename T, typename... List>
static constexpr std::size_t index_of_v = index_of<T, List...>::value;

// Primary template, defaults to false
template <typename T>
struct is_optional: std::false_type {
};

// Specialization for std::optional
template <typename T>
struct is_optional<std::optional<T>>: std::true_type {
};

// Helper variable template
template <typename T>
inline constexpr bool is_optional_v = is_optional<T>::value;

template <typename T>
struct unwrap_optional {
    using type = T;
};

template <typename T>
struct unwrap_optional<std::optional<T>> {
    using type = T;
};

template <typename T>
using unwrap_optional_t = typename unwrap_optional<T>::type;

template <typename>
struct is_tuple: std::false_type {
};

template <typename... T>
struct is_tuple<std::tuple<T...>>: std::true_type {
};

// Helper to check if the decayed type is a tuple
template <typename T>
inline constexpr bool is_tuple_v = is_tuple<std::decay_t<T>>::value;

template <typename T, std::size_t... Is>
auto replicate_tuple_helper(const T& value, std::index_sequence<Is...>) {
    return std::make_tuple((static_cast<void>(Is), value)...);
}

template <std::size_t N, typename T>
auto replicate_tuple(const T& value) {
    return replicate_tuple_helper(value, std::make_index_sequence<N>{});
}

// Helper type trait to check if a type is derived from the same template
template <template <typename...> class Template, typename... T>
struct is_instantiation_of : std::false_type {};

template <template <typename...> class Template, typename... T>
struct is_instantiation_of<Template, Template<T...>> : std::true_type {};

template <typename T>
concept Resizeable = requires(T t) {
    { t.resize(std::declval<std::size_t>()) } -> std::same_as<void>;
};

template <typename T>
concept Iterable = requires(T t) {
    { std::begin(t) } -> std::input_or_output_iterator;
    { std::end(t) } -> std::input_or_output_iterator;
};

template <typename T>
concept Indexable = requires(T t, const T ct) {
    // Non-const version
    { t[std::size_t()] };

    // Const version
    { ct[std::size_t()] };
};

template <typename T>
concept IndexableReturningReference = requires(T t, const T ct) {
    // Non-const version
    {
        t[std::size_t()]
    } -> std::same_as<
          typename std::add_lvalue_reference_t<typename T::value_type>>;

    // Const version
    {
        ct[std::size_t()]
    } -> std::same_as<typename std::add_lvalue_reference_t<
          typename std::add_const_t<typename T::value_type>>>;
};

class UUID
{
  public:
    using value_type = int;
    static value_type next()
    {
        static std::atomic<value_type> counter{0};
        return counter++;
    }
};

template <typename Rep, typename Period>
std::ostream& operator<<(std::ostream& os,
                         const std::chrono::duration<Rep, Period>& duration)
{
    // Convert duration to count and format the output
    os << duration.count() * Period::num / Period::den << " seconds";
    return os;
}

/**
// * As of the move to gcc 13.2.0 the chrono_io lib now
// * include its own overload for this leading to ambiguous
// * overload errors if this code is uncommented.
template <typename Clock, typename Duration>
std::ostream&
operator<<(std::ostream& os,
           const std::chrono::time_point<Clock, Duration>& time_point)
{
    using namespace std::chrono;

    // Convert time_point to system_clock::time_point
    auto tp                 = time_point.time_since_epoch();
    auto seconds            = duration_cast<std::chrono::seconds>(tp);
    auto fractional_seconds = tp - duration_cast<std::chrono::seconds>(tp);

    // Convert seconds to std::time_t for formatting
    std::time_t time =
        system_clock::to_time_t(system_clock::time_point(seconds));
    std::tm* tm = std::gmtime(&time);

    // Print formatted time
    os << std::put_time(tm, "%Y-%m-%d %H:%M:%S");

    // Optionally include fractional seconds
    if(fractional_seconds.count() > 0) {
        os << '.' << std::setw(6) << std::setfill('0')
           << fractional_seconds.count() / 1000; // e.g., milliseconds
    }

    return os;
}
*/

template <typename T>
struct is_device_container: std::false_type {
};

template <typename T, typename Alloc>
struct is_device_container<thrust::device_vector<T, Alloc>>: std::true_type {
};

template <typename T>
struct is_device_container<cuda::std::span<T>>: std::true_type {
};

template <typename T>
struct is_host_container: std::false_type {
};

template <typename T, typename Alloc>
struct is_host_container<thrust::host_vector<T, Alloc>>: std::true_type {
};

template <typename T, typename Alloc>
struct is_host_container<std::vector<T, Alloc>>: std::true_type {
};

template <typename T>
struct is_host_container<std::span<T>>: std::true_type {
};

template <typename T>
static constexpr bool is_device_container_v = is_device_container<T>::value;

template <typename T>
static constexpr bool is_host_container_v = is_host_container<T>::value;

// Primary template for function_traits
template<typename T>
struct function_traits;

// Specialization for regular functions and function pointers
template<typename Ret, typename... Args>
struct function_traits<Ret(Args...)> {
    using return_type = Ret;
    using argument_types = std::tuple<Args...>;
    static constexpr std::size_t arity = sizeof...(Args);
};

// Specialization for function pointers
template<typename Ret, typename... Args>
struct function_traits<Ret(*)(Args...)> : function_traits<Ret(Args...)> {};

// Specialization for member function pointers (const)
template<typename ClassType, typename Ret, typename... Args>
struct function_traits<Ret(ClassType::*)(Args...) const> : function_traits<Ret(Args...)> {};

// Specialization for member function pointers (non-const)
template<typename ClassType, typename Ret, typename... Args>
struct function_traits<Ret(ClassType::*)(Args...)> : function_traits<Ret(Args...)> {};

// Specialization for lambdas and other functors with operator()
template<typename T>
struct function_traits : function_traits<decltype(&T::operator())> {};

// Trait to deduce function traits for a specific invocation
template<typename Callable, typename... Args>
struct invocation_traits {
    using return_type = std::invoke_result_t<Callable, Args...>;
    using argument_types = std::tuple<Args...>;
    static constexpr std::size_t arity = sizeof...(Args);
};

template <typename KeyType, typename ValueType>
class BiDirectonalMap {
public:
    using ForwardMap = std::unordered_map<KeyType, ValueType>;
    using ReverseMap = std::unordered_map<ValueType, KeyType>;

    BiDirectonalMap(std::initializer_list<std::pair<KeyType, ValueType>> list) {
        for (const auto& [key, value] : list) {
            forward_map_.emplace(key, value);
            reverse_map_.emplace(value, key);
        }
    }

    ValueType get_value(KeyType key) const {
        if (auto it = forward_map_.find(key); it != forward_map_.end()) {
            return it->second;
        }
        throw std::out_of_range("Key not found in forward mapping: " + std::to_string(key));
    }

    KeyType get_key(ValueType value) const {
        if (auto it = reverse_map_.find(value); it != reverse_map_.end()) {
            return it->second;
        }
        throw std::out_of_range("Value not found in reverse mapping: " + std::string{value});
    }

private:
    ForwardMap forward_map_;
    ReverseMap reverse_map_;
};

} // namespace psrdada_cpp