#pragma once

#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/merger/merger.hpp"

namespace psrdada_cpp {
namespace merger {

template<class MergerType, class Handler>
class Pipeline
{
public:
    Pipeline(MergerType& merger, Handler& handler);
    ~Pipeline();

    /**
     * @brief      A callback to be called on connection
     *             to a ring buffer.
     *
     * @details     The first available header block in the
     *             in the ring buffer is provided as an argument.
     *             It is here that header parameters could be read
     *             if desired.
     *
     * @param      block  A RawBytes object wrapping a DADA header buffer
     */
    void init(RawBytes& block);

    /**
     * @brief      A callback to be called on acqusition of a new
     *             data block.
     *
     * @param      block  A RawBytes object wrapping a DADA data buffer
     */
    bool operator()(RawBytes& block);

private:
    MergerType& _merger;
    Handler& _handler;
};

}
}

#include "psrdada_cpp/merger/src/pipeline.cpp"