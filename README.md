# PSRDADA_CPP

PSRDADA_CPP contains C++ wrappers for a subset for PSRDADA functionality (HDUs, read/write clients, etc.) and implements various CLI application (from and to disk r/w, buffer synchronisation, buffer splitting etc.). PSRDADA_CPP also provides an interface for (high-performance) processing pipelines and DADA buffers. A few examples exists in the submodules (e.g. `gated_spectrometer`).

## Requirements
- psrdada
- boost 1.67 or later
- CMake 3.12 or later
- CUDA (optional, required to compile `.cu` and `.cuh` source)

## Installation

Clone the repository and enter the root folder

`git clone https://gitlab.mpcdf.mpg.de/mpifr-bdg/psrdada_cpp && cd psrdada_cpp`

Prepare and build the entire project

`cmake -S . -B <path-to-build> -DENABLE_CUDA=TRUE -DPSRDADA_INCLUDE_DIR=<path-to-psrdada-headers> && make -C <path-to-build>/`

### Custom CMake flags

All flags are passed to cmake with the CMake prefix `-D<flagname>`

| Flagname | Description | Values        | Command |
|----------|-------------|---------------|---------|
| `ENABLE_CUDA` | Allows compilation with `nvcc` which is require to compile `.cuh` and `.cu` | `ON` (default), `OFF` | - |
| `BUILD_SUBMODULES` | Build submodules (excluding dadaflow). | `ON` (default), `OFF` | - |
| `BUILD_DADAFLOW` | Builds the dadaflow submodule. | `OFF` (default), `ON` | - |
| `ENABLE_TESTING` | Enables testing relying on  `googletest`.  If `googletest` is not found by cmake it gets pulled from remote. | `ON` (default), `OFF` | `make -C <path-to-build> test` |
| `ENABLE_BENCHMARK` | Enables benchmarks relying on `googlebenchmark`. If `googlebenchmark` is not found by cmake it gets pulled from remote. Only has effect when `BUILD_SUBMODULES` is `ON`. | `OFF` (default), `ON` | `make -C <path-to-build> run_benchmark` |
| `FORCE_DOWNLOAD` | Fetches the content of `googlebenchmark` and `googletest` from remote. Useful when an existing installation causes problems. | `OFF` (default), `ON` | - |

## Documentation
The documentation of psrdadad-cpp can be found at: http://mpifr-bdg.pages.mpcdf.de/psrdada_cpp/

