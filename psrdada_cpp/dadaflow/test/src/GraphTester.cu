#include "psrdada_cpp/dadaflow/test/GraphTester.cuh"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/FixedWidth.hpp"
#include "psrdada_cpp/dadaflow/pipelines/Demo.hpp"

#include <gmock/gmock.h>

namespace psrdada_cpp
{
namespace test
{

TEST_F(GraphTester, simple_graph)
{
    DemoPipeline<int> pipeline;
    Graph& graph = pipeline.graph();
    graph.show_dependencies();
    graph.show_topological_order();
    // GraphRunner::run(&graph); // Only usable from the CLI
    graph.run_async();
    std::this_thread::sleep_for(2s);
    graph.stop();
    graph.await();
    graph.show_statistics();
    graph.make_dot_graph();
}

} // namespace test
} // namespace psrdada_cpp