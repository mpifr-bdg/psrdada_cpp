#include "psrdada_cpp/dadaflow/AsyncDadaReadClient.hpp"

namespace psrdada_cpp
{

//
// AsyncStream
//

template <typename DerivedAsyncStream>
AsyncStream<DerivedAsyncStream>::AsyncStream(AsyncDadaReadClient& parent)
    : _parent(parent), _has_block(false), _block_counter(0)
{
}

template <typename DerivedAsyncStream>
AsyncStream<DerivedAsyncStream>::~AsyncStream()
{
}

template <typename DerivedAsyncStream>
std::pair<RawBytes, std::function<void()>>
AsyncStream<DerivedAsyncStream>::next()
{
    std::unique_lock<std::mutex> lock(mutex);
    _cv.wait(lock, [this]() { 
        return !_has_block; });
    BOOST_LOG_TRIVIAL(debug)
        << _parent.id() << "Acquiring block " << _block_counter;
    uint64_t total_bytes{}, used_bytes{};
    char * tmp;
    while(true)
    {
        if(_parent._stop.load())
        {
            throw DadaInterruptException("Interrupt called in AsyncDadaReadClient::AsyncStream::next()");
        }
        if(static_cast<DerivedAsyncStream*>(this)->is_stream_empty())
        {
            usleep(100); // With very small buffers and high rates this might not be suffcient
        }
        else
        {
            tmp  = static_cast<DerivedAsyncStream*>(this)->next_impl(total_bytes,
                                                                  used_bytes);
            break;
        }
    }
    _has_block = true;
    RawBytes block(tmp, total_bytes, used_bytes);
    auto release_callback = [this]() { this->block_release_callback(); };
    ++_block_counter;
    return std::make_pair(block, release_callback);
}

template <typename DerivedAsyncStream>
void AsyncStream<DerivedAsyncStream>::block_release_callback()
{
    std::unique_lock<std::mutex> lock(mutex);
    static_cast<DerivedAsyncStream*>(this)->release_impl();
    _has_block = false;
    _cv.notify_one();
}

template <typename DerivedAsyncStream>
bool AsyncStream<DerivedAsyncStream>::at_end() const
{
    return static_cast<DerivedAsyncStream const*>(this)->at_end_impl();
}

template <typename DerivedAsyncStream>
void AsyncStream<DerivedAsyncStream>::await() const
{
    std::unique_lock<std::mutex> lock(mutex);
    _cv.wait(lock, [this]() { return !_has_block; });
}

template <typename DerivedAsyncStream>
bool AsyncStream<DerivedAsyncStream>::has_block() const
{
    return _has_block.load();
}


//
// HeaderStream
//

char* HeaderStream::next_impl(uint64_t& total_bytes, uint64_t& used_bytes)
{
    char* tmp   = ipcbuf_get_next_read(_parent._hdu->header_block, &used_bytes);
    total_bytes = _parent.header_buffer_size();
    if(!tmp) {
        _parent._log.write(LOG_ERR, "Could not get header\n");
        throw std::runtime_error("Could not get header");
    }
    return tmp;
}

bool HeaderStream::is_stream_empty() const 
{
    return _parent.header_buffer_is_empty();
}

void HeaderStream::release_impl()
{
    BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Releasing header block";
    if(ipcbuf_mark_cleared(_parent._hdu->header_block) < 0) {
        _parent._log.write(LOG_ERR, "Could not mark cleared header block\n");
        throw std::runtime_error("Could not mark cleared header block");
    }
}

bool HeaderStream::at_end_impl() const
{
    return static_cast<bool>(ipcbuf_eod(_parent._hdu->header_block));
}

//
// DataStream
//

char* DataStream::next_impl(uint64_t& total_bytes, uint64_t& used_bytes)
{
    uint64_t block_idx = 0;
    char* tmp          = ipcio_open_block_read(_parent._hdu->data_block,
                                      &used_bytes,
                                      &block_idx);
    total_bytes        = _parent.data_buffer_size();
    if(!tmp) {
        _parent._log.write(LOG_ERR, "Could not get data block\n");
        throw std::runtime_error("Could not get data block");
    }
    return tmp;
}


bool DataStream::is_stream_empty() const 
{
    return _parent.data_buffer_is_empty();
}

void DataStream::release_impl()
{
    BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Releasing data block";
    if(ipcio_close_block_read(_parent._hdu->data_block,
                              _parent.data_buffer_size()) < 0) {
        _parent._log.write(LOG_ERR,
                           "close_buffer: ipcio_close_block_read failed\n");
        throw std::runtime_error("Could not close ipcio data block");
    }
}

bool DataStream::at_end_impl() const
{
    return static_cast<bool>(ipcbuf_eod((ipcbuf_t*)(_parent._hdu->data_block)));
}

//
// AsyncDadaReadClient
//

AsyncDadaReadClient::AsyncDadaReadClient(key_t key, MultiLog& log)
    : DadaClientBase(key, log), _locked(false), _header_stream(*this),
      _data_stream(*this)
{
    lock();
}

AsyncDadaReadClient::~AsyncDadaReadClient()
{
    BOOST_LOG_TRIVIAL(debug) << this->id() << "Destructing AsyncDadaReadClient";
    if(_connected && _locked)
    {
        release();
    }
}


void AsyncDadaReadClient::connect()
{
    DadaClientBase::connect();
    lock();
}


void AsyncDadaReadClient::disconnect()
{
    if(_data_stream.has_block())
    {
        BOOST_LOG_TRIVIAL(error) << this->id() << "Data block was acquired but never released, leaving buffer in undefined state";
        throw DadaResourceInUseException("Data block was acquired but never released, leaving buffer in undefined state");
    }
    if(_header_stream.has_block())
    {
        BOOST_LOG_TRIVIAL(error) << this->id() << "Header block was acquired but never released, leaving buffer in undefined state";
        throw DadaResourceInUseException("Header block was acquired but never released, leaving buffer in undefined state");
    }
    if(_locked)
    {
        release();
    }
    DadaClientBase::disconnect();
}

void AsyncDadaReadClient::lock()
{
    if(!_connected) {
        throw std::runtime_error("Lock requested on unconnected HDU\n");
    }
    BOOST_LOG_TRIVIAL(debug)
        << this->id() << "Acquiring reading lock on dada buffer";
    if(dada_hdu_lock_read(_hdu) < 0) {
        _log.write(LOG_ERR, "open_hdu: could not lock read\n");
        throw std::runtime_error(std::string("Error locking HDU with key: ") +
                                 std::to_string(_key));
    }
    BOOST_LOG_TRIVIAL(debug)
        << this->id() << "Acquired";
    
    _locked = true;
}

void AsyncDadaReadClient::release()
{
    if(!_locked) {
        throw std::runtime_error("Release requested on unlocked HDU\n");
    }
    BOOST_LOG_TRIVIAL(debug)
        << this->id() << "Releasing reading lock on dada buffer";
    if(dada_hdu_unlock_read(_hdu) < 0) {
        _log.write(LOG_ERR, "open_hdu: could not release read\n");
        throw std::runtime_error(std::string("Error releasing HDU with key: ") +
                                 std::to_string(_key));
    }
    _locked = false;
}

HeaderStream& AsyncDadaReadClient::header_stream()
{
    return _header_stream;
}

DataStream& AsyncDadaReadClient::data_stream()
{
    return _data_stream;
}

template class AsyncStream<HeaderStream>;
template class AsyncStream<DataStream>;

} // namespace psrdada_cpp
