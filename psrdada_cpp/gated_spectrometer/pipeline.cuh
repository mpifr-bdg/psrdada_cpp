#pragma once

#include <cuda.h>
#include <cufft.h>
#include <cuda_profiler_api.h>
#include <thrust/device_vector.h>
#include <thrust/system/cuda/execution_policy.h>

#include <bitset>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <sstream>

#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/double_device_buffer.cuh"
#include "psrdada_cpp/double_host_buffer.cuh"
#include "psrdada_cpp/mk_buffer_view.hpp"
#include "psrdada_cpp/common/unpacker.cuh"
#include "psrdada_cpp/common/detector_accumulator.cuh"
#include "psrdada_cpp/gated_spectrometer/kernels.cuh"
#include "psrdada_cpp/gated_spectrometer/io.cuh"

namespace psrdada_cpp {
namespace gated_spectrometer {

struct pipe_config
{
    /**
     * @brief      pipe_config defining behavior of Pipeline
     *
     * @param      buffer_bytes A RawBytes object wrapping a DADA header buffer
     * @param      nSideChannels Number of side channel items in the data stream,
     * @param      selectedSideChannel Side channel item used for gating
     * @param      selectedBit bit of side channel item used for gating
     * @param      speadHeapSize Size of the spead heap block.
     * @param      fftLength Size of the FFT
     * @param      naccumulate Number of samples to integrate in the individual
     *             FFT bins
     * @param      nbits Bit depth of the sampled signal
     * @param      handler Output handler
     *
     */

    MKDadaBufferView dadaBufferLayout;
    key_t dada_key;
    size_t selectedSideChannel = 0;
    size_t speadHeapSize = 0;
    size_t nSideChannels = 0;
    size_t selectedBit = 0;
    size_t fft_length = 0;
    size_t naccumulate = 0;
    unsigned int nbits = 0;
    std::bitset<2> active_gates = 3; // Default both gates active

    // pipe_config(){};
};

/**
 @class Pipeline
 @brief Split data into two streams and create integrated spectra depending on
 bit set in side channel data.
 */
template <class HandlerType, class InputType, class OutputType>
class Pipeline {
public:

  /**
   * @brief      Constructor

   */
  Pipeline(const pipe_config &ip, HandlerType &handler);


  ~Pipeline();

  /**
   * @brief      A callback to be called on connection
   *             to a ring buffer.
   *
   * @details     The first available header block in the
   *             in the ring buffer is provided as an argument.
   *             It is here that header parameters could be read
   *             if desired.
   *
   * @param      block  A RawBytes object wrapping a DADA header buffer
   */
  void init(RawBytes &block);

  /**
   * @brief      A callback to be called on acqusition of a new
   *             data block.
   *
   * @param      block  A RawBytes object wrapping a DADA data buffer output
   *             are the integrated specttra with/without bit set.
   */
  bool operator()(RawBytes &block);

  // make the relevant processing methods proteceed only for testing

  std::size_t call_count(){return _call_count;}
  std::size_t nblocks(){return _nBlocks;}
protected:
  /// Processing strategy for single pol mode
  void process(SinglePolarizationInput *inputDataStream, GatedPowerSpectrumOutput *outputDataStream);

  /// Processing strategy for dual pol  power mode
  //void process(DualPolarizationInput*inputDataStream, GatedPowerSpectrumOutput *outputDataStream);

  /// Processing strategy for full stokes mode
  void process(DualPolarizationInput *inputDataStream, GatedFullStokesOutput *outputDataStream);

  OutputType* outputDataStream;
  InputType* inputDataStream;

private:
  // gate the data from the input stream and fft data per gate. Number of bit
  // sets in every gate is stored in the output datasets
  void gated_fft(PolarizationData &data,
    thrust::device_vector<uint64_cu> &_noOfBitSetsIn_G0,
    thrust::device_vector<uint64_cu> &_noOfBitSetsIn_G1);

  MKDadaBufferView _dadaBufferLayout;

  std::size_t _fft_length;
  std::size_t _naccumulate;
  std::size_t _selectedSideChannel;
  std::size_t _selectedBit;
  std::size_t _batch;
  std::size_t _nsamps_per_heap;
  std::bitset<2> _active_gates;

  HandlerType &_handler;
  cufftHandle _fft_plan;
  uint64_t _nchans;
  uint64_t _nBlocks;            // Number of dada blocks to integrate into one spectrum
  uint64_t _call_count;
  uint64_t _nOutBlocks = 0;

  std::unique_ptr<Unpacker> _unpacker;

  // Temporary processing block
  // ToDo: Use inplace FFT to avoid temporary voltage array
  thrust::device_vector<UnpackedVoltageType> _unpacked_voltage_G0;
  thrust::device_vector<UnpackedVoltageType> _unpacked_voltage_G1;

  cudaStream_t _h2d_stream;
  cudaStream_t _proc_stream;
  cudaStream_t _d2h_stream;
};

}
}

#include "psrdada_cpp/gated_spectrometer/src/pipeline.cu"
