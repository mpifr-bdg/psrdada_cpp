#include "psrdada_cpp/dadaflow/test/MessageServerTester.hpp"

using namespace testing;
using ::testing::Return;
using ::testing::Throw;
using ::testing::_;
using ::testing::Invoke;
namespace js = boost::json;

namespace psrdada_cpp {
namespace test {

TEST_F(MessageServerTester, CallbackRegistrationAndInvocation)
{
    // First register a callback
    server.register_callback("test_command",
                             {{"key1", "str", "a test argument"}},
                             "Test command description",
                             [](js::object const& args) -> js::object {
                                 return args;
                             });

    // Mock request message
    std::string mock_request = R"({"command": "test_command", "arguments": {"key1": "value1"}})";
    request_queue.input_queue.push(std::move(mock_request));
    std::string response = response_queue.output_queue.pop();
    js::object json_response = js::parse(response).as_object();
    EXPECT_EQ(json_response["status"].as_string(), std::string{"success"});
    EXPECT_EQ(json_response["command"].as_string(), std::string{"test_command"});
    EXPECT_EQ(json_response["response"].as_object()["key1"].as_string(), std::string{"value1"});
    ASSERT_EQ(response_queue.send_count, std::size_t{1});
    server.shutdown();
    server.await();
}

TEST_F(MessageServerTester, NoCommandRegistered)
{
    // Mock request message
    std::string mock_request = R"({"command": "test_command", "arguments": {"key1": "value1"}})";
    request_queue.input_queue.push(std::move(mock_request));
    std::string response = response_queue.output_queue.pop();
    js::object json_response = js::parse(response).as_object();
    EXPECT_EQ(json_response["status"].as_string(), std::string{"error"});
    EXPECT_THAT(json_response["message"].as_string(), HasSubstr("Command not found"));
    server.shutdown();
    server.await();
}

TEST_F(MessageServerTester, FailedCommand)
{
    server.register_callback("test_command",
                             {{"key1", "str", "a test argument"}},
                             "Test command description",
                             [](js::object const&) -> js::object {
                                throw std::runtime_error("Fail");
                              });

    // Mock request message
    std::string mock_request = R"({"command": "test_command", "arguments": {"key1": "value1"}})";
    request_queue.input_queue.push(std::move(mock_request));
    std::string response = response_queue.output_queue.pop();
    js::object json_response = js::parse(response).as_object();
    EXPECT_EQ(json_response["status"].as_string(), std::string{"error"});
    EXPECT_THAT(json_response["message"].as_string(), HasSubstr("Fail"));
    server.shutdown();
    server.await();
}

TEST_F(MessageServerTester, SuccessAfterFail)
{
    server.register_callback("fail_command",
                             {{"key1", "str", "a test argument"}},
                             "Test command description",
                             [](js::object const&) -> js::object {
                                throw std::runtime_error("Fail");
                              });

    server.register_callback("success_command",
                            {{"key1", "str", "a test argument"}},
                            "Test command description",
                            [](js::object const& args) -> js::object {
                                return args;
                            });

    // Mock request message
    std::string mock_fail_request = R"({"command": "fail_command", "arguments": {"key1": "value1"}})";
    request_queue.input_queue.push(std::move(mock_fail_request));
    std::string fail_response = response_queue.output_queue.pop();
    js::object json_response_f = js::parse(fail_response).as_object();
    EXPECT_EQ(json_response_f["status"].as_string(), std::string{"error"});
    EXPECT_THAT(json_response_f["message"].as_string(), HasSubstr("Fail"));
    ASSERT_EQ(response_queue.send_count, std::size_t{1});

    std::string mock_success_request = R"({"command": "success_command", "arguments": {"key1": "value1"}})";
    request_queue.input_queue.push(std::move(mock_success_request));
    std::string success_response = response_queue.output_queue.pop();
    js::object json_response_s = js::parse(success_response).as_object();
    EXPECT_EQ(json_response_s["status"].as_string(), std::string{"success"});
    EXPECT_EQ(json_response_s["command"].as_string(), std::string{"success_command"});
    EXPECT_EQ(json_response_s["response"].as_object()["key1"].as_string(), std::string{"value1"});
    ASSERT_EQ(response_queue.send_count, std::size_t{2});
    server.shutdown();
    server.await();
}

} // namespace test
} // namespace psrdada_cpp