#include "psrdada_cpp/dadaflow/SharedStruct.hpp"

#include <chrono>
#include <iostream>
#include <thread>

using namespace psrdada_cpp;
using namespace std::chrono_literals;

namespace fields
{
ARRAYFIELD(weights, float2, "Antenna weights")
FIELD(lo_frequency, float, "LO frequency (Hz)")
ARRAYFIELD(state, char, "My name")
} // namespace fields

using MonitoringData =
    Descriptor<fields::weights<8>, fields::lo_frequency, fields::state<64>>;

int main()
{
    SharedStruct<MonitoringData> buffer("shared_struct_example");
    auto& data = buffer.data();
    int ii     = 0;
    while(true) {
        std::cout << "--------- " << ii << " ---------\n";
        int count = 0;
        for(auto const& [x, y]: data.weights) {
            std::cout << "weights[" << count << "]: " << x << " + " << y
                      << "j\n";
            ++count;
        }
        std::cout << "lo_frequency: " << data.lo_frequency << "\n";
        std::cout << "state: " << data.state.data() << "\n\n";
        ++ii;
        std::this_thread::sleep_for(1s);
        buffer.with_write_lock([&](auto& data) {
            data.weights[ii % 8].x = static_cast<float>(ii);
            data.weights[ii % 8].y = static_cast<float>(-ii);
        });
    }

    return 0;
}