#include "psrdada_cpp/dadaflow/processors/AsyncDadaSource.hpp"

#include "psrdada_cpp/dada_db.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <atomic>
#include <chrono>
#include <cmath>
#include <condition_variable>
#include <functional>
#include <thread>
#include <type_traits>

namespace psrdada_cpp
{

template <typename Parser>
AsyncDadaSource<Parser>::AsyncDadaSource(key_t key_,
                                         MultiLog& log_,
                                         Parser&& parser)
    : key(key_), log(log_), _parser(std::move(parser))
{
}

template <typename Parser>
auto AsyncDadaSource<Parser>::operator()()
{
    if(initialize) {
        client.reset(new AsyncDadaReadClient(key, log));
        auto [header_bytes, release_callback] = client->header_stream().next();
        _parser.parse_header(header_bytes);
        release_callback();
        initialize = false;
    }
    auto [data_bytes, release_callback] = client->data_stream().next();
    // Here we wrap the release call in a non-dereferenceable shared_ptr
    // this is passed to the parser and if the parser requires the
    // memory of the dada buffer it must capture this shared_ptr in a custom
    // deleter for the types it produces.
    auto release_lifetime =
        std::shared_ptr<void>(new int(0), [release_callback](int* ptr) {
            delete ptr;
            release_callback();
        });
    auto output = _parser.parse_data(data_bytes, release_lifetime);
    if(client->data_stream().at_end()) {
        client->data_stream().await();
        initialize = true;
    }
    return output;
}

} // namespace psrdada_cpp
