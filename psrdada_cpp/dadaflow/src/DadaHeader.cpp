#include "psrdada_cpp/dadaflow/DadaHeader.hpp"

#include "psrdada_cpp/raw_bytes.hpp"

#include <iomanip>

namespace psrdada_cpp
{

// Iteration support methods
DadaHeader::MapIterator DadaHeader::begin() const
{
    return _header_map.begin();
}

DadaHeader::MapIterator DadaHeader::end() const
{
    return _header_map.end();
}

DadaHeader::MapIterator DadaHeader::cbegin() const
{
    return _header_map.cbegin();
}

DadaHeader::MapIterator DadaHeader::cend() const
{
    return _header_map.cend();
}

bool DadaHeader::has_key(std::string_view key) const
{
    return _header_map.find(key) != _header_map.end(); 
}

std::string_view DadaHeader::trim(std::string_view str)
{
    const auto str_begin = str.find_first_not_of(" \t");
    if(str_begin == std::string::npos)
        return ""; // no content

    const auto str_end   = str.find_last_not_of(" \t");
    const auto str_range = str_end - str_begin + 1;

    return str.substr(str_begin, str_range);
}

void DadaHeader::read_from(const char* data, std::size_t size)
{
    std::istringstream stream(std::string(data, size));
    read_from_impl(stream);
}

void DadaHeader::read_from(RawBytes const& bytes)
{
    std::istringstream stream(std::string{bytes.ptr(), bytes.used_bytes()});
    read_from_impl(stream);
}

void DadaHeader::read_from_impl(std::istringstream& stream)
{
    std::string line;
    while(std::getline(stream, line)) { parse_line(line); }
}

void DadaHeader::purge()
{
    _header_map.clear();
}

void DadaHeader::set(std::string_view key, std::string const& value)
{
    _header_map[std::string{key}] = value;
}

std::vector<char> DadaHeader::to_vector(bool compact) const
{
    std::ostringstream oss;
    if(compact) {
        // Compact mode: write key-value pairs with tab and newline
        for(const auto& pair: _header_map) {
            oss << pair.first << " " << pair.second << "\n";
        }
    } else {
        // Non-compact mode: align keys for readability

        // Step 1: Find the length of the longest key
        std::size_t max_key_length = 0;
        for(const auto& pair: _header_map) {
            max_key_length = std::max(max_key_length, pair.first.size());
        }

        // Step 2: Write each key-value pair with aligned keys
        for(const auto& pair: _header_map) {
            // Pad the key with spaces so that all keys align
            oss << std::left << std::setw(max_key_length) << pair.first;
            oss << " : " << pair.second << "\n"; // Separator for readability
        }
    }

    // Convert the string stream into a vector of chars
    std::string result = oss.str();
    return std::vector<char>(result.begin(), result.end());
}

std::string DadaHeader::to_string(std::string_view value) const
{
    return std::string(value.begin(), value.end());
}

void DadaHeader::parse_line(std::string_view line)
{
    // Step 1: Trim leading and trailing whitespace
    std::string_view trimmed_line = trim(line);

    // Step 2: Ignore lines that are comments or empty
    if(trimmed_line.empty() || trimmed_line[0] == '#') {
        return; // Ignore this line
    }

    // Step 3: Find if there is an inline comment (e.g., "# comment")
    std::size_t comment_pos = trimmed_line.find('#');
    if(comment_pos != std::string::npos) {
        // Remove everything after the '#' character (including the '#')
        trimmed_line = trimmed_line.substr(0, comment_pos);
        trimmed_line = trim(trimmed_line); // Trim any remaining spaces
    }

    // Step 4: Split by whitespace between key and value
    std::istringstream iss{std::string(trimmed_line)};
    std::string key, value;
    if(!(iss >> key)) // Try to extract the key
    {
        return; // No key found, invalid line
    }

    // Step 5: Extract the rest as the value
    std::getline(iss, value);
    value = trim(value); // Trim any extra spaces

    // Only insert valid key-value pairs
    if(!key.empty() && !value.empty()) {
        _header_map[key] = value;
    }
}

std::string_view DadaHeader::safe_lookup(std::string_view key) const
{
    auto it = _header_map.find(key);
    if(it == _header_map.end()) {
        throw InvalidHeaderKey(std::string{key});
    }
    return it->second;
}

} // namespace psrdada_cpp
