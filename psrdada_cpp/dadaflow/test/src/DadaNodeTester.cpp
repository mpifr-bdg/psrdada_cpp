#include "psrdada_cpp/dadaflow/test/DadaNodeTester.hpp"

#include "psrdada_cpp/dada_db.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/dadaflow/DadaHeader.hpp"
#include "psrdada_cpp/dadaflow/processors/AsyncDadaSink.hpp"
#include "psrdada_cpp/dadaflow/processors/AsyncDadaSource.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <gmock/gmock.h>
#include <memory>
#include <span>
#include <thread>

namespace psrdada_cpp
{
namespace test
{

template <typename T>
struct TestStruct {
    std::span<T> span;
    std::string data_type;
};

template <typename T>
class SpanParser
{
  public:
    using OutputType      = TestStruct<int>;
    using OutputPtr       = std::shared_ptr<OutputType>;
    using OutputTuple     = std::tuple<OutputPtr>;
    using ReleaseLifetime = std::shared_ptr<void>;

    SpanParser()                             = default;
    SpanParser(SpanParser const&)            = delete;
    SpanParser& operator=(SpanParser const&) = delete;
    SpanParser(SpanParser&&)                 = default;

    void parse_header(RawBytes& header_bytes)
    {
        _header.purge();
        _header.read_from(header_bytes);
    }

    OutputTuple parse_data(RawBytes& bytes, ReleaseLifetime releaser)
    {
        const std::size_t nelements = bytes.used_bytes() / sizeof(T);
        OutputPtr output_ptr        = OutputPtr(
            new OutputType{{reinterpret_cast<T*>(bytes.ptr()), nelements},
                           _header.get<std::string>("DATA_TYPE")},
            [releaser](OutputType* ptr) {
                delete ptr;
                // Here the releaser doesn't need to be called. It is captured
                // in the lambda and when the lambda is called it will go out of
                // scope, releasing the buffer
            });
        return std::tie(output_ptr);
    }

  private:
    DadaHeader _header;
};

TEST_F(DadaNodeTester, simple_async_source)
{
    using DataType = std::vector<int>;
    MultiLog logA("AsyncDadaSource");
    MultiLog logB("AsyncDadaSink");
    // Set up DADA buffer
    const std::size_t header_size_bytes = 4096;
    const std::size_t data_size_bytes   = 16384;
    const std::size_t nelements         = 1024;
    DadaDB db(16, data_size_bytes, 4, header_size_bytes);
    db.create();

    // Set up writer
    AsyncDadaSink writer(db.key(),
                         logB,
                         GenericHeaderFormatter([](DataType const& data) {
                             DadaHeader header;
                             header.set("DATA_TYPE", "INTVEC");
                             header.set("SIZE", data.size());
                             return header.to_vector();
                         }),
                         GenericDataFormatter());

    // Writer thread
    const std::size_t nwrites = 20;
    std::thread writer_thread([&]() {
        auto data_ptr = std::make_shared<DataType>(nelements, 0);
        for(std::size_t ii = 0; ii < nwrites; ++ii) {
            writer(data_ptr);
            for(auto& val: *data_ptr) { val += 1; }
        }
    });

    // Set up reader
    AsyncDadaSource source(db.key(), logA, SpanParser<int>());

    // Reader thread
    const std::size_t nreads = 2;
    std::thread reader_thread([&]() {
        std::size_t start_idx = 0;
        for(std::size_t ii = 0; ii < nreads; ++ii) {
            auto result_tuple      = source();
            auto const& result_ptr = std::get<0>(result_tuple);
            auto const& result     = *result_ptr;
            auto const& span       = result.span;
            auto const& dtype      = result.data_type;
            ASSERT_EQ(dtype, std::string{"INTVEC"});
            ASSERT_EQ(span.size(), data_size_bytes / sizeof(int));
            for(std::size_t jj = 0; jj < span.size(); ++jj) {
                ASSERT_EQ(span[jj],
                          static_cast<int>((start_idx + jj) / nelements))
                    << "Error at index " << start_idx + jj;
            }
            start_idx += data_size_bytes / sizeof(int);
        }
    });

    writer_thread.join();
    reader_thread.join();
}

} // namespace test
} // namespace psrdada_cpp