#include "psrdada_cpp/dadaflow/NodeSelector.hpp"
#include <iostream>
#include <thread>
#include <mutex>
#include <tuple>
#include <chrono>

using namespace std::chrono_literals;
using namespace psrdada_cpp;


int main()
{
    using T = int;
    using Tptr = std::shared_ptr<T>;
    std::mutex writer_mutex;
    Graph graph;
    
    auto& counter = graph.add_node([val=0]() mutable ->Tptr {
        std::this_thread::sleep_for(100ms);
        return std::make_shared<T>(val++);
    }, "counter");

    NodeSelector<3, T, T> option_set(graph);

    auto& times2 = graph.add_node([&](Tptr input) -> Tptr {
        return std::make_shared<T>(*input * 2);
    }, "x2");

    auto& times10 = graph.add_node([&](Tptr input) -> Tptr {
        return std::make_shared<T>(*input * 10);
    }, "x10");

    auto& plus13 = graph.add_node([&](Tptr input) -> Tptr {
        return std::make_shared<T>(*input * 13);
    }, "x13");
    
    option_set.template set_option<0>("x2", times2);
    option_set.template set_option<1>("x10", times10);
    option_set.template set_option<2>("x13", plus13);

    auto& receiver = graph.add_node([&](Tptr input) -> void {
        std::unique_lock<std::mutex> lock(writer_mutex);
        BOOST_LOG_TRIVIAL(debug) << "Received " << *input;
    }, "receiver");
    
    option_set.connect_input(counter.template output_port<0>());
    option_set.connect_output(receiver.template input_port<0>());
    option_set.select_option("x10");
    graph.prepare();    
    graph.run_async();

    std::jthread switch_mode([&](){
        std::this_thread::sleep_for(2s);
        BOOST_LOG_TRIVIAL(debug) << "Switching to mode x13";
        option_set.select_option("x13");

        std::this_thread::sleep_for(2s);
        BOOST_LOG_TRIVIAL(debug) << "Switching to mode x2";
        option_set.select_option("x2");
    
        std::this_thread::sleep_for(2s);
        graph.stop();
    });

    graph.await();
    return 0;
}
