#ifndef PSRDADA_CPP_EFFELSBERG_EDD_DETECTORACCUMULATOR_CUH
#define PSRDADA_CPP_EFFELSBERG_EDD_DETECTORACCUMULATOR_CUH

#include "psrdada_cpp/common.hpp"
#include <thrust/device_vector.h>

namespace psrdada_cpp {
namespace kernels {


// template argument unused but needed as nvcc gets otherwise confused.
template <typename T>
__global__
void detect_and_accumulate(float2 const* __restrict__ in, int8_t* __restrict__ out,
    int nchans, int nsamps, int naccumulate, float scale, float offset, int stride, int out_offset)
{
    // grid stride loop over output array to keep
    for (size_t i = blockIdx.x * blockDim.x + threadIdx.x; (i < nsamps * nchans / naccumulate); i += blockDim.x * gridDim.x)
    {
      double sum = 0.0f;
      size_t currentOutputSpectra = i / nchans;
      size_t currentChannel = i % nchans;

      for (size_t j = 0; j < naccumulate; j++)
      {
        float2 tmp = in[ j * nchans + currentOutputSpectra * nchans * naccumulate + currentChannel];
        double x = tmp.x * tmp.x;
        double y = tmp.y * tmp.y;
        sum += x + y;
      }
      size_t toff = out_offset * nchans + currentOutputSpectra * nchans *stride;
      out[toff + i] += (int8_t) ((sum - offset)/scale);
      // no atomic add for int8, thus no optimized version here.A tomic add can be
      // implemented using an in32 atomicAdd and bit shifting, but this needs more effort.
    }

}


/**
 * @brief Integrates output power from complex voltage input.
 *
 * @param in            Pointer to input data of size nsamps.
 * @param out           Pointer to output data of size nsamps / (naccumualte * nchans) * nchans
 * @param nchans        Number of channels
 * @param naccumulate   Number of input spectra to integrate into one output spectrum
 * @param scale         Normalization constant for the utput spectrum
 * @param offset        Output is shifted by this value
 * @param stride        Stride of the output spectra. For 2, leave a gap of
 * nchan between every output spetrum, for 1 no gap.
 * @param out_offset
 *
 *
 */
template <typename T>
__global__
void detect_and_accumulate(float2 const* __restrict__ in, float* __restrict__ out,
    size_t nchans, size_t nsamps, size_t naccumulate, float scale, float offset, int stride, int out_offset)
{

    const int nb = naccumulate / blockDim.x + 1;
    const int bs = blockDim.x;
    const int number_of_spectra = nsamps / (nchans * naccumulate);

    for (size_t current_spectrum =0; current_spectrum < number_of_spectra; current_spectrum++)
    { // I could just launch more blocks in a second dimension
        for (size_t i = blockIdx.x * blockDim.x + threadIdx.x; (i < nchans * nb); i += blockDim.x * gridDim.x)
        {
          const size_t channel_number = i % nchans;
          const size_t bn = i / nchans;

          double sum = 0;
          for (size_t k = 0; k < bs; k++)
          {
            int cidx = k + bn * bs;

            if (cidx >= naccumulate)
                break;

            const float2 tmp = in[channel_number + cidx * nchans + current_spectrum * naccumulate * nchans];

            double x = tmp.x * tmp.x;
            double y = tmp.y * tmp.y;
            sum += x + y;
          }
          size_t toff = out_offset * nchans + current_spectrum * nchans * stride;

          atomicAdd(&out[toff + channel_number], ((sum - offset)/scale));
        }
    }

}





} // namespace kernels



template <typename T>
class DetectorAccumulator
{
public:
    typedef thrust::device_vector<float2> InputType;
    typedef thrust::device_vector<T> OutputType;

public:
    DetectorAccumulator(DetectorAccumulator const&) = delete;


  DetectorAccumulator(
      int nchans, int tscrunch, float scale,
      float offset, cudaStream_t stream)
      : _nchans(nchans)
      , _tscrunch(tscrunch)
      , _scale(scale)
      , _offset(offset)
      , _stream(stream)
  {

  }

  ~DetectorAccumulator()
  {

  }

  // stride sets an offset of _nChans * stride to the detection in the output
  // to allow multiple spectra in one output
  void detect(InputType const& input, OutputType& output, int stride = 0, int stoff = 0)
  {
      assert(input.size() % (_nchans * _tscrunch) == 0 /* Input is not a multiple of _nchans * _tscrunch*/);
      //output.resize(input.size()/_tscrunch);
      int nsamps = input.size() / _nchans;
      float2 const* input_ptr = thrust::raw_pointer_cast(input.data());
      T * output_ptr = thrust::raw_pointer_cast(output.data());
      kernels::detect_and_accumulate<T> <<<1024, 1024, 0, _stream>>>(
          input_ptr, output_ptr, _nchans, nsamps, _tscrunch, _scale, _offset, stride, stoff);
  }



private:
    int _nchans;
    int _tscrunch;
    float _scale;
    float _offset;
    cudaStream_t _stream;
};

} //namespace psrdada_cpp

#endif // PSRDADA_CPP_EFFELSBERG_EDD_DETECTORACCUMULATOR_CUH



