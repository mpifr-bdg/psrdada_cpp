#include "psrdada_cpp/dadaflow/test/DetectorAccumulatorTester.cuh"

namespace psrdada_cpp
{
namespace test
{

    using InputType = typename DetectorAccumulatorTester::InputType;

    std::shared_ptr<InputType> DetectorAccumulatorTester::make_input(
        std::size_t npol, std::size_t nsamples, std::size_t nchannels) const {
        auto now   = std::chrono::system_clock::now();
        auto epoch = std::chrono::time_point_cast<std::chrono::seconds>(now);
        std::chrono::duration<long double, pico> offset(12342342.2432);
        auto tsamp = std::chrono::duration<long double, pico>(4785.046728971963);
        std::shared_ptr<InputType> input = std::make_shared<InputType>();
        input->resize({npol, nsamples, nchannels});
        input->timestamp(Timestamp(epoch, offset));
        input->timestep(tsamp);
        input->channel_zero_freq(856e6 * hertz);
        input->channel_bandwidth((856e6/4096) * hertz);
        input->polarisations({"H", "V"});
        return input;
    }

    std::vector<double> DetectorAccumulatorTester::cpu_stokes_sum(InputType const& input, 
        std::size_t input_offset, std::size_t naccumulate) const
    {
        const std::size_t nchannels = input.nchannels();
        const std::size_t nsamples = input.nsamples();
        // Just use the modulo of the input samples index to get the correct position
        std::vector<double> output;
        output.resize(4 * nchannels);
        thrust::host_vector<float2> tmp = input.container();
        for (std::size_t chan_idx = 0; chan_idx < nchannels; ++chan_idx)
        {
            StokesVector<double, I, Q, U, V> sum{};
            for (std::size_t samp_idx = input_offset; samp_idx < (input_offset + naccumulate); ++samp_idx) {
                std::size_t mod_samp_idx = samp_idx % nsamples;
                std::size_t pol0_idx = mod_samp_idx * nchannels + chan_idx;
                std::size_t pol1_idx = (nsamples * nchannels) + (mod_samp_idx * nchannels) + chan_idx;
                sum += calculate_stokes<double, I, Q, U, V>(tmp[pol0_idx], tmp[pol1_idx]);
            }
            output[0 * nchannels + chan_idx] = sum.x;
            output[1 * nchannels + chan_idx] = sum.y;
            output[2 * nchannels + chan_idx] = sum.z;
            output[3 * nchannels + chan_idx] = sum.w;
        }
        return output;
    }

    std::vector<double> DetectorAccumulatorTester::cpu_dualpoln_sum(InputType const& input, 
        std::size_t input_offset, std::size_t naccumulate) const
    {
        const std::size_t nchannels = input.nchannels();
        const std::size_t nsamples = input.nsamples();
        // Just use the modulo of the input samples index to get the correct position
        std::vector<double> output;
        output.resize(4 * nchannels);
        thrust::host_vector<float2> tmp = input.container();
        for (std::size_t chan_idx = 0; chan_idx < nchannels; ++chan_idx)
        {
            double2 sum{};
            for (std::size_t samp_idx = input_offset; samp_idx < (input_offset + naccumulate); ++samp_idx) {
                std::size_t mod_samp_idx = samp_idx % nsamples;
                std::size_t pol0_idx = mod_samp_idx * nchannels + chan_idx;
                std::size_t pol1_idx = (nsamples * nchannels) + (mod_samp_idx * nchannels) + chan_idx;
                sum.x += cuCmagf(tmp[pol0_idx]);
                sum.y += cuCmagf(tmp[pol1_idx]);
            }
            output[0 * nchannels + chan_idx] = sum.x;
            output[1 * nchannels + chan_idx] = sum.y;
        }
        return output;
    } 

    void DetectorAccumulatorTester::correctness_test_stokes(
        std::size_t npol,
        std::size_t nsamples,
        std::size_t nchannels,
        std::size_t naccumulate,
        std::size_t ncalls
    ) const
    {
        using DetectorType = SpectralDetectorAccumulator<InputType, OutputType, AllocatorType, DetAccStokesPolicy<double, I, Q, U, V>>;
        const int exepected_output_spectra = nsamples * ncalls / naccumulate;
        std::shared_ptr<InputType> input = this->make_input(npol, nsamples, nchannels);
        float2 value{0.0f, 0.0f};
        for (std::size_t ii = 0; ii < input->size(); ++ii)
        {
            (*input)[ii] = value;
            value += float2{0.0001f, 0.0001f};
        }

        AllocatorType pool(20);
        DetectorType detector(naccumulate, pool);
        
        int output_spectra_counter = 0;
        std::size_t input_offset = 0;
        for (std::size_t call_idx = 0; call_idx < ncalls; ++call_idx)
        {
            auto generator = detector(input);
            while(generator.next()) {
                auto output_optional = generator.get_value();
                if (output_optional){
                    auto const& output_tuple = *output_optional;
                    auto const& output_ptr = std::get<0>(output_tuple);
                    auto const& output = *output_ptr;
                    ASSERT_EQ(output.nsamples(), std::size_t(1));
                    ASSERT_EQ(output.nchannels(), nchannels);
                    ASSERT_EQ(output.npol(), std::size_t(4));
                    ASSERT_EQ(output.polarisations(), (std::vector<std::string>{"I", "Q", "U", "V"}));
                    ASSERT_EQ(output.timestamp(), input->timestamp() + input->timestep() * static_cast<double>(output_spectra_counter * naccumulate));
                    ASSERT_EQ(output.timestep(), input->timestep() * static_cast<double>(naccumulate));
                    ++output_spectra_counter; 
                    std::vector<double> expected_output = this->cpu_stokes_sum(*input, input_offset, naccumulate);
                    input_offset = (input_offset + naccumulate) % input->nsamples();
                    for (std::size_t pol_idx = 0; pol_idx < output.npol(); ++pol_idx){
                        for (std::size_t chan_idx = 0; chan_idx < output.nchannels(); ++chan_idx){
                            std::size_t output_idx = pol_idx * output.nchannels() + chan_idx;
                            
                            float actual_value = output[output_idx];
                            float expected_value = expected_output[output_idx];
                            float tolerance = 0.1f;
                            float abs_tolerance = 0.1f;  // Tolerance for near-zero comparisons

                            if (std::abs(expected_value) > abs_tolerance) {
                                // Use relative comparison if expected value is large enough
                                ASSERT_LT(std::abs((actual_value - expected_value) / expected_value), tolerance) 
                                    << "Pol " << pol_idx << " -- Chan " << chan_idx;
                            } else {
                                // Use absolute comparison if expected value is near zero
                                ASSERT_LT(std::abs(actual_value - expected_value), abs_tolerance) 
                                    << "Pol " << pol_idx << " -- Chan " << chan_idx;    
                            }
                        }
                    }   
                }
            }
        }
        ASSERT_EQ(output_spectra_counter, exepected_output_spectra);
    }

    void DetectorAccumulatorTester::correctness_test_dualpoln(
        std::size_t npol,
        std::size_t nsamples,
        std::size_t nchannels,
        std::size_t naccumulate,
        std::size_t ncalls
    ) const
    {
        using DetectorType = SpectralDetectorAccumulator<InputType, OutputType, AllocatorType, DetAccDualPolnPolicy<double>>;
        const int exepected_output_spectra = nsamples * ncalls / naccumulate;
        std::shared_ptr<InputType> input = this->make_input(npol, nsamples, nchannels);
        float2 value{0.0f, 0.0f};
        for (std::size_t ii = 0; ii < input->size(); ++ii)
        {
            (*input)[ii] = value;
            value += float2{0.0001f, 0.0001f};
        }

        AllocatorType pool(20);
        DetectorType detector(naccumulate, pool);
        
        int output_spectra_counter = 0;
        std::size_t input_offset = 0;
        for (std::size_t call_idx = 0; call_idx < ncalls; ++call_idx)
        {
            auto generator = detector(input);
            while(generator.next()) {
                auto output_optional = generator.get_value();
                if (output_optional){
                    auto const& output_tuple = *output_optional;
                    auto const& output_ptr = std::get<0>(output_tuple);
                    auto const& output = *output_ptr;
                    ASSERT_EQ(output.nsamples(), std::size_t(1));
                    ASSERT_EQ(output.nchannels(), nchannels);
                    ASSERT_EQ(output.npol(), std::size_t(2));
                    ASSERT_EQ(output.polarisations(), (std::vector<std::string>{"XX", "YY"}));
                    ASSERT_EQ(output.timestamp(), input->timestamp() + input->timestep() * static_cast<double>(output_spectra_counter * naccumulate));
                    ASSERT_EQ(output.timestep(), input->timestep() * static_cast<double>(naccumulate));
                    ++output_spectra_counter; 
                    std::vector<double> expected_output = this->cpu_dualpoln_sum(*input, input_offset, naccumulate);
                    input_offset = (input_offset + naccumulate) % input->nsamples();
                    for (std::size_t pol_idx = 0; pol_idx < output.npol(); ++pol_idx){
                        for (std::size_t chan_idx = 0; chan_idx < output.nchannels(); ++chan_idx){
                            std::size_t output_idx = pol_idx * output.nchannels() + chan_idx;
                            ASSERT_FLOAT_EQ(output[output_idx], expected_output[output_idx]) << "Pol " << pol_idx << " -- Chan " 
                            << chan_idx << ": " << output[output_idx] << " (expected " << expected_output[output_idx] 
                            <<" diff " << (output[output_idx] - expected_output[output_idx]) << ")";
                        }
                    }   
                }
            }
        }
        ASSERT_EQ(output_spectra_counter, exepected_output_spectra);
    }

    TEST_F(DetectorAccumulatorTester, stokes_correctness)
    {
        this->correctness_test_stokes(2, 128, 256, 128, 4);
        this->correctness_test_stokes(2, 256, 2333, 128, 4);
        this->correctness_test_stokes(2, 128, 12, 256, 4);
        this->correctness_test_stokes(2, 128, 1, 33, 10);
        this->correctness_test_stokes(2, 33, 64, 123, 8);
    }

    TEST_F(DetectorAccumulatorTester, dualpoln_correctness)
    {
        this->correctness_test_dualpoln(2, 128, 256, 128, 4);
        this->correctness_test_dualpoln(2, 256, 2333, 128, 4);
        this->correctness_test_dualpoln(2, 128, 12, 256, 4);
        this->correctness_test_dualpoln(2, 128, 1, 33, 10);
        this->correctness_test_dualpoln(2, 33, 64, 123, 8);
    }

    TEST_F(DetectorAccumulatorTester, invalid_inputs)
    {
        EXPECT_THROW((this->correctness_test_dualpoln(0, 128, 256, 128, 4)), std::runtime_error);
        EXPECT_THROW((this->correctness_test_dualpoln(1, 128, 256, 128, 4)), std::runtime_error);
        EXPECT_THROW((this->correctness_test_dualpoln(3, 128, 256, 128, 4)), std::runtime_error);
    }

} // namespace test
} // namespace psrdada_cpp