#pragma once

#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <atomic>
#include <condition_variable>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <utility>

namespace psrdada_cpp
{

// So it turns out that you can't open multiple DADA blocks for reading
// but you can for writing, so a ton of the code in the AsyncDadaReadClient
// is redundant.
// open_block_read contains a check to see that ipc->curbuf == 0, i.e. there is
// no active current buffer. I see no reason to implement this function.

class AsyncDadaReadClient;

/**
 * @brief An async, but not actually async, stream manager.
 *
 * @tparam DerivedAsyncStream A type deriving from this type.
 *
 * @details This provides a base class for the data and header
 *          streams from a DADA buffer.
 *
 * @note The AsyncStream uses the curiously recurring template
 *       pattern (CRTP).
 */
template <typename DerivedAsyncStream>
class AsyncStream
{
    friend AsyncDadaReadClient;

  protected:
    AsyncDadaReadClient& _parent;

  private:
    std::atomic<bool> _has_block;
    std::size_t _block_counter;
    mutable std::mutex mutex;
    mutable std::condition_variable _cv;

  public:
    /**
     * @brief Construct a new Async Stream object
     *
     * @param parent The reader client parent of this instance.
     */
    AsyncStream(AsyncDadaReadClient& parent);
    AsyncStream(AsyncStream const&) = delete;
    ~AsyncStream();

    /**
     * @brief Get the next block in the stream for reading
     *
     * @return std::pair<RawBytes, std::function<void()>> The first parameter in
     *         the pair is a RawBytes object wrapping the DADA buffer memory.
     * The second is a callable that will release the block.
     *
     * @details The intent here is to allow users to own the release of the
     * buffer block by deciding on when to call the release callback. A typical
     *          pattern would be to create a share pointer around some data that
     * uses the memory of the buffer and to put the release callback in a custom
     * destructor.
     */
    std::pair<RawBytes, std::function<void()>> next();

    /**
     * @brief Check if the EOD marker has been reached.
     *
     * @return true
     * @return false
     */
    bool at_end() const;

    /**
     * @brief Wait until any open block has been released.
     *
     */
    void await() const;

    /**
     * @brief Returns true if the stream has a block to process, otherwise false
     */
    bool has_block() const;


  private:
    void block_release_callback();
};

class HeaderStream: public AsyncStream<HeaderStream>
{
    friend AsyncStream<HeaderStream>;

  public:
    /**
     * @brief Construct a new Header Stream object.
     *
     * @param parent The parent read client of this instance.
     */
    explicit HeaderStream(AsyncDadaReadClient& parent)
        : AsyncStream<HeaderStream>(parent)
    {
    }

  protected:
    bool is_stream_empty() const; 
    char* next_impl(uint64_t& total_bytes, uint64_t& used_bytes);
    void release_impl();
    bool at_end_impl() const;
};

class DataStream: public AsyncStream<DataStream>
{
    friend AsyncStream<DataStream>;

  public:
    /**
     * @brief Construct a new Data Stream object
     *
     * @param parent The parent read client of this instance.
     */
    explicit DataStream(AsyncDadaReadClient& parent)
        : AsyncStream<DataStream>(parent)
    {
    }

  protected:
    bool is_stream_empty() const; 
    char* next_impl(uint64_t& total_bytes, uint64_t& used_bytes);
    void release_impl();
    bool at_end_impl() const;
};

/**
 * @brief      Class that provides means for reading from
 *             a DADA ring buffer
 */
class AsyncDadaReadClient: public DadaClientBase
{
  friend DataStream;
  friend HeaderStream;
  friend AsyncStream<DataStream>;
  friend AsyncStream<HeaderStream>;

  private:
    bool _locked;
    HeaderStream _header_stream;
    DataStream _data_stream;

  public:
    /**
     * @brief      Create a new client for reading from a DADA buffer
     *
     * @param[in]  key   The hexidecimal shared memory key
     * @param      log   A MultiLog instance for logging buffer transactions
     */
    AsyncDadaReadClient(key_t key, MultiLog& log);
    AsyncDadaReadClient(AsyncDadaReadClient const&) = delete;
    ~AsyncDadaReadClient();

    /**
     * @brief      Get a reference to a header stream manager
     *
     * @return     A HeaderStream manager object for the current buffer
     */
    HeaderStream& header_stream();

    /**
     * @brief      Get a reference to a data stream manager
     *
     * @return     A DataStream manager object for the current buffer
     */
    DataStream& data_stream();

    /**
     * @brief Release then reconnect to the buffer.
     *
     */
    void reset()
    {
        release();
        reconnect();
        lock();
    }

    /**
     * @brief      Connect the reader to the DADA buffer. Specializtion of the DadaClientBase::connect
     *
     * @details    DadaReadClient::connect() calls DadaClientBase::connect() 
     */
    void connect();

    /**
     * @brief      Disconnect the reader from the DADA buffer. Specializtion of the DadaClientBase::disconnect
     *
     * @details    AsyncDadaReadClient::disconnect() calls DadaClientBase::disconnect() after releasing the current 
     *             acquisition state (e.g. if data and/or header block is open and/or the reading lock acquired)
     */
    void disconnect();

  private:
    void lock();
    void release();
};

} // namespace psrdada_cpp

