#ifndef PSRDADA_CPP_DOUBLE_BUFFER_HPP
#define PSRDADA_CPP_DOUBLE_BUFFER_HPP

#include "thrust/device_vector.h"
#include <utility>

namespace psrdada_cpp {

/**
 * @brief      Class for double buffer.
 *
 * @tparam     T     The internal data type for the buffers
 *
 * @details     An implementation of the double buffer concept
 *             using thrust::device_vector. Provides double
 *             buffers in GPU memory.
 *
 *             This class exposes two buffers "a" and "b"
 *             which can be independently accessed. The buffers
 *             can be swapped to make b=a and a=b which is useful
 *             for staging inputs and outputs in a streaming pipeline
 */
template <typename VectorType>
class DoubleBuffer
{
public:
    using ValueType = typename VectorType::value_type;
    /**
     * @brief      Constructs the object.
     */
    DoubleBuffer(){}

    ~DoubleBuffer(){}

    DoubleBuffer(DoubleBuffer const&) = delete;

    /**
     * @brief      Resize the buffer
     *
     * @param[in]  size  The desired size in units of the data type
     */
    void resize(std::size_t size)
    {
        _buf0.resize(size);
        _buf1.resize(size);
    }

    /**
     * @brief      Resize the buffer
     *
     * @param[in]  size        The desired size in units of the data type
     * @param[in]  fill_value  The fill value for newly allocated memory
     */
    void resize(std::size_t size, typename VectorType::value_type fill_value)
    {
        _buf0.resize(size, fill_value);
        _buf1.resize(size, fill_value);
    }

    /**
     * @brief   Return the size of the buffer in elements
     */
    std::size_t size() const
    {
        return _buf0.size();
    }

    /**
     * @brief      Swap the a and b buffers
     */
    void swap()
    {
        _buf0.swap(_buf1);
    }


    /**
     * @brief      Return a reference to the "a" vector
     */
    VectorType& a()
    {
        return _buf0;
    }

    /**
     * @brief      Return a reference to the "b" vector
     */
    VectorType& b()
    {
        return _buf1;
    }

    /**
     * @brief      Return a pointer to the contents of the "a" vector
     */
    ValueType* a_ptr()
    {
        return thrust::raw_pointer_cast(_buf0.data());
    }


    /**
     * @brief      Return a pointer to the contents of the "b" vector
     */
    ValueType* b_ptr()
    {
        return thrust::raw_pointer_cast(_buf1.data());
    }


private:
    VectorType _buf0;
    VectorType _buf1;
};

} //namespace psrdada_cpp


#endif //PSRDADA_CPP_DOUBLE_DEVICE_BUFFER_HPP
