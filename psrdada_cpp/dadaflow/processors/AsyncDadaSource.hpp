#pragma once
#include "psrdada_cpp/dadaflow/AsyncDadaReadClient.hpp"

namespace psrdada_cpp
{

template <typename Parser_>
class AsyncDadaSource
{
  public:
    using Parser      = Parser_;

  public:
    AsyncDadaSource(key_t key_, MultiLog& log_, Parser&& parser);
    auto operator()();

  private:
    key_t key;
    MultiLog& log;
    Parser _parser;
    std::unique_ptr<AsyncDadaReadClient> client;
    bool initialize = true;
};

} // namespace psrdada_cpp

#include "psrdada_cpp/dadaflow/processors/detail/AsyncDadaSource.cpp"