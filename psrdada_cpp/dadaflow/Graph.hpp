#pragma once
#include "psrdada_cpp/dadaflow/Executor.hpp"
#include "psrdada_cpp/dadaflow/processors/InputOutputSelector.hpp"
#include <condition_variable>
#include <csignal>
#include <memory>
#include <mutex>
#include <utility>
#include <vector>

/**
 * Below we make a hashing function for std::pair<ID, ID> that be used
 * for graph manipulation.
 */
namespace std
{
template <>
struct hash<
    std::pair<psrdada_cpp::UUID::value_type, psrdada_cpp::UUID::value_type>> {
    std::size_t
    operator()(const std::pair<psrdada_cpp::UUID::value_type,
                               psrdada_cpp::UUID::value_type>& p) const noexcept
    {
        return std::hash<psrdada_cpp::UUID::value_type>{}(p.first) ^
               (std::hash<psrdada_cpp::UUID::value_type>{}(p.second) << 1);
    }
};
} // namespace std

namespace psrdada_cpp
{
/**
 * @brief A class for building and managing directed acycling processing graphs
 * 
 * @details The Graph class manages the connecting and wrapping of processing nodes
 *          and provides tools for runtime graph validation.
 *         
 */
class Graph
{
  public:

    /**
     * @brief Add a processing node to the graph with input and output queues
     * 
     * @tparam ProcessType The type of the processing node.
     * @tparam ProcessTraits The set of traits describing the callables properties.
     * @param process The instance of the processing node (will be moved).
     * @param name A user-friendly name for the node.
     */
    template <typename ProcessType, typename ProcessTraits = function_traits<ProcessType>>
    ProcessExecutor<ProcessType, ProcessTraits>& add_node(ProcessType&& process,
                                                 std::string&& name)
    {
        using ExecutorType = ProcessExecutor<ProcessType, ProcessTraits>;
        std::string name_cpy = name; // copy is needed as we move the name during emplace_back
        _nodes.emplace_back(new ExecutorType(
            std::move(process),
            std::move(name),
            [this, name = std::move(name_cpy)](std::exception const& err) {
                std::unique_lock<std::mutex> lock(_mutex);
                _node_exceptions[name] = err;
                _exception_caught      = true;
                _cv.notify_one();
            }));
        _node_map[_nodes.back()->id()] = _nodes.back().get();
        return *dynamic_cast<ExecutorType*>(_nodes.back().get());
    }

    /**
     * @brief Add a mutli-input, multi-output switch to the graph. 
     * 
     * @details The switch is itself a node of type InputOutputSelector<...>. This
     *          call just provides a convenient wrapper for adding this node type
     *          to the graph.
     * 
     * @tparam Ninputs The number of inputs to the switch 
     * @tparam Noutputs The number of outputs from the switch 
     * @tparam T The value type of the switch (will be wrapped in a shared_ptr)
     * @param name The name of the switch
     * @return auto& The switch node
     */
    template <int Ninputs, int Noutputs, typename T>
    auto& add_switch(std::string&& name)
    {
        using InputSelectorType = InputOutputSelector<Noutputs, T>;
        static_assert(Ninputs <= InputSelectorType::max_inputs,
            "Number of inputs exceeds that allowed InputOutputSelector");
        return add_node(InputSelectorType(), std::move(name));
    }

    /**
     * @brief Prepare the graph for execution.
     * 
     * @details This method performs pre-flight checks for the graph such 
     *          as checking for loops and detecting unbound or double consumed
     *          queues. Additionally this method will internally sort the graph 
     *          into topological order.
     * 
     * @note It is not required to call prepare before running the graph. As the graphs 
     *       are built at compile time but only checked at runtime, once a single runtime 
     *       execution has succeeded for a given build, this call is guaranteed to have 
     *       the same results on all runs. If start-up latency is a concern, calls to this
     *       method can be omitted. Not calling this method will result in invalid outputs
     *       from show_dependencies, make_dot_graph and other graph introspection functions.
     * 
     */
    void prepare()
    {
        // Clear previous dependencies and topological order
        _dependencies.clear();
        _in_degree.clear();
        _adj_list.clear();

        // Verify that all queues are non-null
        detect_unset_queues();

        // Verify that all queues are both sourced and sunk
        detect_unbound_queues();

        // Verify that all queues are only consumed once
        detect_double_consumed_queues();

        // Compute dependencies and initialize adjacency list and in-degree map
        initialize_graph_data();

        // Sort nodes topologically and update the _nodes vector
        sort_nodes();
    }

    /**
     * @brief Show the dependency relationships between nodes in the graph.
     * 
     */
    void show_dependencies() const
    {
        // ? Should change this to an ostream
        std::cout << "Dependencies: \n";
        for(const auto& pair: _dependencies) {
            std::cout << _node_map.at(pair.first)->name() << " --> "
                      << _node_map.at(pair.second)->name() << "\n";
        }
    }

    /**
     * @brief Make a graphviz compatible dot graph and print it to stdout.
     * 
     */
    void make_dot_graph() const
    {
        // ? Should change this to an ostream
        std::map<std::string, std::pair<std::string, std::string>> queue_map;
        for(const auto& node: _nodes) {
            for(const auto& name: node->input_queue_names()) {
                queue_map[name].second = node->name();
            }
            for(const auto& name: node->output_queue_names()) {
                queue_map[name].first = node->name();
            }
        }
        std::cout << "------------ Graphviz-----------\n";
        std::cout << "digraph G {\n";
        std::cout << "rankdir=UD;\n";
        for(const auto& [name, endpoints]: queue_map) {
            auto const& [src, dest] = endpoints;
            std::cout << "\"" << src << "\"" << " -> " << "\"" << dest << "\""
                      << " [label=" << "\"" << name << "\"" << "];\n";
        }
        std::cout << "}\n";
        std::cout << "--------------------------------\n";
    }

    /**
     * @brief Show the topological order of the nodes in the graph.
     * 
     */
    void show_topological_order() const
    {
        std::cout << "Topological Order:\n";
        bool first = true;
        for(const auto& node: _nodes) {
            if(first) {
                first = false;
            } else {
                std::cout << ", ";
            }
            std::cout << node->name();
        }
        std::cout << "\n";
    }

    /**
     * @brief Show the runtime statistics of all the nodes in the graph.
     * 
     */
    void show_statistics() const
    {
        for(auto const& node: _nodes) {
            node->show_statistics();
            std::cout << "\n";
        }
    }

    /**
     * @brief Start async execution of the graph.
     * 
     * @details This function will call the async run member fuctions on all
     *          graph nodes and then return.
     */
    void run_async()
    {
        for(auto& node: _nodes) { node->run_async(); }
    }

    /**
     * @brief Request that the graph stops.
     * 
     * @details This will set the stop status on all nodes in the graph
     *          but will not block. To wait for the graph to exit and to
     *          catch any exceptions, the await() function must be subsequently
     *          called.
     */
    void stop()
    {
        _stop_requested = true;
        _cv.notify_all();
    }

    /**
     * @brief Wait for the completion of the graph. 
     * 
     * @details This method will wait for the full completion of all nodes in the graph. 
     *          Completion may occur from a natural conclusion of processing (reaching the
     *          end of the input), from an explcit stop request or from a non-recoverable
     *          exception being raised in the graph.
     */
    void await()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock, [this] { return _exception_caught || _stop_requested; });
        if(_exception_caught) {
            for(auto const& [node, err]: _node_exceptions) {
                BOOST_LOG_TRIVIAL(error)
                    << "Exception caught from " << node << ": " << err.what();
            }
            for(auto& node: _nodes) { node->stop(); }
            throw std::runtime_error("Exception caught from graph");
        } else {
            for(auto& node: _nodes) { node->stop(); }
        }
    }

    /**
     * @brief Connect two nodes in the graph
     * 
     * @tparam src_idx The ordinal index of the output of the source node
     * @tparam dest_idx The ordinal index of the input to the destination node
     * @tparam SrcNode The type of the source node
     * @tparam DestNode The type of the destination node
     * @param src The source node
     * @param dest The destination node
     * @param name A user-friendly name for the queue
     * @param queue_depth The maximum number of elements in the queue
     */
    template <int src_idx, int dest_idx, 
              typename SrcNode, typename DestNode>
    void connect(SrcNode& src, DestNode& dest, std::string&& name, std::size_t queue_depth)
    {
        auto& src_port = src.template output_port<src_idx>();
        auto& dest_port = dest.template input_port<dest_idx>();
        src_port.template connect<ThreadSafeQueue>(dest_port, std::move(name), queue_depth);
    }

  private:
    void initialize_graph_data()
    {
        std::unordered_map<UUID::value_type, UUID::value_type>
            output_to_node_map;

        // Initialize in-degree and adjacency list
        for(const auto& node: _nodes) {
            UUID::value_type node_id = node->id();
            _in_degree[node_id]      = 0; // Initialize in-degree count
            for(const auto& output_id: node->output_queue_ids()) {
                output_to_node_map[output_id] = node_id;
            }
        }

        // Populate adjacency list and in-degree count
        for(const auto& node: _nodes) {
            UUID::value_type node_id = node->id();
            for(const auto& input_id: node->input_queue_ids()) {
                auto it = output_to_node_map.find(input_id);
                if(it != output_to_node_map.end()) {
                    // Add edge from node_id to the node that has output_id
                    _adj_list[it->second].push_back(node_id);
                    ++_in_degree[node_id]; // Increment in-degree of the target
                                           // node
                }
            }
        }

        // Store dependencies for debugging
        _dependencies.clear();
        for(const auto& [node_id, neighbors]: _adj_list) {
            for(const auto& neighbor: neighbors) {
                _dependencies.emplace(node_id, neighbor);
            }
        }
    }

    void sort_nodes()
    {
        std::queue<UUID::value_type> zero_in_degree_nodes;
        std::vector<UUID::value_type> topological_order;

        // Enqueue nodes with zero in-degree
        for(const auto& [node_id, degree]: _in_degree) {
            if(degree == 0) {
                zero_in_degree_nodes.push(node_id);
            }
        }

        while(!zero_in_degree_nodes.empty()) {
            UUID::value_type node_id = zero_in_degree_nodes.front();
            zero_in_degree_nodes.pop();
            topological_order.push_back(node_id);

            // For each node that is dependent on the current node
            for(const auto& neighbor: _adj_list[node_id]) {
                if(--_in_degree[neighbor] == 0) {
                    zero_in_degree_nodes.push(neighbor);
                }
            }
        }

        if(topological_order.size() != _nodes.size()) {
            throw std::runtime_error("Graph has a cycle!");
        } else {
            // Update the _nodes vector in topological order
            std::unordered_map<UUID::value_type,
                               std::unique_ptr<ExecutorInterface>>
                node_map;
            for(auto& node: _nodes) { node_map[node->id()] = std::move(node); }

            _nodes.clear();
            for(const auto& id: topological_order) {
                _nodes.push_back(std::move(node_map[id]));
            }
        }
    }

    void detect_unset_queues() const
    {
        for(auto const& node: _nodes) {
            if (node->has_invalid_ports()){
                std::stringstream ss;
                ss << "Node " << node->name() << " has invalid ports";
                throw std::runtime_error(ss.str());
            }
        }
    }

    void detect_unbound_queues() const
    {
        std::unordered_set<UUID::value_type> output_queues;
        std::unordered_set<UUID::value_type> input_queues;
        for(auto const& node: _nodes) {
            for(auto const& iq: node->input_queue_ids()) {
                input_queues.insert(iq);
            }
            for(auto const& oq: node->output_queue_ids()) {
                output_queues.insert(oq);
            }
        }
        if(input_queues != output_queues) {
            throw std::runtime_error("Graph contains unbound queues");
        }
    }

    void detect_double_consumed_queues() const
    {
        std::unordered_map<UUID::value_type, std::size_t> counts;
        for(auto const& node: _nodes) {
            for(auto const& iq: node->input_queue_ids()) {
                if(++counts[iq] > 1) {
                    throw std::runtime_error(
                        std::string("Queue is consumed more than once: ") +
                        std::to_string(counts[iq]));
                }
            }
        }
    }

    std::mutex _mutex;
    std::condition_variable _cv;
    bool _exception_caught = false;
    bool _stop_requested   = false;
    std::unordered_map<std::string, std::exception> _node_exceptions;
    std::vector<std::unique_ptr<ExecutorInterface>> _nodes;
    std::unordered_map<UUID::value_type, ExecutorInterface*> _node_map;
    std::unordered_set<std::pair<UUID::value_type, UUID::value_type>>
        _dependencies;
    std::unordered_map<UUID::value_type, int>
        _in_degree; // In-degree count for topological sorting
    std::unordered_map<UUID::value_type, std::vector<UUID::value_type>>
        _adj_list; // Adjacency list
};

/**
 * @brief This is a class with static members to allow for
 *        for signal handling during graph execution. It is
 *        not necessary to use this to execute graphs but doing
 *        so provides safe SIGINT handling.
 */
class GraphRunner
{
  public:
    GraphRunner() = delete;

    /**
     * @brief Run a graph with signal handling
     * 
     * @param graph A pointer to the graph to run
     */
    static void run(Graph* graph)
    {
        _graph = graph;
        _graph->run_async();
        std::signal(SIGINT, GraphRunner::stop);
        _graph->await();
        GraphRunner::reset_sigint_handler();
    }

  private:
    // Static method to stop all graphs
    static void stop(int)
    {
        if(GraphRunner::_graph) {
            GraphRunner::_graph->stop();
        }
    }
    static void reset_sigint_handler()
    {
        if(std::signal(SIGINT, SIG_DFL) == SIG_ERR) {
            std::cerr << "Failed to reset signal handler for signal " << SIGINT
                      << std::endl;
        }
    }

  private:
    // Static members
    static Graph* _graph;
};

} // namespace psrdada_cpp
