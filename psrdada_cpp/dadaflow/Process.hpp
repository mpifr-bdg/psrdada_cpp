#pragma once
#include "psrdada_cpp/dadaflow/Generator.hpp"
#include <thread>
#include <chrono>
#include <memory>
#include <vector>
#include <algorithm>
#include <tuple>
#include <iostream>

/**
This file just contains some examples of toy processes. It is not inteded to be kept
long term. Useful, reusable processes should be moved to the processors/ directory.
*/

namespace psrdada_cpp {

template <typename InputType1, typename InputType2, typename OutputType>
class ElementWiseMultiply {
public:
  using OutputPtr = std::shared_ptr<OutputType>;
  using InputPtr1 = std::shared_ptr<InputType1>;
  using InputPtr2 = std::shared_ptr<InputType2>;
  using OutputTuple = std::tuple<OutputPtr>;

  Generator<OutputTuple> operator()(InputPtr1 a, InputPtr2 b) {
    auto const &vec1 = *a;
    auto const &vec2 = *b;
    if (vec1.size() != vec2.size()) {
      throw std::runtime_error("Vector lengths do not match");
    }
    auto output_ptr = std::make_shared<OutputType>(vec1.size());
    std::transform(vec1.begin(), vec1.end(), vec2.begin(), output_ptr->begin(),
                   std::multiplies<>());
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    co_yield std::make_tuple(output_ptr);
    co_return;
  }
};

template <int N, typename T>
class RateIncreaser
{
public:
    using InputPtr = std::shared_ptr<T>;
    using Output = std::tuple<InputPtr>;

    Generator<std::optional<Output>> operator()(InputPtr input) {
      for (int ii = 0; ii < N; ++ii)
      {
        co_yield std::optional<Output>({input});
      }
      co_return;
    }
};

template <int N, typename T>
class RateReducer
{
public:
    using InputPtr = std::shared_ptr<T>;
    using Output = std::tuple<InputPtr>;

    Generator<std::optional<Output>> operator()(InputPtr input) {
      ++count;
      if (count % N == 0)
      {
        count = 0;
        co_yield std::make_tuple(input);
      }
      co_return;
    }

private:
  int count = 0;
};

} // namespace psrdada_cpp