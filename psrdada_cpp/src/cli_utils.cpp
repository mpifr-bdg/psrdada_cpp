#include <algorithm>

#include "psrdada_cpp/cli_utils.hpp"

namespace psrdada_cpp {

key_t string_to_key(std::string const& in)
{
    key_t key;
    std::stringstream converter;
    converter << std::hex << in;
    converter >> key;
    return key;
}


void init_log_level()
{
  char * val = getenv("LOG_LEVEL");
  if ( val )
  {
    set_log_level(val);
  }
  else
  {
    set_log_level("info");
  }
}


void set_log_level(std::string level)
{
    std::transform(level.begin(), level.end(), level.begin(), [](unsigned char c){ return std::tolower(c); });

    using namespace boost::log;
    if (level == "debug")
    {
        core::get()->set_filter(trivial::severity >= trivial::debug);
    }
    else if (level == "info")
    {
        core::get()->set_filter(trivial::severity >= trivial::info);
    }
    else if (level == "warning")
    {
        core::get()->set_filter(trivial::severity >= trivial::warning);
    }
    else if (level == "error")
    {
        core::get()->set_filter(trivial::severity >= trivial::error);
    }
    else
    {
        throw std::runtime_error("Unknown log level: " + level);
    }
}
} //namespace
