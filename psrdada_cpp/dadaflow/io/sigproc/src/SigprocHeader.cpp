#include "psrdada_cpp/dadaflow/io/sigproc/SigprocHeader.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <iterator>
#include <string>
#include <ostream>
#include <iostream>
#include <iomanip>
#include <fstream>

namespace psrdada_cpp {
namespace sigproc {

void SigprocHeader::header_write(std::ostream& stream, std::string const& str) const
{
    int len = str.size();
    stream.write(reinterpret_cast<char*>(&len), sizeof(int));
    stream << str;
}

void SigprocHeader::header_write(std::ostream& stream,
                                 std::string const& str,
                                 std::string const& val) const
{
    header_write(stream, str);
    header_write(stream, val);
}

std::string SigprocHeader::read_string(std::istream& stream) const {
    int key_length;
    stream.read(reinterpret_cast<char*>(&key_length), sizeof(int));
    if (!stream) {
        throw std::runtime_error("Failed to read value from stream. Stream may "
                                    "have reached EOF or encountered an error.");
    }
    if (key_length <= 0 || key_length > 80) {
        throw std::runtime_error("Unexpected key length on sigproc header parse: " 
            + std::to_string(key_length));
    }
    std::string key;
    key.resize(key_length);
    stream.read(key.data(), key_length * sizeof(char));
    return key;
}

std::size_t SigprocHeader::to_stream(std::ostream& stream) const
{
    auto stream_start = stream.tellp();
    header_write(stream, "HEADER_START");
    header_write(stream, "source_name", _source_name);
    header_write<double>(stream, "src_raj", _src_raj);
    header_write<double>(stream, "src_dej", _src_dej);
    header_write<std::uint32_t>(stream, "telescope_id", _telescope_id);
    header_write<std::uint32_t>(stream, "machine_id", _machine_id);
    header_write<std::uint32_t>(stream, "data_type", _data_type);
    header_write<std::uint32_t>(stream, "nbits", _nbits);
    header_write<std::uint32_t>(stream, "nifs", _nifs); 
    header_write<std::uint32_t>(stream, "nchans", _nchans);
    header_write<double>(stream, "fch1", _fch1); 
    header_write<double>(stream, "foff", _foff);
    header_write<double>(stream, "tstart", _tstart);
    header_write<double>(stream, "tsamp", _tsamp);
    if (_barycentric.has_value()) {
        header_write<std::uint32_t>(stream, "barycentric", _barycentric.value()); 
    }
    if (_pulsarcentric.has_value()) { 
        header_write<std::uint32_t>(stream, "pulsarcentric", _pulsarcentric.value() ); 
    } 
    if (_rawfile.has_value()) {
        header_write(stream, "rawdatafile", _rawfile.value());
    }
    if (_za_start.has_value()) {
        header_write<double>(stream, "za_start", _za_start.value());
    }
    if (_az_start.has_value()) {
        header_write<double>(stream, "az_start", _az_start.value());
    }
    if (_is_signed.has_value()) {
        header_write<char>(stream, "signed", _is_signed.value());
    }
    if (_nbeams.has_value()) {
        header_write<std::uint32_t>(stream, "nbeams", _nbeams.value());
    }
    if (_ibeam.has_value()) {
        header_write<std::uint32_t>(stream, "ibeam", _ibeam.value());
    }
    if (_refdm.has_value()) {
        header_write<double>(stream, "refdm", _refdm.value());
    }
    header_write(stream, "HEADER_END");
    auto offset = stream.tellp() - stream_start;
    if (offset >= 0) { 
        return static_cast<std::size_t>(offset);
    } else {
        throw std::runtime_error("Negative header size calculated");
    }
}

std::size_t SigprocHeader::from_stream(std::istream& stream) {
    auto stream_start = stream.tellg();
    std::string key = read_string(stream);
    if (key != "HEADER_START") {
        throw std::runtime_error("HEADER_START not found");
    }
    while (true)
    {
        key = read_string(stream);
        if( key == "HEADER_END" ) break;
        else if( key == "source_name" )   _source_name = read_string(stream);
        else if( key == "rawdatafile")    _rawfile = read_string(stream);
        else if( key == "az_start" )      _az_start = read_value<double>(stream);
        else if( key == "za_start" )      _za_start = read_value<double>(stream);
        else if( key == "src_raj" )       _src_raj = read_value<double>(stream);
        else if( key == "src_dej" )       _src_dej = read_value<double>(stream);
        else if( key == "tstart" )        _tstart = read_value<double>(stream);
        else if( key == "tsamp" )         _tsamp = read_value<double>(stream);
        else if( key == "fch1" )          _fch1 = read_value<double>(stream);
        else if( key == "foff" )          _foff = read_value<double>(stream);
        else if( key == "nchans" )        _nchans = read_value<uint32_t>(stream);
        else if( key == "telescope_id" )  _telescope_id = read_value<uint32_t>(stream);
        else if( key == "machine_id" )    _machine_id = read_value<uint32_t>(stream);
        else if( key == "data_type" )     _data_type = read_value<uint32_t>(stream);
        else if( key == "ibeam" )         _ibeam = read_value<uint32_t>(stream);
        else if( key == "nbeams" )        _nbeams = read_value<uint32_t>(stream);
        else if( key == "nbits" )         _nbits = read_value<uint32_t>(stream);
        else if( key == "barycentric" )   _barycentric = read_value<uint32_t>(stream);
        else if( key == "pulsarcentric" ) _pulsarcentric = read_value<uint32_t>(stream);
        else if( key == "nifs" )          _nifs = read_value<uint32_t>(stream);
        else if( key == "refdm" )         _refdm = read_value<double>(stream);
        else if( key == "signed" )        _is_signed = read_value<uint8_t>(stream);
        else {
            BOOST_LOG_TRIVIAL(warning) << "Unknown header key: " 
                                        << key << ", assuming uint32_t";
            read_value<uint32_t>(stream);
        }
    } 
    auto offset = stream.tellg() - stream_start;
    if (offset >= 0) { 
        return static_cast<std::size_t>(offset);
    } else {
        throw std::runtime_error("Negative header size calculated");
    }
}

std::ostream& operator<<(std::ostream& os, const SigprocHeader& header)
{
    auto print_optional = [](
        std::ostream& os, const std::optional<auto>& opt) 
        -> std::ostream& {
        using T = typename std::decay_t<decltype(opt)>::value_type;
        if (opt.has_value()) {
            if constexpr(std::is_same_v<T, uint8_t>){
                return os << static_cast<int>(opt.value());
            } else {
                return os << opt.value();
            }
        } else {
            return os << "(unset)";
        }
    };
    os << "SigprocHeader:\n";
    os << "  Source Name: " << header.source_name() << "\n";
    os << "  Source RA (J2000): " << header.src_raj() << "\n";
    os << "  Source DEC (J2000): " << header.src_dej() << "\n";
    os << "  Frequency of First Channel (MHz): " << header.fch1() << "\n";
    os << "  Channel Bandwidth (MHz): " << header.foff() << "\n";
    os << "  Sampling Time (s): " << header.tsamp() << "\n";
    os << "  Observation Start Time (MJD): " << header.tstart() << "\n";
    os << "  Data Type ID: " << header.data_type()
       << " (" << data_types.get_value(header.data_type()) << ")" "\n";
    os << "  Machine ID: " << header.machine_id() 
       << " (" << backends.get_value(header.machine_id()) << ")" "\n";
    os << "  Number of Bits per Sample: " << header.nbits() << "\n";
    os << "  Number of Frequency Channels: " << header.nchans() << "\n";
    os << "  Number of IFs (Polarizations): " << header.nifs() << "\n";
    os << "  Telescope ID: " << header.telescope_id()
       << " (" << telescopes.get_value(header.telescope_id()) << ")" "\n";
    os << "  Raw File: "; print_optional(os, header.rawfile()) << "\n";
    os << "  Azimuth Start (deg): "; print_optional(os, header.az_start()) << "\n";
    os << "  Zenith Start (deg): "; print_optional(os, header.za_start()) << "\n";
    os << "  Reference DM: "; print_optional(os, header.refdm()) << "\n";
    os << "  Barycentric Flag: "; print_optional(os, header.barycentric()) << "\n";
    os << "  Pulsarcentric Flag: "; print_optional(os, header.pulsarcentric()) << "\n";
    os << "  Beam Number: "; print_optional(os, header.ibeam()) << "\n";
    os << "  Number of Beams: "; print_optional(os, header.nbeams()) << "\n";
    os << "  Is signed: "; print_optional(os, header.is_signed()) << "\n";

    return os;
}

} // namespace sigproc
} // namespace psrdada_cpp
