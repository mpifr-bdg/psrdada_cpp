#pragma once
#include "psrdada_cpp/dadaflow/IPCBuffer.hpp"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/ResourceAllocator.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <type_traits>
#include <memory>
#include <optional>
#include <thread>
#include <functional>
#include <algorithm>
#include <chrono>

using namespace std::chrono_literals;

namespace psrdada_cpp {

template <typename HeaderType, typename DataType>
class IPCBufferReader;

template <typename HeaderType, typename ContainerType, typename... Dimensions>
requires (std::is_trivial_v<HeaderType> 
          && std::is_standard_layout_v<HeaderType> 
          && std::is_trivial_v<typename ContainerType::value_type>
          && std::is_standard_layout_v<typename ContainerType::value_type>
          && Resizeable<ContainerType>)
class IPCBufferReader<HeaderType, DescribedData<ContainerType, Dimensions...>>
{
public:
  using DataType = DescribedData<ContainerType, Dimensions...>;
  using ValueType = typename ContainerType::value_type;
  using ExtentsType = std::array<std::size_t, sizeof...(Dimensions)>;
  using AllocatorInterface = ResourceAllocatorInterface<DataType>;
  using WaitType = std::chrono::duration<double>;
  using OnReadCallback = std::function<void(HeaderType const& header, DataType& data)>;

  IPCBufferReader(std::string_view key, ExtentsType const& extents, 
                  OnReadCallback&& callback, WaitType wait = 1s)
  : _extents(extents), _callback(std::move(callback)), _wait(wait) {
    _nelements = std::accumulate(_extents.begin(), _extents.end(), 1, std::multiplies<>());
    std::size_t nbytes = _nelements * sizeof(ValueType);
    _shm_buffer = std::make_unique<IPCBuffer>(key, nbytes);
    _allocator = std::make_unique<DefaultAllocator<DataType>>();
  }

  void set_allocator(std::unique_ptr<AllocatorInterface>&& allocator) {
    _allocator = std::move(allocator);
  }
  
  std::optional<std::shared_ptr<DataType>> operator()() {
    if (!_shm_buffer->has_update()) {
      std::this_thread::sleep_for(_wait);
      return std::nullopt;
    } else {
      char *ptr = _shm_buffer->data();
      HeaderType* header = reinterpret_cast<HeaderType*>(ptr);
      ValueType* data = reinterpret_cast<ValueType*>(ptr + sizeof(HeaderType));
      auto output_ptr = _allocator->get();
      auto& output = *output_ptr;
      output.resize(_extents);
      std::copy(data, data + _nelements, output.begin());
      _callback(*header, output);
      return output_ptr;
    }
  }

private:
  ExtentsType _extents;
  OnReadCallback _callback;
  WaitType _wait;
  std::unique_ptr<AllocatorInterface> _allocator;
  std::size_t _nelements;
  std::unique_ptr<IPCBuffer> _shm_buffer;
};

} // namespace psrdada_cpp