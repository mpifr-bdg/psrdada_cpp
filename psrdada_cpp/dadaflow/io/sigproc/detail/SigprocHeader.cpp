#include "psrdada_cpp/dadaflow/io/sigproc/SigprocHeader.hpp"

namespace psrdada_cpp {
namespace sigproc {

template <typename T>
void SigprocHeader::header_write(std::ostream& stream,
                                 std::string const& name,
                                 T val) const
{
    header_write(stream, name);
    stream.write(reinterpret_cast<char*>(&val), sizeof(val));
}

template <typename T>
T SigprocHeader::read_value(std::istream& stream) const {
    T value{};
    stream.read(reinterpret_cast<char*>(&value), sizeof(T));
    if (!stream) {
        throw std::runtime_error("Failed to read value from stream. Stream may "
                                    "have reached EOF or encountered an error.");
    }
    return value;
}

} // namespace sigproc
} // namespace psrdada_cpp
