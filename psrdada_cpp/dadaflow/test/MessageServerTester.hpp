#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "psrdada_cpp/dadaflow/MessageQueue.hpp"
#include "psrdada_cpp/dadaflow/ThreadSafeQueue.hpp"
#include <thread>
#include <string>

using namespace std::chrono_literals;
using namespace testing;
using ::testing::Return;
using ::testing::Throw;
using ::testing::_;
using ::testing::Invoke;


namespace psrdada_cpp {
namespace test {

class MockQueue
{
  public:
    MockQueue()
    :input_queue("input", 1)
    , output_queue("output", 1)  
    {}

    void send(const std::string& msg){
        std::cout << "Send: " << msg << "\n";
        ++send_count;
        output_queue.push(std::string{msg});
    }

    std::string recv(float) {
        if (input_queue.empty()) {
            std::this_thread::sleep_for(1ms);
            throw TimeoutException("Timeout");
        }
        ++recv_count;
        return input_queue.pop();
    }

    ThreadSafeQueue<std::string> input_queue;
    ThreadSafeQueue<std::string> output_queue;
    std::size_t send_count = 0;
    std::size_t recv_count = 0;
};

class MessageServerTester : public ::testing::Test
{
  protected:
    MockQueue request_queue;
    MockQueue response_queue;
    MessageServer<MockQueue> server;

    MessageServerTester() : server(request_queue, response_queue) {}

    void SetUp() override
    {
        
    }

    void TearDown() override
    {
        // Clean-up code (if necessary)
    }
};

} // namespace test
} // namespace psrdada_cpp