#pragma once
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/ResourceAllocator.hpp"
#include "psrdada_cpp/common.hpp"
#include <memory>


namespace psrdada_cpp {

template <typename InputT, typename OutputT>
class Transposer;

template <template <typename, typename...> class DD, 
          typename InputContainer, 
          typename OutputContainer,
          typename T0,
          typename A,
          typename P,
          typename F,
          typename T1>
class Transposer< DD<InputContainer, T0, A, P, F, T1>, 
                  DD<OutputContainer, T0, T1, A, F, P>>
{
public:
    using InputType = DD<InputContainer, T0, A, P, F, T1>;
    using OutputType = DD<OutputContainer, T0, T1, A, F, P>;
    using InputPtr = std::shared_ptr<InputType>;
    using OutputPtr = std::shared_ptr<OutputType>;
    using Allocator = std::shared_ptr<ResourceAllocatorInterface<OutputType>>;

    Transposer(Allocator const& allocator, std::size_t nthreads = 1)
    : _allocator(allocator), _nthreads(nthreads)
    {}

    Transposer(Transposer const&) = delete;
    Transposer& operator=(Transposer const&) = delete;
    Transposer(Transposer&&) = default;
    Transposer& operator=(Transposer&&) = default;

    OutputPtr operator()(InputPtr input_ptr)
    {
        OutputPtr output_ptr = _allocator->get();

        auto const& input = *input_ptr;
        auto& output = *output_ptr;

        // aliases for sizes
        const std::size_t t0 = input.T0::nsamples();
        const std::size_t t1 = input.T1::nsamples();
        const std::size_t f = input.nchannels();
        const std::size_t a = input.nantennas();
        const std::size_t p = input.npol();
        const std::size_t fp = f * p;
        const std::size_t afp = a * fp;
        output.resize({a, t0, t1, f, p});
        output.metalike(input);

        //ATTFP

        // We are transposing TAPFT to TTAFP
        // Outer loop is over the outer T
        #pragma omp parallel for num_threads(_nthreads)
        for (std::size_t t0_i = 0; t0_i < t0; ++t0_i)
        {
            // Here we have to transpose APFT -> TAFP
            const std::size_t offset = t0_i * t1 * afp;
            std::size_t input_idx = 0;
            for (std::size_t a_i = 0; a_i < a; ++a_i) {   
                const std::size_t a_offset = a_i * fp;
                for (std::size_t p_i = 0; p_i < p; ++p_i) {
                    for (std::size_t f_i = 0; f_i < f; ++f_i) {
                        const std::size_t f_offset = f_i * p;
                        for (std::size_t t1_i = 0; t1_i < t1; ++t1_i) {
                            const std::size_t output_idx = t1_i * afp + a_offset + f_offset + p_i;
                            output[offset + output_idx] = input[offset + input_idx];
                            ++input_idx;
                        }
                    }
                }
            }
        }
        return output_ptr;
    }

private:
    Allocator _allocator;
    std::size_t _nthreads;
};

}