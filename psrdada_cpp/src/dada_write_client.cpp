#include "psrdada_cpp/dada_write_client.hpp"

namespace psrdada_cpp {

    DadaWriteClient::DadaWriteClient(key_t key, MultiLog& log)
    : DadaClientBase(key, log)
    , _locked(false)
    , _header_stream(*this)
    , _data_stream(*this)
    {
        lock();
    }

    DadaWriteClient::~DadaWriteClient()
    {
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Destructing DadaWriteClient";
        if(_connected && _locked)
        {
            release();
        }
    }

    void DadaWriteClient::lock()
    {
        if (!_connected)
        {
            throw std::runtime_error("Lock requested on unconnected HDU\n");
        }
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Acquiring writing lock on dada buffer";
        if (dada_hdu_lock_write (_hdu) < 0)
        {
            _log.write(LOG_ERR, "open_hdu: could not lock write\n");
            throw std::runtime_error(std::string("Error locking HDU with key: ")
                + std::to_string(_key));
        }
        _locked = true;
    }

    void DadaWriteClient::release()
    {
        if (!_locked)
        {
            throw std::runtime_error("Release requested on unlocked HDU\n");
        }
        BOOST_LOG_TRIVIAL(info) << this->id() << "Releasing writing lock on dada buffer";
        if (dada_hdu_unlock_write (_hdu) < 0)
        {
            _log.write(LOG_ERR, "open_hdu: could not release write\n");
            throw std::runtime_error(std::string("Error releasing HDU with key: ")
                + std::to_string(_key));
        }
        _locked = false;
    }

    void DadaWriteClient::connect()
    {
        DadaClientBase::connect();
        lock();
    }

    void DadaWriteClient::disconnect()
    {
        if(_data_stream.has_block())
        {
            BOOST_LOG_TRIVIAL(warning) << "Disconnecting while data block is acquired but not released, releasing it now";
            _data_stream.release();
        }
        if(_header_stream.has_block())
        {
            BOOST_LOG_TRIVIAL(warning) << "Disconnecting while header block is acquired but not released, releasing it now";
            _header_stream.release();
        }
        if(_locked)
        {
            release();
        }
        DadaClientBase::disconnect();
    }

    DadaWriteClient::HeaderStream& DadaWriteClient::header_stream()
    {
        return _header_stream;
    }

    DadaWriteClient::DataStream& DadaWriteClient::data_stream()
    {
        return _data_stream;
    }

    DadaWriteClient::HeaderStream::HeaderStream(DadaWriteClient& parent)
    : _parent(parent)
    , _current_block(nullptr)
    {
    }

    DadaWriteClient::HeaderStream::~HeaderStream()
    {
    }

    RawBytes& DadaWriteClient::HeaderStream::next()
    {
        if (_current_block)
        {
            throw std::runtime_error("Previous header block not released");
        }
        BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Acquiring next header block";
        // Proof if the buffer is writeable 
        // if the buffer is full it is not writeable leading to block on 'ipcio_open_block_write'-call 
        while(true)
        {
            if(_parent._stop.load())
            {
                throw DadaInterruptException("Interrupt called in DadaWriteClient::DataStream::next()");
            }
            if(_parent.data_buffer_is_full())
            {
                usleep(1000);
            }
            else
            {
                char* tmp = ipcbuf_get_next_write(_parent._hdu->header_block);
                _current_block.reset(new RawBytes(tmp, _parent.header_buffer_size()));
                break;
            }
        }
        return *_current_block;
    }

    void  DadaWriteClient::HeaderStream::release()
    {
        if (!_current_block)
        {
            throw std::runtime_error("No header block to be released");
        }
        BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Writing header content:\n" << _current_block->ptr();
        BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Header bytes used " << _current_block->used_bytes();
        BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Releasing header block";
        if (ipcbuf_mark_filled(_parent._hdu->header_block, _current_block->used_bytes()) < 0)
        {
            _parent._log.write(LOG_ERR, "Could not mark filled header block\n");
            throw std::runtime_error("Could not mark filled header block");
        }
        _current_block.reset();
    }

    bool DadaWriteClient::HeaderStream::has_block() const
    {
        return bool(_current_block);
    }


    DadaWriteClient::DataStream::DataStream(DadaWriteClient& parent)
    : _parent(parent)
    , _current_block(nullptr)
    , _block_idx(0)
    {
    }

    DadaWriteClient::DataStream::~DataStream()
    {
    }

    RawBytes& DadaWriteClient::DataStream::next()
    {
        if (_current_block)
        {
            throw std::runtime_error("Previous data block not released");
        }
        BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Acquiring next data block";
        // Proof if the buffer is writeable 
        // if the buffer is full it is not writeable leading to block on 'ipcio_open_block_write'-call 
        while(true)
        {
            if(_parent._stop.load())
            {
                throw DadaInterruptException("Interrupt called in DadaWriteClient::DataStream::next()");
            }
            if(_parent.data_buffer_is_full())
            {
                usleep(100); // With very small buffers and high rates this might not be suffcient
            }
            else
            {
                char* tmp = ipcio_open_block_write(_parent._hdu->data_block, &_block_idx);
                _current_block.reset(new RawBytes(tmp, _parent.data_buffer_size()));
                BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Acquired data block " << _block_idx;
                BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Free data slots: " << _parent.data_buffer_free_slots();
                break;
            }
        }
        return *_current_block;
    }

    void DadaWriteClient::DataStream::release(bool eod)
    {
        if (!_current_block)
        {
             throw std::runtime_error("No data block to be released");
        }
        BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Releasing data block";
        if (eod)
        {
            BOOST_LOG_TRIVIAL(debug) << _parent.id() << "Setting EOD markers";
            if (ipcio_update_block_write (_parent._hdu->data_block, _current_block->used_bytes()) < 0)
            {
                _parent._log.write(LOG_ERR, "close_buffer: ipcio_update_block_write failed\n");
                throw std::runtime_error("Could not close ipcio data block");
            }
        }
        else
        {
            if (ipcio_close_block_write (_parent._hdu->data_block, _current_block->used_bytes()) < 0)
            {
                _parent._log.write(LOG_ERR, "close_buffer: ipcio_close_block_write failed\n");
                throw std::runtime_error("Could not close ipcio data block");
            }
            _current_block.reset();
        }
    }
    
    bool DadaWriteClient::DataStream::has_block() const
    {
        return bool(_current_block);
    }

    RawBytes& DadaWriteClient::DataStream::current_block()
    {
        if (!has_block()){
            throw std::runtime_error("No current block!");
        }
        return *_current_block;
    }

    void DadaWriteClient::reset()
    {
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Resetting dada buffer";
        if(_connected)
        {
            if(reset_buffer((ipcbuf_t *) _hdu->data_block))
            {
                _log.write(LOG_ERR, "Unable to reset data block\n");
                throw std::runtime_error(std::string("Unable to reset data block")
                    + std::to_string(_key));
            }
            if(reset_buffer((ipcbuf_t *) _hdu->header_block))
            {
                _log.write(LOG_ERR, "Unable to reset header block\n");
                throw std::runtime_error(std::string("Unable to reset header block")
                    + std::to_string(_key));
            }
        }
        else
        {
            _log.write(LOG_ERR, "Cannot reset buffer when client is not connected\n");
            throw std::runtime_error(std::string("Cannot reset buffer when client is not connected")
                + std::to_string(_key));
        }
        _stop.store(false);
        BOOST_LOG_TRIVIAL(debug) << this->id() << "dada buffer resetted";
    }

    int DadaWriteClient::reset_buffer(ipcbuf_t* id)
    {
        uint64_t ibuf = 0;
        unsigned int iread = 0;
        uint64_t nbufs = ipcbuf_get_nbufs (id);
        ipcsync_t* sync = id->sync;

        /* if the reader has reached end of data, reset the state */
        if (id->state == IPCBUF_RSTOP)
        {
            id->state = IPCBUF_READER;
            return 0;
        }

        /* otherwise, must be the designated writer */
        if (!ipcbuf_is_writer(id))  {
            _log.write(LOG_ERR, "ipcbuf_reset: invalid state");
            return -1;
        }

        // TODO AJ check this logic
        if (sync->w_buf_curr == 0 && sync->w_buf_next == 0)
            return 0;

        for (ibuf = 0; ibuf < nbufs; ibuf++)
        {
            while (id->count[ibuf])
            {
                for (iread = 0; iread < sync->n_readers; iread++)
                {
                    /* decrement the buffers cleared semaphore */
                    if (ipc_semop (id->semid_data[iread], IPCBUF_CLEAR, -1, 0) < 0)
                    {
                        _log.write(LOG_ERR, "ipcbuf_reset: error decrementing CLEAR\n");
                        return -1;
                    }
                }
                id->count[ibuf] --;
            }
        }

        for (iread = 0; iread < sync->n_readers; iread++)
        {
            if (ipc_semop (id->semid_data[iread], IPCBUF_SODACK, -IPCBUF_XFERS, 0) < 0)
            {
                _log.write(LOG_ERR, "ipcbuf_reset: error decrementing SODACK\n");
                return -1;
            }
            if (ipc_semop (id->semid_data[iread], IPCBUF_EODACK, -IPCBUF_XFERS, 0) < 0)
            {
                _log.write(LOG_ERR, "ipcbuf_reset: error decrementing EODACK\n");
                return -1;
            }
            if (ipc_semop (id->semid_data[iread], IPCBUF_SODACK, IPCBUF_XFERS, 0) < 0)
            {
                _log.write(LOG_ERR, "ipcbuf_reset: error resetting SODACK\n");
                return -1;
            }
            if (ipc_semop (id->semid_data[iread], IPCBUF_EODACK, IPCBUF_XFERS, 0) < 0)
            {
                _log.write(LOG_ERR, "ipcbuf_reset: error resetting EODACK\n");
                return -1;
            }
            sync->r_bufs[iread] = 0;
            sync->r_xfers[iread] = 0;
        }
        sync->w_buf_curr = 0;
        sync->w_buf_next = 0;
        sync->w_xfer = 0;

        return 0;
    }

    std::size_t DadaWriteClient::DataStream::block_idx() const
    {
        return _block_idx;
    }

} //namespace psrdada_cpp





