#pragma once
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <optional>
#include <queue>
#include <string>

namespace psrdada_cpp
{

/**
 * @brief A common base class for queues.
 *
 * @details Used for type erasure when managing queues.
 */
class QueueBase
{
  public:
    /**
     * @brief Construct a new QueueBase object
     *
     * @param name A user-friendly name for the queue.
     */
    QueueBase(std::string name): _id(UUID::next())
    {
        std::stringstream full_name;
        full_name << name << " [" << std::to_string(_id) << "]";
        _name = full_name.str();
    }

    virtual ~QueueBase() = default;

    /**
     * @brief Return a unique ID for this queue.
     *
     * @return UUID::value_type
     */
    UUID::value_type id() const { return _id; }

    /**
     * @brief Return the name of the queue.
     *
     * @return std::string const&
     */
    std::string const& name() const { return _name; }

  private:
    UUID::value_type _id;
    std::string _name;
};

/**
 * @brief An intermediate baseclass for queues that preserves their value type
 * 
 * @tparam T The type of the elements in the queue
 * 
 * @note See ThreadSafeQueue for member documentation.
 */
template<typename T>
class QueueBaseT: public QueueBase
{
public:
    using QueueBase::QueueBase;
    using value_type = T;
    virtual void push(T&& value) = 0;
    virtual void push_noblock(T&& value) = 0;
    virtual T pop() = 0;
    virtual std::optional<T> peek() = 0;
    virtual bool empty() const = 0;
    virtual void flush() = 0;
    virtual size_t size() const = 0;
    virtual size_t pushed_count() const = 0;
    virtual size_t popped_count() const = 0;
    virtual double push_rate() const = 0;
    virtual double pop_rate() const = 0;
};

/**
 * @brief A thread-safe queue.
 *
 * @tparam T The type of the elements stored in the queue.
 */
template <typename T>
class ThreadSafeQueue: public QueueBaseT<T>
{
  public:
    using clock      = std::chrono::steady_clock;

    /**
     * @brief Construct a new ThreadSafeQueue object
     *
     * @param name A user-friendly name for the queue.
     * @param max_size The maximum number of elements in the queue.
     *
     * @note Pushes to the queue will block when the maximum number of
     *       elements is reached.
     */
    explicit ThreadSafeQueue(std::string name,
                             std::optional<size_t> max_size = std::nullopt)
        : QueueBaseT<T>(name), _max_size(max_size), _items_pushed(0),
          _items_popped(0), _start_time(clock::now())
    {
    }

    /**
     * @brief Push an element to the queue
     *
     * @param value The element to be pushed to the queue
     *
     * @note The call will block until there is space in the
     *       queue to put the element.
     */
    void push(T&& value) override
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cond_push.wait(lock, [this] {
            return !_max_size || _queue.size() < _max_size;
        });
        _queue.push(std::forward<T>(value));
        ++_items_pushed;
        _cond_pop.notify_one();
    }

    /**
     * @brief Push an element to the queue without blocking
     *
     * @param value The element to be pushed to the queue
     *
     * @note This method may increase the queue to larger than
     *       its maximum size.
     */
    void push_noblock(T&& value) override
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _queue.push(std::forward<T>(value));
        ++_items_pushed;
        _cond_pop.notify_one();
    }

    /**
     * @brief Pops the front element off the queue and returns it.
     *
     * @return T The front element of the queue.
     *
     * @note This call will block until there is an element to pop.
     */
    T pop() override
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cond_pop.wait(lock, [this] { return !_queue.empty(); });
        T value = std::move(_queue.front());
        _queue.pop();
        ++_items_popped;
        _cond_push.notify_one();
        // Should result in NVRO.
        return value;
    }

    /**
     * @brief Take a peek at the first element in the queue.
     *
     * @return std::optional<T>
     *
     * @note This is a non-blocking, but thread-safe call. If the
     *       queue is empty the returned optional will not have a
     *       value.
     */
    std::optional<T> peek() override
    {
        std::unique_lock<std::mutex> lock(_mutex);
        return _queue.empty() ? std::nullopt : std::optional<T>{_queue.front()};
    }

    /**
     * @brief Check if the queue is empty.
     *
     * @return true   The queue is empty.
     * @return false  The queue is not empty.
     */
    bool empty() const override
    {
        std::lock_guard<std::mutex> lock(_mutex);
        return _queue.empty();
    }

    void flush() override {
        std::unique_lock<std::mutex> lock(_mutex);
        while (!_queue.empty()) {
            _queue.pop();
            ++_items_popped;
        }
        _cond_push.notify_one();
    }

    /**
     * @brief Return the number of elements in the queue.
     *
     * @return size_t The number of queue elements.
     */
    size_t size() const override
    {
        std::lock_guard<std::mutex> lock(_mutex);
        return _queue.size();
    }

    /**
     * @brief Returns the number of items pushed into the queue.
     *
     * @return size_t
     */
    size_t pushed_count() const override { return _items_pushed; }

    /**
     * @brief Returns the number of items popped out of the queue.
     *
     * @return size_t
     */
    size_t popped_count() const override { return _items_popped; }

    /**
     * @brief Returns the rate of items being pushed into the queue per second.
     *
     * @return double
     */
    double push_rate() const override
    {
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(
                            clock::now() - _start_time)
                            .count();
        return duration > 0 ? static_cast<double>(_items_pushed) / duration
                            : 0.0;
    }

    /**
     * @brief Returns the rate of items being popped out of the queue per
     * second.
     *
     * @return double
     */
    double pop_rate() const override
    {
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(
                            clock::now() - _start_time)
                            .count();
        return duration > 0 ? static_cast<double>(_items_popped) / duration
                            : 0.0;
    }

  private:
    UUID::value_type _id;
    mutable std::mutex _mutex;
    std::queue<T> _queue;
    std::condition_variable _cond_push;
    std::condition_variable _cond_pop;
    std::optional<size_t> _max_size;
    size_t _items_pushed;
    size_t _items_popped;
    clock::time_point _start_time;
};

} // namespace psrdada_cpp