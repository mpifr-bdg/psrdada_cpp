#include "psrdada_cpp/dadaflow/Graph.hpp"
#include "psrdada_cpp/dadaflow/processors/InputOutputSelector.hpp"
#include "psrdada_cpp/dadaflow/Executor.hpp"
#include <iostream>
#include <thread>
#include <memory>
#include <chrono>
#include <mutex>
#include <optional>

using namespace psrdada_cpp;
using namespace std::chrono_literals;

int main()
{
    using T = int;
    using Tptr = std::shared_ptr<T>;
    std::mutex writer_mutex;
    Graph graph;


    auto& input0 = graph.add_node([]() ->Tptr {
        std::this_thread::sleep_for(500ms);
        return std::make_shared<T>(0);
    }, "input0");

    auto& input1 = graph.add_node([]() ->Tptr {
        std::this_thread::sleep_for(500ms);
        return std::make_shared<T>(1);
    }, "input1");

    auto& input2 = graph.add_node([]() ->Tptr {
        std::this_thread::sleep_for(500ms);
        return std::make_shared<T>(2);
    }, "input2");

    auto& output0 = graph.add_node([&](Tptr input) ->void {
        std::unique_lock<std::mutex> lock(writer_mutex);
        BOOST_LOG_TRIVIAL(debug) << "Received " << *input << " in output channel 0";
    }, "output0");

    auto& output1 = graph.add_node([&](Tptr input) ->void {
        std::unique_lock<std::mutex> lock(writer_mutex);
        BOOST_LOG_TRIVIAL(debug) << "Received " << *input << " in output channel 1";
    }, "output_");

    auto& selector = graph.add_switch<3, 2, T>("selector");
    graph.connect<0, 0>(input0, selector, "i0->switch", 3);
    graph.connect<0, 1>(input1, selector, "i1->switch", 3);
    graph.connect<0, 2>(input2, selector, "i2->switch", 3);
    graph.connect<0, 0>(selector, output0, "switch->o0", 3);
    graph.connect<1, 0>(selector, output1, "switch->o1", 3);
    selector.select_inputs<0>();
    selector.select_outputs<0>();
    graph.prepare();  

    std::jthread switcher(
        [&](){
            std::this_thread::sleep_for(1s);
            BOOST_LOG_TRIVIAL(debug) << "Switching to input 1";
            selector.select_inputs<1>();

            std::this_thread::sleep_for(1s);
            BOOST_LOG_TRIVIAL(debug) << "Switching to output 1";
            selector.select_outputs<1>();

            std::this_thread::sleep_for(1s);
            BOOST_LOG_TRIVIAL(debug) << "Switching to inputs 0, 1, 2";
            selector.select_inputs<0, 1, 2>();

            std::this_thread::sleep_for(1s);
            BOOST_LOG_TRIVIAL(debug) << "Switching to outputs 0, 1";
            selector.select_outputs<0, 1>();

            BOOST_LOG_TRIVIAL(debug) << "Switching to in/out 0, 0";
            selector.select_inputs<0>();
            selector.select_outputs<0>();

            std::this_thread::sleep_for(1s);
            graph.stop();
        }
    );
  
    graph.run_async();
    graph.await();
    return 0;
}
