#pragma once
#include "psrdada_cpp/dadaflow/IPCBuffer.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <array>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/json.hpp>
#include <cstdint>
#include <type_traits>

namespace bip = boost::interprocess;

#define MAGIC_NUMBER     0x51a0cbb8;
#define PROTOCOL_VERSION 0x1;

namespace psrdada_cpp
{

struct DescriptorField {
};

// Macro to define a field with metadata
#define FIELD(NAME, TYPE, DESCRIPTION)                      \
    struct NAME: public DescriptorField {                   \
        static_assert(std::is_trivial<TYPE>::value,         \
                      "TYPE must be a trivial type");       \
        static_assert(std::is_standard_layout<TYPE>::value, \
                      "T must have a standard layout");     \
        TYPE NAME;                                          \
        static constexpr const char* description_()         \
        {                                                   \
            return DESCRIPTION;                             \
        }                                                   \
        static constexpr const char* name_()                \
        {                                                   \
            return #NAME;                                   \
        }                                                   \
        static constexpr const char* type_()                \
        {                                                   \
            return #TYPE;                                   \
        }                                                   \
        static constexpr std::size_t count_()               \
        {                                                   \
            return 1;                                       \
        }                                                   \
    };

// Macro to define an array field with metadata
#define ARRAYFIELD(NAME, TYPE, DESCRIPTION)                 \
    template <std::size_t N>                                \
    struct NAME: public DescriptorField {                   \
        static_assert(std::is_trivial<TYPE>::value,         \
                      "TYPE must be a trivial type");       \
        static_assert(std::is_standard_layout<TYPE>::value, \
                      "T must have a standard layout");     \
        std::array<TYPE, N> NAME;                           \
        static constexpr const char* description_()         \
        {                                                   \
            return DESCRIPTION;                             \
        }                                                   \
        static constexpr const char* name_()                \
        {                                                   \
            return #NAME;                                   \
        }                                                   \
        static constexpr const char* type_()                \
        {                                                   \
            return #TYPE;                                   \
        }                                                   \
        static constexpr std::size_t count_()               \
        {                                                   \
            return N;                                       \
        }                                                   \
    };

/** --- Example usage ---
 * ...using a new namespace to avoid pollution of the psrdada_cpp namespace...
 * namespace fields {
 *   ARRAYFIELD(antenna_indices, int, "Antenna indices")
 *   FIELD(lo_frequency, float, "LO frequency (Hz)")
 *   ARRAYFIELD(state, char, "My name")
 * }
 */

/**
 * @brief A class for building serialisable struct definitions.
 *
 * @tparam Fields A struct that wraps a trivial type
 *
 * @details The Descriptor class takes a set of Fields as template arguments
 *          where each Field describes a single trivial type like int, float,
 *          float2, etc. as either a single value or as a std::array. Fields
 *          should be generated with the FIELD and ARRAYFIELD macros.
 *
 *          This class works as each Field adds a single trivial type member
 *          variable. The memory ordering of these types is guaranteed to match
 *          the order of the Fields in the template parameter pack. As the type
 *          created is only composed of trivial types and there are no virtual
 *          member functions, the class is considered POD and is thus
 * representable by a plain C structure.
 */
template <typename... Fields>
struct Descriptor: public Fields... {
    static_assert((std::is_base_of_v<DescriptorField, Fields> && ...),
                  "All Fields must be derived from DescriptorField");

    /**
     * @brief Create a JSON description of the described Fields
     *
     * @return std::string
     */
    static std::string describe()
    {
        boost::json::object json_obj;
        boost::json::array fields_array;
        (add_field<Fields>(fields_array), ...);
        json_obj["fields"] = fields_array;
        return boost::json::serialize(json_obj);
    }

  private:
    template <typename Field>
    static void add_field(boost::json::array& fields_array)
    {
        boost::json::object field_obj;
        field_obj["name"]        = Field::name_();
        field_obj["type"]        = Field::type_();
        field_obj["count"]       = Field::count_();
        field_obj["description"] = Field::description_();
        fields_array.push_back(field_obj);
    }
};

/**
 * @brief Header for SharedStruct memory layouts
 *
 */
struct DescriptorHeader {
    // Magic number to allow clients to determine if this
    // is an automagically parsable buffer
    uint32_t magic_number;
    // Version of the serialisation protocol
    uint32_t protocol_version;
    // Size of the JSON descriptor in bytes
    uint32_t descriptor_size;
};

/**
 * @brief A self-describing thread-safe shared memory buffer
 *
 * @tparam DescriptorType An instance of the Descriptor<...> template
 *
 * @details The SharedStruct class wraps the mapping of a DescriptorType
 *          to the memory in an IPCBuffer. The mapping is prefixed with
 *          a header and description data allowing the contents of the
 *          data to be dynamically discovered by client applications.
 *          The layout of the underlying IPCBuffer memory is as follows:
 *
 *          [DescriptorHeader]
 *          [JSON description string]
 *          [DescriptorType]
 *
 *          The header block contain protocol information and the size of
 *          the description string. Once parse, the description string
 *          provides information to map the DescriptorType memory in an
 *          extenrnal application (e.g. a Python script)
 *
 */
template <typename DescriptorType>
class SharedStruct
{
  public:
    static_assert(is_instantiation_of<Descriptor, DescriptorType>::value,
                  "SharedStruct requires an DescriptorType that is an "
                  "instantiation of the Descriptor type");

    /**
     * @brief Construct a new Shared Struct object
     *
     * @param key The name of the shared memory to use.
     */
    SharedStruct(std::string_view key)
        : _ipcbuf(key, calculate_nbytes()), _data(nullptr)
    {
        std::string description  = DescriptorType::describe();
        char* ptr                = _ipcbuf.data();
        DescriptorHeader* header = reinterpret_cast<DescriptorHeader*>(ptr);
        header->magic_number     = MAGIC_NUMBER;
        header->protocol_version = PROTOCOL_VERSION;
        header->descriptor_size  = description.size();
        ptr += sizeof(DescriptorHeader);
        std::memcpy(ptr, description.data(), description.size());
        ptr += description.size();
        _data = reinterpret_cast<DescriptorType*>(ptr);
    }

    /**
     * @brief A wrapper for executing thread-safe writes to the buffer
     *
     * @tparam Func The type of a callable that takes a reference to the
     * DescriptorType
     * @param func An instance of the Func type
     *
     * @details The call to func will be wrapped in a mutex lock on the
     * underlying buffer. After the call is complete, the update semaphore on
     * the buffer will be incremented.
     *
     * @code{.cpp}
     * SharedStruct<SomeType> sstruct("some_key");
     * int some_int_value = 4;
     * sstruct.with_write_lock([&](SomeType& inst){
     *     inst.some_int_value = some_int_value
     * });
     * @endcode
     */
    template <typename Func>
    void with_write_lock(Func func)
    {
        IPCBuffer::WriteLock lock(_ipcbuf);
        func(data());
    }

    /**
     * @brief A wrapper for executing thread-safe read from the buffer
     *
     * @tparam Func The type of a callable that takes a const reference to the
     * DescriptorType
     * @param func An instance of the Func type
     *
     * @details The call to func will be wrapped in a mutex lock on the
     * underlying buffer. After the call is complete, the update semaphore on
     * the buffer will be incremented.
     *
     * @code{.cpp}
     * SharedStruct<SomeType> sstruct("some_key");
     * int some_int_value;
     * sstruct.with_read_lock([&](SomeType const& inst){
     *     some_int_value = inst.some_int_value;
     * });
     * @endcode
     */
    template <typename Func>
    void with_read_lock(Func func)
    {
        IPCBuffer::WriteLock lock(_ipcbuf);
        func(data());
    }

    /**
     * @brief An thread UN-safe accessor to the data in the buffer
     *
     * @return DescriptorType&
     */
    DescriptorType& data() { return *_data; }

    /**
     * @brief An thread UN-safe const accessor to the data in the buffer
     *
     * @return DescriptorType const&
     */
    DescriptorType const& data() const { return *_data; }

  private:
    static constexpr std::size_t calculate_nbytes()
    {
        return (sizeof(DescriptorHeader) + DescriptorType::describe().size() +
                sizeof(DescriptorType));
    }

    IPCBuffer _ipcbuf;
    DescriptorType* _data;
};

} // namespace psrdada_cpp