if(ENABLE_CUDA)
    if(CMAKE_VERSION GREATER_EQUAL 3.17)
        find_package(CUDAToolkit REQUIRED)
    else() # Compatibility for cmake 3.16 (ubuntu20.4)
        find_package(CUDA REQUIRED)
        if (CUDA_CUFFT_LIBRARIES)
            add_library(CUFFT_IMPORTED SHARED IMPORTED GLOBAL)
            set_target_properties(CUFFT_IMPORTED PROPERTIES IMPORTED_LOCATION ${CUDA_CUFFT_LIBRARIES})
            add_library(CUDA::cufft ALIAS CUFFT_IMPORTED)
        else()
            message(FATAL_ERROR "CUDA_CUFFT_LIBRARIES not found!")
        endif()
    endif()

    set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
    set(CUDA_PROPAGATE_HOST_FLAGS OFF)

    set(CMAKE_CUDA_FLAGS "-DENABLE_CUDA --std=c++${CMAKE_CXX_STANDARD} -Wno-deprecated-gpu-targets --extended-lambda")
    set(CMAKE_CUDA_FLAGS_DEBUG "-O0 -G --ptxas-options='-v' '-Xcompiler=-Wextra,-Werror,--all-warnings'")
    set(CMAKE_CUDA_FLAGS_RELEASE "-O3 -use_fast_math -restrict")
    set(CMAKE_CUDA_FLAGS_RELWITHDEBINFO "${CMAKE_CUDA_FLAGS_RELEASE} --generate-line-info")
    
    # Here we need to add the cuda include path - otherwise it might fail when it is not set as ENV
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -I${CUDAToolkit_INCLUDE_DIRS} -DENABLE_CUDA")

endif()