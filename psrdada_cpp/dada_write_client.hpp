#ifndef PSRDADA_CPP_DADA_WRITE_CLIENT_HPP
#define PSRDADA_CPP_DADA_WRITE_CLIENT_HPP

#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/common.hpp"
#include <psrdada/ipcutil.h>
#include <inttypes.h>
#include <atomic>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

#define IPCBUF_READER  5  /* one process that reads from the buffer */
#define IPCBUF_READING 6  /* start-of-data flag has been raised */
#define IPCBUF_RSTOP   7  /* end-of-data flag has been raised */

namespace psrdada_cpp {

    /**
     * @brief      Class that provides means for writing to
     *             a DADA ring buffer
     */
    class DadaWriteClient: public DadaClientBase
    {
    public:

        /**
         * @brief      A helper class for encapsulating
         *             the DADA buffer header blocks.
         */
        class HeaderStream
        {
        private:
            DadaWriteClient& _parent;
            std::unique_ptr<RawBytes> _current_block;

        public:
            /**
             * @brief      Create a new instance
             *
             * @param      parent  A reference to the parent writing client
             */
            HeaderStream(DadaWriteClient& parent);
            HeaderStream(HeaderStream const&) = delete;
            ~HeaderStream();

            /**
             * @brief      Get the next header block in the ring buffer
             *
             * @details     As only one block can be open at a time, release() must
             *             be called between subsequenct next() calls.
             *
             * @return     A RawBytes instance wrapping a pointer to share memory
             */
            RawBytes& next();

            /**
             * @brief      Release the current header block.
             *
             * @details     This will mark the block as filled, making it
             *             readable by reading client.
             */
            void release();

            /**
             * Is there a currently active block e.g. acquired a block but not released
             */
            bool has_block() const;
        };

        class DataStream
        {
        private:
            DadaWriteClient& _parent;
            std::unique_ptr<RawBytes> _current_block;
            std::size_t _block_idx;

        public:
            /**
             * @brief      Create a new instance
             *
             * @param      parent  A reference to the parent writing client
             */
            DataStream(DadaWriteClient& parent);
            DataStream(DataStream const&) = delete;
            ~DataStream();

            /**
             * @brief      Get the next data block in the ring buffer
             *
             * @details     As only one block can be open at a time, release() must
             *             be called between subsequenct next() calls.
             *
             * @return     A RawBytes instance wrapping a pointer to share memory
             */
            RawBytes& next();

            /**
             * @brief      Release the current data block.
             *
             * @details     This will mark the block as filled, making it
             *             readable by reading client.
             */
            void release(bool eod=false);

            /**
             * @brief      Return the index of the currently open block
             */
            std::size_t block_idx() const;

            /**
             * Is there a currently active block e.g. acquired a block but not released
             */
            bool has_block() const;

            /**
             * Return a reference to the currently open block
             */
            RawBytes& current_block();

        };

    public:
        /**
         * @brief      Create a new client for writing to a DADA buffer
         *
         * @param[in]  key   The hexidecimal shared memory key
         * @param      log   A MultiLog instance for logging buffer transactions
         */
        DadaWriteClient(key_t key, MultiLog& log);
        DadaWriteClient(DadaWriteClient const&) = delete;
        ~DadaWriteClient();

        /**
         * @brief      Get a reference to a header stream manager
         *
         * @return     A HeaderStream manager object for the current buffer
         */
        HeaderStream& header_stream();

        /**
         * @brief      Get a reference to a data stream manager
         *
         * @return     A DataStream manager object for the current buffer
         */
        DataStream& data_stream();

        /**
         * @brief      Connect the reader to the DADA buffer. Specializtion of the DadaClientBase::connect
         *
         * @details    DadaWriteClient::connect() calls DadaClientBase::connect() 
         */
        void connect();

        /**
         * @brief      Disconnect the reader from the DADA buffer. Specializtion of the DadaClientBase::disconnect
         *
         * @details    DadaWriteClient::disconnect() calls DadaClientBase::disconnect() after releasing the current 
         *             acquisition state (e.g. if data and/or header block is open and/or the reading lock acquired)
         */
        void disconnect();

        /**
         * @brief      Resets the the ring buffer without prejudice
         */
        void reset();


    private:
        void lock();
        void release();
        int reset_buffer(ipcbuf_t* id);
        bool _locked;
        HeaderStream _header_stream;
        DataStream _data_stream;
    };

} //namespace psrdada_cpp

#endif //PSRDADA_CPP_DADA_WRITE_CLIENT_HPP
