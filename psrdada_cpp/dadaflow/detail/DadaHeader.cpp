#include "psrdada_cpp/dadaflow/DadaHeader.hpp"

#include <iomanip>

namespace psrdada_cpp
{

template <typename Container>
    requires(std::is_same_v<typename Container::value_type, char> &&
             Iterable<Container>)
void DadaHeader::read_from(Container const& data)
{
    std::istringstream stream(std::string(data.begin(), data.end()));
    read_from_impl(stream);
}

template <typename T>
std::string DadaHeader::to_string(T const& value) const
{
    std::ostringstream ss;
    if constexpr(std::is_floating_point_v<T>) {
        ss << std::setprecision(15) << value;
    } else {
        ss << value;
    }
    return ss.str();
}

template <typename T>
void DadaHeader::set(std::string_view key, T const& value)
{
    _header_map[std::string{key}] = to_string(value);
}

template <template <typename, typename> class Container, typename T, typename A>
void DadaHeader::set(std::string_view key,
                     Container<T, A> const& values,
                     std::size_t precision)
{
    std::stringstream ss;
    ss << std::setprecision(precision);
    bool first = true;
    for(auto const& val: values) {
        if(!first) {
            ss << ",";
        }
        ss << val;
        first = false;
    }
    _header_map[std::string{key}] = ss.str();
}

// Helper function for converting tokens
template <typename T>
T DadaHeader::convert_token(std::string_view token) const
{
    // Handle string type conversion
    if constexpr(std::is_same_v<T, std::string>) {
        return std::string(token);
    }
    // Handle unsigned integral type conversion
    else if constexpr(std::is_unsigned_v<T>) {
        try {
            std::size_t pos;
            unsigned long long value = std::stoull(std::string(token), &pos);
            if(pos != token.size()) {
                throw std::invalid_argument("Invalid numeric value: '" +
                                            std::string(token) + "'");
            }
            // Check for out-of-range for the target type
            if(value == std::numeric_limits<T>::max()) {
                throw std::out_of_range("Value out of range for type");
            }
            return static_cast<T>(value);
        } catch(const std::exception& e) {
            throw std::runtime_error("Failed to convert unsigned token '" +
                                     std::string(token) +
                                     "' to the desired type: " + e.what());
        }
    }
    // Handle signed integral type conversion
    else if constexpr(std::is_signed_v<T> && std::is_integral_v<T>) {
        try {
            std::size_t pos;
            long long value = std::stoll(std::string(token), &pos);
            if(pos != token.size()) {
                throw std::invalid_argument("Invalid numeric value: '" +
                                            std::string(token) + "'");
            }
            // Check for out-of-range for the target type
            if(value < std::numeric_limits<T>::min() ||
               value > std::numeric_limits<T>::max()) {
                throw std::out_of_range("Value out of range for type");
            }
            return static_cast<T>(value);
        } catch(const std::exception& e) {
            throw std::runtime_error("Failed to convert signed token '" +
                                     std::string(token) +
                                     "' to the desired type: " + e.what());
        }
    }
    // Handle floating point conversion
    else if constexpr(std::is_floating_point_v<T>) {
        try {
            std::size_t pos;
            T value = static_cast<T>(std::stold(std::string(token), &pos));
            if(pos != token.size()) {
                throw std::invalid_argument("Invalid floating-point value: '" +
                                            std::string(token) + "'");
            }
            return value;
        } catch(const std::exception& e) {
            throw std::runtime_error(
                "Failed to convert floating-point token '" +
                std::string(token) + "' to the desired type: " + e.what());
        }
    } else {
        throw std::invalid_argument("Unsupported type for token conversion.");
    }
}

// Single-value getter
template <typename T>
T DadaHeader::get(std::string_view key) const
{
    std::string_view value = safe_lookup(key);
    return convert_token<T>(value);
}

// Single-value getter
template <typename T>
std::optional<T> DadaHeader::get_optional(std::string_view key) const
{
    if (has_key(key)) {
        return {get<T>(key)};
    } else {
        return std::nullopt;
    }
}

// Vector-like getter with delimiter
template <typename T>
std::vector<T> DadaHeader::get(std::string_view key,
                               std::string_view delimiter) const
{
    std::string_view value = safe_lookup(key);
    std::vector<T> result_container;

    size_t start = 0;
    size_t end   = value.find(delimiter);

    while(end != std::string::npos) {
        std::string_view token = value.substr(start, end - start);
        result_container.push_back(convert_token<T>(token));
        start = end + delimiter.length();
        end   = value.find(delimiter, start);
    }

    // Handle the last token
    std::string_view last_token = value.substr(start);
    result_container.push_back(convert_token<T>(last_token));

    return result_container;
}

template <typename T>
std::optional<std::vector<T>> DadaHeader::get_optional(std::string_view key,
                                                       std::string_view delimiter) const
{
    if (has_key(key)) {
        return {get<T>(key, delimiter)};
    } else {
        return std::nullopt;
    }
}

} // namespace psrdada_cpp