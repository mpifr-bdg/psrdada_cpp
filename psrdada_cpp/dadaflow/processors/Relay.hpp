#pragma once
#include <optional>

/**
 * @brief A relay that can be enabled to allow data forwarding.
 * 
 * @tparam T The value type of the relay
 * 
 * @note This class is not particularly useful, as the same dropping/forwarding
 *       behaviour can be achieved by enabling or disabling the output ports on
 *       a ProcessExecutor instance. This serves only as an example of how relay
 *       -like behaviour can be achieved in a node. A more useful (but application-
 *       specific) implementation may enable or disable itself based on its inputs.
 */
template <typename T>
struct Relay
{
    std::optional<T> operator()(T value){
        if (_enabled) {
            return {value};
        } else {
            return std::nullopt;
        }
    }

    void enable(){_enabled=true;}
    
    void disable(){_enabled=false;}
    
    bool _enabled = true;
};