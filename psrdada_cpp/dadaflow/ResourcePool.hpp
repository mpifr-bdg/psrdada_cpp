/**
 *  The idea of this class is to produce a pool resource that
 *  allows for a fixed number of a given type to be created.
 *  It thus acts like a ring buffer for types. No ordering is
 *  guaranteed.
 */

#pragma once
#include "psrdada_cpp/dadaflow/ResourceAllocator.hpp"
#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

namespace psrdada_cpp
{

/**
 * @brief A thread-safe instance pool.
 *
 * @tparam T The type of the the pool instances.
 */
template <typename T>
class ResourcePool: public ResourceAllocatorInterface<T>
{
  public:
    using OutputType = T;

  public:
    /**
     * @brief Construct a new Resource Pool object.
     *
     * @param size The number of instances in the pool.
     * @param args The constructor arguments for the pool instance type.
     *
     * @details Uses perfect forwarding to construct pool instances in place.
     */
    ResourcePool(std::size_t size, auto&&... args): _size(size)
    {
        for(std::size_t i = 0; i < _size; ++i) {
            _pool.push(std::make_unique<OutputType>(
                std::forward<decltype(args)>(args)...));
        }
    }

    /**
     * @brief Get an instance from the pool.
     *
     * @return std::shared_ptr<OutputType>
     *
     * @details If the pool is currently empty this call will block until a
     *          pool instance becomes available. The pool instance is returned
     * as a std::shared_ptr with a custom destructor that will return the
     *          instance to the pool when called.
     *
     * @code
     * ResourcePool<std::vector<float>> pool(3);
     * auto inst = pool.get();
     * @endcode
     */
    std::shared_ptr<OutputType> get() override
    {
        BOOST_LOG_TRIVIAL(debug) << "Attempting to acquire pool resource";
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock, [this] { return !_pool.empty(); });
        std::unique_ptr<OutputType> resource = std::move(_pool.front());
        _pool.pop();
        BOOST_LOG_TRIVIAL(debug) << "Pool resource acquired";
        return std::shared_ptr<OutputType>(
            resource.release(),
            [this](OutputType* ptr) {
                BOOST_LOG_TRIVIAL(debug) << "Releasing pool resource";
                std::unique_lock<std::mutex> lock(_mutex);
                BOOST_LOG_TRIVIAL(debug)
                    << "Pushing pool resource back to queue";
                _pool.push(std::unique_ptr<OutputType>(ptr));
                _cv.notify_one();
            });
    }

  private:
    std::size_t _size;
    std::queue<std::unique_ptr<OutputType>> _pool;
    std::mutex _mutex;
    std::condition_variable _cv;
};


template <typename T>
class SafeResourcePool : public ResourceAllocatorInterface<T>, public std::enable_shared_from_this<SafeResourcePool<T>>
{
  public:
    using OutputType = T;

    // Factory method to create instances as shared pointers
    template <typename... Args>
    static std::shared_ptr<SafeResourcePool> create(std::size_t size, Args&&... args)
    {
        return std::shared_ptr<SafeResourcePool>(new SafeResourcePool(size, std::forward<Args>(args)...));
    }

    // Get an instance from the pool
    std::shared_ptr<OutputType> get() override
    {
        BOOST_LOG_TRIVIAL(debug) << "Attempting to acquire pool resource";
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock, [this] { return !_pool.empty(); });
        std::unique_ptr<OutputType> resource = std::move(_pool.front());
        _pool.pop();
        BOOST_LOG_TRIVIAL(debug) << "Pool resource acquired";

        // Capture a shared pointer to the pool (shared_from_this()) in the deleter
        std::shared_ptr<SafeResourcePool> self = this->shared_from_this();
        return std::shared_ptr<OutputType>(
            resource.release(),
            [self](OutputType* ptr) {
                BOOST_LOG_TRIVIAL(debug) << "Releasing pool resource";
                std::unique_lock<std::mutex> lock(self->_mutex);
                self->_pool.push(std::unique_ptr<OutputType>(ptr));
                self->_cv.notify_one();
                BOOST_LOG_TRIVIAL(debug) << "Pool resource returned to queue";
            });
    }

  private:
    std::size_t _size;
    std::queue<std::unique_ptr<OutputType>> _pool;
    std::mutex _mutex;
    std::condition_variable _cv;

    // Private constructor to enforce shared pointer creation via factory method
    template <typename... Args>
    SafeResourcePool(std::size_t size, Args&&... args) : _size(size)
    {
        for (std::size_t i = 0; i < _size; ++i) {
            _pool.push(std::make_unique<OutputType>(std::forward<Args>(args)...));
        }
    }
};


} // namespace psrdada_cpp