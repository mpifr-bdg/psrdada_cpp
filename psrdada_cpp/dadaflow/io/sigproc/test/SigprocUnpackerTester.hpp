#include "psrdada_cpp/dadaflow/io/sigproc/SigprocUnpacker.hpp"
#include "psrdada_cpp/dadaflow/io/sigproc/SigprocIO.hpp"
#include <gtest/gtest.h>
#include <sstream>
#include <memory>
#include <limits>

namespace psrdada_cpp {
namespace sigproc {
namespace test {

template <typename InputType_, typename OutputType_>
struct SigprocUnpackerTestTraits
{
    using InputType = InputType_;
    using OutputType = OutputType_;
};

template <typename TestTraits>
class SigprocUnpackerTester : public ::testing::Test
{
public:

    SigprocUnpackerTester(){}

    ~SigprocUnpackerTester(){}

    void SetUp() override {}

    void TearDown() override {}

    void populate_pattern(Filterbank1Bit& ar) {
        for (auto& val: ar.container().container()) {
            val = 0xAA;
        }
    }

    void populate_pattern(Filterbank2Bit& ar) {
        for (auto& val: ar.container().container()) {
            val = 0xE4;
        }
    }

    void populate_pattern(Filterbank4Bit& ar) {
        std::size_t ii = 0;
        for (auto& val: ar.container().container()) {
            val = 0x10 + 0x22 * (ii % 8);
            ++ii;
        }
    }

    template <typename T>
    void populate_pattern(T& ar) {
        std::size_t ii = 0;
        for (auto& val: ar) {
            val = (ii++) % 23;
        }
    }

    void validate_pattern(Filterbank1Bit const& input, Filterbank<std::vector<float>> const& output) {
        ASSERT_EQ(input.extents(), output.extents());
        std::size_t ii = 0;
        for (auto const& val: output) {
            ASSERT_FLOAT_EQ(val, static_cast<float>((ii++) % 2));
        }
    }

    void validate_pattern(Filterbank2Bit const& input, Filterbank<std::vector<float>> const& output) {
        ASSERT_EQ(input.extents(), output.extents());
        std::size_t ii = 0;
        for (auto const& val: output) {
            ASSERT_FLOAT_EQ(val, static_cast<float>((ii++) % 4));
        }
    }

    void validate_pattern(Filterbank4Bit const& input, Filterbank<std::vector<float>> const& output) {
        ASSERT_EQ(input.extents(), output.extents());
        std::size_t ii = 0;
        for (auto const& val: output) {
            ASSERT_FLOAT_EQ(val, static_cast<float>((ii++) % 16));
        }
    }

    template <typename T>
    void validate_pattern(T const& input, Filterbank<std::vector<float>> const& output) {
        ASSERT_EQ(input.extents(), output.extents());
        std::size_t ii = 0;
        for (auto const& val: output) {
            ASSERT_FLOAT_EQ(val, static_cast<float>((ii++) % 23));
        }
    }
};

} // namespace test
} // namespace sigproc
} // namespace psrdada_cpp