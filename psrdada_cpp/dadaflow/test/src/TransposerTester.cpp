#include "psrdada_cpp/dadaflow/test/TransposerTester.hpp"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/ResourcePool.hpp"

#include <vector>
#include <memory>

namespace psrdada_cpp {
namespace test {


TEST_F(TransposerTester, index_transpose) {

    // Idea: transpose  TAPFT to ATTFP
    // Each element of the test vector can be a 5 tuple of indices
    using IndicesT = std::tuple<std::size_t, std::size_t, std::size_t, std::size_t, std::size_t>;
    using ContainerT = std::vector<IndicesT>;
    using T0 = TimeDimensionN<0>;
    using T1 = TimeDimensionN<1>;
    using A = AntennaDimension;
    using P = PolarisationDimension;
    using F = FrequencyDimension;
    using InputType = DescribedData<ContainerT, T0, A, P, F, T1>;
    using OutputType = DescribedData<ContainerT, T0, T1, A, F, P>;
    using AllocatorType = SafeResourcePool<OutputType>;
    using TransposerType = Transposer<InputType, OutputType>;

    std::size_t n_t0 = 3;
    std::size_t n_t1 = 7;
    std::size_t n_a = 8;
    std::size_t n_f = 5;
    std::size_t n_p = 2;

    auto input_ptr = std::make_shared<InputType>();
    auto& input = *input_ptr;
    input.resize({n_t0, n_a, n_p, n_f, n_t1});

    std::size_t index = 0;
    for (std::size_t t0 = 0; t0 < n_t0; ++t0) {
        for (std::size_t a = 0; a < n_a; ++a) {
            for (std::size_t p = 0; p < n_p; ++p) {
                for (std::size_t f = 0; f < n_f; ++f) {
                    for (std::size_t t1 = 0; t1 < n_t1; ++t1) {
                        input[index] = {t0, a, p, f, t1};
                        ++index;
                    }
                }
            }
        }
    }

    auto allocator = AllocatorType::create(2);
    TransposerType transposer(allocator, 4);
    
    auto output_ptr = transposer(input_ptr);
    auto& output = *output_ptr;

    ASSERT_EQ(output.size(), input.size());
    std::size_t outdex = 0;
    for (std::size_t t0 = 0; t0 < n_t0; ++t0) {
        for (std::size_t t1 = 0; t1 < n_t1; ++t1) {
            for (std::size_t a = 0; a < n_a; ++a) {
                for (std::size_t f = 0; f < n_f; ++f) {
                    for (std::size_t p = 0; p < n_p; ++p) {
                        ASSERT_EQ(output[outdex], (IndicesT{t0, a, p, f, t1})) << "Error at index: " << outdex;
                        ++outdex;
                    }
                }
            }
        }
    }
    
}

} // namespace test
} // namespace psrdada_cpp