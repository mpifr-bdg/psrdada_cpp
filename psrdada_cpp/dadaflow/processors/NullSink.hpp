#pragma once
#include "psrdada_cpp/common.hpp"
#include <iostream>
#include <tuple>

/**
 * @brief A generic sink block that can sink any data
 * 
 * @details Generally it preferred to used a lambda for this. Instead of the NullSink
 *          we can use [](auto...){} when adding a node to a graph.
 */
class NullSink {
public:
  template <typename... Args> 
  void operator()(Args...) {}
};