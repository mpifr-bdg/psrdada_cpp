#include "psrdada_cpp/dadaflow/io/sigproc/SigprocIO.hpp"

namespace psrdada_cpp {
namespace sigproc {

template <typename FilterbankType>
FilterbankVariant SigprocGenerator<FilterbankType>::generate(std::size_t nsamples) {
    OutputPtr data_ptr = _allocator->get();
    auto& data = *data_ptr;
    data.resize({nsamples, _header.nchans()});
    data.timestep(std::chrono::duration<long double, pico>(_header.tsamp() * 1e12));
    data.timestamp(Timestamp::from_mjd(_header.tstart()));
    data.channel_zero_freq(_header.fch1() * 1e6 * hertz);
    data.channel_bandwidth(_header.foff() * 1e6 * hertz);
    data.metadata_ptr(_metadata_ptr);
    BOOST_LOG_TRIVIAL(debug) << "Generated filterbank data: " << data.describe();
    return data_ptr;
};

} // namespace sigproc
} // namespace psrdada_cpp