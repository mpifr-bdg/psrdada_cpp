#pragma once
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <random>
#include <complex>
#include <cmath>

// ToDo: Generalize the bit packing

namespace psrdada_cpp {
namespace testing_tools {

// Type trait to check if a type is an instance of complex<T>
template <typename T>
struct is_complex : std::false_type {};

template <typename T>
struct is_complex<std::complex<T>> : std::true_type {};

// Create a vector with random values of type T
template<typename T>
thrust::host_vector<T> random_vector(int mean, int sigma, int size)
{
  std::normal_distribution<double> dist(mean, sigma);
  std::default_random_engine engine;
  thrust::host_vector<T> data(size);
  for (std::size_t i = 0; i < data.size(); i++)
  {
#if __cplusplus >= 201703L
    if constexpr (is_complex<T>::value)
    {
        data[i] = T(dist(engine), dist(engine));
    }
    else if(std::is_integral<T>::value)
#else
    if(std::is_integral<T>::value)
#endif
    {
      data[i] = (T)(dist(engine) + 1);
    }
    else
    {
      data[i] = (T)(dist(engine));
    }
  }
  return data;
}

template<typename T, typename U>
struct conversion_unary : public thrust::unary_function<T, U>
{
    __host__ __device__
    U operator()(const T& x) const{
        return static_cast<U>(x);
    }
};

// Convert a thrust::host_vector from type T to type U
template<typename T, typename U>
thrust::host_vector<U> conversion(thrust::host_vector<T> idata)
{
    thrust::host_vector<U> odata(idata.size());
    thrust::transform(idata.begin(), idata.end(), odata.begin(), conversion_unary<T, U>());
    return odata;
}

// Convert a thrust::device_vector from type T to type U
template<typename T, typename U>
thrust::device_vector<U> conversion(thrust::device_vector<T> idata)
{
    thrust::device_vector<U> odata(idata.size());
    thrust::transform(idata.begin(), idata.end(), odata.begin(), conversion_unary<T, U>());
    return odata;
}


inline uint64_t swapEndianess(uint64_t value)
{
    value = ((value >> 56) & 0xFF) |
            ((value << 40) & 0xFF000000000000) |
            ((value >> 40) & 0xFF00) |
            ((value << 24) & 0xFF0000000000) |
            ((value >> 24) & 0xFF0000) |
            ((value << 8) & 0xFF00000000) |
            ((value >> 8) & 0xFF000000) |
            ((value << 56) & 0xFF00000000000000);
    return value;
}

inline void set_value(char* ptr, int8_t val, std::size_t nbit=8, std::size_t pos=0)
{
    if(nbit == 12)
    {
        ptr[pos] = val >> 4;
        ptr[pos + 1] = (val & 0x0F) << 4;
    }
    else if(nbit == 10)
    {
        ptr[pos] = val >> 2;
        ptr[pos + 1] = (val & 0x03) << 6;
    }else
    {
        ptr[pos] = val;
    }
}

inline void fill_8bit(char* ptr, std::size_t nbytes, std::size_t val)
{
    for(std::size_t i = 0; i < nbytes; i++){
        ptr[i] = val;
    }
}


inline void fill_10bit(char* ptr, std::size_t nbytes, std::size_t val)
{
    assert(nbytes % (sizeof(uint64_t) * 5) == 0);
    uint64_t* tmp_ptr = reinterpret_cast<uint64_t *>(ptr);
    short masks[5] = {0x03C0, 0x03FC, 0x0300, 0x03F0, 0x0000};
    for(std::size_t i = 0; i < nbytes/sizeof(uint64_t); i+=5){
        std::size_t rest = 10;
        for(std::size_t ii = 0; ii < 5; ii++){
            std::size_t shift = sizeof(uint64_t) * 8 - rest;
            std::size_t steps = shift / 10 + 1;
            tmp_ptr[i + ii] = 0;
            for(std::size_t iii = 0; iii < steps; iii++){
                tmp_ptr[i + ii] |= ((val & 0x03FF) << (shift - iii * 10));
            }
            rest = 10 - shift % 10;
            tmp_ptr[i + ii] |= (val & masks[ii]) >> rest;
            tmp_ptr[i + ii] = swapEndianess(tmp_ptr[i + ii]);
        }
    }
}

inline void fill_12bit(char* ptr, std::size_t nbytes, std::size_t val)
{
    assert(nbytes % (sizeof(uint64_t) * 3) == 0);
    uint64_t* tmp_ptr = reinterpret_cast<uint64_t *>(ptr);
    short masks[3] = {0x0FF0, 0x0F00, 0x0000};
     for(std::size_t i = 0; i < nbytes/sizeof(uint64_t); i+=3){
        std::size_t rest = 12;
        for(std::size_t ii = 0; ii < 3; ii++){
            std::size_t shift = sizeof(uint64_t) * 8 - rest;
            std::size_t steps = shift / 12 + 1;
            tmp_ptr[i + ii] = 0;
            for(std::size_t iii = 0; iii < steps; iii++){
                tmp_ptr[i + ii] |= ((val & 0x0FFF) << (shift - iii * 12));
            }
            rest = 12 - shift % 12;
            tmp_ptr[i + ii] |= (val & masks[ii]) >> rest;
            tmp_ptr[i + ii] = swapEndianess(tmp_ptr[i + ii]);
        }
    }
}

inline void fill_value(char* ptr, std::size_t nbytes, std::size_t nbit, std::size_t val)
{
    if(nbit == 12)
        fill_12bit(ptr, nbytes, val);
    else if(nbit == 10)
        fill_10bit(ptr, nbytes, val);
    else
        fill_8bit(ptr, nbytes, val);
}

}
}
