#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_db_split.hpp"
#include "psrdada_cpp/cli_utils.hpp"

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#include <sys/types.h>
#include <iostream>
#include <string>
#include <ios>
#include <vector>
#include <fstream>

using namespace psrdada_cpp;

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

int main(int argc, char** argv)
{
    try
    {
        key_t in_key;
        std::vector<key_t> out_keys;
        std::string header_file;
        std::string log_level;
        std::size_t chunk_size;
        /** Define and parse the program options
        */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("chunk,c", po::value<std::size_t>(&chunk_size)->default_value(0), "Size of chunks going into output buffers. If set to zero, chunk size is equal to output buffer size")
        ("in_key,i", po::value<std::string>()->required()->notifier([&in_key](std::string in){
          in_key = string_to_key(in);
          }),"The shared memory key of the input dada buffer to connect to (hex string)")
        ("out_keys,o", po::value<std::string>()->required()->notifier([&out_keys](std::string in){
            std::vector<std::string> result;
            boost::split(result, in, boost::is_any_of(","));
            for(uint i = 0; i < result.size(); i++){
              out_keys.push_back(string_to_key(result[i]));
            }
          }),"The shared memory keys for the output dada buffers to connect to (hex string). Keys are separated by comma ','.")
        ("log_level", po::value<std::string>()->default_value("info")->notifier([](std::string level){set_log_level(level);}),"The logging level to use (debug, info, warning, error)");
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "dbsplit -- reads contents of a single dada buffer, splits it and writes it into several ringbuffers" << std::endl
                << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        /**
         * All the application code goes here
         */

        MultiLog log("dbsplit");
        DbSplit splitter(out_keys, log, chunk_size);
        DadaInputStream<decltype(splitter)> istream(in_key, log, splitter);
        istream.start();

        /**
         * End of application code
         */
    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
        << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
