#pragma once
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/DadaHeader.hpp"
#include "psrdada_cpp/dadaflow/FixedWidth.hpp"
#include "psrdada_cpp/dadaflow/units.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <tuple>
#include <memory>
#include <span>
#include <but>

namespace psrdada_cpp {
namespace {
    static constexpr unsigned int samples_per_heap = 4096;
}


template <typename Container>
using EDDPacketiserData = DescribedData<Container,
                                        TimeDimensionN<0>,
                                        PolarisationDimension,
                                        TimeDimensionN<1>>;

template <typename Container>
using EDDPacketiserSCIData = DescribedData<Container, 
                                           TimeDimension, 
                                           PolarisationDimension>;

struct PacketiserItem0x3101 {
    uint64_t digitiser_serial : 24;
    uint64_t digitiser_type : 8;
    uint64_t receptor_id : 14;
    uint64_t pol_id : 2;

    static PacketiserItem0x3101 from_uint64(uint64_t packed_data)
    {
        PacketiserItem0x3101 data;
        data.pol_id = packed_data & 0x3;  
        packed_data >>= 2;  
        data.receptor_id = packed_data & 0x3FFF; 
        packed_data >>= 14; 
        data.digitiser_type = packed_data & 0xFF;  
        packed_data >>= 8; 
        data.digitiser_serial = packed_data & 0xFFFFFF;  
        return data;
    }
};


struct PacketiserItem0x3102 {
    uint64_t adc_count : 16;
    uint64_t zeros : 30;
    uint64_t adc_saturation_flag : 1;
    uint64_t ndiode_flag : 1;

    static constexpr PacketiserItem0x3102 from_uint64(uint64_t packed_data)
    {
        PacketiserItem0x3102 packed_data;
        data.ndiode_flag = packed_data & 0x1;  
        packed_data >>= 1;  
        data.adc_saturation_flag = packed_data & 0x1;  
        packed_data >>= 1;  
        data.zeros = packed_data & 0x3FFFFFFF;  
        packed_data >>= 30;  
        data.adc_count = packed_data & 0xFFFF; 
        return data;
    }
};


template <int nbits, ...>
class MkrecvEDDPacketiserParser 
{
public:
    using DataContainer     = FixedWidth<std::span<uint64_t>, nbits, std::endian:big>;
    using OutputDataType    = EDDPacketiserData<DataContainer>;
    using OutputDataPtr     = std::shared_ptr<OutputDataType>;
    using SciContainer      = std::span<uint64_t>;
    using OutputSciType     = EDDPacketiserSCIData<SciContainer>;
    using OutputSciPtr      = std::shared_ptr<OutputSciType>;
    using OutputTuple       = std::tuple<OutputDataPtr, OutputSciPtr>;
    using FWTraits          = typename DataContainer::Traits;
    using DataType          = EDDPacketiserData<DataContainer>;
    using DataPtr           = std::shared_ptr<DataType>;
    using ReleaseLifetime   = std::shared_ptr<void>;
    using MetadataType      = typename OutputDataType::MetadataType;
    using MetadataPtrType   = std::shared_ptr<MetadataType>; 
    using MetadataCallback  = std::function<void(MetadataType&, DadaHeader const&)>;

    MkrecvEDDPacketiserParser() = default;
    MkrecvEDDPacketiserParser(MetadataCallback metadata_callback)
    : _metadata_callback(metadata_callback) {}
    MkrecvEDDPacketiserParser(MkrecvEDDPacketiserParser const&)            = delete;
    MkrecvEDDPacketiserParser& operator=(MkrecvEDDPacketiserParser const&) = delete;
    MkrecvEDDPacketiserParser(MkrecvEDDPacketiserParser&&)                 = default;

    void parse_header(RawBytes& header_bytes)
    {
        _header.purge();
        _header.read_from(header_bytes);
        const std::size_t sample_clock_start =
            _header.get<std::size_t>("SAMPLE_CLOCK_START");
        const long double sample_clock =
            _header.get<long double>("SAMPLE_CLOCK");
        const long double sync_time = _header.get<long double>("SYNC_TIME");
        long double unix_epoch = sync_time + sample_clock_start / sample_clock;
        _npol                  = _header.get<uint8_t>("NPOL");
        _timestamp             = Timestamp(unix_epoch);
        _timestep = std::chrono::duration<long double, pico>(1e12l / sample_clock);
        _metadata = std::make_shared<MetadataType>();
        if (_metadata_callback) {
            _metadata_callback(_metadata.get(), _header);
        }
    }

    OutputTuple parse_data(RawBytes& bytes, ReleaseLifetime releaser)
    {
        // First calculate the size of a heap in bytes.
        const std::size_t heap_size = samples_per_heap * nbits / BITS_PER_BYTE;

        // Assuming only one (packed) side channel that will be parsed out.
        const std::size_t sci_size = sizeof(uint64_t);

        // Buffer must contain a whole number of heaps. MKRECV will add padding between
        // the data and the side-channel items if the buffer is mis-sized.
        const std::size_t nheaps = bytes.used_bytes() / (heap_size + sci_size);

        // Total number of data elements in the buffer
        const std::size_t nelements = nheaps * samples_per_heap * _npol;
        
        // Parse the data
        std::span<uint64_t> data_span{
            reinterpret_cast<DataContainer::valie_type*>(bytes.ptr()),
            (nheaps * heap_size) / sci_size
        };
        DataContainer fw_wrapper{data_span, nelements};
        OutputDataPtr output_data_ptr = OutputDataPtr{
            new OutputDataType{fw_wrapper, {nheaps/_npol, _npol, samples_per_heap}}, 
            [releaser](OutputDataType* ptr) { delete ptr; }};
        output_data_ptr->timestamp(_timestamp);
        output_data_ptr->TimeDimensionN<0>::timestep(
            _timestep * static_cast<double>(samples_per_heap));
        output_data_ptr->polarisations({"0", "1"});
        output_data_ptr->TimeDimensionN<1>::timestep(_timestep);
        output_data_ptr->metadata_ptr(_metadata);
        
        // Parse the SCIs
        char* start_of_scis = bytes.ptr() + bytes.used_bytes() - (sci_size * nheaps); 
        std::span<uint64_t> sci_span{
            reinterpret_cast<DataContainer::valie_type*>(start_of_scis),
            nheaps
        };     
        OutputSciPtr output_sci_ptr = OutputSciPtr{
            new OutputSciType{sci_span, {nheaps/_npol, _npol}},
            [releaser](OutputSciType* ptr) { delete ptr; }};
        output_sci_ptr->timestamp(_timestamp);
        output_sci_ptr->timestep(_timestep * static_cast<double>(samples_per_heap));
        output_sci_ptr->polarisations({"0", "1"});
        output_sci_ptr->metadata_ptr(_metadata);
        return {output_data_ptr, output_sci_ptr};
    }

private:
    MetadataCallback _metadata_callback;
    DadaHeader _header;
    Timestamp _timestamp;
    std::chrono::duration<long double, pico> _timestep;
    uint8_t _npol;
    MetadataPtrType _metadata;
};

} // namespace psrdada_cpp