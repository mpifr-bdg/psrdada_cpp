#include "psrdada_cpp/dadaflow/test/IPCBufferTester.hpp"
#include <gtest/gtest.h>
#include <string>

namespace psrdada_cpp {
namespace test {

// Test the constructor and initialization of the buffer
TEST_F(IPCBufferTester, Constructor) {
    IPCBuffer buffer("test_monitoring", sizeof(MonitorInfo));

    EXPECT_EQ(buffer.size(), std::size_t{sizeof(MonitorInfo)});
    std::span<MonitorInfo> span = buffer.as<MonitorInfo>();
    EXPECT_EQ(span.size(), std::size_t{1});

    // Check that the memory is initialized to zero (optional behavior)
    EXPECT_EQ(span.front().lo_frequency, 0.0);
}

// Test writing to the buffer using WriteLock
TEST_F(IPCBufferTester, WriteBuffer) {
    IPCBuffer buffer("test_monitoring", sizeof(MonitorInfo));

    MonitorInfo& info = buffer.as<MonitorInfo>().front();

    {
        IPCBuffer::WriteLock lock(buffer);
        info.lo_frequency = 100.5;
    }

    // Verify the value has been updated
    EXPECT_EQ(info.lo_frequency, 100.5);
}

// Test reading from the buffer using ReadLock
TEST_F(IPCBufferTester, ReadBuffer) {
    IPCBuffer buffer("test_monitoring", 1);

    MonitorInfo& info = buffer.as<MonitorInfo>().front();

    // Write to the buffer first
    {
        IPCBuffer::WriteLock lock(buffer);
        info.lo_frequency = 200.25;
    }

    // Read the buffer using ReadLock
    {
        IPCBuffer::ReadLock lock(buffer);
        EXPECT_EQ(info.lo_frequency, 200.25);
    }
}

// Test that has_update works correctly after writing to the buffer
TEST_F(IPCBufferTester, HasUpdate) {
    IPCBuffer buffer("test_monitoring", 1);

    MonitorInfo& info = buffer.as<MonitorInfo>().front();

    // Initially, there should be no update
    EXPECT_FALSE(buffer.has_update());

    // Write to the buffer
    {
        IPCBuffer::WriteLock lock(buffer);
        info.lo_frequency = 300.75;
    }

    // Now, there should be an update
    EXPECT_TRUE(buffer.has_update());
    // Check again, and it should return false unless another update has occurred
    EXPECT_FALSE(buffer.has_update());
}

// Test multiple writes and updates
TEST_F(IPCBufferTester, MultipleUpdates) {
    IPCBuffer buffer("test_monitoring", 1);

    MonitorInfo& info = buffer.as<MonitorInfo>().front();

    // First update
    {
        IPCBuffer::WriteLock lock(buffer);
        info.lo_frequency = 400.0;
    }
    EXPECT_TRUE(buffer.has_update());

    // Second update
    {
        IPCBuffer::WriteLock lock(buffer);
        info.lo_frequency = 500.0;
    }
    EXPECT_TRUE(buffer.has_update());

    // Ensure no update has happened since the last check
    EXPECT_FALSE(buffer.has_update());
}

// Test synchronization by running concurrent write and read operations
TEST_F(IPCBufferTester, Synchronization) {
    IPCBuffer buffer("test_monitoring", 1);

    MonitorInfo& info = buffer.as<MonitorInfo>().front();

    // Start a thread to simulate a write operation
    std::thread writer([&]() {
        for (int i = 0; i < 5; ++i) {
            IPCBuffer::WriteLock lock(buffer);
            info.lo_frequency = 1000.0 + i;
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
    });

    // Read from the buffer concurrently
    std::thread reader([&]() {
        double last_value = -1.0;
        while (true) {
            {
                IPCBuffer::ReadLock lock(buffer);
                if (info.lo_frequency != last_value) {
                    last_value = info.lo_frequency;
                    std::cout << "Reader: Updated frequency = " << last_value << std::endl;
                }
            }
            if (last_value >= 1004.0) {
                break;
            }
        }
    });

    writer.join();
    reader.join();
}

} // namespace test
} // namespace psrdada_cpp 