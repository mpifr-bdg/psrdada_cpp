#ifndef PSRDADA_CPP_DOUBLE_BUFFER_HPP
#define PSRDADA_CPP_DOUBLE_BUFFER_HPP

namespace psrdada_cpp {

template <typename T>
class DoubleBuffer
{
public:
    DoubleBuffer()

    {
        _a_ptr = &_buf0;
        _b_ptr = &_buf1;
    }


    ~DoubleBuffer(){}

    void resize(std::size_t size)
    {
        _buf0.resize(size);
        _buf1.resize(size);
    }

    void swap()
    {
        std::swap(_a_ptr, _b_ptr);
    }


    T* a() const
    {
        return _a_ptr;
    }

    T* b() const
    {
        return _b_ptr;
    }


private:
    T _buf0;
    T _buf1;
    T* _a_ptr;
    T* _b_ptr;
};

} //namespace psrdada_cpp

#endif //PSRDADA_CPP_DOUBLE_BUFFER_HPP