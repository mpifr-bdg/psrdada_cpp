include(cmake/compiler_settings.cmake)
include(cmake/cuda.cmake)
include(cmake/googletest.cmake)
include(cmake/googlebenchmark.cmake)
include(cmake/psrdada.cmake)
include(cmake/doxygen.cmake)

# Find boost dependencies. DADAFLOW has an addtional dependency to Boost::json
if(BUILD_DADAFLOW)
    set(BOOST_COMPONENTS log program_options system json)
else()
    set(BOOST_COMPONENTS log program_options system)
endif()
find_package(Boost COMPONENTS ${BOOST_COMPONENTS} REQUIRED)

# Set all dependecy libraries required by psrdada_cpp
set(DEPENDENCY_LIBRARIES ${Boost_LIBRARIES} ${PSRDADA_LIBRARIES})
