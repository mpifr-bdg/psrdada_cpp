#include <iomanip>

#include "psrdada_cpp/mk_buffer_view.hpp"
#include "psrdada_cpp/common.hpp"

namespace psrdada_cpp
{
MKDadaBufferView::MKDadaBufferView() {}

void MKDadaBufferView::initialize(std::size_t bufferSize, std::size_t heapSize, std::size_t nSideChannels)
{
    _bufferSize = bufferSize;
    _heapSize = heapSize;
    _nSideChannels = nSideChannels;
    _sideChannelSize = nSideChannels * sizeof(int64_t);
    std::size_t totalHeapSize = _heapSize + _sideChannelSize;
    _nHeaps = _bufferSize / totalHeapSize;
    _gapSize = (_bufferSize - _nHeaps * totalHeapSize);
    _dataBlockBytes = _nHeaps * _heapSize;

    BOOST_LOG_TRIVIAL(info) << "Memory configuration of dada buffer '"
        << std::hex << std::dec << "'\n with " << nSideChannels
        << " side channels items and heapsize " << heapSize << " byte: \n"
        << "  total size of buffer: " << _bufferSize << " byte\n"
        << "  number of heaps per buffer: " << _nHeaps << "\n"
        << "  datablock size in buffer: " << _dataBlockBytes << " byte\n"
        << "  resulting gap: " << _gapSize << " byte\n"
        << "  size of sidechannel data: " << _sideChannelSize << " byte\n";
}


std::size_t MKDadaBufferView::getBufferSize() const
{
  return _bufferSize;
}

std::size_t MKDadaBufferView::getHeapSize() const
{
  return  _heapSize;
}

std::size_t MKDadaBufferView::getNSideChannels() const
{
  return _nSideChannels;
}

std::size_t MKDadaBufferView::sizeOfData() const
{
  return _dataBlockBytes;
}

std::size_t MKDadaBufferView::sizeOfGap() const
{
  return _gapSize;
}

std::size_t MKDadaBufferView::sizeOfSideChannelData() const
{
  return _sideChannelSize * _nHeaps;
}

std::size_t MKDadaBufferView::getNHeaps() const
{
  return _nHeaps;
}

} // namespace
