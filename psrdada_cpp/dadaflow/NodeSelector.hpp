#pragma once
#include "psrdada_cpp/dadaflow/Graph.hpp"
#include <memory>
#include <map>

namespace psrdada_cpp {

/**
 * @brief A wrapper for a set of nodes that are individually selectable
 * 
 * @tparam Noptions   The number of nodes to be supported
 * @tparam InputType  The input type to the nodes
 * @tparam OutputType The output type from the nodes
 *
 * @note Currently only supports single input, single output nodes. All nodes
 *       must share the same input and output types.  
 */
template <int Noptions, typename InputType, typename OutputType>
class NodeSelector
{
public:

    using IngressSwitchType = ProcessExecutor<InputOutputSelector<Noptions, InputType>>;
    using EngressSwitchType = ProcessExecutor<InputOutputSelector<1, OutputType>>; 
    using OptionSelectCallback = std::function<void(void)>;
    using InputPtr = std::shared_ptr<InputType>;
    using OutputPtr = std::shared_ptr<OutputType>;

    NodeSelector(Graph& graph)
    : _graph(graph)
    , _ingress_switch(_graph.add_switch<1, Noptions, InputType>("optionset ingress"))
    , _egress_switch(_graph.add_switch<Noptions, 1, OutputType>("optionset egress"))
    {
        _ingress_switch.template select_inputs<0>();
        _ingress_switch.template select_outputs<0>();
        _egress_switch.template select_outputs<0>();
        _egress_switch.template select_inputs<0>();
    }

    template <int N, typename NodeType>
    void set_option(std::string&& name, NodeType& node)
    {
        _ingress_switch.template output_port<N>().template connect<ThreadSafeQueue>(node.template input_port<0>(), "option queue", 3);
        _egress_switch.template input_port<N>().template connect<ThreadSafeQueue>(node.template output_port<0>(), "option queue", 3);
        _option_select.emplace(std::move(name), [this](){
            _egress_switch.template select_inputs<N>();
            _ingress_switch.template select_outputs<N>();
        });
    }

    void select_option(std::string const& name)
    {
        _option_select.at(name)();
    }

    void connect_input(OutputPort<InputPtr>& port)
    {
        _ingress_switch.template input_port<0>().template connect<ThreadSafeQueue>(port, "option ingress", 3);
    }

    void connect_output(InputPort<OutputPtr>& port)
    {
        _egress_switch.template output_port<0>().template connect<ThreadSafeQueue>(port, "option egress", 3);
    }


private:
    Graph& _graph;
    IngressSwitchType& _ingress_switch;
    EngressSwitchType& _egress_switch;
    std::map<std::string, OptionSelectCallback> _option_select;
};

} // namespace psrdada_cpp