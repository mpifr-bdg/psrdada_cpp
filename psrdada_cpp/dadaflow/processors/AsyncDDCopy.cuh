#pragma once
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/dadaflow/CudaStream.cuh"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <iostream>
#include <thrust/copy.h>
#include <tuple>

namespace psrdada_cpp
{

template <
    typename InputType,
    typename OutputType,
    typename ResourceAllocator,
    typename = std::enable_if_t<
        is_same_dimensions<InputType, OutputType>::value &&
        std::is_same_v<typename ResourceAllocator::OutputType, OutputType>>>
class AsyncDDCopy
{
  public:
    using InputPtr  = std::shared_ptr<InputType>;
    using OutputPtr = std::shared_ptr<OutputType>;
    using Output    = OutputPtr;

  public:
    AsyncDDCopy(ResourceAllocator& allocator): _allocator(allocator) {}

    AsyncDDCopy(AsyncDDCopy const&)            = delete;
    AsyncDDCopy& operator=(AsyncDDCopy const&) = delete;
    AsyncDDCopy(AsyncDDCopy&& other)           = default;

    ~AsyncDDCopy() {}

    Output operator()(InputPtr source)
    {
        BOOST_LOG_TRIVIAL(debug) << "AsyncDDCopy operator called";
        BOOST_LOG_TRIVIAL(debug) << "AsyncDDCopy input: " << source->describe();
        OutputPtr dest = _allocator.get();
        dest->like(*source);
        BOOST_LOG_TRIVIAL(debug) << "AsyncDDCopy output: " << dest->describe();
        BOOST_LOG_TRIVIAL(debug)
            << "AsyncDDCopy perfoming copy of kind: " << kind();
        BOOST_LOG_TRIVIAL(debug)
            << "Input ptr: "
            << static_cast<void*>(thrust::raw_pointer_cast(dest->data()));
        BOOST_LOG_TRIVIAL(debug)
            << "Output ptr: "
            << static_cast<void const*>(
                   thrust::raw_pointer_cast(source->data()));
        BOOST_LOG_TRIVIAL(debug)
            << "Size: "
            << source->size() * sizeof(typename InputType::value_type);
        BOOST_LOG_TRIVIAL(debug) << "Kind: " << kind();
        BOOST_LOG_TRIVIAL(debug) << "Stream: " << _stream.stream();
        BOOST_LOG_TRIVIAL(debug) << source->size() << ", " << dest->size();
        CUDA_ERROR_CHECK(cudaMemcpyAsync(
            static_cast<void*>(thrust::raw_pointer_cast(dest->data())),
            static_cast<void const*>(thrust::raw_pointer_cast(source->data())),
            source->size() * sizeof(typename InputType::value_type),
            kind(),
            _stream.stream()));
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream.stream()));
        BOOST_LOG_TRIVIAL(debug) << "AsyncDDCopy copy complete";
        return dest;
    }

  private:
    template <typename>
    struct always_false: std::false_type {
    };

    static constexpr cudaMemcpyKind kind()
    {
        using IC = typename InputType::ContainerType;
        using OC = typename OutputType::ContainerType;
        if constexpr(is_host_container<IC>::value &&
                     is_device_container<OC>::value) {
            return cudaMemcpyHostToDevice;
        } else if constexpr(is_device_container<IC>::value &&
                            is_host_container<OC>::value) {
            return cudaMemcpyDeviceToHost;
        } else if constexpr(is_device_container<IC>::value &&
                            is_device_container<OC>::value) {
            return cudaMemcpyDeviceToDevice;
        } else if constexpr(is_host_container<IC>::value &&
                            is_host_container<OC>::value) {
            return cudaMemcpyHostToHost;
        } else {
            static_assert(always_false<InputType>::value,
                          "Unsupported copy type");
        }
    }
    ResourceAllocator& _allocator;
    CudaStream _stream;
};

} // namespace psrdada_cpp