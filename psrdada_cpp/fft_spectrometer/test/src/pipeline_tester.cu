#include "psrdada_cpp/fft_spectrometer/test/pipeline_tester.cuh"

namespace psrdada_cpp {
namespace fft_spectrometer {
namespace test {

PipelineTester::PipelineTester()
    : ::testing::TestWithParam<test_config>(),
    _config(GetParam())
{

}

PipelineTester::~PipelineTester()
{

}

void PipelineTester::SetUp()
{
}

void PipelineTester::TearDown()
{
}

void PipelineTester::performance_test()
{
    std::size_t input_block_bytes = _config.tscrunch * _config.fft_length * _config.nsamps_out * _config.nbits/8;

    DoublePinnedHostBuffer<char> input_block;
    input_block.resize(input_block_bytes);
    RawBytes input_raw_bytes(input_block.a_ptr(), input_block_bytes, input_block_bytes);
    std::vector<char> header_block(4096);
    RawBytes header_raw_bytes(header_block.data(), 4096, 4096);
    NullSink null_sink;
    Pipeline<NullSink> spectrometer(input_block_bytes, _config.fft_length, _config.tscrunch, _config.nbits, null_sink);
    spectrometer.init(header_raw_bytes);
    for (int ii = 0; ii < 100; ++ii)
    {
        spectrometer(input_raw_bytes);
    }
}


TEST_P(PipelineTester, simple_exec_test)
{
    performance_test();
}

INSTANTIATE_TEST_SUITE_P (ParameterizedTest, PipelineTester, testing::Values(
    //           fft_length | nacc | nbits | nheaps_block | msg
    test_config({1024, 16, 128, 12}),
    test_config({1024, 16, 128, 8}),
    test_config({1024, 16, 128, 10})
));


} //namespace test
} //namespace fft_spectrometer
} //namespace psrdada_cpp
