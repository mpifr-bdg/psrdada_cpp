#pragma once
#include "psrdada_cpp/dadaflow/Graph.hpp"
#include "psrdada_cpp/dadaflow/Process.hpp"
#include "psrdada_cpp/dadaflow/ResourcePool.hpp"
#include "psrdada_cpp/dadaflow/ThreadSafeQueue.hpp"
#include "psrdada_cpp/dadaflow/processors/JunkSource.hpp"
#include "psrdada_cpp/dadaflow/processors/Multiplexer.hpp"
#include "psrdada_cpp/dadaflow/processors/NullSink.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

namespace psrdada_cpp
{

template <typename T>
class DemoPipeline
{
  public:
    using VectorType = std::vector<T>;
    using VectorPtr = std::shared_ptr<std::vector<T>>;
    using SourceType = JunkSource<VectorType, ResourcePool<VectorType>>;
    using ElementWiseMultiplyType =
        ElementWiseMultiply<VectorType, VectorType, VectorType>;
    using SinkType          = NullSink;
    using MulOutMultiplexer = Multiplexer<2, VectorType>;
    using RateReducerType   = RateReducer<10, VectorType>;
    using RateIncreaserType = RateIncreaser<10, VectorType>;

  public:
    DemoPipeline(): input_pool1(3), input_pool2(3)
    {
        auto& source0 = _graph.add_node(SourceType(input_pool1,
                                   [](auto ptr) {
                                       if(ptr->empty()) {
                                           ptr->push_back(0);
                                       } else {
                                           ptr->push_back(ptr->back() + 1);
                                       }
                                   }),
                        "source block");
        auto& source1 = _graph.add_node(SourceType(input_pool2,
                                   [](auto ptr) {
                                       if(ptr->empty()) {
                                           ptr->push_back(0);
                                       } else {
                                           ptr->push_back(ptr->back() + 1);
                                       }
                                   }),
                        "source block");
        auto& multiply = _graph.add_node(ElementWiseMultiplyType(),
                        "vector multiplier");
        auto& multiplex = _graph.add_node(MulOutMultiplexer(),
                        "multiplexer");
        auto& reducer = _graph.add_node(RateReducerType(),
                                       "rate reducer");
        // Because SinkType (which is an alias of NullSink) has an overloaded operator()
        // it is necessary to explicitly tell the compiler how this will be called
        // by using the invocation_traits helper. This helper allows the function traits
        // to be determined for an explicit overload call.                                       
        auto& sink0 = _graph.add_node<SinkType, invocation_traits<SinkType, VectorPtr>>(SinkType(),
                            "sink block");
        auto& increaser = _graph.add_node(RateIncreaserType(),
                       "rate increaser");
        auto& sink1 = _graph.add_node([](VectorPtr) -> void {},
                       "sink block");

        _graph.connect<0, 0>(source0, multiply, "source0->multiply", 3);
        _graph.connect<0, 1>(source1, multiply, "source1->multiply", 3);
        _graph.connect<0, 0>(multiply, multiplex, "multiply->multiplex", 3);
        _graph.connect<0, 0>(multiplex, reducer, "multiplex->reducer", 3);
        _graph.connect<1, 0>(multiplex, increaser, "multiplex->increaser", 3);
        _graph.connect<0, 0>(reducer, sink0, "reducer->sink0", 3);
        _graph.connect<0, 0>(increaser, sink1, "increaser->sink1", 3);
        _graph.prepare();
    }

    Graph& graph() { return _graph; }

  private:
    Graph _graph;
    ResourcePool<VectorType> input_pool1;
    ResourcePool<VectorType> input_pool2;
};

} // namespace psrdada_cpp