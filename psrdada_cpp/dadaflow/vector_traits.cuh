#pragma once
#include <iostream>
#include <type_traits>

namespace
{

#define MAKE_VECTOR_TRAITS(TYPE, PREFIX) \
    template <>                          \
    struct vector_type<TYPE, 1> {        \
        using type = PREFIX##1;          \
    };                                   \
                                         \
    template <>                          \
    struct vector_type<TYPE, 2> {        \
        using type = PREFIX##2;          \
    };                                   \
                                         \
    template <>                          \
    struct vector_type<TYPE, 3> {        \
        using type = PREFIX##3;          \
    };                                   \
                                         \
    template <>                          \
    struct vector_type<TYPE, 4> {        \
        using type = PREFIX##4;          \
    };

#define EXPAND_OPERATOR(OPERATOR) OPERATOR

#define MAKE_OPERATORS_VECTOR_VECTOR_INPLACE(OPERATOR)      \
    template <typename T, typename X>                       \
        requires VecN<T> && VecN<X>                         \
    __host__ __device__ inline T& operator EXPAND_OPERATOR( \
        OPERATOR)(T & lhs, const X & rhs)                   \
    {                                                       \
        lhs.x EXPAND_OPERATOR(OPERATOR) rhs.x;              \
        if constexpr(HasY<T>) {                             \
            lhs.y EXPAND_OPERATOR(OPERATOR) rhs.y;          \
        }                                                   \
        if constexpr(HasZ<T>) {                             \
            lhs.z EXPAND_OPERATOR(OPERATOR) rhs.z;          \
        }                                                   \
        if constexpr(HasW<T>) {                             \
            lhs.w EXPAND_OPERATOR(OPERATOR) rhs.w;          \
        }                                                   \
        return lhs;                                         \
    };

#define MAKE_OPERATORS_VECTOR_VECTOR_OUTOFPLACE(OPERATOR)                      \
    template <typename T, typename X>                                          \
        requires VecN<T> && VecN<X>                                            \
    __host__ __device__ inline T operator EXPAND_OPERATOR(                     \
        OPERATOR)(const T& lhs, const X& rhs)                                  \
    {                                                                          \
        T r;                                                                   \
        r.x = static_cast<value_type_t<T>>(lhs.x EXPAND_OPERATOR(OPERATOR)     \
                                               rhs.x);                         \
        if constexpr(HasY<T>) {                                                \
            r.y = static_cast<value_type_t<T>>(lhs.y EXPAND_OPERATOR(OPERATOR) \
                                                   rhs.y);                     \
        }                                                                      \
        if constexpr(HasZ<T>) {                                                \
            r.z = static_cast<value_type_t<T>>(lhs.z EXPAND_OPERATOR(OPERATOR) \
                                                   rhs.z);                     \
        }                                                                      \
        if constexpr(HasW<T>) {                                                \
            r.w = static_cast<value_type_t<T>>(lhs.w EXPAND_OPERATOR(OPERATOR) \
                                                   rhs.w);                     \
        }                                                                      \
        return r;                                                              \
    };

#define MAKE_OPERATORS_VECTOR_SCALAR_INPLACE(OPERATOR)      \
    template <typename T, typename X>                       \
        requires VecN<T> && std::is_arithmetic_v<X>         \
    __host__ __device__ inline T& operator EXPAND_OPERATOR( \
        OPERATOR)(T & lhs, const X & rhs)                   \
    {                                                       \
        lhs.x EXPAND_OPERATOR(OPERATOR) rhs;                \
        if constexpr(HasY<T>) {                             \
            lhs.y EXPAND_OPERATOR(OPERATOR) rhs;            \
        }                                                   \
        if constexpr(HasZ<T>) {                             \
            lhs.z EXPAND_OPERATOR(OPERATOR) rhs;            \
        }                                                   \
        if constexpr(HasW<T>) {                             \
            lhs.w EXPAND_OPERATOR(OPERATOR) rhs;            \
        }                                                   \
        return lhs;                                         \
    };

#define MAKE_OPERATORS_VECTOR_SCALAR_OUTOFPLACE(OPERATOR)                      \
    template <typename T, typename X>                                          \
        requires VecN<T> && std::is_arithmetic_v<X>                            \
    __host__ __device__ inline T operator EXPAND_OPERATOR(                     \
        OPERATOR)(const T& lhs, const X& rhs)                                  \
    {                                                                          \
        T r;                                                                   \
        r.x =                                                                  \
            static_cast<value_type_t<T>>(lhs.x EXPAND_OPERATOR(OPERATOR) rhs); \
        if constexpr(HasY<T>) {                                                \
            r.y = static_cast<value_type_t<T>>(lhs.y EXPAND_OPERATOR(OPERATOR) \
                                                   rhs);                       \
        }                                                                      \
        if constexpr(HasZ<T>) {                                                \
            r.z = static_cast<value_type_t<T>>(lhs.z EXPAND_OPERATOR(OPERATOR) \
                                                   rhs);                       \
        }                                                                      \
        if constexpr(HasW<T>) {                                                \
            r.w = static_cast<value_type_t<T>>(lhs.w EXPAND_OPERATOR(OPERATOR) \
                                                   rhs);                       \
        }                                                                      \
        return r;                                                              \
    };

#define MAKE_INPLACE_OPERATORS(OPERATOR)           \
    MAKE_OPERATORS_VECTOR_VECTOR_INPLACE(OPERATOR) \
    MAKE_OPERATORS_VECTOR_SCALAR_INPLACE(OPERATOR)

#define MAKE_OUTOFPLACE_OPERATORS(OPERATOR)           \
    MAKE_OPERATORS_VECTOR_VECTOR_OUTOFPLACE(OPERATOR) \
    MAKE_OPERATORS_VECTOR_SCALAR_OUTOFPLACE(OPERATOR)

} // namespace

// Concept to check if a type T has a member named "x"
template <typename T>
concept HasX = requires(T t) {
    { t.x } -> std::convertible_to<decltype(t.x)>;
};

// Concept to check if a type T has a member named "y"
template <typename T>
concept HasY = requires(T t) {
    { t.y } -> std::convertible_to<decltype(t.y)>;
};

// Concept to check if a type T has a member named "z"
template <typename T>
concept HasZ = requires(T t) {
    { t.z } -> std::convertible_to<decltype(t.z)>;
};

// Concept to check if a type T has a member named "w"
template <typename T>
concept HasW = requires(T t) {
    { t.w } -> std::convertible_to<decltype(t.w)>;
};

// Vec1 concept: requires x, but not y, z or w
template <typename T>
concept Vec1 = HasX<T> && !HasY<T> && !HasZ<T> && !HasW<T>;

// Vec2 concept: requires x and y, but not z or w
template <typename T>
concept Vec2 = HasX<T> && HasY<T> && !HasZ<T> && !HasW<T>;

// Vec3 concept: requires x, y, z but not w
template <typename T>
concept Vec3 = HasX<T> && HasY<T> && HasZ<T> && !HasW<T>;

// Vec4 concept: requires x, y, z, and w
template <typename T>
concept Vec4 = HasX<T> && HasY<T> && HasZ<T> && HasW<T>;

template <typename T>
concept VecN = Vec1<T> || Vec2<T> || Vec3<T> || Vec4<T>;

template <typename T>
struct vector_size {
    static constexpr int value = [] {
        if constexpr(Vec1<T>)
            return 1;
        else if constexpr(Vec2<T>)
            return 2;
        else if constexpr(Vec3<T>)
            return 3;
        else if constexpr(Vec4<T>)
            return 4;
    }();
};

template <typename T>
static constexpr int vector_size_v = vector_size<T>::value;

template <typename T, int S>
struct vector_type;

template <typename T>
struct vector_type<T, 1> {
    using type = T;
};

template <typename T>
struct value_type {
    using type = decltype(T::x);
};

MAKE_VECTOR_TRAITS(signed char, char)
MAKE_VECTOR_TRAITS(unsigned char, uchar)
MAKE_VECTOR_TRAITS(short, short)
MAKE_VECTOR_TRAITS(unsigned short, ushort)
MAKE_VECTOR_TRAITS(int, int)
MAKE_VECTOR_TRAITS(unsigned int, uint)
MAKE_VECTOR_TRAITS(long int, long)
MAKE_VECTOR_TRAITS(unsigned long int, ulong)
MAKE_VECTOR_TRAITS(long long int, longlong)
MAKE_VECTOR_TRAITS(unsigned long long int, ulonglong)
MAKE_VECTOR_TRAITS(float, float)
MAKE_VECTOR_TRAITS(double, double)

template <typename T, int S>
using vector_type_t = vector_type<T, S>::type;

template <typename T>
using value_type_t = value_type<T>::type;

template <typename SrcType, typename TargetType>
struct equivalent_type {
    using type = vector_type<TargetType, vector_size<SrcType>::value>::type;
};

template <typename SrcType, typename TargetType>
using equivalent_type_t = equivalent_type<SrcType, TargetType>::type;

template <typename T>
auto constexpr format_element(T val)
{
    if constexpr(std::is_same_v<decltype(val), signed char> ||
                 std::is_same_v<decltype(val), char>) {
        return static_cast<int>(val);
    } else if constexpr(std::is_same_v<decltype(val), unsigned char>) {
        return static_cast<unsigned int>(val);
    } else {
        return val;
    }
}

template <typename T>
    requires VecN<T>
inline std::ostream& operator<<(std::ostream& stream, T const& val)
{
    stream << "(" << format_element(val.x);
    if constexpr(HasY<T>) {
        stream << ", " << format_element(val.y);
    }
    if constexpr(HasZ<T>) {
        stream << ", " << format_element(val.z);
    }
    if constexpr(HasW<T>) {
        stream << ", " << format_element(val.w);
    }
    stream << ")";
    return stream;
}

MAKE_INPLACE_OPERATORS(+=)
MAKE_INPLACE_OPERATORS(-=)
MAKE_INPLACE_OPERATORS(*=)
MAKE_INPLACE_OPERATORS(/=)
MAKE_INPLACE_OPERATORS(%=)
MAKE_OUTOFPLACE_OPERATORS(+)
MAKE_OUTOFPLACE_OPERATORS(-)
MAKE_OUTOFPLACE_OPERATORS(*)
MAKE_OUTOFPLACE_OPERATORS(/)
MAKE_OUTOFPLACE_OPERATORS(%)

template <typename U, typename T>
    requires std::is_arithmetic_v<T> && std::is_arithmetic_v<U>
__host__ __device__ static inline U clamp(T const& value)
{
    return static_cast<U>(
        fmaxf(static_cast<float>(std::numeric_limits<U>::lowest()),
              fminf(static_cast<float>(std::numeric_limits<U>::max()),
                    static_cast<float>(value))));
}

template <typename U, typename T>
    requires VecN<U> && VecN<T>
__host__ __device__ static inline U clamp(T const& value)
{
    U clamped;
    clamped.x = clamp<typename value_type<U>::type, value_type_t<T>>(value.x);
    if constexpr(HasY<T>) {
        clamped.y =
            clamp<typename value_type<U>::type, value_type_t<T>>(value.y);
    }
    if constexpr(HasZ<T>) {
        clamped.z =
            clamp<typename value_type<U>::type, value_type_t<T>>(value.z);
    }
    if constexpr(HasW<T>) {
        clamped.w =
            clamp<typename value_type<U>::type, value_type_t<T>>(value.w);
    }
    return clamped;
}
