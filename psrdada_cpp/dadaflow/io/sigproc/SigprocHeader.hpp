#pragma once
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include <iostream>
#include <string>
#include <optional>
#include <cstdint>

namespace psrdada_cpp {
namespace sigproc {

    static const BiDirectonalMap<uint32_t, std::string_view> telescopes{
        {0, "Fake"},
        {1, "Arecibo"},
        {2, "Ooty"},
        {3, "Nancay"},
        {4, "Parkes"},
        {5, "Jodrell"},
        {6, "GBT"},
        {7, "GMRT"},
        {8, "Effelsberg"},
        {9, "Effelsberg LOFAR"},
        {10, "SRT"},
        {12, "VLA"},
        {20, "CHIME"},
        {64, "MeerKAT"},
        {65, "KAT-7"},
        {82, "eMerlin"}
    };

    static const BiDirectonalMap<uint32_t, std::string_view> backends{
        {0, "FAKE"},
        {1, "PSPM"},
        {2, "WAPP"},
        {3, "AOFTM"},
        {4, "BPP"},
        {5, "OOTY"},
        {6, "SCAMP"},
        {7, "GMRTFB"},
        {8, "PULSAR2000"},
        {9, "PARSPEC"},
        {10, "BPSR"},
        {11, "COBALT"},
        {14, "GMRTNEW"},
        {20, "CHIME"},
        {64, "KAT"},
        {65, "KAT-DC2"},
        {82, "loft-e"},
        {83, "ROACH"},
        {88, "AT-PAF"}
    };

    static const BiDirectonalMap<uint32_t, std::string_view> data_types{
        {0, "raw data"},
        {1, "filterbank"},
        {2, "time series"},
        {3, "pulse profiles"},
        {4, "amplitude spectrum"},
        {5, "complex spectrum"},
        {6, "dedispersed subbands"}
    };

/**
 * @class SigprocHeader
 * @brief Represents a header for SIGPROC data format, providing accessors for various parameters.
 *
 * The SigprocHeader class encapsulates the metadata associated with SIGPROC data files,
 * including both required and optional parameters. It provides methods to serialize
 * and deserialize the header information to and from streams.
 *
 * Required parameters include source name, right ascension, declination, frequency
 * information, data type, machine ID, and telescope ID. Optional parameters cover
 * additional metadata such as azimuth and zenith angles, reference DM, and beam information.
 *
 * The class also supports bidirectional mapping of telescope, backend, and data type IDs
 * to their respective string representations.
 */
class SigprocHeader
{
public:
    SigprocHeader() = default;
    ~SigprocHeader() = default;

    /**
     * @brief Write the header in SIGPROC format to a stream
     * 
     * @param stream A generic output stream
     * @return std::size_t The size of the written header in bytes
     */
    std::size_t to_stream(std::ostream& stream) const;

    /**
     * @brief Read a SIGPROC format header from a stream
     * 
     * @param stream A generic input stream
     * @return std::size_t The size of the read header in bytes
     */
    std::size_t from_stream(std::istream& stream);

    // Accessors for required parameters (getters and setters)
    std::string const& source_name() const { return _source_name; }
    void source_name(std::string const& source_name) { _source_name = source_name; }

    // Note these are in standard SIGPROC format, e.g. 12:12:23.045 -> 121223.045
    double src_raj() const { return _src_raj; }
    void src_raj(double src_raj) { _src_raj = src_raj; }

    double src_dej() const { return _src_dej; }
    void src_dej(double src_dej) { _src_dej = src_dej; }

    double fch1() const { return _fch1; }
    void fch1(double fch1) { _fch1 = fch1; }

    double foff() const { return _foff; }
    void foff(double foff) { _foff = foff; }

    double tsamp() const { return _tsamp; }
    void tsamp(double tsamp) { _tsamp = tsamp; }

    double tstart() const { return _tstart; }
    void tstart(double tstart) { _tstart = tstart; }

    uint32_t data_type() const { return _data_type; }
    void data_type(uint32_t data_type) { _data_type = data_type; }

    uint32_t machine_id() const { return _machine_id; }
    void machine_id(uint32_t machine_id) { _machine_id = machine_id; }

    uint32_t nbits() const { return _nbits; }
    void nbits(uint32_t nbits) { _nbits = nbits; }

    uint32_t nchans() const { return _nchans; }
    void nchans(uint32_t nchans) { _nchans = nchans; }

    uint32_t nifs() const { return _nifs; }
    void nifs(uint32_t nifs) { _nifs = nifs; }

    uint32_t telescope_id() const { return _telescope_id; }
    void telescope_id(uint32_t telescope_id) { _telescope_id = telescope_id; }

    // Accessors for optional parameters (getters and setters)
    std::optional<std::string> const& rawfile() const { return _rawfile; }
    void rawfile(std::string const& rawfile) { _rawfile = rawfile; }

    std::optional<double> const& az_start() const { return _az_start; }
    void az_start(double const& az_start) { _az_start = az_start; }

    std::optional<double> const& za_start() const { return _za_start; }
    void za_start(double const& za_start) { _za_start = za_start; }

    std::optional<double> const& refdm() const { return _refdm; }
    void refdm(double const& refdm) { _refdm = refdm; }

    std::optional<uint32_t> const& barycentric() const { return _barycentric; }
    void barycentric(uint32_t const& barycentric) { _barycentric = barycentric; }

    std::optional<uint32_t> const& pulsarcentric() const { return _pulsarcentric; }
    void pulsarcentric(uint32_t const& pulsarcentric) { _pulsarcentric = pulsarcentric; }

    std::optional<uint32_t> const& ibeam() const { return _ibeam; }
    void ibeam(uint32_t const& ibeam) { _ibeam = ibeam; }

    std::optional<uint32_t> const& nbeams() const { return _nbeams; }
    void nbeams(uint32_t const& nbeams) { _nbeams = nbeams; }

    std::optional<unsigned char> const& is_signed() const { return _is_signed; }
    void is_signed(unsigned char const& signed_) { _is_signed = signed_; }

private:
    // Private member variables (same as original)
    std::string _source_name   = "unset";
    double _src_raj        = 0.0; // source right ascension
    double _src_dej        = 0.0; // source declination
    double _fch1           = 0.0; // frequency of the top channel in MHz
    double _foff           = 0.0; // channel bandwidth in MHz
    double _tsamp          = 0.0; // sampling time in seconds
    double _tstart         = 0.0; // observation start time in MJD format
    uint32_t _data_type    = 0;   // data type ID
    uint32_t _machine_id   = 0;   // machine ID 0 = FAKE
    uint32_t _nbits        = 0;   // number of bits per sample
    uint32_t _nchans       = 0;   // number of frequency channels
    uint32_t _nifs         = 0;   // number of ifs (pols)
    uint32_t _telescope_id = 0;   // telescope ID

    // Optional parameters
    std::optional<std::string> _rawfile    = std::nullopt;
    std::optional<double> _az_start        = std::nullopt; // azimuth angle in deg
    std::optional<double> _za_start        = std::nullopt; // zenith angle in deg
    std::optional<double> _refdm           = std::nullopt; // reference DM
    std::optional<uint32_t> _barycentric   = std::nullopt; // barycentric flag
    std::optional<uint32_t> _pulsarcentric = std::nullopt; // pulsarcentric flag
    std::optional<uint32_t> _ibeam         = std::nullopt; // beam number
    std::optional<uint32_t> _nbeams        = std::nullopt; // number of beams
    std::optional<uint8_t> _is_signed      = std::nullopt; // signedness

    void header_write(std::ostream& stream, std::string const& str) const;
    void header_write(std::ostream& stream, std::string const& str, std::string const& name) const;
    std::string read_string(std::istream& stream) const;

    template <typename T>
    void header_write(std::ostream& stream, std::string const& name, T val) const;

    template <typename T>
    T read_value(std::istream& stream) const;
};

std::ostream& operator<<(std::ostream& os, const SigprocHeader& header);

} // namespace sigproc
} // namespace psrdada_cpp

#include "psrdada_cpp/dadaflow/io/sigproc/detail/SigprocHeader.cpp"