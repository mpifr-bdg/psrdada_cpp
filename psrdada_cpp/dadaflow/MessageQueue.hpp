#pragma once
#include "boost/json.hpp"
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <atomic>
#include <functional>
#include <map>
#include <mqueue.h>
#include <stdexcept>
#include <string>
#include <sys/types.h>
#include <thread>
#include <vector>

namespace js = boost::json;

namespace psrdada_cpp
{

inline auto check_mqueue_call(auto call)
{
    auto result = call;
    if(result == -1) {
        switch(errno) {
        case EAGAIN:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Resource temporarily unavailable");
        case EINVAL:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Invalid argument");
        case EMSGSIZE:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Message too long");
        case EEXIST:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Message queue already exists");
        case ENOENT:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Message queue does not exist");
        case EBADF:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Bad file descriptor");
        case EINTR:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Operation interrupted");
        case EPERM:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Operation not permitted");
        case ETIMEDOUT:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Operation timed out");
        default:
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Unknown message queue error");
        }
    }
    return result;
}

class TimeoutException: public std::runtime_error
{
  public:
    explicit TimeoutException(const std::string& message)
        : std::runtime_error(message)
    {
    }
};

class NoCallbackRegistered: public std::runtime_error
{
  public:
    explicit NoCallbackRegistered(const std::string& message)
        : std::runtime_error(message)
    {
    }
};

/**
 * @brief Makes a future timespec to be used as a timeout on mq calls.
 *
 * @param timeout_s Timeout in seconds.
 * @return timespec A timespec object referencing timeout_s seconds in the
 * future
 */
timespec make_timeout(float timeout_s)
{
    timespec ts{};
    clock_gettime(CLOCK_REALTIME, &ts);
    float whole_seconds = 0.0f;
    float frac_seconds  = std::modf(timeout_s, &whole_seconds);
    ts.tv_sec += static_cast<int>(whole_seconds);
    ts.tv_nsec += static_cast<long>(frac_seconds * 1e9);
    // Normalize tv_nsec to ensure it's within the valid range [0, 999999999]
    if(ts.tv_nsec >= 1000000000L) {
        ts.tv_sec += ts.tv_nsec / 1000000000L; // Add overflow seconds to tv_sec
        ts.tv_nsec = ts.tv_nsec % 1000000000L; // Reduce tv_nsec to valid range
    }
    return ts;
}

struct KeyDescriptor
{
    std::string name;
    std::string type;
    std::string description;
};

/**
 * @brief A wrapper for handler callbacks
 *
 * @details Callbacks take a JSON object with parameters and
            return a JSON object with any return information
 */
class CallbackWrapper
{
  public:
    using CallbackType = std::function<js::object(js::object const&)>;

    /**
     * @brief Construct a new Callback Wrapper object
     *
     * @param required_keys The set of JSON keys required by this callback
     * @param description A description of what the callback does
     * @param callback The callback object
     */
    CallbackWrapper(std::initializer_list<KeyDescriptor> required_keys,
                    std::string_view description,
                    CallbackType callback)
        : _required_keys(std::move(required_keys)),
          _description(std::move(description)), _callback(callback)
    {
    }

    /**
     * @brief Invoke the callback
     *
     * @param json_obj A JSON object containing the required keys
     * @return js::object
     */
    js::object operator()(js::object const& json_obj)
    {
        validate(json_obj);
        return _callback(json_obj);
    }

    /**
     * @brief Create a JSON block containing help information for this callback
     *
     * @return js::object
     */
    js::object help() const
    {
        js::object help;
        js::object required_keys_json;
        for(const auto& key_descriptor: _required_keys) {
            js::object key_json;
            key_json["type"] = key_descriptor.type;
            key_json["description"] = key_descriptor.description;
            required_keys_json[key_descriptor.name] = std::move(key_json);
        }
        help["required_keys"] = std::move(required_keys_json);
        help["description"]   = _description;
        return help;
    }

  private:
    void validate(js::object const& json_obj)
    {
        std::vector<std::string> missing_keys;
        for(auto const& key: _required_keys) {
            if(json_obj.find(key.name) == json_obj.end()) {
                missing_keys.push_back(key.name);
            }
        }
        if(missing_keys.size() > 0) {
            std::stringstream msg;
            msg << "Missing required keys: " << missing_keys;
            throw std::runtime_error(msg.str());
        }
    }

    std::vector<KeyDescriptor> _required_keys;
    std::string _description;
    CallbackType _callback;
};

/**
 * @brief A registry of callbacks that can be dispatched by name
 *
 */
class CallbackRegistry
{
  public:
    using CallbackType = typename CallbackWrapper::CallbackType;

    /**
     * @brief Register a new callback
     *
     * @param command The name of this callback, to be used for dispatching
     * @param required_keys The set of JSON keys required by this callback
     * @param description A description of the callback behaviour
     * @param callback The callback object
     *
     * @note Callbacks must have unique command names. If a callback already
     * exists with the requested name an exception will be thrown.
     */
    void register_callback(std::string_view command,
                           std::initializer_list<KeyDescriptor> required_keys,
                           std::string_view description,
                           CallbackType callback)
    {
        std::string key{command};
        if(_callbacks.find(key) != _callbacks.end()) {
            throw std::runtime_error(
                std::string{"A handler for command is already set: " + key});
        }
        _callbacks.emplace(
            key,
            std::move(CallbackWrapper(required_keys, description, callback)));
    }

    /**
     * @brief Invoke the callback for the given command
     *
     * @param command The callback name to invoke
     * @param json_args A JSON object containing parameters for the callback
     * @return js::object
     */
    js::object invoke_callback(std::string const& command,
                               js::object const& json_args)
    {
        if(_callbacks.find(command) != _callbacks.end()) {
            return _callbacks.at(command)(json_args);
        } else {
            throw NoCallbackRegistered("No callback registered");
        }
    }

    /**
     * @brief Create a JSON help message for all callbacks
     *
     * @return js::object
     */
    js::object help()
    {
        js::object help;
        for(auto const& [command, cb]: _callbacks) {
            help[command] = cb.help();
        }
        return help;
    }

  private:
    std::map<std::string, CallbackWrapper> _callbacks;
};

/**
 * @brief A wrapper for POSIX message queues
 *
 */
class PosixMessageQueue
{
  public:
    /**
     * @brief Construct a new Posix Message Queue object
     *
     * @param name The name of the queue (must start with /)
     * @param create If true create a new queue, else join an existing queue
     * @param max_msg_size The maximum message size in bytes
     */
    PosixMessageQueue(const std::string& name,
                      bool create         = true,
                      size_t max_msg_size = 1024)
        : _name(name), _queue(-1), _max_msg_size(max_msg_size), _running(true)
    {
        struct mq_attr attr;
        attr.mq_flags   = 0;
        attr.mq_maxmsg  = 10;           // Max number of messages in queue
        attr.mq_msgsize = max_msg_size; // Max size of each message
        attr.mq_curmsgs = 0;

        if(create) {
            BOOST_LOG_TRIVIAL(debug) << "Creating message queue: " << _name;
            mq_unlink(
                _name.c_str()); // unlink a previous queue, allowed to fail.
            _queue = check_mqueue_call(
                mq_open(_name.c_str(), O_CREAT | O_RDWR, 0644, &attr));
        } else {
            BOOST_LOG_TRIVIAL(debug) << "Attaching to message queue: " << _name;
            _queue = check_mqueue_call(mq_open(_name.c_str(), O_RDWR));
        }

        if(_queue == (mqd_t)-1) {
            throw std::runtime_error("Failed to open/create message queue");
        }
    }

    PosixMessageQueue(PosixMessageQueue const&)            = delete;
    PosixMessageQueue& operator=(PosixMessageQueue const&) = delete;
    PosixMessageQueue(PosixMessageQueue&&)                 = delete;
    PosixMessageQueue& operator=(PosixMessageQueue&&)      = delete;

    /**
     * @brief Destroy the Posix Message Queue object
     *
     * @details If the queue exists, close it and unlink
     */
    ~PosixMessageQueue()
    {
        // Ensure valid descriptor before closing and unlinking
        if(_queue != static_cast<mqd_t>(-1)) {
            mq_close(_queue);
            mq_unlink(_name.c_str());
        }
    }

    /**
     * @brief Send a message to the queue
     *
     * @param msg A string message.
     */
    void send(const std::string& msg)
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if(msg.size() > _max_msg_size) {
            throw std::runtime_error("Message exceeds maximum size");
        }
        check_mqueue_call(mq_send(_queue, msg.c_str(), msg.size(), 0));
    }

    /**
     * @brief Receive a message from the queue (blocking)
     *
     * @return std::string the received message
     */
    std::string recv()
    {
        std::lock_guard<std::mutex> lock(_mutex);
        std::string buffer(_max_msg_size, ' ');
        ssize_t bytes_read = check_mqueue_call(
            mq_receive(_queue, buffer.data(), _max_msg_size, nullptr));
        if(bytes_read >= 0) {
            buffer.resize(bytes_read);
            return buffer;
        } // Note: bytes_read == 0 should not be possible.
        throw std::runtime_error("Error receiving message");
    }

    /**
     * @brief Receive a message from the queue (blocking with timeout)
     *
     * @param timeout_s Timeout in seconds
     * @return std::string the received message
     */
    std::string recv(float timeout_s)
    {
        std::lock_guard<std::mutex> lock(_mutex);
        std::string buffer(_max_msg_size, ' ');
        ssize_t bytes_read = 0;
        timespec ts        = make_timeout(timeout_s);
        try {
            bytes_read = check_mqueue_call(mq_timedreceive(_queue,
                                                           buffer.data(),
                                                           _max_msg_size,
                                                           nullptr,
                                                           &ts));
        } catch(std::system_error& e) {
            if(errno == ETIMEDOUT) {
                throw TimeoutException("Timeout on receive from message queue");
            } else {
                throw;
            }
        }
        if(bytes_read >= 0) {
            buffer.resize(bytes_read);
            return buffer;
        }
        throw std::runtime_error("Error receiving message");
    }

  private:
    std::string _name;
    mqd_t _queue;
    size_t _max_msg_size;
    bool _running;
    std::mutex _mutex;
};

/**
 * @brief A server for message queue pairs
 *
 * @tparam QueueType The type of queues being served
 */
template <typename QueueType>
class MessageServer
{
  public:
    using CallbackType = typename CallbackRegistry::CallbackType;

    /**
     * @brief Construct a new MessageServer object
     *
     * @param request_queue A queue on which to receive requests
     * @param response_queue A queue on which to return responses
     */
    explicit MessageServer(QueueType& request_queue, QueueType& response_queue)
        : _request_queue(request_queue), _response_queue(response_queue),
          _running(true)
    {
        // Initialise default server callbacks
        register_callback(
            "help",
            {},
            "Return help for all server methods",
            [&](boost::json::object const&) -> boost::json::object {
                return _callbacks.help();
            });
        _receiver_thread = std::thread(&MessageServer::receiver, this);
    }

    MessageServer(MessageServer const&)            = delete;
    MessageServer& operator=(MessageServer const&) = delete;
    MessageServer(MessageServer&&)                 = delete;
    MessageServer& operator=(MessageServer&&)      = delete;

    /**
     * @brief Destroy the MessageServer object
     *
     * @details Requests graceful shutdown and then awaits
     *          completion.
     */
    ~MessageServer()
    {
        shutdown();
        await();
    }

    /**
     * @brief Join the server receiving thread (blocking).
     *
     */
    void await()
    {
        if(_receiver_thread.joinable()) {
            _receiver_thread.join();
        }
    }

    /**
     * @brief Request a stop of the server's receving loop
     *
     */
    void shutdown() { _running = false; }

    /**
     * @brief Register a callback handler on the server
     *
     * @param command The name of the command that will dispatch to this
     * callback
     * @param required_keys The set of required JSON parameters for the callback
     * @param description A description of the callback behaviour
     * @param callback The callback object
     */
    void register_callback(std::string_view command,
                           std::initializer_list<KeyDescriptor> required_keys,
                           std::string_view description,
                           CallbackType callback)
    {
        _callbacks.register_callback(command,
                                     required_keys,
                                     description,
                                     callback);
    }

  private:
    void receiver()
    {
        while(_running) {
            std::string message;
            try {
                message = _request_queue.recv(1.0f);
                BOOST_LOG_TRIVIAL(debug) << "Received message: " << message;
            } catch(TimeoutException& e) {
                continue;
            } catch(...) {
                BOOST_LOG_TRIVIAL(error) << "Error on message queue receive";
                throw;
            }

            try {
                handle_message(message);
            } catch(const std::exception& e) {
                send_error_message(e.what());
            }
        }
    }

    void handle_message(const std::string& message)
    {
        auto json_message = js::parse(message);
        auto command_str  = json_message.at("command").as_string();
        std::string command{command_str.c_str(), command_str.size()};

        // Check if there are callbacks for this command
        js::object response;
        try {
            response = _callbacks.invoke_callback(
                command,
                json_message.at("arguments").as_object());
        } catch(NoCallbackRegistered& e) {
            send_command_not_found_message(command);
            return;
        }
        send_success_message(command, response);
    }

    void send_error_message(const std::string& error_message)
    {
        js::object error_json;
        error_json["status"]  = "error";
        error_json["message"] = error_message;
        _response_queue.send(js::serialize(error_json));
    }

    void send_success_message(const std::string& command,
                              js::object const& response)
    {
        js::object success_json;
        success_json["status"]   = "success";
        success_json["command"]  = command;
        success_json["response"] = response;
        _response_queue.send(js::serialize(success_json));
    }

    void send_command_not_found_message(const std::string& command)
    {
        js::object error_json;
        error_json["status"]  = "error";
        error_json["message"] = "Command not found: " + command;
        _response_queue.send(js::serialize(error_json));
    }

    QueueType& _request_queue;
    QueueType& _response_queue;
    std::atomic<bool> _running;
    CallbackRegistry _callbacks;
    std::thread _receiver_thread;
    std::mutex _mutex;
};

} // namespace psrdada_cpp