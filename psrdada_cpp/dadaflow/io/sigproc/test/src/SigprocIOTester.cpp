#include "psrdada_cpp/dadaflow/io/sigproc/test/SigprocIOTester.hpp"
#include <gtest/gtest.h>
#include <filesystem>
#include <algorithm>
#include <random>

namespace psrdada_cpp {
namespace sigproc {
namespace test {

namespace fs = std::filesystem;


TEST_F(SigprocIOTester, write_and_read)
{
    // Make a fil file
    SigprocHeader header;
    this->polulate_default_values(header);
    std::size_t nsamples = 1024;
    fs::path filepath = make_filterbank(header, nsamples);
    // Read a fil file
    {
        SigprocFile filterbank{filepath};
        ASSERT_EQ(filterbank.nsamples(), nsamples);
        ASSERT_EQ(filterbank.remaining_samples(), nsamples);
    }
    {
        SigprocInputFileStream input_stream{{filepath}, false};
        auto block_0 = input_stream.read_block(10);
        using expected_type = std::shared_ptr<Filterbank1Bit>;
        ASSERT_TRUE(std::holds_alternative<expected_type>(block_0));
        auto block_1 = input_stream.read_block(9999);
        std::visit([nsamples](auto const& inst){ ASSERT_EQ(inst->nsamples(), std::size_t{nsamples-10}); }, block_1);
    }
}

TEST_F(SigprocIOTester, contiguous_stream_check)
{
    // Make a fil file
    SigprocHeader header;
    this->polulate_default_values(header);
    std::size_t nsamples = 1024;
    std::size_t nfiles = 5;
    std::vector<fs::path> files;
    double tstart = header.tstart();
    double tsamp = header.tsamp();
    for (std::size_t ii = 0; ii < nfiles; ii++) {
        header.tstart(tstart + (nsamples * ii * tsamp)/86400.0);
        files.push_back(make_filterbank(header, nsamples));
    }
    // Randomise the vector order
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::shuffle(files.begin(), files.end(), gen);

    SigprocInputFileStream input_stream{files, false};
    auto block_0 = input_stream.read_block(10);
    std::visit([nsamples](auto const& inst){ ASSERT_EQ(inst->nsamples(), std::size_t{10}); }, block_0);
    auto block_1 = input_stream.read_block(1024);
    std::visit([nsamples](auto const& inst){ ASSERT_EQ(inst->nsamples(), std::size_t{1024}); }, block_1);
    auto block_2 = input_stream.read_block(9000);
    std::visit([nsamples](auto const& inst){ ASSERT_EQ(inst->nsamples(), std::size_t{5 * nsamples - 1034}); }, block_2);
}

TEST_F(SigprocIOTester, non_contiguous_stream_check)
{
    // Make a fil file
    SigprocHeader header;
    this->polulate_default_values(header);
    std::size_t nsamples = 1024;
    std::size_t nfiles = 5;
    std::vector<fs::path> files;
    double tstart = header.tstart();
    double tsamp = header.tsamp() * 1.01;
    for (std::size_t ii = 0; ii < nfiles; ii++) {
        header.tstart(tstart + (nsamples * ii * tsamp)/86400.0);
        files.push_back(make_filterbank(header, nsamples));
    }
    // Randomise the vector order
    std::random_device rd; 
    std::mt19937 gen(rd());
    std::shuffle(files.begin(), files.end(), gen);
    EXPECT_THROW((SigprocInputFileStream{files, false}), std::exception);
    EXPECT_NO_THROW((SigprocInputFileStream{files, true}));
}

TEST_F(SigprocIOTester, non_compatible_stream_check)
{
    // Make a fil file
    SigprocHeader header;
    this->polulate_default_values(header);
    std::size_t nsamples = 1024;
    std::size_t nfiles = 2;
    std::vector<fs::path> files;
    for (std::size_t ii = 0; ii < nfiles; ii++) {
        files.push_back(make_filterbank(header, nsamples));
        header.tsamp(header.tsamp()*1.0001);
    }
    EXPECT_THROW((SigprocInputFileStream{files, true}), std::exception);
}

TEST_F(SigprocIOTester, sample_seek_test)
{
    // Make a fil file
    SigprocHeader header;
    this->polulate_default_values(header);
    std::size_t nsamples = 1024;
    std::size_t nfiles = 2;
    std::vector<fs::path> files;
    for (std::size_t ii = 0; ii < nfiles; ii++) {
        files.push_back(make_filterbank(header, nsamples));
    }
    SigprocInputFileStream stream{files, true};
    stream.seek_sample(1024+512);
    auto block = stream.read_block(1024);
    std::visit([](auto& inst){
        ASSERT_EQ(inst->nsamples(), std::size_t{512});
    }, block);
}

TEST_F(SigprocIOTester, out_of_range_seek)
{
    // Make a fil file
    SigprocHeader header;
    this->polulate_default_values(header);
    std::size_t nsamples = 1024;
    std::size_t nfiles = 2;
    std::vector<fs::path> files;
    for (std::size_t ii = 0; ii < nfiles; ii++) {
        files.push_back(make_filterbank(header, nsamples));
    }
    SigprocInputFileStream stream{files, true};
    EXPECT_THROW(stream.seek_sample(5000), std::out_of_range);
}

} // namespace test
} // namespace sigproc
} // namespace psrdada_cpp