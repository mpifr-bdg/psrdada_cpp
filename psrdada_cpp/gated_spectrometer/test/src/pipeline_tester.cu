#include "psrdada_cpp/gated_spectrometer/test/pipeline_tester.cuh"

namespace psrdada_cpp {
namespace gated_spectrometer {
namespace test {

TEST_P(PipelineTester, test_dc_offset_dual)
{
    test_with_dc_offset<SinglePolarizationInput, GatedPowerSpectrumOutput>();
}

TEST_P(PipelineTester, test_dc_offset_stokes)
{
    test_with_dc_offset<DualPolarizationInput, GatedFullStokesOutput>();
}

TEST_P(PipelineTester, test_delta_impulse_dual_nd_on)
{
    test_with_delta_impulse();
}

TEST_P(PipelineTester, test_delta_impulse_dual_nd_off)
{
    test_with_delta_impulse(true);
}

// ToDo: Needs to be generalized
// TEST_P(PipelineTester, test_sidechannel_items_stokes)
// {
//     test_sidechannel_items_stokes();
// }


// ToDo: Needs to be generalized
// TEST_P(PipelineTester, test_sidechannel_items_dual)
// {
//     test_sidechannel_items_dual();
// }

INSTANTIATE_TEST_SUITE_P (ParameterizedTest, PipelineTester, testing::Values(
    //           fft_length | nacc | nbits | nheaps_block | msg
    test_config({2*1024,        2,      8,      1024,       "1k Channel: in buffer 1x  -> 1x out buffer;  8 Bit"}),
    test_config({2*1024,        2,      10,     1024,       "1k Channel: in buffer 1x  -> 1x out buffer; 10 Bit"}),
    test_config({2*1024,        2,      12,     1024,       "1k Channel: in buffer 1x  -> 1x out buffer; 12 Bit"}),
    test_config({2*1024 * 1024, 2,      8,      2*1024,     "1M Channel, in buffer 1x  -> 2x out buffer; 8 bit"}),
    test_config({2*1024 * 1024, 2,      10,     4*1024,     "1M Channel, in buffer 1x  -> 4x out buffer; 10-bit"}),
    test_config({2*1024 * 1024, 2,      12,     8*1024,     "1M Channel, in buffer 1x  -> 8x out buffer; 12-bit"}),
    test_config({2*1024 * 1024, 8,      8,      1024,       "1M Channel, in buffer 2x  -> 1x out buffer; 8-bit"}),
    test_config({2*1024 * 1024, 16,     8,      1024,       "1M Channel, in buffer 4x  -> 1x out buffer; 8-bit"}),
    test_config({2*1024 * 1024, 32,     12,     1024,       "1M Channel, in buffer 8x  -> 1x out buffer; 12-bit"}),
    test_config({2*1024 * 1024, 64,     10,     1024,       "1M Channel, in buffer 16x -> 1x out buffer; 10-bit"}),
    test_config({2*1024 * 1024, 128,    8,      1024,       "1M Channel, in buffer 32x -> 1x out buffer; 8-bit"}),
    // The only viable config for 'test_sidechannel_items_stokes'
    test_config({64*1024,       64,     8,      2*1024,     "64k Channel: in buffer 1x -> 1x out buffer;  8 Bit"}),
    // The only viable config for 'test_sidechannel_items_dual'
    test_config({64*1024,       64,     8,      1024,       "64k Channel: in buffer 1x -> 1x out buffer;  8 Bit"})
));

}
}
}