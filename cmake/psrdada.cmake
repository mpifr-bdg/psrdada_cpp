# find_package(psrdada)
# if(psrdada_FOUND)
#     message(STATUS "Found psrdada. Version: ${psrdada_VERSION}")
# else()
    # Possibly FetchContent should be implemented here.
    # But when psrdada was installed with autotools instead of cmake find_package fails
    # and psrdada gets installed again.
    message(STATUS "Trying to find psrdada")
    find_path(PSRDADA_INCLUDE_DIR
        NAMES psrdada/dada_def.h
        PATH_SUFFIXES psrdada
        PATHS
            ENV PSRDADA_INC_DIR
            ${PSRDADA_INSTALL_DIR}/include
            /usr/local/include
            /usr/include
    )

    find_library(PSRDADA_LIBRARY
        NAMES psrdada
        PATHS
            ENV PSRDADA_LIBRARY_DIR
            ${PSRDADA_INSTALL_DIR}/lib
            /usr/local/lib
            /usr/lib
    )

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(PSRDADA
        REQUIRED_VARS PSRDADA_LIBRARY PSRDADA_INCLUDE_DIR
        VERSION_VAR PSRDADA_VERSION
    )

    if (PSRDADA_FOUND)
        set(PSRDADA_LIBRARIES ${PSRDADA_LIBRARY})
        set(PSRDADA_INCLUDE_DIRS ${PSRDADA_INCLUDE_DIR})

        mark_as_advanced(PSRDADA_INCLUDE_DIR PSRDADA_LIBRARY)
    else()
        MESSAGE(FATAL_ERROR psrdada not found)
    endif()


    MESSAGE(STATUS "Using psrdada includes from ${PSRDADA_INCLUDE_DIRS}")
    include_directories(${PSRDADA_INCLUDE_DIRS})
# endif()
