#include "psrdada_cpp/dadaflow/test/DescribedDataTester.cuh"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/FixedWidth.hpp"
#include "psrdada_cpp/dadaflow/StokesVector.cuh"

#include <gmock/gmock.h>

namespace psrdada_cpp
{
namespace test
{

// Helpers to test concepts
template <typename T>
struct is_resizeable: std::bool_constant<Resizeable<T>> {
};
template <typename T>
struct is_iterable: std::bool_constant<Iterable<T>> {
};

template <typename T>
using TAFTPVoltagesH = DescribedData<std::vector<T>,
                                     TimeDimensionN<0>,
                                     AntennaDimension,
                                     FrequencyDimension,
                                     TimeDimensionN<1>,
                                     PolarisationDimension>;

TEST_F(DescribedDataTester, resizeable_and_nonresizable_construction_check)
{
    using T = TimeDimension;
    using F = FrequencyDimension;

    DescribedData<std::vector<float>, T, F> vec;
    vec.resize({4, 13});
    ASSERT_TRUE(is_resizeable<decltype(vec)::ContainerType>::value);

    std::span<float> span_from_vec(vec.container().data(),
                                   vec.container().size());
    DescribedData<std::span<float>, T, F> span(span_from_vec, {4, 13});
    ASSERT_FALSE(is_resizeable<decltype(span)::ContainerType>::value);

    using FixedVecT = FixedWidth<std::vector<uint64_t>, 10, std::endian::big>;
    DescribedData<FixedVecT, T, F> fixed_vec;
    fixed_vec.resize({4, 13});
    ASSERT_TRUE(is_resizeable<decltype(fixed_vec)::ContainerType>::value);

    using FixedSpanT = FixedWidth<std::span<uint64_t>, 10, std::endian::big>;
    std::span<uint64_t> span_from_fixed(
        fixed_vec.container().container().data(),
        fixed_vec.container().container().size());
    FixedSpanT fixed_from_span_from_fixed(span_from_fixed, 4 * 13);
    ASSERT_FALSE(is_resizeable<decltype(fixed_from_span_from_fixed)>::value);
    DescribedData<FixedSpanT, T, F> fixed_span(fixed_from_span_from_fixed,
                                               {4, 13});
    ASSERT_FALSE(is_resizeable<decltype(fixed_span)::ContainerType>::value);
}

TEST_F(DescribedDataTester, same_dimensions_check)
{
    using T  = TimeDimension;
    using F  = FrequencyDimension;
    using C0 = std::vector<float>;
    using C1 = std::vector<int>;

    using TypeA = DescribedData<C0, T, F>;
    using TypeB = DescribedData<C0, F, T>;
    using TypeC = DescribedData<C1, T, F>;
    using TypeD = DescribedData<C1, F, T>;

    ASSERT_FALSE((is_same_dimensions<TypeA, TypeB>::value));
    ASSERT_TRUE((is_same_dimensions<TypeB, TypeD>::value));
    ASSERT_TRUE((is_same_dimensions<TypeA, TypeC>::value));
}

TEST_F(DescribedDataTester, resize_test)
{
    TAFTPVoltagesH<float> data;
    data.resize({7, 2, 3, 4, 5});
    ASSERT_EQ(data.size(), std::size_t(7 * 2 * 3 * 4 * 5));
    ASSERT_THAT(data.extents(),
                testing::ElementsAre(std::size_t(7),
                                     std::size_t(2),
                                     std::size_t(3),
                                     std::size_t(4),
                                     std::size_t(5)));
    ASSERT_EQ(data.total_nchannels(), std::size_t(3));
    ASSERT_EQ(data.total_nsamples(), std::size_t(7 * 4));
}

TEST_F(DescribedDataTester, resize_zero_extent_test)
{
    TAFTPVoltagesH<float> data;
    data.resize({1, 2, 0, 4, 5});
    ASSERT_EQ(data.size(), std::size_t(0));
}

TEST_F(DescribedDataTester, invalidate_metadata_on_resize)
{
    TAFTPVoltagesH<float> data;
    data.resize({1, 2, 3, 4, 5});
    std::vector<std::string> antenna_descriptions;
    antenna_descriptions.push_back("antenna0,blah,blah");
    antenna_descriptions.push_back("antenna1,blah,blah");
    data.antennas(antenna_descriptions);
    EXPECT_NO_THROW(data.antennas());
    data.resize({1, 1, 3, 4, 5});
    EXPECT_THROW(data.antennas(), std::runtime_error);
}

TEST_F(DescribedDataTester, invalid_metadata_size)
{
    TAFTPVoltagesH<float> data;
    data.resize({1, 2, 3, 4, 5});
    std::vector<std::string> antenna_descriptions;
    antenna_descriptions.push_back("antenna0,blah,blah");
    EXPECT_THROW(data.antennas(antenna_descriptions), std::runtime_error);
}

TEST_F(DescribedDataTester, metadata_transfer)
{
    TAFTPVoltagesH<float> data1;
    TAFTPVoltagesH<int8_t> data2;
    data1.resize({1, 2, 3, 4, 5});
    std::vector<std::string> antenna_descriptions;
    antenna_descriptions.push_back("antenna0,blah,blah");
    antenna_descriptions.push_back("antenna1,blah,blah");
    data1.antennas(antenna_descriptions);
    EXPECT_THROW(data2.metalike(data1), std::runtime_error);
    EXPECT_NO_THROW(data2.like(data1));
    ASSERT_EQ(data1.extents(), data2.extents());
    ASSERT_EQ(data1.antennas(), antenna_descriptions);
}

class CustomDimension: protected BaseDimension
{
public:
    using BaseDimension::BaseDimension;

    const char* short_name() const override { return "C"; }
    const char* long_name() const override { return "Custom"; }

    std::size_t ncustom() const {return extent();}
    float custom() const {return _custom.get();}
    void custom(float const& val) {return _custom.set(val);}

    std::string describe() const override{
        std::stringstream stream;
        stream << long_name() << "\n";
        stream << "  Ncustom: " << ncustom() << "\n";
        stream << std::setprecision(15);
        stream << "  Custom: " << _custom << "\n";
        return stream.str();
    }

    void from(CustomDimension const& other) { _custom = other._custom; }

private:
    MetaDataWrapper<float> _custom;
};

TEST_F(DescribedDataTester, custom_dimension)
{
    DescribedData<std::vector<int>, TimeDimension, CustomDimension> data;
    data.resize({1,2});
    data.custom(123.0f);
    ASSERT_EQ(data.ncustom(), std::size_t(2));

    DescribedData<std::vector<int>, TimeDimension, CustomDimension> data2;
    EXPECT_NO_THROW(data2.like(data));
    ASSERT_EQ(data2.custom(), 123.0f);
    EXPECT_NO_THROW(data2.describe());
}

template <typename T>
struct is_taftp_like;

// Partial specialization for DescribedData with five dimensions
template <typename Container,
          typename T0,
          typename A,
          typename F,
          typename T1,
          typename P>
struct is_taftp_like<DescribedData<Container, T0, A, F, T1, P>> {
    static constexpr bool value =
        std::conjunction_v<std::is_convertible<T0*, BaseTimeDimension*>,
                           std::is_convertible<A*, BaseAntennaDimension*>,
                           std::is_convertible<F*, BaseFrequencyDimension*>,
                           std::is_convertible<T1*, BaseTimeDimension*>,
                           std::is_convertible<P*, BasePolarisationDimension*>>;
};

template <typename T>
concept TAFTPLike = is_taftp_like<T>::value;

template <typename T>
    requires TAFTPLike<T>
struct TAFTPProcessor {
    using T0 = std::tuple_element<0, typename T::DimensionTypes>::type;
    using T1 = std::tuple_element<3, typename T::DimensionTypes>::type;
    static std::size_t get_total_nsamples(T const& dv) {
        return dv.total_nsamples();
    }
};

TEST_F(DescribedDataTester, generic_processor_test)
{
    TAFTPVoltagesH<float> data;
    data.resize({10,9,8,7,6});
    std::size_t expected_total_nsamps = data.TimeDimensionN<0>::nsamples() * data.TimeDimensionN<1>::nsamples();
    ASSERT_EQ(expected_total_nsamps, std::size_t(10 * 7));
    ASSERT_EQ(expected_total_nsamps, TAFTPProcessor<decltype(data)>::get_total_nsamples(data));
}

TEST_F(DescribedDataTester, complete_metadata_test)
{
    TAFTPVoltagesH<float> data;
    auto now   = std::chrono::system_clock::now();
    auto epoch = std::chrono::time_point_cast<std::chrono::seconds>(now);
    std::chrono::duration<long double, pico> offset(12342342.2432);
    auto tsamp = std::chrono::duration<long double, pico>(4785.046728971963);

    data.resize({2,2,3,5,2});
    
    // Common to all DescribedDatas
    data.timestamp(Timestamp(epoch, offset));

    // Outer time dimension
    data.TimeDimensionN<0>::timestep(tsamp * data.TimeDimensionN<1>::nsamples());

    // Antenna dimension
    std::vector<std::string> antenna_descriptions;
    antenna_descriptions.push_back("antenna0,blah,blah");
    antenna_descriptions.push_back("antenna1,blah,blah");
    data.antennas(antenna_descriptions);

    // Frequency dimension
    data.channel_zero_freq(856.0 * 1e6 * hertz);
    data.channel_bandwidth((856.0 * 1e6 / 4096) * hertz);

    // Inner time dimension
    data.TimeDimensionN<1>::timestep(tsamp);

    // Polarisation dimension
    data.polarisations({"H", "V"});
    ASSERT_EQ(data.channel_frequency(2), ((856.0 * 1e6) + 2 * (856.0 * 1e6 / 4096)) * hertz);
}

template <typename T>
requires Vec4<T>
void process(T* data)
{
    data[0].x = static_cast<value_type_t<T>>(1.0);
}


TEST_F(DescribedDataTester, stokes_test)
{
    using StokesType = StokesVector<float, I, Q, U, V>;
    using TStokesIQUVH = DescribedData<std::vector<StokesType>, TimeDimension>;
    TStokesIQUVH data;
    data.resize({1000});
    auto result = data[0] * data[1];
    data[3] = result;
    ASSERT_TRUE((std::is_same_v<decltype(result), StokesType>));
    process(data.data());
    ASSERT_EQ(data[0].x, 1.0f);
    auto mask = stokes_mask<Q, StokesType>::value;
    ASSERT_EQ(mask.x, 0.0f);
    ASSERT_EQ(mask.y, 1.0f);
    ASSERT_EQ(mask.z, 0.0f);
    ASSERT_EQ(mask.w, 0.0f);
}

TEST_F(DescribedDataTester, indexing)
{
    DescribedData<std::vector<float>, TimeDimension, FrequencyDimension> data;
    data.resize({100,13});
    ASSERT_EQ(data.index(std::size_t(3),std::size_t(4)), std::size_t(3*13 + 4));
}

TEST_F(DescribedDataTester, gate_dimension)
{
    DescribedData<std::vector<float>, GateDimension, TimeDimension> data;
    data.resize({2,13});
    data.gates({"ON", "OFF"});
    ASSERT_EQ(data.ngates(), std::size_t(2));
}

} // namespace test
} // namespace psrdada_cppc