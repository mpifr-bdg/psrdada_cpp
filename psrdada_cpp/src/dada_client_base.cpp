#include "psrdada_cpp/dada_client_base.hpp"

namespace psrdada_cpp {

    DadaClientBase::DadaClientBase(key_t key, MultiLog& log)
    : _key(key)
    , _log(log)
    {
        std::stringstream _key_string_stream;
        _key_string_stream << "["<< std::hex << _key << std::dec << "] ["<<_log.name()<<"] ";
        _id = _key_string_stream.str();
        connect();
        cuda_register_memory();
    }

    DadaClientBase::~DadaClientBase()
    {
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Destructing DadaClientBase";
        if(_cuda_registered_memory)
        {
            cuda_unregister_memory();
        }
        if(!_stop.load())
        {
            _stop.store(true);
        }
        if (_connected)
        {
            disconnect();
        }
    }

    std::size_t DadaClientBase::data_buffer_size() const
    {
        return ipcbuf_get_bufsz((ipcbuf_t *) _hdu->data_block);
    }

    std::size_t DadaClientBase::header_buffer_size() const
    {
        return ipcbuf_get_bufsz(_hdu->header_block);
    }

    std::size_t DadaClientBase::data_buffer_count() const
    {
        return ipcbuf_get_nbufs((ipcbuf_t *) _hdu->data_block);
    }

    std::size_t DadaClientBase::header_buffer_count() const
    {
        return ipcbuf_get_nbufs(_hdu->header_block);
    }

    std::size_t DadaClientBase::data_buffer_full_slots() const
    {
        return ipcbuf_get_nfull((ipcbuf_t *)_hdu->data_block);
    }

    std::size_t DadaClientBase::data_buffer_free_slots() const
    {
        return data_buffer_count() - data_buffer_full_slots();
    }

    bool DadaClientBase::data_buffer_is_full() const
    {
        return (0 == data_buffer_free_slots());
    }

    bool DadaClientBase::data_buffer_is_empty() const
    {
        return (0 == data_buffer_full_slots());
    }

    std::size_t DadaClientBase::header_buffer_full_slots() const
    {
        return ipcbuf_get_nfull(_hdu->header_block);
    }

    std::size_t DadaClientBase::header_buffer_free_slots() const
    {
        return header_buffer_count() - header_buffer_full_slots();

    }

    bool DadaClientBase::header_buffer_is_full() const
    {
        return (0 == header_buffer_free_slots());
    }

    bool DadaClientBase::header_buffer_is_empty() const
    {
        return (0 == header_buffer_full_slots());
    }

    void DadaClientBase::connect()
    {

        BOOST_LOG_TRIVIAL(info) << this->id() << "Connecting to dada buffer";
        _hdu = dada_hdu_create(_log.native_handle());
        dada_hdu_set_key(_hdu, _key);
        if (dada_hdu_connect (_hdu) < 0){
            _log.write(LOG_ERR, "could not connect to hdu\n");
            throw std::runtime_error(std::string("Unable to connect to buffer with key: ")
                + std::to_string(_key));
        }
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Header buffer is " << header_buffer_count()
            << " x " << header_buffer_size() << " bytes";
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Data buffer is " << data_buffer_count()
            << " x " << data_buffer_size() << " bytes";
        _connected = true;
    }

    void DadaClientBase::disconnect()
    {
        BOOST_LOG_TRIVIAL(info) << this->id() << "Disconnecting from dada buffer";
        if (dada_hdu_disconnect (_hdu) < 0){
            _log.write(LOG_ERR, "could not disconnect from hdu\n");
            throw std::runtime_error(std::string("Unable to disconnect from buffer with key: ")
                + std::to_string(_key));
        }
        dada_hdu_destroy(_hdu);
        _connected = false;
    }

    void DadaClientBase::reconnect()
    {
        disconnect();
        connect();
    }

    void DadaClientBase::cuda_register_memory()
    {
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Pinning dada buffer memory";
#ifdef ENABLE_CUDA
        int res = dada_cuda_dbregister(_hdu); 
        if (res < 0)
        {
            BOOST_LOG_TRIVIAL(error) << "Failed to register HDU DADA buffers as pinned memory";
        }
        else
        {
            _cuda_registered_memory = true;
        }
#else
        BOOST_LOG_TRIVIAL(warning) << "cuda_register_memory can only be used with ENABLE_CUDA is defined";
#endif //ENABLE_CUDA
    }

    void DadaClientBase::cuda_unregister_memory()
    {
        BOOST_LOG_TRIVIAL(debug) << this->id() << "Unpinning dada buffer memory";
#ifdef ENABLE_CUDA
        if (dada_cuda_dbunregister(_hdu) < 0)
        {
            BOOST_LOG_TRIVIAL(error) << "Failed to unregister pinned memory from HDU DADA buffers";
        }
        else
        {
            _cuda_registered_memory = false;
        }
#else
        BOOST_LOG_TRIVIAL(warning) << "dada_cuda_dbunregister can only be used with ENABLE_CUDA is defined";
#endif //ENABLE_CUDA
    }

    std::string const& DadaClientBase::id() const
    {
        return _id;
    }


    void DadaClientBase::stop_next()
    {
        _stop.store(true);
    }

    bool DadaClientBase::is_connected()
    {
        return _connected;
    }

    bool DadaClientBase::has_registered()
    {
        return _cuda_registered_memory;
    }


} //namespace psrdada_cpp
