#pragma once
#include "psrdada_cpp/dadaflow/io/sigproc/SigprocIO.hpp"
#include "psrdada_cpp/dadaflow/io/sigproc/SigprocHeader.hpp"
#include <gtest/gtest.h>
#include <memory>
#include <filesystem>
#include <random>

namespace psrdada_cpp {
namespace sigproc {
namespace test {

namespace fs = std::filesystem;

class SigprocIOTester : public ::testing::Test
{
public:
    SigprocIOTester(){}

    ~SigprocIOTester(){}

    void SetUp() override {};

    void TearDown() override {
        for (auto const& path: _paths) {
            fs::remove(path);
        }
    };

    void polulate_default_values(SigprocHeader& header) {
        header.source_name("incywincypulsar");
        header.src_raj(122303.23123);
        header.src_dej(-12424.001);
        header.fch1(544.0);
        header.foff(0.1328125);
        header.tsamp(0.00153123);
        header.tstart(61000.234234);
        header.data_type(1);
        header.machine_id(0);
        header.nbits(1);
        header.nchans(16);
        header.nifs(1);
        header.telescope_id(0);
        header.is_signed(1);
    }

    fs::path make_filterbank(SigprocHeader const& header, std::size_t nsamples) {
        fs::path filename = get_temp_filepath();
        std::ofstream ofs(filename, std::ios::binary);
        if (!ofs) {
            throw std::ios_base::failure("Failed to create temp file");
        }
        header.to_stream(ofs);
        std::size_t nbytes = nsamples * header.nchans() * header.nifs() * header.nbits() / 8;
        std::vector<char> data(nbytes);
        ofs.write(data.data(), data.size());
        ofs.close();
        return filename;
    }

    fs::path get_temp_filepath() {
        fs::path temp_dir = fs::temp_directory_path();
        std::random_device rd;
        std::uniform_int_distribution<int> dist(0, 99999);
        std::string filename = "tempfile_" + std::to_string(dist(rd)) + ".tmp";
        fs::path temp_file_path = temp_dir / filename;
        _paths.push_back(temp_file_path);
        return temp_file_path;
    }

    std::vector<fs::path> _paths;
};

} // namespace test
} // namespace sigproc
} // namespace psrdada_cpp