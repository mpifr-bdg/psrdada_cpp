#include "psrdada_cpp/dadaflow/test/AsyncDadaClientTester.hpp"

namespace psrdada_cpp {
namespace dadaflow {
namespace test {

DadaClientTester::DadaClientTester() 
    :   ::testing::TestWithParam<dada_buffer_config>(),
        wlog("DadaWriteClient"),
        rlog("AsyncDadaReadClient"),
        config(GetParam()){}


void DadaClientTester::SetUp()
{
    test_buffer = std::make_unique<DadaDB>(config.nbufs, config.bufsz, config.nhdrs, config.hdrsz);
    test_buffer->create();
    test_writer = std::make_unique<DadaWriteClient>(test_buffer->key(), wlog);
    test_reader = std::make_unique<AsyncDadaReadClient>(test_buffer->key(), rlog);
}

void DadaClientTester::TearDown()
{
    test_writer.reset();
    test_reader.reset();
    test_buffer->destroy();
}

void DadaClientTester::test_buffer_is_full()
{
    ASSERT_FALSE(test_writer->header_buffer_is_full());
    ASSERT_FALSE(test_reader->header_buffer_is_full());
    ASSERT_FALSE(test_reader->data_buffer_is_full());
    ASSERT_FALSE(test_writer->data_buffer_is_full());
    this->write_header_until_full();
    this->write_data_until_full();
    ASSERT_TRUE(test_writer->header_buffer_is_full());
    ASSERT_TRUE(test_reader->header_buffer_is_full());
    ASSERT_TRUE(test_writer->data_buffer_is_full());
    ASSERT_TRUE(test_reader->data_buffer_is_full());
    auto [hblock, hreleaser] = test_reader->header_stream().next();
    hreleaser();
    ASSERT_FALSE(test_writer->header_buffer_is_full());
    ASSERT_FALSE(test_reader->header_buffer_is_full());
    auto [dblock, dreleaser] = test_reader->data_stream().next();
    dreleaser();
    ASSERT_FALSE(test_writer->data_buffer_is_full());
    ASSERT_FALSE(test_reader->data_buffer_is_full());
}

void DadaClientTester::test_buffer_is_empty()
{

    ASSERT_TRUE(test_writer->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->data_buffer_is_empty());
    ASSERT_TRUE(test_writer->data_buffer_is_empty());
    this->write_header_until_full();
    this->write_data_until_full();
    ASSERT_FALSE(test_writer->header_buffer_is_empty());
    ASSERT_FALSE(test_reader->header_buffer_is_empty());
    ASSERT_FALSE(test_writer->data_buffer_is_empty());
    ASSERT_FALSE(test_reader->data_buffer_is_empty());
    // Read a single header and data slot
    this->read_data_slot();
    this->read_header_slot();
    ASSERT_FALSE(test_writer->header_buffer_is_empty());
    ASSERT_FALSE(test_reader->header_buffer_is_empty());
    ASSERT_FALSE(test_writer->data_buffer_is_empty());
    ASSERT_FALSE(test_reader->data_buffer_is_empty());
    // read until header and data buffer are empty
    this->read_header_until_empty();
    this->read_data_until_empty();
    ASSERT_TRUE(test_writer->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->data_buffer_is_empty());
    ASSERT_TRUE(test_writer->data_buffer_is_empty());
}

void DadaClientTester::test_buffer_nfree()
{
    for(std::size_t i = config.nhdrs; i > 1; i--)
    {
        ASSERT_EQ(i, test_writer->header_buffer_free_slots());
        ASSERT_EQ(i, test_reader->header_buffer_free_slots());
        write_header_slot();
    }
    for(std::size_t i = config.nbufs; i > 1; i--)
    {
        ASSERT_EQ(i, test_writer->data_buffer_free_slots());
        ASSERT_EQ(i, test_reader->data_buffer_free_slots());
        write_data_slot();
    }
}

void DadaClientTester::test_buffer_nfull()
{

    auto& hstream = test_writer->header_stream();
    for(std::size_t i = 0; i > config.nhdrs; i++)
    {
        ASSERT_EQ(i, test_writer->header_buffer_full_slots());
        ASSERT_EQ(i, test_reader->header_buffer_full_slots());
        auto& hbuffer = hstream.next();
        hbuffer.used_bytes(config.hdrsz);
        hstream.release();
    }
    auto& dstream = test_writer->data_stream();
    for(std::size_t i = 0; i > config.nbufs; i++)
    {
        ASSERT_EQ(i, test_writer->data_buffer_full_slots());
        ASSERT_EQ(i, test_reader->data_buffer_full_slots());
        auto& dbuffer = dstream.next();
        dbuffer.used_bytes(config.bufsz);
        dstream.release();
    }
}

void DadaClientTester::test_buffer_size()
{
    ASSERT_EQ(config.hdrsz, test_writer->header_buffer_size());
    ASSERT_EQ(config.hdrsz, test_reader->header_buffer_size());
    ASSERT_EQ(config.bufsz, test_writer->data_buffer_size());
    ASSERT_EQ(config.bufsz, test_reader->data_buffer_size());
}

void DadaClientTester::test_buffer_count()
{
    ASSERT_EQ(config.nhdrs, test_writer->header_buffer_count());
    ASSERT_EQ(config.nhdrs, test_reader->header_buffer_count());
    ASSERT_EQ(config.nbufs, test_writer->data_buffer_count());
    ASSERT_EQ(config.nbufs, test_reader->data_buffer_count());
}

void DadaClientTester::test_reader_has_block()
{
    auto& hstream = test_reader->header_stream();
    auto& dstream = test_reader->data_stream();
    this->write_data_slot(); // Write one slot, so it can be read
    this->write_header_slot(); // Write one slot, so it can be read
    auto [hblock, hreleaser] = hstream.next();
    auto [dblock, dreleaser] = dstream.next();
    ASSERT_TRUE(dstream.has_block());
    ASSERT_TRUE(hstream.has_block());
    hreleaser(); // Release the read header slot
    dreleaser(); // Release the read data slot
    ASSERT_FALSE(dstream.has_block());
    ASSERT_FALSE(hstream.has_block());
}

void DadaClientTester::test_reader_stop_next()
{
    auto& hstream = test_reader->header_stream();
    auto& dstream = test_reader->data_stream();
    test_reader->stop_next();
    EXPECT_THROW(hstream.next(), DadaInterruptException);
    EXPECT_THROW(dstream.next(), DadaInterruptException);
}

void DadaClientTester::test_reader_stop_next_from_thread()
{
    auto& hstream = test_reader->header_stream();
    auto& dstream = test_reader->data_stream();
    std::thread hthread([&hstream]() {
        EXPECT_THROW(hstream.next(), DadaInterruptException);
    });
    test_reader->stop_next();
    hthread.join();
    std::thread dthread([&dstream]() {
        EXPECT_THROW(dstream.next(), DadaInterruptException);
    });
    test_reader->stop_next();
    dthread.join();
}

void DadaClientTester::test_reader_connect()
{
    ASSERT_TRUE(test_reader->is_connected());
    test_reader->disconnect();
    ASSERT_FALSE(test_reader->is_connected());
    test_reader->connect();
    ASSERT_TRUE(test_reader->is_connected());
}

void DadaClientTester::test_reader_disconnect_while_acquired()
{
    this->write_data_slot(); // Write one slot, so it can be read
    auto [dbuf, dreleaser] = test_reader->data_stream().next(); // Acquire the slot
    ASSERT_TRUE(test_reader->data_stream().has_block());
    EXPECT_THROW(test_reader->disconnect(), DadaResourceInUseException);
}

void DadaClientTester::test_writer_connect()
{
    ASSERT_TRUE(test_writer->is_connected());
    test_writer->disconnect();
    ASSERT_FALSE(test_writer->is_connected());
    test_writer->connect();
    ASSERT_TRUE(test_writer->is_connected());
}

void DadaClientTester::test_writer_disconnect_while_acquired()
{
    test_writer->data_stream().next(); // Acquire the slot
    test_writer->header_stream().next(); // Acquire the slot
    ASSERT_TRUE(test_writer->data_stream().has_block());
    ASSERT_TRUE(test_writer->header_stream().has_block());
    test_writer->disconnect(); // Disconnect while acquired
    ASSERT_EQ(test_reader->data_buffer_full_slots(), 1); // Use reader to get full slots as writer is disconnected
    ASSERT_EQ(test_reader->header_buffer_full_slots(), 1); // Use reader to get full slots as writer is disconnected
    ASSERT_FALSE(test_writer->is_connected());
    test_writer->connect();
}

void DadaClientTester::test_writer_stop_next()
{
    auto& hstream = test_writer->header_stream();
    auto& dstream = test_writer->data_stream();
    test_writer->stop_next(); // Set before next call
    EXPECT_THROW(hstream.next(), DadaInterruptException);
    EXPECT_THROW(dstream.next(), DadaInterruptException);
}

void DadaClientTester::test_writer_stop_next_from_thread()
{
    auto& hstream = test_writer->header_stream();
    auto& dstream = test_writer->data_stream();
    std::thread hthread([&hstream]() {
        EXPECT_THROW(hstream.next(), DadaInterruptException);
    });
    test_writer->stop_next();
    hthread.join();
    std::thread dthread([&dstream]() {
        EXPECT_THROW(dstream.next(), DadaInterruptException);
    });
    test_writer->stop_next();
    dthread.join();
}

// ----------------
// HELPER FUNCTIONS 
// ----------------
void DadaClientTester::write_header_slot()
{
    auto& hstream = test_writer->header_stream();
    auto& hbuffer = hstream.next();
    hbuffer.used_bytes(config.hdrsz);
    hstream.release();
}


void DadaClientTester::write_data_slot()
{
    auto& dstream = test_writer->data_stream();
    auto& dbuffer = dstream.next();
    dbuffer.used_bytes(config.bufsz);
    dstream.release();
}

void DadaClientTester::read_header_slot()
{
    auto& hstream = test_reader->header_stream();
    auto [hblock, hreleaser] = hstream.next();
    hreleaser();
}


void DadaClientTester::read_data_slot()
{
    auto& dstream = test_reader->data_stream();
    auto [dblock, dreleaser] = dstream.next();
    dreleaser();
}

void DadaClientTester::write_header_until_full()
{
    while(test_writer->header_buffer_free_slots())
    {
        write_header_slot();
    }
}

void DadaClientTester::write_data_until_full()
{
    while(test_writer->data_buffer_free_slots())
    {
        write_data_slot();
    }
}

void DadaClientTester::read_header_until_empty()
{
    while(test_reader->header_buffer_full_slots())
    {
        read_header_slot();
    }
}

void DadaClientTester::read_data_until_empty()
{
    while(test_reader->data_buffer_full_slots())
    {
        read_data_slot();
    }
}


TEST_P(DadaClientTester, test_buffer_is_full){ test_buffer_is_full(); }
TEST_P(DadaClientTester, test_buffer_is_empty){ test_buffer_is_empty(); }
TEST_P(DadaClientTester, test_buffer_nfree){ test_buffer_nfree(); }
TEST_P(DadaClientTester, test_buffer_nfull){ test_buffer_nfull(); }
TEST_P(DadaClientTester, test_buffer_size){ test_buffer_size(); }
TEST_P(DadaClientTester, test_buffer_count){ test_buffer_count(); }

TEST_P(DadaClientTester, test_reader_has_block){ test_reader_has_block(); }
TEST_P(DadaClientTester, test_reader_connect){ test_reader_connect(); }
TEST_P(DadaClientTester, test_reader_disconnect_while_acquired){ test_reader_disconnect_while_acquired(); }
TEST_P(DadaClientTester, test_reader_interrupt_when_acquire){ test_reader_stop_next(); }
TEST_P(DadaClientTester, test_reader_stop_next_from_thread){ test_reader_stop_next_from_thread(); }

TEST_P(DadaClientTester, test_writer_connect){ test_writer_connect(); }
TEST_P(DadaClientTester, test_writer_disconnect_while_acquired){ test_writer_disconnect_while_acquired(); }
TEST_P(DadaClientTester, test_writer_stop_next){ test_writer_stop_next(); }
TEST_P(DadaClientTester, test_writer_stop_next_from_thread){ test_writer_stop_next_from_thread(); }

INSTANTIATE_TEST_SUITE_P(DadaClientBaseTesterInstantiation, DadaClientTester, ::testing::Values(
    //          in_key | out_key | device_id | heap_size | n_sample | n_channel | n_element | n_pol | n_acc | n_bit
    dada_buffer_config{16, 2048, 8, 4096},
    dada_buffer_config{11, 2345, 12, 1111},
    dada_buffer_config{123, 24, 2, 1337},
    dada_buffer_config{9, 111222, 19, 32}
));

}
}
}