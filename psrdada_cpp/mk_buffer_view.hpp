#ifndef PSRDADA_CPP_MK_BUFFER_VIEW_HPP
#define PSRDADA_CPP_MK_BUFFER_VIEW_HPP

#include <cstdlib>

/**
 @class MKDadaBufferView
 @brief Calculate the data layout of the buffer sizes
 */

namespace psrdada_cpp {


class MKDadaBufferView
{
  private:
    std::size_t _bufferSize;
    std::size_t _heapSize;
    std::size_t _nSideChannels;

    std::size_t _sideChannelSize;
    std::size_t _nHeaps;
    std::size_t _gapSize;
    std::size_t _dataBlockBytes;


  public:
    // input key of the dadad buffer
    // size of spead heaps in bytes
    //  number of side channels
    MKDadaBufferView();
    void initialize(std::size_t bufferSize, std::size_t heapSize, std::size_t nSideChannels);

    key_t getInputkey() const;

    std::size_t getBufferSize() const;

    // get size of heaps in bytes
    std::size_t getHeapSize() const;

    std::size_t getNSideChannels() const;

    // returns size of data in buffer block in bytes
    std::size_t sizeOfData() const;

    // return size of gap between data and side channel
    std::size_t sizeOfGap() const;

    // returns size of side channelm data in buffer block in bytes
    std::size_t sizeOfSideChannelData() const;

    // number of heaps stored in one block of the buffer
    std::size_t getNHeaps() const;
};

} // namespace
#endif
