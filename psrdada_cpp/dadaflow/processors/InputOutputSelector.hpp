#pragma once
#include "psrdada_cpp/dadaflow/Generator.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/common.hpp"
#include <optional>
#include <memory>

namespace psrdada_cpp {

/**
 * @brief Processor for selecting valid inputs from a list of optional inputs
 * 
 * @tparam N The number of outputs to replicate to
 * @tparam T The type of the data (wrapped in shared_ptr)
 */
template <int N, typename T>
class InputOutputSelector
{
public:
    using InputPtr = std::shared_ptr<T>;
    using OutputPtr = std::shared_ptr<T>;
    using InputT = std::optional<InputPtr>;
    using Output = decltype(replicate_tuple<N>(OutputPtr(nullptr)));
    static constexpr int max_inputs = 5;

    Generator<std::optional<Output>> operator()(InputT i0, InputT i1, InputT i2, InputT i3, InputT i4){
        for (const auto& input : { i0, i1, i2, i3, i4 }) {
            if (input.has_value()) {
                co_yield replicate_tuple<N>(*input);
            }
        }
        co_return;
    }
};
    
} // namespace psrdada_cpp