#include "psrdada_cpp/dadaflow/io/sigproc/SigprocHeader.hpp"
#include <gtest/gtest.h>
#include <sstream>

namespace psrdada_cpp {
namespace sigproc {
namespace test {

TEST(SigprocHeaderTester, write_and_read)
{
    double tsamp = 0.0001234123234;
    std::string source_name{"my_source"};
    double refdm = 100.0;
    uint8_t signed_ = 1;

    SigprocHeader header;
    header.tsamp(tsamp);
    header.source_name(source_name);
    header.refdm(refdm);
    header.is_signed(signed_);

    std::stringstream stream;
    header.to_stream(stream);

    SigprocHeader rheader;
    rheader.from_stream(stream);
    ASSERT_DOUBLE_EQ(rheader.tsamp(), tsamp);
    ASSERT_DOUBLE_EQ(rheader.refdm().value(), refdm);
    ASSERT_EQ(rheader.source_name(), source_name);
    ASSERT_EQ(rheader.is_signed().value(), signed_);

    std::cout << rheader << "\n";
}

TEST(SigprocHeaderTester, invalid_stream)
{
    SigprocHeader header;
    std::stringstream stream;
    EXPECT_THROW(header.from_stream(stream), std::exception);
}

} // namespace test
} // namespace sigproc
} // namespace psrdada_cpp