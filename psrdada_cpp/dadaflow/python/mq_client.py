import posix_ipc
import json
import time
import pprint
import asyncio
import logging

log = logging.getLogger("mq-client")

class MQClient:
    def __init__(self, request_q_key, response_q_key):
        self._request_q_key = request_q_key
        self._request_q = None
        self._response_q_key = response_q_key
        self._response_q = None
        self._ready = False
    
    async def connect(self, timeout=15):
        start = time.time()
        while ((time.time() - start) < timeout):
            try:
                self._request_q = posix_ipc.MessageQueue(self._request_q_key)
                self._response_q = posix_ipc.MessageQueue(self._response_q_key)
                self._ready = True
                break
            except posix_ipc.ExistentialError:
                await asyncio.sleep(1)
        else:
            raise Exception("Timeout on connecting to message queues")
    
    def __del__(self):
        if self._request_q:
            self._request_q.close()
        if self._response_q:
            self._response_q.close()
    
    async def send(self, command, timeout=10, **kwargs):
        if not self._ready:
            raise Exception("Client is not connected to queues, did you call connect()?")
        message = json.dumps({
            "command": command,
            "arguments": kwargs
        })
        await asyncio.to_thread(self._request_q.send, message)
        try:
            response_str, _ = await asyncio.to_thread(self._response_q.receive, timeout=timeout)
            response = json.loads(response_str)
        except posix_ipc.BusyError:
            log.error(f"Timeout waiting for a response to message: {message}")
            raise
        return response

    async def show_help(self):
        response = await self.send("help")
        for key, params in response["response"].items():
            msg = "Command:\n"
            msg += f"{key} -- {params['description']}\n"
            if len(params['required_keys']) > 0:
                msg += "\nArgs:\n"
                for key, detail in params['required_keys'].items():
                    msg += f"--> {key} ({detail['type']}): {detail['description']} \n"
            print(msg)


async def main(client):
    await client.connect()
    await client.show_help()
    await client.send("say_hello", repeats=10)
    await client.send("shutdown")

if __name__ == "__main__":
    client = MQClient("/message_server_example", "/message_server_example_r")
    asyncio.run(main(client))
