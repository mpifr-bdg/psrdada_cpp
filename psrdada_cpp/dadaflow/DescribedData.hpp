#pragma once
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/units.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <array>
#include <chrono>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <span>
#include <sstream>
#include <type_traits>
#include <vector>
#include <any>

using namespace boost::units;
using namespace boost::units::si;
using namespace boost::units::astronomical;
using namespace std::chrono;

namespace psrdada_cpp
{

/**
 * The intention here is to allow the DescrivedVector class to also
 * represent vector-like instances. This will allow the class to
 * operate on data that it does not own. This should be possible
 * with specialisations of the functions that are not common between
 * the different vector types. In particular it is necessary to
 * support:
 * - std::vector
 * - thrust::host_vector
 * - thrust::device_vector
 * - std::span
 *
 * All the vector types above have common-ish interfaces. In all these
 * cases it is understood that the DescrivedVector owns its own memory.
 * In the case of std::span the DescrivedVector does not own its own
 * memory and cannot be default constructed or resized.
 */

// Define a concept to check if a type is an unsigned integer
template<typename T>
concept UnsignedInteger = std::is_unsigned_v<T>;

// Define a concept to ensure all types in the pack are unsigned integers
template<typename... Args>
concept UnsignedIntegers = (UnsignedInteger<Args> && ...);

// Trait to determine if two types share
// the same set of dimensions.
template <typename T, typename U>
struct is_same_dimensions: std::false_type {
};

template <template <typename, typename...> class DD1,
          template <typename, typename...>
          class DD2,
          typename T1,
          typename T2,
          typename... Dimensions>
struct is_same_dimensions<DD1<T1, Dimensions...>, DD2<T2, Dimensions...>>
    : std::true_type {
};

template <typename A, typename B>
concept IsSameDimensions = is_same_dimensions<A, B>::value;

template<typename Derived, typename... Bases>
consteval bool are_all_bases_of() {
    return (... && std::is_base_of_v<Bases, Derived>);
}

template <typename A, typename B>
struct has_subset_of_dimensions_of: std::false_type {};

template<template <typename, typename...> class DD,
         typename T,
         typename Other, 
         typename... Dimensions>
struct has_subset_of_dimensions_of<DD<T, Dimensions...>, Other> {
    static constexpr bool value = are_all_bases_of<Other, Dimensions...>();
};

template <typename A, typename B>
concept HasSubsetOfDimensions = has_subset_of_dimensions_of<A, B>::value;

using MetadataMap = std::unordered_map<std::string, std::any>;

/**
 * @brief A class for holding data of specific dimension with associated
 *        metadata.
 *
 * @tparam ContainerType_    The container type used for storing the data.
 * @tparam Dimensions        The dimension types of the data.
 *
 * @details The DescribedData class wraps a STL container-compatible
 *          object in logic that tracks the order of the data and the
 *          extents of its various dimensions. The class uses variadic
 *          inheritance to build is interface.
 */
template <typename ContainerType_, typename... Dimensions>
class DescribedData: public Dimensions...
{
  // All DescribedData types are buddies
  template <typename OtherContainerType, typename... OtherDimensionTypes>
  friend class DescribedData;

  public:
    using ContainerType   = ContainerType_;
    using value_type      = typename ContainerType::value_type;
    using DimensionTypes  = std::tuple<Dimensions...>;
    using ExtentsType     = std::array<std::size_t, sizeof...(Dimensions)>;
    using ProductsType    = std::array<std::size_t, sizeof...(Dimensions)>;
    using TimestampType   = Timestamp;
    using MetadataType    = MetadataMap;
    using MetadataPtrType = std::shared_ptr<const MetadataType>;

  public:
    /**
     * @brief Default construct a zero-size instance.
     * 
     * @note Only usable with resizeable containers.
     */
    DescribedData()
        requires Resizeable<ContainerType>
        : Dimensions()...
    {
        _metadata = std::make_shared<MetadataType>();
    }

    /**
     * @brief Construct a sized instance of DescribedData.
     * 
     * @param extents The size of each dimension in the instance.
     * 
     * @note Only usable with resizeable containers.
     */
    DescribedData(std::initializer_list<std::size_t> extents)
        requires Resizeable<ContainerType>
        : DescribedData()
    {
        resize(extents);
    }

    /**
     * @brief Construct from an existing container of given size.
     * 
     * @param container A container of type ContainerType to copy from.
     * @param extents  The size of each dimension in the instance.
     * 
     * @note This is inteded for use with non-resizable containers such as
     *       std::span and cuda::std::span. The container passed will be
     *       copied not moved and as such this method is disabled for 
     *       containers that satisfy the resizeable concept.
     */
    DescribedData(ContainerType const& container,
                  std::initializer_list<std::size_t> extents)
        requires (!Resizeable<ContainerType>)
        : DescribedData(container, std::vector<std::size_t>{extents})
    {}

    /**
     * @brief Construct from an existing container of given size.
     * 
     * @param container A container of type ContainerType to copy from.
     * @param extents  The size of each dimension in the instance.
     * 
     * @note This is inteded for use with non-resizable containers such as
     *       std::span and cuda::std::span. The container passed will be
     *       copied not moved and as such this method is disabled for 
     *       containers that satisfy the resizeable concept.
     */
    DescribedData(ContainerType const& container,
                  std::vector<std::size_t> const& extents)
        requires (!Resizeable<ContainerType>)
        : Dimensions()..., _container(container)
    {
        if(extents.size() != sizeof...(Dimensions)) {
            throw std::invalid_argument(
                "Number of extents must match the number of dimensions");
        }
        auto it = extents.begin();
        (static_cast<void>(Dimensions::resize_dimension(*it++)), ...);
        if(this->_container.size() != this->calculate_nelements()) {
            throw std::runtime_error(
                "Size of container does not match extents");
        }
        _metadata = std::make_shared<MetadataType>();
    }

    DescribedData(DescribedData const&)            = delete;
    DescribedData& operator=(DescribedData const&) = delete;

    /**
     * @brief Destroy the Described Vector object.
     *
     */
    ~DescribedData() {};

    /**
     * @brief Set the dimension descriptions and metadata to match another instance.
     *
     * @tparam OtherDescribedData   The type of the other instance.
     * @param other                 The other instance.
     * 
     * @note Is compatible with any DescribedData type that contains a superset of 
     *       this types dimensions. Ordering does not matter.
     * 
     * @details The timestamp is additionally copied from the other instance.
     */
    template <typename OtherDescribedData>
        requires HasSubsetOfDimensions<DescribedData, OtherDescribedData>
    void metalike(const OtherDescribedData& other)
    {
        (static_cast<void>(Dimensions::from(other)), ...);
        _timestamp = other._timestamp;
        _metadata = other._metadata;
    }

    /**
     * @brief Fetch the data from the underlying container at a linear index.
     *
     * @param idx     The linearized index into the data
     * @return auto&  A reference to the underlying data
     *
     * @note Despite the dimensions being tracked, the indexing to the
     *       underlying data is linear (flat indexing).
     */
    auto& operator[](std::size_t idx)
        requires IndexableReturningReference<ContainerType>
    {
        return _container[idx];
    }

    /**
     * @brief Fetch the data from the underlying container at a linear index.
     *
     * @param idx     The index to fetch.
     * @return auto   A non-reference to the underlying data.
     * 
     * @note Despite the dimensions being tracked, the indexing to the
     *       underlying data is linear (flat indexing).
     * 
     * @details This implementation returns as copy to whatever the underlying
     *          container returns. This will likely be a proxy reference type
     *          like thrust::device_reference. This member function is required 
     *          to support types like std::vector<bool> and thrust::device_vector.
     */
    auto operator[](std::size_t idx) 
        requires (Indexable<ContainerType> && !IndexableReturningReference<ContainerType>)
    {
        return _container[idx];
    }

    /**
     * @brief Fetch the data from the underlying container at a linear index.
     *
     * @param idx     The index to fetch.
     * @return auto&  A const reference to the underlying data.
     *
     * @note Despite the dimensions being tracked, the indexing to the
     *       underlying data is linear (flat indexing).
     */
    auto const& operator[](std::size_t idx) const
        requires IndexableReturningReference<ContainerType>
    {
        return _container[idx];
    }

    /**
     * @brief Fetch the data from the underlying container at a linear index.
     *
     * @param idx     The index to fetch.
     * @return auto   A non-reference to the underlying data.
     *
     * @note Despite the dimensions being tracked, the indexing to the
     *       underlying data is linear (flat indexing).
     * 
     * @details This implementation returns as copy to whatever the underlying
     *          container returns. This will likely be a proxy const reference type
     *          like thrust::device_reference. This member function is required 
     *          to support types like std::vector<bool> and thrust::device_vector.
     */
    auto operator[](std::size_t idx) const
        requires (Indexable<ContainerType> && !IndexableReturningReference<ContainerType>)
    {
        return _container[idx];
    }

    /**
     * @brief Get the size of the underlying container.
     *
     * @return std::size_t The container size.
     * 
     * @note It is not guaranteed that the size multiplied by the container 
     *       value_type is equal to the byte-size of the container data. 
     */
    std::size_t size() const { return _container.size(); }

    /**
     * @brief Get a pointer-like object to the underlying data.
     *
     * @return auto An accessor to the underlying types data.
     */
    auto data() noexcept { return _container.data(); }

    /**
     * @brief Get a pointer-like object to the underlying data.
     *
     * @return auto A const accessor to the underlying types data.
     */
    auto data() const noexcept { return _container.data(); }

    /**
     * @brief Get an iterator that points to the start of the underlying data.
     *
     * @return auto An iterator to the start of the data.
     */
    auto begin() noexcept
        requires Iterable<ContainerType>
    {
        return _container.begin();
    }

    /**
     * @brief Get a const iterator that points to the start of the underlying
     *        data. 
     *
     * @return auto A const iterator to the start of the data.
     */
    auto begin() const noexcept
        requires Iterable<ContainerType>
    {
        return _container.begin();
    }

    /**
     * @brief Get an iterator that points to the end of the underlying data
     *
     * @return auto An iterator at the end of the data.
     */
    auto end() noexcept
        requires Iterable<ContainerType>
    {
        return _container.end();
    }

    /**
     * @brief Get a const iterator that points to the end of the underlying data
     *
     * @return auto An const iterator at the end of the data.
     */
    auto end() const noexcept
        requires Iterable<ContainerType>
    {
        return _container.end();
    }

    /**
     * @brief Get a const reference to the underlying container.
     *
     * @return ContainerType a const reference to the underlying container.
     */
    ContainerType const& container() const { return _container; }

    /**
     * @brief Get a mutable reference to the underlying container
     *
     * @return ContainerType A reference to the underlying container.
     */
    ContainerType& container() { return _container; }

    /**
     * @brief Set the reference timestamp of this instance.
     *
     * @param t0 A timestamp defining the reference epoch of this instance.
     * 
     * @note The meaning of this timestamp is implementation specific. For 
     *       instance it may reference the start time of the logical data 
     *       stream or it may reference the start time of some sample in
     *       the current instance (usually the first sample or the time 
     *       centroid of the data).
     */
    void timestamp(TimestampType const& t0) { _timestamp.set(t0); }

    /**
     * @brief Get the reference timestamp of this instance.
     *
     * @return TimestampType The reference timestamp of this instance.
     * 
     * @note The meaning of this timestamp is implementation specific. For 
     *       instance it may reference the start time of the logical data 
     *       stream or it may reference the start time of some sample in
     *       the current instance (usually the first sample or the time 
     *       centroid of the data).
     */
    TimestampType const& timestamp() const { return _timestamp.get(); }

    /**
     * @brief Check if the timestamp is set
     * 
     * @return true  Yes there is a timestamp
     * @return false No there is no timestamp
     */
    bool has_timestamp() const { return !(_timestamp.is_stale()); }

    /**
     * @brief Get the sizes of each of the dimensions.
     *
     * @return ExtentsType an array of the sizes of each dimension in the 
     *                     in they order they are specified by the classes
     *                     template arguments.
     */
    ExtentsType const extents() const
    {
        return std::array<std::size_t, sizeof...(Dimensions)>{
            Dimensions::extent()...};
    }

    /**
     * @brief Resize the dimensions of the instance.
     * 
     * @param extents an initialiser-list of dimension extents.
     * 
     * @code
     * DescribedData<std::vector<int>, T, F> dd;
     * dd.resize({10, 5}) // resize T dimension to 10 and F dimension to 5
     * dd.size(); // == 50;
     * dd.extents(); // == ExtentsType{10, 5};
     * @endcode
     * 
     * @note Only works with resizeable container types.
     */
    void resize(std::initializer_list<std::size_t> extents)
        requires Resizeable<ContainerType>
    {
        resize(std::vector<std::size_t>(extents));
    }
    
    /**
     * @brief Resize the dimensions of the instance.
     * 
     * @param extents an iterable of dimension extents.
     * 
     * @code
     * DescribedData<std::vector<int>, T, F> dd;
     * std::vector<std::size_t> extents{10, 5};
     * dd.resize(extents) // resize T dimension to 10 and F dimension to 5
     * dd.size(); // == 50;
     * dd.extents(); // == ExtentsType{10, 5};
     * @endcode
     * 
     * @note Only works with resizeable container types.
     */
    template <typename T>
        requires Iterable<T>
    void resize(T const& sizes)
        requires Resizeable<ContainerType>
    {
        if(sizes.size() != sizeof...(Dimensions)) {
            throw std::invalid_argument(
                "Number of sizes must match number of dimensions");
        }
        auto it = sizes.begin();
        (static_cast<void>(Dimensions::resize_dimension(*it++)), ...);
        this->_container.resize(this->calculate_nelements());
        ExtentsType exts = extents();
        std::partial_sum(exts.rbegin(), exts.rend()-1, _inner_products.rbegin()+1, std::multiplies<std::size_t>());
        _inner_products.back() = 1;
    }
    
    /**
     * @brief Copy the description data and metadata and resize to the extents
     *        of another instance.
     * 
     * @tparam OtherDescribedData The type of the other instance.
     * @param other The DescribedData instance to become like.
     * 
     * @details The other instance must share the same dimensions as 
     *          this instance.
     */
    template <typename OtherDescribedData>
        requires IsSameDimensions<DescribedData, OtherDescribedData>
    void like(const OtherDescribedData& other)
        requires Resizeable<ContainerType>
    {
        resize(other.extents());
        this->metalike(other);
    }

    /**
     * @brief Get the dimensions of the data as a string
     *
     * @return std::string
     *
     * @details The dimensions are ordered slowest to fastest (e.g. TAFTP)
     *          where T [time] is the slowest dimension and P [poln.] is
     *          fastest dimension.
     */
    std::string dims_as_string() const
    {
        std::stringstream stream;
        ((stream << Dimensions::short_name() << ""), ...);
        return stream.str();
    }

    /**
     * @brief Return a string describing the instance in detail
     *
     * @return std::string
     */
    std::string describe() const
    {
        std::stringstream stream;
        stream << "<<< DescribedData: " << dims_as_string() << " >>>\n";
        stream << "Shape: ";
        (static_cast<void>(stream << Dimensions::extent() << " "), ...);
        stream << "\n";
        ((stream << Dimensions::describe() << "\n"), ...);
        return stream.str();
    }

    /**
     * @brief Return the aggregate extent of all time-like dimensions
     * 
     * @return std::size_t 
     */
    std::size_t total_nsamples() const
    {
        return get_aggregate_extent<BaseTimeDimension>();
    }

    /**
     * @brief Return the aggregate extent of all frequency-like dimensions
     * 
     * @return std::size_t 
     */
    std::size_t total_nchannels() const
    {
        return get_aggregate_extent<BaseFrequencyDimension>();
    }

    /**
     * @brief Calculate a linear index to the underlying data from extent indices.
     * 
     * @param indices  A variadic list of unsigned integer type indices.
     * @return std::size_t The linearized index to the data.
     * 
     * @note Indicies must be specified for all dimensions.
     */
    inline std::size_t index(UnsignedIntegers auto... indices)
    {
        static_assert(sizeof...(indices) == sizeof...(Dimensions));
        return (... + (indices * _inner_products[get_index<Dimensions>()])); 
        // ? Do we want to change the operator[] behaviour to use this or
        // ? do we add something like an at(...) method.   
    }

    // TODO: When moving to C++23 enable the multidimensional subscript 
    // operator here. Also std::mdspan (C++23) and std::submdspan (C++26) are coming.
    // TODO: Make use of [[assume(expr)]] expressions

    /**
     * @brief Get a const reference to the metadata map
     * 
     * @return MetadataType 
     *
     * @details Due to the lack of thread-safety on std::map and the fact that all
     *          DescribedData instances that like each other share the same metadata
     *          pointer, it is easy to create bugs by modifying the metadata. As such
     *          to modify the metadata it is necessary to copy the existing metadata 
     *          and create a new shared pointer. The pattern for doing this is shown 
     *          below.
     *
     * @code 
     * auto copy = std::make_shared<std::remove_const<MetadataType>>(data.metadata());
     * @endcode
     */
    MetadataType const& metadata() const
    {
        return *_metadata;
    }

    /**
     * @brief Set the metadata map pointer.
     * 
     * @param metadata A shared pointer to a map of metadata parameters.
     */
    void metadata_ptr(MetadataPtrType metadata)
    {
        _metadata = metadata;
    }

    /**
     * @brief Get the metadata map pointer.
     *
     */
    MetadataPtrType metadata_ptr() const
    {
        return _metadata;
    }

  protected:
    // Helper function to get the index of the current argument
    template <typename T, std::size_t... Is>
    constexpr std::size_t get_index_impl(std::index_sequence<Is...>) {
        std::size_t index = 0;
        (void(std::is_same_v<T, typename std::tuple_element<Is, std::tuple<Dimensions...>>::type> ?
         (index = Is) : 0), ...);
        return index;
    }

    template <typename T>
    constexpr std::size_t get_index() {
        return get_index_impl<T>(std::make_index_sequence<sizeof...(Dimensions)>{});
    }

    std::size_t calculate_nelements() const
    {
        return (Dimensions::extent() * ...);
    }

    template <typename BaseDimension>
    std::size_t get_aggregate_extent() const
    {
        std::size_t product = 1;
        (void(std::is_convertible_v<Dimensions*, BaseDimension*> ? 
             (product *= Dimensions::extent()) : 0), ...);
        return product;
    }

    MetaDataWrapper<TimestampType> _timestamp;
    ContainerType _container;
    ProductsType _inner_products;
    MetadataPtrType _metadata;
};

template <typename ContainerType, typename... Dimensions>
std::ostream& operator<<(std::ostream& stream,
                         DescribedData<ContainerType, Dimensions...> const& dd)
{
    stream << dd.describe();
    return stream;
}

} // namespace psrdada_cpp