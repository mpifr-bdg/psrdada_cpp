include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_edd_merge
    src/gtest_edd_merge.cpp
    src/merge_tester.cpp
    src/pipeline_tester.cpp
)

add_executable(gtest_edd_merge ${gtest_edd_merge} )
target_link_libraries(gtest_edd_merge ${PSRDADA_CPP_MERGE_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_edd_merge gtest_edd_merge --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
