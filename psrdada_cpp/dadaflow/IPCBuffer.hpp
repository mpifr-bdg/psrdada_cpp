#pragma once
#include "psrdada_cpp/dadaflow/Semaphore.hpp"

#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <cstring>
#include <span>
#include <stdexcept>
#include <string>

namespace psrdada_cpp
{

namespace bip = boost::interprocess;

/**
 * @brief A class for wrapping posix shared memory allocations
 * 
 */
class IPCBuffer
{
  public:
    /**
     * @brief An RAII lock that will increment the update semaphore on release
     * 
     */
    struct WriteLock {
        explicit WriteLock(IPCBuffer& buf): _buf(buf)
        {
            _buf._mutex_sem.wait();
        }
        ~WriteLock()
        {
            _buf._mutex_sem.post();
            _buf._update_sem.post();
        }

      private:
        IPCBuffer& _buf;
    };

    /**
     * @brief An RAII lock that will not increment the update semaphore on release
     * 
     */
    struct ReadLock {
        explicit ReadLock(IPCBuffer& buf): _buf(buf) { _buf._mutex_sem.wait(); }
        ~ReadLock() { _buf._mutex_sem.post(); }

      private:
        IPCBuffer& _buf;
    };

    friend WriteLock;
    friend ReadLock;

    /**
     * @brief Construct a new IPCBuffer object
     * 
     * @param key The shared memory name to be used
     * @param nbytes The size of the buffer to allocate
     * 
     * @note Clients that are waiting on this shared memory segment
     *       to exist must check that the update semaphore has a count
     *       >= 1 to be sure that the buffer has been fully 
     *       initialised.
     */
    IPCBuffer(std::string_view key, std::size_t nbytes)
        : _shared_memory_name(key),
          _update_sem(_shared_memory_name + "_update",
                      0) // Semaphore for signaling updates
          ,
          _mutex_sem(_shared_memory_name + "_mutex",
                     1) // Mutex semaphore for mutual exclusion
          ,
          _nbytes(nbytes)
    {
        // Remove any existing shared memory segment with the same name
        bip::shared_memory_object::remove(_shared_memory_name.c_str());

        // Create a new shared memory object
        _shared_memory = bip::shared_memory_object(bip::create_only,
                                                   _shared_memory_name.c_str(),
                                                   bip::read_write);

        _shared_memory.truncate(nbytes);

        // We are currently always assuming that the memory is also writable
        _region = bip::mapped_region(_shared_memory, bip::read_write);

        // Zero out the buffer for safety
        std::memset(_region.get_address(), 0, _region.get_size());

        // Clients shouldn't use the buffer until the update count is at least 1
        _update_sem.post();
        _last_update_count = _update_sem.count();
    }

    ~IPCBuffer()
    {
        bip::shared_memory_object::remove(_shared_memory_name.c_str());
    }

    /**
     * @brief Check if the buffer has been updated
     * 
     * @return true The contents of buffer have been updated
     * @return false The contents of the buffer have not been updated
     * 
     * @details This call specifically checks the value of the update semaphore
     *          compared to the last time it was read. This means that the call
     *          is returning whether the update semaphore has incremented since
     *          the last has_update() call that returned true.
     * 
     * @note A return value of true does not guarantee that the contents of the buffer
     *       have changed. Only that a writer to the buffer has notified a change via
     *       incrementing the update semaphore.
     */
    bool has_update()
    {
        int current_count = _update_sem.count();
        if(_last_update_count == current_count) {
            return false;
        } else {
            _last_update_count = current_count;
            return true;
        }
    }

    /**
     * @brief Return a std::span over the allocated buffer
     * 
     * @tparam T The type to interpret the memory as
     * @return std::span<T> 
     * 
     * @note This is a low-level access to the buffer and manually locking
     *       is needed for thread-safe reads/writes. For most use-cases it is 
     *       preferably to use the SharedStruct wrapper.
     */
    template <typename T>
    std::span<T> as() const
    {
        return std::span<T>(reinterpret_cast<T*>(_region.get_address()),
                            _nbytes / sizeof(T));
    }

    /**
     * @brief Return a pointer to the start of the buffer
     * 
     * @return char* 
     */
    char* data() { return static_cast<char*>(_region.get_address()); }

    /**
     * @brief Return the size of the buffer in bytes
     * 
     * @return std::size_t 
     */
    std::size_t size() const { return _nbytes; }

  private:
    std::string _shared_memory_name; // Name of the shared memory object
    Semaphore _update_sem;           // Semaphore for signaling updates
    Semaphore _mutex_sem;            // Semaphore for mutual exclusion
    std::size_t _nbytes;             // Number of elements in the buffer
    bip::shared_memory_object _shared_memory; // Shared memory object
    bip::mapped_region _region; // Mapped region of the shared memory
    int _last_update_count;     // The last seen update count
};

} // namespace psrdada_cpp
