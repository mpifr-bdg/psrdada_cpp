#include "psrdada_cpp/dadaflow/io/sigproc/test/SigprocUnpackerTester.hpp"

namespace psrdada_cpp {
namespace sigproc {
namespace test {

typedef ::testing::Types<
   SigprocUnpackerTestTraits<Filterbank1Bit, Filterbank<std::vector<float>>>,
   SigprocUnpackerTestTraits<Filterbank2Bit, Filterbank<std::vector<float>>>,
   SigprocUnpackerTestTraits<Filterbank4Bit, Filterbank<std::vector<float>>>,
   SigprocUnpackerTestTraits<Filterbank8Bit, Filterbank<std::vector<float>>>,
   SigprocUnpackerTestTraits<Filterbank8BitSigned, Filterbank<std::vector<float>>>,
   SigprocUnpackerTestTraits<Filterbank32Bit, Filterbank<std::vector<float>>>,
   SigprocUnpackerTestTraits<Filterbank32BitSigned, Filterbank<std::vector<float>>>
> TestTypes;
TYPED_TEST_SUITE(SigprocUnpackerTester, TestTypes);

TYPED_TEST(SigprocUnpackerTester, unpack_pattern)
{
    using InputType = typename TypeParam::InputType;
    using OutputType = typename TypeParam::OutputType;
    auto input_ptr = std::make_shared<InputType>();
    input_ptr->resize({123, 11});
    FilterbankVariant input_var = input_ptr;
    this->populate_pattern(*input_ptr);
    SigprocUnpacker<OutputType> unpacker;
    auto output_ptr = unpacker(input_var);
    this->validate_pattern(*input_ptr, *output_ptr);
}

TYPED_TEST(SigprocUnpackerTester, zero_size)
{
    using InputType = typename TypeParam::InputType;
    using OutputType = typename TypeParam::OutputType;
    auto input_ptr = std::make_shared<InputType>();
    FilterbankVariant input_var = input_ptr;
    this->populate_pattern(*input_ptr);
    SigprocUnpacker<OutputType> unpacker;
    auto output_ptr = unpacker(input_var);
    this->validate_pattern(*input_ptr, *output_ptr);
}

} // namespace test
} // namespace sigproc
} // namespace psrdada_cpp