#pragma once
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dadaflow/Generator.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "psrdada_cpp/dadaflow/ThreadSafeQueue.hpp"

#include <atomic>
#include <functional>
#include <iostream>
#include <queue>
#include <string>
#include <thread>
#include <semaphore>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <optional>
#include <memory>
#include <sched.h>

using namespace std::chrono_literals;

namespace psrdada_cpp
{

// Helper trait to check if a callable returns void
template <typename Callable, typename... Args>
struct is_void_return_type {
    static constexpr bool value =
        std::is_same_v<std::invoke_result_t<Callable, Args...>, void>;
};

/**
 * @brief Common interface for all node executors
 *
 * @details All Executors take a processing node as an argument and
 *          handle the execution of that node for the given sets of
 *          input and output queues.
 */
class ExecutorInterface
{
  public:
    /**
     * @brief Execute a single pass of the processing node
     *
     * @return true -- Execution should continue
     * @return false -- A termination condition was met
     *
     * ? Should this be public. It is not called.
     */
    virtual bool run() = 0;

    /**
     * @brief Run the executor forever in its own loop
     *
     */
    virtual void run_async() = 0;

    /**
     * @brief Request the exector thread to stop
     *
     */
    virtual void stop() = 0;

    /**
     * @brief Join the executor thread and wait for completion
     *
     */
    virtual void await() = 0;

    /**
     * @brief Get the IDs of all input queues for the executor
     *
     * @return std::vector<UUID::value_type>
     */
    virtual std::vector<UUID::value_type> input_queue_ids() const = 0;

    /**
     * @brief Get the IDs of all output queues for the executor
     *
     * @return std::vector<UUID::value_type>
     */
    virtual std::vector<UUID::value_type> output_queue_ids() const = 0;

    /**
     * @brief Get the names of all input queues for the executor
     *
     * @return std::vector<UUID::value_type>
     */
    virtual std::vector<std::string> input_queue_names() const = 0;

    /**
     * @brief Get the names of all output queues for the executor
     *
     * @return std::vector<UUID::value_type>
     */
    virtual std::vector<std::string> output_queue_names() const = 0;

    /**
     * @brief Show the statistics of the executor
     *
     */
    virtual void show_statistics() const = 0;

    /**
     * @brief Return the ID of this executor
     *
     * @return UUID::value_type
     */
    virtual UUID::value_type id() const = 0;

    /**
     * @brief Return the name of this executor
     *
     * @return std::string const&
     */
    virtual std::string const& name() const = 0;

    /**
     * @brief Check for nullptr queues
     * 
     */
    virtual bool has_invalid_ports() const = 0;

    /**
     * @brief Executor dtor
     *
     */
    virtual ~ExecutorInterface() {}
};

// Forward declarations
template <typename T> class InputPort;
template <typename T> class OutputPort;

/**
 * @brief The interface for input port strategies
 * 
 * @tparam T The value type of the port.
 */
template <typename T>
class InputStrategyInterface {
public:
    virtual ~InputStrategyInterface() = default;

    /**
     * @brief Get an element from the port
     * 
     * @param port The port to get from.
     * @return T 
     */
    virtual T get(InputPort<T>& port) = 0;

    virtual bool is_valid(InputPort<T>const& port) const = 0;
};

/**
 * @brief A blocking queue strategy for ports
 * 
 * @details Blocks on the incoming queue until a value is available.
 * 
 * @note Requires that the port has a queue. 
 *
 * @tparam T 
 */
template <typename T>
class BlockingQueueStrategy : public InputStrategyInterface<T> {
public:
    T get(InputPort<T>& port) override {
        return port.queue().pop();
    }

    bool is_valid(InputPort<T>const& port) const override {
        return port.has_queue();
    };
};

/**
 * @brief A non-blocking constant value strategy
 * 
 * @details Returns a constant value. Beware that if this constant
 *          value is a shared_ptr it will literally be the same shared_ptr
 *          returned on each get call. 
 * 
 * @tparam T 
 */
template <typename T>
class ConstantValueStrategy : public InputStrategyInterface<T> {
public:
    ConstantValueStrategy(T const& value): _value(value) {}

    T get(InputPort<T>& port) override {
        return _value;
    }

    bool is_valid(InputPort<T> const& port) const override {
        return true;
    };

private:
    T _value;
};

/**
 * @brief A cached queue consumer
 * 
 * @details Will block until it receives an initial value. After that
 *          it will be non-blocking, returning the last popped value 
 *          if the input queue is empty.
 * 
 * @note Requires a connected queue.
 * 
 * @tparam T 
 */
template <typename T>
class CachedBlockingQueueStrategy : public InputStrategyInterface<T> {
public:
    T get(InputPort<T>& port) override {
        auto& queue = port.queue();
        if (queue.empty()) {
            if (has_cached_value) {
                return _value;
            } else {
                _value = queue.pop();
                has_cached_value = true;
                return _value;
            }
        } else {
            _value = queue.pop();
            return _value;
        }
    }

    bool is_valid(InputPort<T> const& port) const override {
        return port.has_queue();
    };

private:
    bool has_cached_value = false;
    T _value;
};

class PortDisabledException : public std::runtime_error {
public:
    PortDisabledException() : std::runtime_error("Operation cannot be performed on disabled port.") {}
};

class NoQueueException : public std::runtime_error {
public:
    NoQueueException() : std::runtime_error("No queue attached to port.") {}
};


/**
 * @brief The base interface for all ports
 * 
 * @tparam T The value type of the port
 */
template <typename T>
class BasePort
{
public:
    using value_type = unwrap_optional_t<T>;
    using QueueType = QueueBaseT<value_type>;
    using QueuePtr = std::shared_ptr<QueueType>;

    /**
     * @brief Construct a new BasePort object
     * 
     * @note Ports are enabled by default.
     */
    BasePort() noexcept : _enabled(true) {}

    /**
     * @brief Enable the port.
     * 
     * @details Enabled ports can be gotten from and pushed to.
     * 
     */
    void enable() noexcept {
        _enabled = true;
    }

    /**
     * @brief Disable the port.
     * 
     * @details Disabled ports cannot be gotten from. Pushes to a 
     *          disabled port will be discarded.
     * 
     */
    void disable() noexcept {
        _enabled = false;
    }

    /**
     * @brief Check if the ports state is valid
     * 
     * @return true Port is valid and can be used
     * @return false Port is invalid and is not usable in a graph
     */
    virtual bool is_valid() const noexcept = 0;

    /**
     * @brief Connect a queue to this port.
     * 
     * @param queue_ptr A shared pointer to the queue object
     */
    void set_queue_ptr(QueuePtr queue_ptr) noexcept {
        _queue_ptr = queue_ptr;
    }

    /**
     * @brief Get the queue connected to this port.
     * 
     * @param queue_ptr The connected queue.
     */
    QueuePtr get_queue_ptr() const noexcept {
        return _queue_ptr;
    }

    /**
     * @brief Check if there is a queue connected to this port
     * 
     * @return true There is a queue
     * @return false There is no queue
     */
    bool has_queue() const noexcept {
        return _queue_ptr != nullptr;
    }

    /**
     * @brief Safe accessor for any connected queue
     * 
     * @details Will throw NoQueueException if no queue is attached
     * 
     * @return QueueType& A reference to the underlying queue
     */
    QueueType& queue() {
        if (!has_queue()) {
            throw NoQueueException();
        }
        return *_queue_ptr;
    }

    /**
     * @brief Safe const accessor for any connected queue
     * 
     * @details Will throw NoQueueException if no queue is attached
     * 
     * @return QueueType& A reference to the underlying queue
     */
    QueueType const& queue() const {
        if (!has_queue()) {
            throw NoQueueException();
        }
        return *_queue_ptr;
    }

    /**
     * @brief Flush any queue attached to the port.
     * 
     */
    void flush() noexcept {
        if (has_queue()) {
            queue().flush();
        }
    }

protected:
    std::atomic<bool> _enabled;
    QueuePtr _queue_ptr;
};


/**
 * @brief A port for receiving inputs.
 * 
 * @tparam T The value type of the port
 */
template <typename T>
class InputPort: public BasePort<T>
{
public:
    /**
     * @brief Construct a new InputPort object
     * 
     * @details The port strategy will default to a blocking queue
     */
    InputPort() noexcept : BasePort<T>(), _strategy(std::make_unique<BlockingQueueStrategy<T>>()) {}

    /**
     * @brief Get an item from the port
     * 
     * @return T 
     */
    T get() {
        if (!this->_enabled)
        {
            if constexpr (is_optional_v<T>) {
                return {};
            } else {
                throw PortDisabledException();
            }
        } else {
            return _strategy->get(*this);
        }
    }

    /**
     * @brief Connect this port to the output port of another node with a queue.
     * 
     * @tparam QueueVariant The queue type as a template template.
     * @tparam OtherPort The port to connect to.
     * @tparam Args Arguments for the queue being created.
     */
    template <template <typename> class QueueVariant, typename OtherPort, typename... Args>
    requires (std::is_same_v<typename BasePort<T>::value_type, typename OtherPort::value_type>
              && is_instantiation_of<OutputPort, OtherPort>::value)
    void connect(OtherPort& other, Args&&... args) noexcept
    {
        auto queue = std::make_shared<QueueVariant<typename BasePort<T>::value_type>>(std::forward<Args>(args)...);
        this->set_queue_ptr(queue);
        other.set_queue_ptr(queue);
    }

    /**
     * @brief Set the strategy for the port
     * 
     * @tparam Strategy The type of the strategy to use for the port
     * @tparam Args The types of the constructor arguments for the strategy
     * @param args The arguments to the strategy constructor
     */
    template <typename Strategy, typename... Args>
    void set_strategy(Args&&... args) noexcept {
        _strategy = std::make_unique<Strategy>(std::forward<Args>(args)...);
    }

    /**
     * @brief Check if the port has a valid state
     *  
     * @return true The port is valid
     * @return false The port is invalid
     */
    bool is_valid() const noexcept override {
        if (this->_enabled) {
            return _strategy->is_valid(*this);
        } else {
            return true;
        }
    }

private:
    std::unique_ptr<InputStrategyInterface<T>> _strategy;
};

/**
 * @brief A port for pushing outputs
 * 
 * @tparam T The value type of the port
 */
template <typename T>
class OutputPort: public BasePort<T>
{
public:
    /**
     * @brief Construct a new OutputPort object
     * 
     */
    OutputPort() noexcept : BasePort<T>() {}

    /**
     * @brief Push a value to the output port.
     * 
     * @details If enabled this will push to a queue if disabled this will discard. Call
     *          will block if the queue is full. 
     * 
     * @param value Value to push
     * 
     * @note Function is marked as noexcept but this->queue() can throw. The intention
     *       is never to use this port if is_valid() == false;
     */
    void push(T&& value) noexcept {
        if (this->_enabled) {
            auto& q = this->queue();
            if constexpr (is_optional_v<T>) {
                if (value.has_value()) {
                    q.push(std::move(*value));
                }
            } else {
                q.push(std::move(value));
            }
        }
    }

    /**
     * @brief Push a value to the output port. 
     * 
     * @details If enabled this will push to a queue if disabled this will discard. 
     *          Call is non-blocking. 
     * 
     * @param value Value to push
     * 
     * @note Function is marked as noexcept but this->queue() can throw. The intention
     *       is never to use this port if is_valid() == false;
     */
    void push_noblock(T&& value) noexcept {
        if (this->_enabled) {
            auto& q = this->queue();
            if constexpr (is_optional_v<T>) {
                if (value.has_value()) {
                    q.push_noblock(std::move(*value));
                }
            } else {
                q.push_noblock(std::move(value));
            }
        }
    }

    /**
     * @brief Check if the port is in a valid state. 
     * 
     * @note Invalid ports will throw when used.
     * 
     * @return true Port is valid.
     * @return false Port is not valid.
     */
    bool is_valid() const noexcept override {
        if (this->_enabled) {
            return this->has_queue();
        } else {
            return true;
        }
    }

    /**
     * @brief Connect this port to the input port of another node with a queue.
     * 
     * @tparam QueueVariant The type of the queue to be used (template, template)
     * @tparam OtherPort The port to connect to.
     * @tparam Args The types of the args for the queue constructor
     * 
     * @param other The port to connect to
     * @param args The args for the queue constructor.
     */
    template <template <typename> class QueueVariant, typename OtherPort, typename... Args>
    requires (std::is_same_v<typename BasePort<T>::value_type, typename OtherPort::value_type> &&
              is_instantiation_of<InputPort, OtherPort>::value)
    void connect(OtherPort& other, Args&&... args) noexcept
    {
        auto queue = std::make_shared<QueueVariant<T>>(std::forward<Args>(args)...);
        this->set_queue_ptr(queue);
        other.set_queue_ptr(queue);
    }
};


template <template <typename> class PortType, typename Tuple>
struct make_port_tuple_type;

// Specialization for std::tuple types.
template <template <typename> class PortType, typename... Args>
struct make_port_tuple_type<PortType, std::tuple<Args...>> {
    using type = std::tuple<PortType<Args>...>;
};

// Main template alias to generate the desired std::tuple of std::unique_ptr<QueueBaseT<T>> types
template <template <typename> class PortType, typename Tuple>
using make_port_tuple_t = typename make_port_tuple_type<PortType, Tuple>::type;

template <typename T>
struct unwrap_return_type {
    using type = std::tuple<T>;
};

template <typename T>
struct unwrap_return_type<Generator<T>> {
    using type = unwrap_return_type<T>::type;
};

template <typename... T>
struct unwrap_return_type<std::tuple<T...>> {
    using type = std::tuple<T...>;
};

template <typename T>
struct unwrap_return_type<std::optional<T>> {
    using type = unwrap_return_type<T>::type;
};

template <>
struct unwrap_return_type<void> {
    using type = std::tuple<>;
};

template <typename T>
using unwrap_return_type_t = typename unwrap_return_type<T>::type;

/**
 * @brief A class for executing user-define callable
 *
 * @tparam ProcessType       The type of callable that will be executed in this
 * node
 *
 * @details This class behaves like a node in the execution graph
 */
template <typename ProcessType, typename ProcessTraits = function_traits<ProcessType>>
class ProcessExecutor: public ExecutorInterface
{
  public:
    using ArgTypes = typename ProcessTraits::argument_types;
    using ReturnTypes = unwrap_return_type_t<typename ProcessTraits::return_type>;
    using InputPorts = make_port_tuple_t<InputPort, ArgTypes>;
    using OutputPorts = make_port_tuple_t<OutputPort, ReturnTypes>;
    using ExceptionHandlerType = std::function<void(std::exception const&)>;
    template <int N> using InputPortType = std::tuple_element_t<N, InputPorts>;
    template <int N> using OutputPortType = std::tuple_element_t<N, OutputPorts>;
    template <int N> using InputValueType = typename InputPortType<N>::value_type;
    template <int N> using OutputValueType = typename OutputPortType<N>::value_type;

  private:
    using clock = std::chrono::high_resolution_clock;
    InputPorts _input_ports;
    OutputPorts _output_ports;
    static_assert(std::tuple_size_v<InputPorts> == std::tuple_size_v<ArgTypes>);
    static_assert(std::tuple_size_v<OutputPorts> == std::tuple_size_v<ReturnTypes>);
    ProcessType _process_instance;
    std::thread _worker_thread;
    std::string _name;
    std::string _full_name;
    ExceptionHandlerType _exception_handler;
    std::atomic<bool>  _stop_requested;
    UUID::value_type _id;
    clock::time_point _start_time;
    std::chrono::duration<double> _idle_duration; 
    std::binary_semaphore _sem0;
    std::binary_semaphore _sem1;  

  public:
    /**
     * @brief Construct a new ProcessExecutor object
     *
     * @param process_instance   The callable being wrapped by the executor.
     * @param name_              A user-friendly name for this executor, e.g.
     * steve.
     * @param exception_handler  An exeception handler to be called if the
     * wrapped process throws.
     *
     * @details Both the process_instance and the exception_handler will be
     *          moved during construction.
     *
     * @note The ProcessExecutor is used by the Graph class to wrap processes.
     * There are no explicit protections to prevent this class being used in
     * isolation, but that is not the inteded use case.
     */
    ProcessExecutor(ProcessType&& process_instance,
                    std::string&& name_,
                    ExceptionHandlerType&& exception_handler) noexcept
        : _process_instance(std::move(process_instance)),
          _name(std::move(name_)),
          _exception_handler(std::move(exception_handler)),
          _stop_requested(false), _id(UUID::next()), _start_time(clock::now()), 
          _sem0(1), _sem1(1)
    {
        // BOOST_LOG_TRIVIAL(debug) << "Creating ProcessExecutor: " << name();
        // BOOST_LOG_TRIVIAL(debug) << "Executor type: " <<
        // getTypeName<decltype(*this)>(); BOOST_LOG_TRIVIAL(debug) << "N input
        // queues: "
        //           << std::tuple_size<decltype(_input_queues)>::value;
        // BOOST_LOG_TRIVIAL(debug) << "N output queues: "
        //           << std::tuple_size<decltype(_output_queues)>::value;
        _full_name = _name + " [" + std::to_string(id()) + "]";
    }

    // Disable copy construction
    ProcessExecutor(ProcessExecutor const&) = delete;

    /**
     * @brief Destroy the Process Executor object
     *
     * @details Will call stop on the pipeline and await
     *          a graceful shutdown.
     */
    ~ProcessExecutor()
    {
        stop();
    }

    /**
     * @brief Get the Nth input port
     * 
     * @tparam N The index of the input port
     * @return auto& The port
     */
    template <int N>
    auto& input_port()
    {
        return std::get<N>(_input_ports);
    }

    /**
     * @brief Get the Nth output port
     * 
     * @tparam N The index of the output port
     * @return auto& The port
     */
    template <int N>
    auto& output_port()
    {
        return std::get<N>(_output_ports);
    }

    /**
     * @brief Return the tuple of all input ports
     * 
     * @return auto& The input ports
     */
    auto& input_ports()
    {
        return _input_ports;
    }

    /**
     * @brief Return the tuple of all output ports
     * 
     * @return auto& The output ports
     */
    auto& output_ports()
    {
        return _output_ports;
    }

    /**
     * @brief Run the wrapped process in an infinite loop.
     *
     * @details This call will invode the process wrapper in a loop
     *          until one of the following conditions is met:
     *            - An exception is thrown
     *            - A stop is requested
     *            - A nullptr is detected on any of the input queues
     *          In the case of a throw, the exception handler will be
     *          called.
     */
    void run_async()
    {
        _start_time     = clock::now();
        _stop_requested = false;
        // Start the worker thread and run until finished or until
        // a stop is requested.
        _worker_thread = std::thread([this]() {
            bool exception_caught = false;
            try {
                while(!_stop_requested) {
                    _sem0.acquire();
                    if(!run()) {
                        _sem0.release();
                        break;
                    }
                    _sem0.release();

                    // We use two semaphores in this loop due to something like (but not the same as) the "thundering herd" 
                    // problem. What seems to happen is that if we only use _sem0, the release and re-acquire in the while
                    // loop can result in only this thread being able to acquire the semaphore. For another thread
                    // to acquire the semaphore this thread must context switch. This can be forced with a usleep
                    // call although it is not clear if this is reliable. Instead we use a second semaphore set to
                    // wrap the first which allows for clean blocking of this thread from another thread.
                    _sem1.acquire();
                    _sem1.release();
                }
            } catch(std::exception& err) {
                BOOST_LOG_TRIVIAL(error)
                    << this->name()
                    << " run_async failed with exception: " << err.what();
                exception_caught = true;
                _exception_handler(err);
            }
            if(!exception_caught) {
                BOOST_LOG_TRIVIAL(debug)
                    << this->name()
                    << " run_async completed successfully (stop requested = "
                    << _stop_requested << ")";
            }
        });
    }

    /**
     * @brief Stop the process executor and wait for completion.
     *
     * @details We first flush the output queues to avoid deadlock on waiting pushes.
     *          After the wait is complete, nullptr sentinal values will be pushed into 
     *          all downstream queues.
     */
    void stop()
    {
        _stop_requested = true;
        // As this node may be blocked on a full output queue
        // we first flush the ports.
        flush_ports(_output_ports);
        // Await completion of the processing loop
        await();
        push_sentinels();
    }

    /**
     * @brief Enable a specific set of input ports
     * 
     * @tparam N The set of input ports to enable
     * 
     * @details All input ports not in the set will be disabled
     */
    template <int... N>
    void select_inputs()
    {
        hold();
        disable_ports(_input_ports);
        (input_port<N>().enable(), ...);
        release();
    }

    /**
     * @brief Enable a specific set of output ports
     * 
     * @tparam N The set of output ports to enable
     * 
     * @details All output ports not in the set will be disabled
     */
    template <int... N>
    void select_outputs()
    {
        hold();
        disable_ports(_output_ports);
        (output_port<N>().enable(), ...);
        release();
    }

    /**
     * @brief Lock the process executor and prevent it from making gets or pushes on its ports.
     * 
     * @details This will block until the current iteration of the processing loop is complete.
     */
    void hold() {
        _sem1.acquire();
        _sem0.acquire();
    }

    /**
     * @brief Unlock the process executor and allow it to continue processing.
     */
    void release() {
        _sem0.release();
        _sem1.release();
    }

    /**
     * @brief Block until the processing loop exits.
     *
     */
    void await()
    {
        if(_worker_thread.joinable()) {
            _worker_thread.join();
        }
    }

    /**
     * @brief Get the callable object
     * 
     * @return ProcessType& 
     */
    ProcessType& get_callable()
    {
        return _process_instance;
    }

    /**
     * @brief Show the statistics of the executor.
     *
     * @details Provides information about the idle time spent blocking on
     *          input or output queues and total data throughput of the process.
     */
    void show_statistics() const
    {
        BOOST_LOG_TRIVIAL(debug) << "Statistics of " << name();
        auto total_run_time =
            std::chrono::duration_cast<std::chrono::microseconds>(clock::now() -
                                                                  _start_time)
                .count();
        auto idle_time = std::chrono::duration_cast<std::chrono::microseconds>(
                             _idle_duration)
                             .count();
        BOOST_LOG_TRIVIAL(debug)
            << "Total running time: " << total_run_time / 1.0e6 << " s";
        BOOST_LOG_TRIVIAL(debug) << "Idle time: " << idle_time / 1.0e6 << " s";
        BOOST_LOG_TRIVIAL(debug)
            << "Efficiency: "
            << 100.0 * (total_run_time - idle_time) / total_run_time << "%";
        if constexpr(std::tuple_size_v<InputPorts> > 0) {
            BOOST_LOG_TRIVIAL(debug) << "Input ports: ";
            for_each_in_tuple(_input_ports, [](auto const& port) {
                if (!port.has_queue())
                {
                    BOOST_LOG_TRIVIAL(debug) << "  |-> No queue";
                } else {
                    auto& q = port.queue();
                    BOOST_LOG_TRIVIAL(debug)
                        << "  |-> [" << q.id() << "]: " << q.pop_rate()
                        << " elements/s";
                }
            });
        }
        if constexpr(std::tuple_size_v<OutputPorts> > 0) {
            BOOST_LOG_TRIVIAL(debug) << "Output ports: ";
            for_each_in_tuple(_output_ports, [](auto const& port) {
                if (!port.has_queue())
                {
                    BOOST_LOG_TRIVIAL(debug) << "  |-> No queue";
                } else {
                    auto& q = port.queue();
                    BOOST_LOG_TRIVIAL(debug)
                        << "  |-> [" << q.id() << "]: " << q.pop_rate()
                        << " elements/s";
                }
            });
        }
    }

    /**
     * @brief Get the IDs of all input queues.
     *
     * @return std::vector<UUID::value_type>
     */
    std::vector<UUID::value_type> input_queue_ids() const
    {
        // Only return ids for connected ports
        return queue_ids(_input_ports);
    }

    /**
     * @brief Get the IDs of all output queues.
     *
     * @return std::vector<UUID::value_type>
     */
    std::vector<UUID::value_type> output_queue_ids() const
    {
        return queue_ids(_output_ports);
    }

    /**
     * @brief Get the names of all input queues.
     *
     * @return std::vector<std::string>
     */
    std::vector<std::string> input_queue_names() const
    {
        return queue_names(_input_ports);
    }

    /**
     * @brief Get the names of all output queues.
     *
     * @return std::vector<std::string>
     */
    std::vector<std::string> output_queue_names() const
    {
        return queue_names(_output_ports);
    }

    /**
     * @brief Get a unique ID for this executor instance.
     *
     * @return UUID::value_type
     */
    UUID::value_type id() const { return _id; }

    /**
     * @brief Return the name of this executor instance.
     *
     * @return std::string const&
     */
    std::string const& name() const { return _full_name; };

    bool has_invalid_ports() const override {
        return !has_valid_ports(_input_ports) || !has_valid_ports(_output_ports);
    }

  private:
    // Run a single pass of the executor
    bool run()
    {
        // BOOST_LOG_TRIVIAL(debug) << "ProcessExecutor: " << name();

        auto start_fetch_inputs = clock::now();
        // Do a blocking get for one input from all input queues
        // Currently it is required that all queues have a value
        // before we can execute.
        auto inputs =
            get_inputs(std::make_index_sequence<std::tuple_size_v<InputPorts>>{});
        _idle_duration += (clock::now() - start_fetch_inputs);

        // Currently we use nullptrs as sentinels. Any time a nullptr is
        // detected on ANY input queue it is assume that execution is now
        // complete. Cascade behaviour is then used as all queues get nullptrs
        // passed to them.
        if(any_nullptr(inputs)) {
            BOOST_LOG_TRIVIAL(debug)
                << "Nullptr detected on input queue, pushing nullptrs to "
                   "outputs";
            push_sentinels();
            return false; // Return true to indicate that iteration should stop
        }

        // BOOST_LOG_TRIVIAL(debug) << "Inputs: ";
        // print_tuple(inputs);

        // Invoke the wrapped processes' process method. The pattern here is
        // that there is a one-to-one mapping between the arguments of the
        // process call and the set of input queues. This type compatibility is
        // enforced at compile time.

        auto result = std::apply(
            [&](auto&&... args) {
                if constexpr(is_void_return_type<ProcessType,
                                                 decltype(args)...>::value) {
                    // For void functions we return empty tuples as we can't
                    // bind auto result to void (as it is an incomplete type).
                    _process_instance(std::forward<decltype(args)>(args)...);
                    return std::tie();
                } else {
                    return _process_instance(
                        std::forward<decltype(args)>(args)...);
                }
            },
            inputs);

        // The result of the process call may or may not be a Generator and
        // values returned from the call (whether wrapped in a generator or not)
        // may be std::optional. All these cases must be handled.
        if constexpr(is_generator_v<decltype(result)>) {
            while(result.next()) {
                auto outputs = result.get_value();
                // Handle std::optional vs non-std::optional values
                handle_output(outputs);
            }
        } else {
            // Handle std::optional vs non-std::optional values
            handle_output(result);
        }

        return true;
    }

    // Handle optional values
    template <typename T>
    void handle_output(T& output)
    {
        if constexpr(is_optional_v<T>) {
            if(output) {
                push_outputs(*output);
            }
        } else {
            push_outputs(output);
        }
    }

    void push_outputs(auto& outputs)
    {
        // Outputs here is either some type or some
        // std::tuple of types. For tuples the elements
        // are mapped to output queues. For standalone
        // types the it is pushed to the first queue.
        auto start_push_outputs = clock::now();
        if constexpr(is_tuple_v<decltype(outputs)>) {
            std::apply(
                [&](auto&... port) {
                    std::apply(
                        [&](auto&... out) {
                            ((port.push(std::move(out))), ...);
                        },
                        outputs);
                },
                _output_ports);
        } else {
            std::get<0>(_output_ports).push(std::move(outputs));
        }
        _idle_duration += clock::now() - start_push_outputs;
    }

    void disable_ports(auto& ports) {
        std::apply(
            [](auto&... port) -> void { (port.disable(), ...); },
            ports);
    }

    void enable_ports(auto& ports) {
        std::apply(
            [](auto&... port) -> void { (port.enable(), ...); },
            ports);
    }

    void flush_ports(auto& ports) {
        std::apply(
            [](auto&... port) -> void { (port.flush(), ...); },
            ports);
    }

    template <std::size_t... Is>
    auto get_inputs(std::index_sequence<Is...>)
    {
        return std::make_tuple(std::get<Is>(_input_ports).get()...);
    }

    template <typename Tuple>
    bool any_nullptr(Tuple const& inputs) const 
    {
        return std::apply(
            [](const auto&... args) { return ((args == nullptr) || ...); },
            inputs);
    }

    template <typename Tuple>
    bool has_valid_ports(Tuple const& ports) const 
    {
        return std::apply(
            [](const auto&... port) { return (port.is_valid() && ...); },
            ports);
    }

    template <typename Tuple>
    std::vector<UUID::value_type> queue_ids(Tuple const& ports) const
    {
        return std::apply(
            [](const auto&... port) {
                std::vector<UUID::value_type> ids;
                ((port.has_queue() ? ids.push_back(port.queue().id()) : void()), ...);
                return ids;
            },
            ports);
    }

    template <typename Tuple>
    std::vector<std::string> queue_names(Tuple const& ports) const
    {
        return std::apply(
            [](const auto&... port) {
                std::vector<std::string> names;
                ((port.has_queue() ? names.push_back(port.queue().name()) : void()), ...);
                return names;
            },
            ports);
    }

    void push_sentinels()
    {
        // Incase there are inactive parts of the graph that are blocking
        // on inputs from this node, we first enable all ports so that the
        // sentinel cascade reaches all nodes.
        enable_ports(_output_ports);
        std::apply(
            [&](auto&... port) { ((port.push_noblock(nullptr)), ...); },
            _output_ports);
    }
};

} // namespace psrdada_cpp