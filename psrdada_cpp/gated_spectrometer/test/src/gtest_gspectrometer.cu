#include <gtest/gtest.h>
#include <cstdlib>

#include "psrdada_cpp/cli_utils.hpp"

int main(int argc, char **argv) {
    psrdada_cpp::init_log_level();
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
