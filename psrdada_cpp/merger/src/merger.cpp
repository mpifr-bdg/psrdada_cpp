#include "psrdada_cpp/merger/merger.hpp"
#include <cstdint>

namespace psrdada_cpp {
namespace merger {

PFBMerger::PFBMerger(std::size_t nchunk, std::size_t nthreads, std::size_t heap_size)
    : _nchunk(nchunk), _nthreads(nthreads), _heap_size(heap_size){
}
PFBMerger::PFBMerger(merge_conf conf)
    : _nchunk(conf.nchunk), _nthreads(conf.nthreads), _heap_size(conf.heap_size){
}

PFBMerger::~PFBMerger(){

}

void PFBMerger::process(char* idata, char* odata, std::size_t size)
{
	const std::size_t bytes_per_chunk = 4;
    const std::size_t heap_group = _heap_size * _nchunk;
    const std::size_t num_chunks = size / heap_group;
    if (size % heap_group != 0){
        throw std::runtime_error("Size is " + std::to_string(size) + " and not a multiple of heap group size " + std::to_string(heap_group));
    }
    #pragma omp parallel for num_threads(_nthreads)
    for (std::size_t xx = 0; xx < num_chunks; ++xx)
    {
        std::vector<const char*> chunk_ptrs(_nchunk);
        for (std::size_t ii = 0; ii < _nchunk; ++ii)
        {
            const std::size_t offset = xx * heap_group + ii * heap_group / _nchunk;
            chunk_ptrs[ii] = idata + offset;
        }
        const char *target = odata + xx * heap_group;
        for (std::size_t yy = 0; yy < heap_group / _nchunk / bytes_per_chunk; ++yy)
        {
            for (std::size_t ii = 0; ii < _nchunk; ++ii)
            {
                std::memcpy((void*)target, chunk_ptrs[ii], bytes_per_chunk);
                chunk_ptrs[ii] += bytes_per_chunk;
                target += bytes_per_chunk;
            }
        }
    }
}



PolnMerger::PolnMerger(std::size_t npol, std::size_t nthreads)
    : _npol(npol), _nthreads(nthreads){
}
PolnMerger::PolnMerger(merge_conf conf)
    : _npol(conf.npol), _nthreads(conf.nthreads){
}

PolnMerger::~PolnMerger(){
}

void PolnMerger::process(char* idata, char* odata, std::size_t size)
{
    const std::size_t _heap_size = 4096;
    #pragma omp parallel for schedule(dynamic, _nthreads) num_threads(_nthreads)
    for (std::size_t kk = 0; kk < size / _heap_size / _npol ; ++kk)
    {
        char *buffer = idata + _heap_size * _npol * kk;
        uint8_t const* qword0 = reinterpret_cast<uint8_t const*>(buffer);
        uint8_t const* qword1 = reinterpret_cast<uint8_t const*>(buffer) + _heap_size;
        uint64_t* tmp = reinterpret_cast<uint64_t*>(odata + kk * _npol * _heap_size);

        __m128i xvec, yvec, interleaved;
        for (size_t i = 0; i < _heap_size / sizeof(__m128i); ++i)
        {
            xvec = _mm_loadu_si128(reinterpret_cast<__m128i const*>(qword0));
            yvec = _mm_loadu_si128(reinterpret_cast<__m128i const*>(qword1));
            interleaved = _mm_unpacklo_epi8(xvec, yvec);
            _mm_storeu_si128(reinterpret_cast<__m128i*>(tmp), interleaved);
            tmp+=2;
            interleaved = _mm_unpackhi_epi8(xvec, yvec);
            _mm_storeu_si128(reinterpret_cast<__m128i*>(tmp), interleaved);
            tmp+=2;
            qword0 += 16;
            qword1 += 16;
        }
    }
}

}
}
