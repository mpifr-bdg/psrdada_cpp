#pragma once
#include "psrdada_cpp/dadaflow/IPCBuffer.hpp"
#include <gtest/gtest.h>
#include <string>

namespace bip = boost::interprocess;

namespace psrdada_cpp {
namespace test {

struct MonitorInfo {
    double lo_frequency;
};

class IPCBufferTester : public ::testing::Test
{
public:
    IPCBufferTester()
    : shared_memory_name("test_monitoring")
    {}

    ~IPCBufferTester(){}

    void SetUp() override {
        bip::shared_memory_object::remove(shared_memory_name.c_str());
    }

    void TearDown() override {
        bip::shared_memory_object::remove(shared_memory_name.c_str());
    }

    std::string shared_memory_name;
};

} // namespace test
} // namespace psrdada_cpp