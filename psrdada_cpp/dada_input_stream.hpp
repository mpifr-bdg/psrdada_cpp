#ifndef PSRDADA_CPP_DADA_INPUT_STREAM_HPP
#define PSRDADA_CPP_DADA_INPUT_STREAM_HPP

#include "psrdada_cpp/dada_read_client.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/common.hpp"

namespace psrdada_cpp
{

template <class HandlerType>
class DadaInputStream
{
public:
    DadaInputStream(key_t key, MultiLog& log, HandlerType& handler)
    : _key(key) , _log(log) , _handler(handler) , _stop(false) , _running(false)
    {
    }


    ~DadaInputStream(){}


    void start()
    {
        bool handler_stop_request = false;
        if (_running)
        {
            throw std::runtime_error("Stream is already running");
        }
        _running = true;
        while (!_stop && !handler_stop_request)
        {
            BOOST_LOG_TRIVIAL(info) << "Attaching new read client to buffer";
            DadaReadClient client(_key,_log);
            auto& header_stream = client.header_stream();
            auto& header_block = header_stream.next();
            _handler.init(header_block);
            header_stream.release();
            auto& data_stream = client.data_stream();
            while (!_stop && !handler_stop_request)
            {
                handler_stop_request = _handler(data_stream.next());
                data_stream.release();
                if (data_stream.at_end())
                {
                    BOOST_LOG_TRIVIAL(info) << "Reached end of data";
                    break;
                }
                //handler_stop_request = _handler(data_stream.next());
                //data_stream.release();
            }
        }
        _running = false;
    }

    void stop(){_stop = true;}

private:
    key_t _key;
    MultiLog& _log;
    HandlerType& _handler;
    bool _stop = false;
    bool _running = false;
};

} //psrdada_cpp

#endif //PSRDADA_CPP_DADA_INPUT_STREAM_HPP
