#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/cli_utils.hpp"

#include "boost/program_options.hpp"

#include <sys/types.h>
#include <iostream>
#include <string>
#include <ios>
#include <vector>
#include <fstream>
#include <chrono>
#include <thread>

#define NANO 1000000000

using namespace psrdada_cpp;

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

namespace psrdada_cpp
{
    template <class Handler>
    void diskdb(Handler& handler,
        std::size_t nfile_read,
        std::string header_file,
        std::string data_file,
        float sample_rate)
    {
        std::size_t header_size = handler.client().header_buffer_size();
        std::size_t block_size = handler.client().data_buffer_size();
        std::vector<char> header(header_size);
      	std::vector<char> data(block_size);
        double sample_step = 1/sample_rate;
        std::chrono::time_point<std::chrono::system_clock> start;
        std::chrono::duration<double, std::ratio<1,NANO>> block_step_ns = std::chrono::duration<double>(sample_step * block_size);

        std::ifstream hinput;
      	std::ifstream dinput;

      	hinput.open(header_file, std::ios::in | std::ios::binary);
        if(!hinput.is_open()){
          std::cerr << "File " << header_file << " not found!" << std::endl;
          return;
        }
      	hinput.read(header.data(), header_size);

        dinput.open(data_file, std::ios::binary | std::ios::ate);
        if(!dinput.is_open()){
          std::cerr << "File " << data_file << " not found!" << std::endl;
          return;
        }
        std::size_t file_size = dinput.tellg();
        if(file_size < block_size){
          std::cerr << "File is smaller than buffer, but must be greater or equal to buffer size";
          return;
        }

        dinput.close();
        RawBytes header_bytes(header.data(), header_size, header_size, false);
        handler.init(header_bytes);
        // Read the file an store it in memory
        dinput.open(data_file, std::ios::in | std::ios::binary);
        std::vector<char> file_vec(file_size);
        dinput.read(file_vec.data(), file_size);

        for(std::size_t i = 0; i < nfile_read; i++)
        {
          std::size_t src_ptr = 0;
          std::size_t fremain = file_size;
          while(fremain > 0){
            start = std::chrono::system_clock::now();
            fremain = file_size - src_ptr;
            if(fremain > block_size){
              memcpy(&data[0], &file_vec[src_ptr], block_size);
              src_ptr += block_size;
            }else{
              memcpy(&data[0], &file_vec[src_ptr], fremain);
              memcpy(&data[fremain], &file_vec[0], block_size - fremain);
              src_ptr = block_size - fremain;
              if(src_ptr == 0){fremain = 0;}
            }
            RawBytes data_bytes(data.data(), block_size, block_size, false);
            handler(data_bytes);
            if(sample_rate == 0){
              continue;
            }
            auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now() - start);
            auto sleep_time = std::chrono::duration_cast<std::chrono::nanoseconds>(block_step_ns - elapsed);
            BOOST_LOG_TRIVIAL(debug) << "Sleep time " << sleep_time.count() << " elapsed time " << elapsed.count() << " block step " << block_step_ns.count() << std::endl;
            std::this_thread::sleep_for(sleep_time);
            BOOST_LOG_TRIVIAL(debug) << "Transferred " << block_size << " bytes in " << block_step_ns.count()/NANO
              << " s (" << block_size / (block_step_ns.count()/NANO) / (1000000) << "(MB/s))." << std::endl;
          }
        }
    }
}

int main(int argc, char** argv)
{
    try
    {
        std::size_t reads;
        key_t key;
        std::string header_file;
        std::string data_file;
        float sample_rate;
        /** Define and parse the program options
        */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("data_file,d", po::value<std::string>(&data_file)->required(),"File containing the data to write to dada buffer")
        ("header_file,f", po::value<std::string>(&header_file)->required(), "Header file to write header block")
        ("rate,s", po::value<float>(&sample_rate)->default_value(0), "Data rate in bytes. If set to 0, it reads as fast as can")
        ("reads,r", po::value<std::size_t>(&reads)->default_value(1), "Number of file reads")
        ("key,k", po::value<std::string>()->default_value("dada")->notifier([&key](std::string in){key = string_to_key(in);}),
            "The shared memory key for the dada buffer to connect to (hex string)")
        ("log_level", po::value<std::string>()->default_value("info")->notifier([](std::string level){set_log_level(level);}),"The logging level to use (debug, info, warning, error)");
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "diskdb -- write file contents into DADA ring buffer" << std::endl
                << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        /**
         * All the application code goes here
         */

        MultiLog log("diskdb");
        DadaOutputStream out_stream(key, log);
        diskdb<decltype(out_stream)>(
            out_stream,
            reads,
            header_file,
            data_file,
            sample_rate);
        /**
         * End of application code
         */
    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
        << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
