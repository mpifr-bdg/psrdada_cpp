#pragma once

#include <cuda.h>
#include "psrdada_cpp/gated_spectrometer/types.hpp"

/// Macro to manipulate single bits of an 64-bit type.
#define BIT_MASK(bit) (1uL << (bit))
#define SET_BIT(value, bit) ((value) |= BIT_MASK(bit))
#define CLEAR_BIT(value, bit) ((value) &= ~BIT_MASK(bit))
#define TEST_BIT(value, bit) (((value)&BIT_MASK(bit)) ? 1 : 0)

namespace psrdada_cpp {
namespace gated_spectrometer {

/**
   * @brief      Splits the input data depending on a bit set into two arrays.
   *
   * @details     The resulting gaps are filled with zeros in the other stream.
   *
   * @param      GO Input data. Data is set to the baseline value if corresponding
   *             sideChannelData bit at bitpos os set.
   * @param      G1 Data in this array is set to the baseline value if corresponding
   *             sideChannelData bit at bitpos is not set.
   * @param      sideChannelData noOfSideChannels items per block of heapSize
   *             bytes in the input data.
   * @param      N lebgth of the input/output arrays G0.
   * @param      heapsize Size of the blocks for which there is an entry in the
                 sideChannelData.
   * @param      bitpos Position of the bit to evaluate for processing.
   * @param      noOfSideChannels Number of side channels items per block of
   *             data.
   * @param      selectedSideChannel No. of side channel item to be eveluated.
                 0 <= selectedSideChannel < noOfSideChannels.
   * @param      stats_G0 No. of sampels contributing to G0, accounting also
   *             for loat heaps
   * @param      stats_G1 No. of sampels contributing to G1, accounting also
   *             for loat heaps
   */
__global__ void gating(
        float* __restrict__ G0,
        float* __restrict__ G1,
        const uint64_t* __restrict__ sideChannelData,
        size_t N,
        size_t heapSize,
        size_t bitpos,
        size_t noOfSideChannels,
        size_t selectedSideChannel,
        const float*  __restrict__ _baseLineG0,
        const float*  __restrict__ _baseLineG1,
        float* __restrict__ baseLineNG0,
        float* __restrict__ baseLineNG1,
        uint64_cu* stats_G0,
        uint64_cu* stats_G1);

/**
 * @brief calculate stokes IQUV from two complex valuies for each polarization
 *
 * @param p1
 * @param p2
 * @param I
 * @param Q
 * @param U
 * @param V
 */
__host__ __device__ void stokes_IQUV(
        const float2 &p1,
        const float2 &p2,
        float &I,
        float &Q,
        float &U,
        float &V);

/**
 * @brief calculate stokes IQUV spectra pol1, pol2 are arrays of naccumulate
 * complex spectra for individual polarizations
 *
 * @param pol1
 * @param pol2
 * @param I
 * @param Q
 * @param U
 * @param V
 * @param nchans
 * @param naccumulate
 */
__global__ void stokes_accumulate(
        float2 const __restrict__* pol1,
        float2 const __restrict__* pol2,
        float* I,
        float* Q,
        float* U,
        float* V,
        int nchans,
        int naccumulate);

/**
 * @brief
 *
 * @details If one of the side channel items is lost, then both are considered as lost
 *      here
 * @param A
 * @param B
 * @param N
 */
__global__ void mergeSideChannels(
        uint64_t* __restrict__ A,
        uint64_t* __restrict__ B,
        size_t N);

/**
 * @brief Updates the baselines of the gates for the polarization set for the next
 *      block
 *
 * @details only few output blocks per input block thus execution on only one thread.
 *      Important is that the execution is async on the GPU.
 * @param baseLineG0
 * @param baseLineG1
 * @param baseLineNG0
 * @param baseLineNG1
 * @param stats_G0
 * @param stats_G1
 * @param N
 * @return __global__
 */
__global__ void update_baselines(
        float*  __restrict__ baseLineG0,
        float*  __restrict__ baseLineG1,
        float* __restrict__ baseLineNG0,
        float* __restrict__ baseLineNG1,
        uint64_cu* stats_G0,
        uint64_cu* stats_G1,
        size_t N);

}
}