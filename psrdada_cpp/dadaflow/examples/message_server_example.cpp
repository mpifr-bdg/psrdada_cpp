#include "psrdada_cpp/dadaflow/MessageQueue.hpp"

#include <boost/json.hpp>
#include <chrono>
#include <iostream>
#include <thread>

using namespace std::chrono_literals;
using namespace psrdada_cpp;
namespace js = boost::json;

int main()
{
    try {
        // We require two queue pairs, one for the server and one for the client
        // if we try to share in the same application we will get lock conflicts

        // Initialize the request and response queues as seen by the server
        PosixMessageQueue request_queue("/message_server_example");
        PosixMessageQueue response_queue("/message_server_example_r");

        // Start the server with request and response queues
        MessageServer server(request_queue, response_queue);

        // Register a callback to handle the "test" command
        server.register_callback(
            "shutdown",
            {},
            "Shutdown the server and prevent further commands.",
            [&](js::object const&) -> js::object {
                server.shutdown();
                return {};
            });

        server.register_callback(
            "say_hello",
            {{"repeats", "int", "The number of times to repeat"}},
            "Says hello, repeatedly.",
            [&](js::object const& args) -> js::object {
                js::object response;
                int nrepeats = 0;
                for(auto ii = 0; ii < args.at("repeats").as_int64(); ++ii) {
                    BOOST_LOG_TRIVIAL(info) << "HELLO!";
                    ++nrepeats;
                }
                response["ntimes_repeated"] = nrepeats;
                return response;
            });

        // Callbacks need to optionally be able to return json blocks.
        server.await();
    } catch(const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
    return 0;
}
