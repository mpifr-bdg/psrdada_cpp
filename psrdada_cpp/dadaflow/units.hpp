#pragma once
#include <boost/units/systems/cgs/length.hpp>
#include <boost/units/systems/cgs/volume.hpp>
#include <boost/units/systems/si/frequency.hpp>
#include <boost/units/base_units/astronomical/parsec.hpp>
#include <boost/units/io.hpp>
#include <ratio>

// Define the parsec unit and its conversions
namespace boost {
namespace units {
namespace astronomical {

using parsec_base_unit = boost::units::astronomical::parsec_base_unit;
using parsec_system = make_system<parsec_base_unit>::type;
using parsec = unit<length_dimension, parsec_system>;
using centimetres_cubed = unit<volume_dimension, cgs::system>;
using dispersion_measure_dimension = decltype(parsec() / centimetres_cubed());
using dispersion_measure = quantity<dispersion_measure_dimension, float>;
using pico = std::ratio<1,1'000'000'000'000>;

static constexpr parsec parsecs = parsec();  
static constexpr parsec pc = parsec();  
static constexpr centimetres_cubed cm3 = centimetres_cubed();

}  // namespace astronomical
}  // namespace units
}  // namespace boost