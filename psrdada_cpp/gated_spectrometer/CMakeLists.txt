
# The gated spectrometer can only be built when CUDA is enabled
if(ENABLE_CUDA)
    set(PSRDADA_CPP_GS_LIBRARIES
        ${PSRDADA_CPP_LIBRARIES}
        ${CMAKE_PROJECT_NAME}_gated_spectrometer
        CUDA::cufft
    )

    set(psrdada_cpp_gated_spectrometer_src
        src/io.cu
        src/kernels.cu
        pipeline.cuh
    )

    set(psrdada_cpp_gated_spectrometer_inc
        io.cuh
        kernels.cuh
        pipeline.cuh
    )

    add_library(${CMAKE_PROJECT_NAME}_gated_spectrometer ${psrdada_cpp_gated_spectrometer_src})
    add_executable(gated_spectrometer cli/gated_spectrometer_cli.cu)
    target_link_libraries(gated_spectrometer ${PSRDADA_CPP_GS_LIBRARIES})
    install(TARGETS gated_spectrometer DESTINATION bin)

    
    # Build the testing executable if testing is enabled - No installation is done 
    if (ENABLE_TESTING)
        add_subdirectory(test)
    endif()

    # Build the benchmar executable if benchmarking is enabled - No installation is done 
    if (ENABLE_BENCHMARK)
        include_directories(${BENCHMARK_INCLUDE_DIR})
        add_executable(gspectrometer_benchmark cli/gspectrometer_benchmark.cu)
        target_sources(gspectrometer_benchmark PRIVATE ${SOURCES})
        target_include_directories(gspectrometer_benchmark PRIVATE "${CMAKE_CURRENT_LIST_DIR}")
        target_link_libraries(gspectrometer_benchmark ${PSRDADA_CPP_GS_LIBRARIES} ${GTEST_LIBRARIES} ${BENCHMARK_LIBRARIES})
    endif()

endif(ENABLE_CUDA)
