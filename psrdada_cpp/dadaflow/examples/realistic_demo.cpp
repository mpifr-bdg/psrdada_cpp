#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Graph.hpp"
#include "psrdada_cpp/dadaflow/Process.hpp"
#include "psrdada_cpp/dadaflow/ResourcePool.hpp"
#include "psrdada_cpp/dadaflow/ThreadSafeQueue.hpp"
#include "psrdada_cpp/dadaflow/utils.hpp"
#include "thrust/device_vector.h"

#include <any>
#include <bit>
#include <memory>
#include <ranges>
#include <span>
#include <vector>
#include <thread>

using namespace std::chrono_literals;

namespace psrdada_cpp
{

// This isn't used but is an example of how to make a concept that captures a
// specific set of base dimensions
template <typename T>
struct is_ptf_like: std::false_type {
};

// Partial specialization for DescribedData with five dimensions
template <typename Container, typename P, typename T, typename F>
struct is_ptf_like<DescribedData<Container, P, T, F>> {
    static constexpr bool value = std::conjunction_v<
        std::is_convertible<P*, BasePolarisationDimension*>,
        std::is_convertible<T*, BaseTimeDimension*>,
        std::is_convertible<F*, ContiguousFrequencyDimension*>>;
};

template <typename T>
concept PTFLike = is_ptf_like<T>::value;

template <typename T>
using TAFTPVoltagesH = DescribedData<std::vector<T>,
                                     TimeDimensionN<0>,
                                     AntennaDimension,
                                     NonContiguousFrequencyDimension,
                                     TimeDimensionN<1>,
                                     PolarisationDimension>;

template <typename T>
using TFPowersH = DescribedData<std::vector<T>,
                                TimeDimension,
                                NonContiguousFrequencyDimension>;

template <typename OutputAllocator>
class TAFTPNullSource
{
  public:
    using OutputType      = TAFTPVoltagesH<int8_t>;
    using OutputPtr       = std::shared_ptr<OutputType>;
    using OutputTuple     = std::tuple<OutputPtr>;
    using FrequenciesType = typename OutputType::FrequenciesType;

    TAFTPNullSource() = delete;

    explicit TAFTPNullSource(OutputAllocator& allocator): _allocator(allocator)
    {
        auto now   = std::chrono::system_clock::now();
        auto epoch = std::chrono::time_point_cast<std::chrono::seconds>(now);
        std::chrono::duration<long double, pico> offset(0.0);
        _timestamp = Timestamp(epoch, offset);
        for(int f_idx = 0; f_idx < 16; ++f_idx) {
            _frequencies.push_back((856.0 + f_idx) * hertz);
        }
    }

    OutputTuple operator()()
    {
        OutputPtr output = _allocator.get();
        auto tsamp =
            std::chrono::duration<long double, pico>(4785.046728971963);
        output->resize({16, 64, 16, 256, 2});
        output->timestamp(_timestamp + _samples_processed * tsamp);
        output->TimeDimensionN<1>::timestep(tsamp);
        std::cout << output->describe() << "\n";
        try {
            output->frequencies(_frequencies);
        } catch(std::exception& e) {
            BOOST_LOG_TRIVIAL(error) << "???????" << ": " << e.what() << "\n";
            BOOST_LOG_TRIVIAL(error)
                << (*output).nchannels() << ", " << _frequencies.size() << "\n";
            throw e;
        }
        _samples_processed += output->total_nsamples();
        return std::make_tuple(output);
    }

    std::size_t samples_processed() const 
    {
        return _samples_processed;
    }

  private:
    OutputAllocator& _allocator;
    Timestamp _timestamp;
    FrequenciesType _frequencies;
    std::size_t _samples_processed = 0;
};

template <typename OutputAllocator>
class IncoherentBeamformer
{
  public:
    using InputType   = TAFTPVoltagesH<int8_t>;
    using InputPtr    = std::shared_ptr<InputType>;
    using OutputType  = TFPowersH<float>;
    using OutputPtr   = std::shared_ptr<OutputType>;
    using OutputTuple = std::tuple<OutputPtr>;

    IncoherentBeamformer() = delete;

    IncoherentBeamformer(OutputAllocator& allocator,
                         unsigned tscrunch = 1,
                         unsigned fscrunch = 1)
        : _allocator(allocator), _tscrunch(tscrunch), _fscrunch(fscrunch)
    {
    }

    OutputTuple operator()(InputPtr voltages)
    {
        if(voltages->total_nchannels() % _fscrunch != 0) {
            throw std::runtime_error("Invalud Fscrunch");
        }
        if(voltages->total_nsamples() % _tscrunch != 0) {
            throw std::runtime_error("Invalud Tscrunch");
        }
        std::cout << "IB getting PR\n";
        OutputPtr output = _allocator.get();
        std::cout << "PR got, resizing\n";
        if(!output) {
            throw std::runtime_error("Somehow the output is a nullptr");
        }
        output->resize({voltages->total_nsamples() / _tscrunch,
                        voltages->total_nchannels() / _fscrunch});
        output->timestamp(voltages->timestamp());
        output->timestep(voltages->TimeDimensionN<1>::timestep() / _tscrunch);
        std::cout << "PR resized, getting voltage frequencies\n";
        // This could/should be cached based on some kind of "stream_id"
        auto const& input_f = voltages->frequencies();

        using FrequenciesType    = std::decay_t<decltype(input_f)>;
        FrequenciesType output_f = FrequenciesType(input_f.size());
        for(std::size_t output_f_idx = 0;
            output_f_idx < output->total_nchannels();
            ++output_f_idx) {
            typename FrequenciesType::value_type acc{};
            for(std::size_t f_idx = output_f_idx * 2;
                f_idx < output_f_idx * 2 + _fscrunch;
                ++f_idx) {
                acc += input_f[f_idx];
            }
            output_f[output_f_idx] = acc / static_cast<double>(_fscrunch);
        }

        // Here would go the actual beamforming code.

        return std::make_tuple(output);
    }

  private:
    OutputAllocator& _allocator;
    unsigned _tscrunch;
    unsigned _fscrunch;
};

} // namespace psrdada_cpp


int main()
{
    using namespace psrdada_cpp;
    using VoltageType      = TAFTPVoltagesH<int8_t>;
    using PowersType       = TFPowersH<float>;
    ResourcePool<VoltageType> taftp_voltage_pool(1);
    ResourcePool<PowersType> tf_powers_pool(1);

    Graph graph;
    [[maybe_unsed]] auto& source = graph.add_node(
        TAFTPNullSource(taftp_voltage_pool),
        "taftp source");

    auto& beamformer = graph.add_node(
        IncoherentBeamformer(tf_powers_pool, 4, 4),
        "incoherent beamformer");

    auto& sink = graph.add_node([](std::shared_ptr<PowersType>) {},
                   "sink block");
    
    graph.connect<0, 0>(source, beamformer, "src->bf", 3);
    graph.connect<0, 0>(beamformer, sink, "bf->sink", 3);

    bool stop = false;
    std::jthread monitor_thread([&](){
        while (!stop)
        {
            BOOST_LOG_TRIVIAL(warning) << "SAMPLES PROCESSED: " << source.get_callable().samples_processed();
            std::this_thread::sleep_for(1ms);
        }
    });
    
    graph.prepare();
    graph.show_dependencies();
    graph.show_topological_order();
    GraphRunner::run(&graph);
    stop = true;
    std::cout << "----- Statistics -----\n";
    graph.show_statistics();
    graph.make_dot_graph();
    return 0;
}