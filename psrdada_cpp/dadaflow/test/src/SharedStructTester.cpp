// Define a set of test fields using the FIELD and ARRAYFIELD macros
#include "psrdada_cpp/dadaflow/test/SharedStructTester.hpp"
#include <gtest/gtest.h>
#include <string>
#include <atomic>
#include <thread>
#include <vector>

namespace psrdada_cpp {
namespace test {

namespace fields {
    ARRAYFIELD(antenna_indices, int, "Antenna indices")
    FIELD(lo_frequency, float, "LO frequency (Hz)")
    ARRAYFIELD(state, char, "My name")
}

// Now define a struct-like object with the fields using Descriptor
using MonitoringData = Descriptor<
    fields::antenna_indices<10>,
    fields::lo_frequency, 
    fields::state<64>
>;

// Test 1: Test that the descriptor is written correctly
TEST_F(SharedStructTester, WriteAndReadSingleField) {
    SharedStruct<MonitoringData> monitor(shared_memory_name);

    // Write data to the shared memory
    float test_lo_freq = 1.2345f;
    monitor.with_write_lock([&](MonitoringData& data) {
        data.lo_frequency = test_lo_freq;
    });

    // Read data from the shared memory
    float read_lo_freq = 0.0f;
    monitor.with_read_lock([&](const MonitoringData& data) {
        read_lo_freq = data.lo_frequency;
    });

    // Verify the value was written and read correctly
    ASSERT_FLOAT_EQ(test_lo_freq, read_lo_freq);
}

// Test 2: Test writing and reading array fields
TEST_F(SharedStructTester, WriteAndReadArrayField) {
    SharedStruct<MonitoringData> monitor(shared_memory_name);

    std::array<int, 10> test_antenna_indices = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    // Write to the array field
    monitor.with_write_lock([&](MonitoringData& data) {
        for (std::size_t i = 0; i < test_antenna_indices.size(); ++i) {
            data.antenna_indices[i] = test_antenna_indices[i];
        }
    });

    // Read back from the array field
    std::array<int, 10> read_antenna_indices;
    monitor.with_read_lock([&](const MonitoringData& data) {
        for (std::size_t i = 0; i < read_antenna_indices.size(); ++i) {
            read_antenna_indices[i] = data.antenna_indices[i];
        }
    });

    // Verify that the array contents are correct
    ASSERT_EQ(test_antenna_indices, read_antenna_indices);
}

// Test 3: Test writing and reading a char array field
TEST_F(SharedStructTester, WriteAndReadCharArrayField) {
    SharedStruct<MonitoringData> monitor(shared_memory_name);

    std::string test_name = "Hello";
    monitor.with_write_lock([&](MonitoringData& data) {
        std::copy(test_name.begin(), test_name.end(), data.state.begin());
    });

    // Read back the char array
    std::string read_name;
    monitor.with_read_lock([&](const MonitoringData& data) {
        read_name.assign(data.state.begin(), data.state.begin() + test_name.size());
    });

    // Verify that the char array content matches
    ASSERT_EQ(test_name, read_name);
}

// Test for concurrent reads and writes
TEST_F(SharedStructTester, ConcurrentReadsAndWrites) {
    SharedStruct<MonitoringData> monitor(shared_memory_name);

    const int num_threads = 10;
    const int iterations = 100;
    std::atomic<int> write_count{0};
    std::atomic<int> read_count{0};

    // Writer thread function
    auto writer = [&]() {
        for (int i = 0; i < iterations; ++i) {
            monitor.with_write_lock([&](MonitoringData& data) {
                data.lo_frequency = static_cast<float>(i);
                ++write_count;
            });
        }
    };

    // Reader thread function
    auto reader = [&]() {
        float lo_freq = 0.0f;
        for (int i = 0; i < iterations; ++i) {
            monitor.with_read_lock([&](const MonitoringData& data) {
                lo_freq = data.lo_frequency;
                ++read_count;
            });
        }
    };

    // Create and launch multiple writer and reader threads
    std::vector<std::thread> threads;
    for (int i = 0; i < num_threads / 2; ++i) {
        threads.push_back(std::thread(writer));  // Add writer threads
        threads.push_back(std::thread(reader));  // Add reader threads
    }

    // Join all threads
    for (auto& thread : threads) {
        thread.join();
    }

    // Verify that all reads and writes occurred
    EXPECT_EQ(write_count, num_threads / 2 * iterations);
    EXPECT_EQ(read_count, num_threads / 2 * iterations);
}

} // namespace test
} // namespace psrdada_cpp