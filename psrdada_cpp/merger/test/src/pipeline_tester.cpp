#include "psrdada_cpp/merger/test/pipeline_tester.hpp"

namespace psrdada_cpp{
namespace merger {
namespace test{

class ProvideParamsForPipelineTester : public ::testing::TestWithParam<merge_conf>{};


TEST_P(ProvideParamsForPipelineTester, test_operator_bad_size_pfb) {
    PipelineTester<PFBMerger> tester(GetParam());
    tester.test_operator_bad_size();
}

TEST_P(ProvideParamsForPipelineTester, test_operator_good_size_pfb) {
    PipelineTester<PFBMerger> tester(GetParam());
    tester.test_operator_good_size();
}

TEST_P(ProvideParamsForPipelineTester, test_operator_bad_size_poln) {
    PipelineTester<PolnMerger> tester(GetParam());
    tester.test_operator_bad_size();
}

TEST_P(ProvideParamsForPipelineTester, test_operator_good_size_poln) {
    PipelineTester<PolnMerger> tester(GetParam());
    tester.test_operator_good_size();
}

// ToDo test also the init funcition
// TEST_P(ProvideParamsForPipelineTester, init_method) {
// }



INSTANTIATE_TEST_SUITE_P(ParameterizedTest, ProvideParamsForPipelineTester, ::testing::Values(
    //          npol | nchunk | nthreads | heap_size
    merge_conf({2,      2,          4,      4096}),
    merge_conf({2,      4,          8,      4096}),
    merge_conf({2,      8,          16,     4096})
));


}
}
}