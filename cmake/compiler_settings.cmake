# Set the default build type
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "RELEASE")
endif ()

# The sub library psrdada_cpp::dadaflow requires at least std=c++20 
if(BUILD_DADAFLOW)
    # Here we check if the compiler (Clang and GCC) is able to support C++ 20
    # Other compilers are not included yet
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL 10)
        set(CMAKE_CXX_STANDARD 20)
        set(CMAKE_CXX_STANDARD_REQUIRED ON)
    # Turn BUILD_DADAFLOW off when the standard is not sufficient
    else()
        message(WARNING "The Compiler does not support C++20, psrdada_cpp::dadaflow won't build")
        message(WARNING "Setting BUILD_DADAFLOW=OFF")
        set(BUILD_DADAFLOW OFF)
    endif()
endif()
# Set the minimum standard when BUILD_DADAFLOW is OFF
if(NOT BUILD_DADAFLOW)
    set(CMAKE_CXX_STANDARD 14)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
endif()

# Set compiler flags
set(CMAKE_CXX_FLAGS "--std=c++${CMAKE_CXX_STANDARD} -fPIC -fopenmp")

set(ARCH "broadwell" CACHE STRING "target architecture (-march=native, x86-64), defaults to broadwell")
set(TUNE "generic" CACHE STRING "target tune architecture (-mtune=native, x86-64), defaults to generic")

if(CMAKE_CXX_COMPILER MATCHES icpc)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcheck -wd2259 -wd1125")
endif()
if (CMAKE_CXX_COMPILER_ID MATCHES Clang)
    set(CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem ")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -march=${ARCH} -mtune=${TUNE}")
endif ()
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-ignored-attributes -pthread -Werror -Wcast-align -march=${ARCH} -mtune=${TUNE}")
endif ()

set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG -Wno-unused-local-typedefs")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -g -Wall -Wextra -pedantic -Wno-unused-local-typedefs")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -pg -Wall -Wextra -pedantic -Wno-unused-local-typedefs")

# Set include directories for dependencies
include_directories(
    ${Boost_INCLUDE_DIRS}
)
