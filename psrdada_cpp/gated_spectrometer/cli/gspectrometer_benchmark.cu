#include <benchmark/benchmark.h>
#include <cuda.h>
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/gated_spectrometer/pipeline.cuh"
#include "psrdada_cpp/gated_spectrometer/test/pipeline_tester.cuh" // we need the test_config
#include "psrdada_cpp/testing_tools/benchmark_tools.hpp"
#include "psrdada_cpp/testing_tools/streams.hpp"

using namespace psrdada_cpp::gated_spectrometer;
namespace psr_testing = psrdada_cpp::testing_tools;

template<typename InputType, typename OutputType>
void benchmark_template(benchmark::State& state)
{
    cudaEvent_t start, stop;
    test::test_config config;
    config.fft_length = state.range(0);
    config.naccumulate = state.range(1);
    config.nbits = state.range(2);
    config.nheaps_block = state.range(3);
    std::size_t buffer_size = config.config().dadaBufferLayout.getBufferSize();
    char* hdata = new char[4096];
    char* idata = nullptr;
    CUDA_ERROR_CHECK(cudaMallocHost((void**)&idata, buffer_size));

    psrdada_cpp::RawBytes header_raw(hdata, 4096, 4096);
    psrdada_cpp::RawBytes rawdata(idata, buffer_size, buffer_size);
    psr_testing::DummyStream<float> dummy(false);
    Pipeline<decltype(dummy), InputType, OutputType> test_object(config.config(), dummy);
    test_object.init(header_raw);

    CUDA_ERROR_CHECK(cudaEventCreate(&start));
    CUDA_ERROR_CHECK(cudaEventCreate(&stop));
    float milliseconds = 0;
    std::vector<double> vec(0);

    // Warm up
    while(dummy.call_count() < 5){
        test_object(rawdata);
    }

    try{
        for (auto _ : state){
            CUDA_ERROR_CHECK(cudaEventRecord(start));
            test_object(rawdata);
            CUDA_ERROR_CHECK(cudaEventRecord(stop));
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&milliseconds, start, stop);
            state.SetIterationTime(milliseconds / 1000);
            vec.push_back(buffer_size / (milliseconds / 1000));
        }
        state.counters["rate"] = benchmark::Counter(buffer_size,
            benchmark::Counter::kIsIterationInvariantRate);
        state.counters["byte-size"] = buffer_size;
        state.counters["fft_length"] = config.fft_length;
        state.counters["naccumulate"] = config.naccumulate;
        state.counters["nbits"] = config.nbits;
        state.counters["nheaps_block"] = config.nheaps_block;
        state.counters["active_gates"] = 3;
        psr_testing::getStats(vec, state);
    }catch(std::exception& e){
        state.counters["rate"] = 0;
        state.counters["byte-size"] = 0;
        state.counters["fft_length"] = 0;
        state.counters["naccumulate"] = 0;
        state.counters["nbits"] = 0;
        state.counters["nheaps_block"] = 0;
        state.counters["active_gates"] = 0;
        psr_testing::getStats(vec, state);
    }
}


static void BM_DP_SPECTROMETER(benchmark::State& state) {
    benchmark_template<SinglePolarizationInput, GatedPowerSpectrumOutput>(state);
}

static void BM_FS_SPECTROMETER(benchmark::State& state) {
    benchmark_template<DualPolarizationInput, GatedFullStokesOutput>(state);
}

BENCHMARK(BM_DP_SPECTROMETER)->Ranges({{1024*256, 1024*1024*16}, {1, 1024}, {8, 12}, {8192, 65536}})->Iterations(50)->UseManualTime();
BENCHMARK(BM_FS_SPECTROMETER)->Ranges({{1024*256, 1024*1024*16}, {1, 1024}, {8, 12}, {8192, 65536}})->Iterations(50)->UseManualTime();

int main(int argc, char** argv) {
    char arg0_default[] = "benchmark";
    char* args_default = arg0_default;
    psrdada_cpp::set_log_level("INFO");
    if (!argv) {
      argc = 1;
      argv = &args_default;
    }
    ::benchmark::Initialize(&argc, argv);
    if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
    ::benchmark::RunSpecifiedBenchmarks();
    ::benchmark::Shutdown();
    return 0;
}
