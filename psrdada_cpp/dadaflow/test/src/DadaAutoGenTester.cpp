#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/DadaHeader.hpp"
#include "psrdada_cpp/dadaflow/DadaAutoGen.hpp"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>
#include <span>
#include <memory>

namespace psrdada_cpp
{
namespace test
{
namespace
{
char valid_TAFTP_header[4096] = R""""(
    # Timestamping
    SAMPLE_CLOCK 1000000000
    SYNC_TIME 0
    SAMPLE_CLOCK_START 6000000000

    # T0 Dimension is unspecified
    
    # A Dimension
    NANTENNAS 4
    ANTENNAS m000,m001,m002,m003

    # F Dimension
    NCHANS 8
    FREQUENCY 1284000000.0
    BANDWIDTH 856000000.0

    # T1 Dimension
    NSAMPLES_1 8
    TSAMP_1 0.003062341

    # P Dimension
    NPOL 2
    POLARISATIONS H,V    

    METADATA_1 SOME_STR_METADATA
    METADATA_2 3.14
)"""";

/** 
char invalid_TF_header[4096] = R""""(
    
    )"""";
*/
}

TEST(DadaAutoGenTester, valid_taftp_parse)
{
    using TAFTPData = DescribedData<std::span<float>, 
                                    TimeDimensionN<0>,
                                    AntennaDimension,
                                    FrequencyDimension,
                                    TimeDimensionN<1>,
                                    PolarisationDimension>;  

    RawBytes header_bytes(valid_TAFTP_header, 4096, 4096);

    DadaAutoGen<TAFTPData> generator([&](MetadataMap& metadata,
                                         DadaHeader const& header){
                                            metadata["METADATA_1"] = header.get<std::string>("METADATA_1");
                                            metadata["METADATA_2"] = header.get<float>("METADATA_2");
                                        });

    DadaHeader header;
    header.read_from(header_bytes);
    std::size_t nantennas = header.get<std::size_t>("NANTENNAS");
    std::size_t nchannels = header.get<std::size_t>("NCHANS");
    std::size_t nsamples_1 = header.get<std::size_t>("NSAMPLES_1");;
    std::size_t npol = header.get<std::size_t>("NPOL");

    generator.parse_header(header_bytes);

    auto release_lifetime =
        std::shared_ptr<void>(new int(0), [](int* ptr) {
            delete ptr;
        });

    std::size_t nsamples_0 = 10;
    std::vector<float> data_vec;
    data_vec.resize(nsamples_0 * nantennas * nchannels * nsamples_1 * npol);
    std::size_t nbytes = sizeof(float) * data_vec.size();
    RawBytes data_bytes(reinterpret_cast<char*>(data_vec.data()), nbytes, nbytes);
    auto data_ptr = generator.parse_data(data_bytes, release_lifetime);
    auto const& data = *data_ptr;

    // Check sizes
    ASSERT_EQ(data.TimeDimensionN<0>::nsamples(), nsamples_0);
    ASSERT_EQ(data.nantennas(), nantennas);
    ASSERT_EQ(data.nchannels(), nchannels);
    ASSERT_EQ(data.TimeDimensionN<1>::nsamples(), nsamples_1);
    ASSERT_EQ(data.npol(), npol);

    // Check values
    ASSERT_EQ(data.channel_bandwidth(), (header.get<double>("BANDWIDTH") / nchannels) * hertz);
    ASSERT_EQ(data.antennas(), (std::vector<std::string>{"m000", "m001", "m002", "m003"}));
    ASSERT_EQ(data.polarisations(), (std::vector<std::string>{"H", "V"}));
    long double unix_epoch =  header.get<long double>("SYNC_TIME") + 
        header.get<std::size_t>("SAMPLE_CLOCK_START") / header.get<long double>("SAMPLE_CLOCK");
    ASSERT_EQ(data.timestamp(), Timestamp(unix_epoch));

    ASSERT_EQ(std::any_cast<std::string>(data.metadata().at("METADATA_1")), std::string{"SOME_STR_METADATA"});
    ASSERT_FLOAT_EQ(std::any_cast<float>(data.metadata().at("METADATA_2")), 3.14f);

}

TEST(DadaAutoGenTester, missing_key_parse)
{
    using TestDataType = DescribedData<std::span<float>, 
                                        TimeDimension,
                                        DispersionDimension>; 

    RawBytes header_bytes(valid_TAFTP_header, 4096, 4096);
    DadaAutoGen<TestDataType> generator([&](MetadataMap& metadata,
                                         DadaHeader const& header){
                                            metadata["METADATA_1"] = header.get<std::string>("METADATA_1");
                                            metadata["METADATA_2"] = header.get<float>("METADATA_2");
                                        });
    ASSERT_THROW(generator.parse_header(header_bytes), InvalidHeaderKey);
    
}

TEST(DadaAutoGenTester, write_dims_to_header)
{
    DadaHeader header;
    DescribedData<std::vector<int>, TimeDimension> data;
    data.resize({10});
    data.timestep(std::chrono::duration<double>(1.0001));
    dimensions_to_header(data, header);
    ASSERT_EQ(header.get<std::size_t>("NSAMPLES"), 10);
    ASSERT_EQ(header.get<double>("TSAMP"), 1.0001);
}



} // namespace test
} // namespace psrdada_cpp