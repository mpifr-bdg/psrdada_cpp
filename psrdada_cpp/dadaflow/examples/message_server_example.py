import posix_ipc
import json
import time
import pprint

class MQClient:
    def __init__(self, request_q, response_q):
        self._request_q = request_q
        self._response_q = response_q
    
    def send(self, command, **kwargs):
        message = json.dumps({
            "command": command,
            "arguments": kwargs
        })
        self._request_q.send(message)
        try:
            response_str, _ = self._response_q.receive(timeout=5)
            response = json.loads(response_str)
        except posix_ipc.BusyError:
            print("Timeout waiting for response")
            return None
        return response

    def show_help(self):
        response = self.send("help")
        for key, params in response["response"].items():
            msg = f"{key} -- {params['description']}\n"
            if len(params['required_keys']) > 0:
                msg += "\n\tArgs:\n"
                for key, detail in params['required_keys'].items():
                    msg += f"\t--> {key} ({detail['type']}): {detail['description']} \n"
            print(msg)

# Define the queue names
request_queue_name = "/message_server_example"
response_queue_name = "/message_server_example_r"
request_queue = posix_ipc.MessageQueue(request_queue_name)
response_queue = posix_ipc.MessageQueue(response_queue_name)

client = MQClient(request_queue, response_queue)
client.show_help()
client.send("say_hello", repeats=10)
client.send("shutdown")

# Close the queues
request_queue.close()
response_queue.close()

