#pragma once
#include "psrdada_cpp/common.hpp"
#include <memory>
#include <functional>
#include <utility>

namespace psrdada_cpp {

template <typename T>
class ResourceAllocatorInterface {
public:
    virtual ~ResourceAllocatorInterface() = default;
    virtual std::shared_ptr<T> get() = 0;
};

template <typename T>
class DefaultAllocator : public ResourceAllocatorInterface<T> {
public:
    template <typename... Args>
    DefaultAllocator(Args&&... args) 
        : _alloc_func([captured_args = std::make_tuple(std::forward<Args>(args)...)]() -> std::shared_ptr<T> {
            return std::apply( [](auto&&... unpacked_args) { 
                return std::make_shared<T>(std::forward<decltype(unpacked_args)>(unpacked_args)...); 
                }, captured_args );
        }){}

    std::shared_ptr<T> get() override {
        return _alloc_func();
    }

private:
    std::function<std::shared_ptr<T>()> _alloc_func;
};


} // namespace psrdada_cpp

