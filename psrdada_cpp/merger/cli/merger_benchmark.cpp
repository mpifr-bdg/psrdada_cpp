#include <benchmark/benchmark.h>
#include "psrdada_cpp/merger/merger.hpp"

using namespace psrdada_cpp::merger;

template <class ...Args>
static void BM_PFBMerger(benchmark::State& state, Args&&... args) {
  auto args_tuple = std::make_tuple(std::move(args)...);
  PFBMerger bobj(std::get<0>(args_tuple));
  char* idata = new char[state.range(0)];
  char* odata = new char[state.range(0)];
  for (auto _ : state){
    bobj.process(idata, odata, state.range(0));
  }
  state.counters["rate"] = benchmark::Counter(state.range(0), benchmark::Counter::kIsIterationInvariantRate, benchmark::Counter::OneK::kIs1024);
  state.counters["byte-size"] = state.range(0);
  state.counters["nthreads"] = std::get<0>(args_tuple).nthreads;
  state.counters["npol"] = std::get<0>(args_tuple).npol;
  state.counters["nchannel"] = std::get<0>(args_tuple).nchunk;
  state.counters["heap-size"] = std::get<0>(args_tuple).heap_size;
}


BENCHMARK_CAPTURE(BM_PFBMerger, pfb_merge_benchmark, merge_conf({2, 1000, 2, 8000}))
    ->DenseRange(8000*2*1000, 8000*2*1000*8, 8000*2*1000);
BENCHMARK_CAPTURE(BM_PFBMerger, pfb_merge_benchmark, merge_conf({1, 1000, 4, 8000}))
    ->DenseRange(8000*2*1000, 8000*2*1000*8, 8000*2*1000);
BENCHMARK_CAPTURE(BM_PFBMerger, pfb_merge_benchmark, merge_conf({2, 1000, 1, 8000}))
    ->DenseRange(8000*2*1000, 8000*2*1000*8, 8000*2*1000);


template <class ...Args>
static void BM_PolMerger(benchmark::State& state, Args&&... args) {
  auto args_tuple = std::make_tuple(std::move(args)...);
  PolnMerger bobj(std::get<0>(args_tuple));
  char* idata = new char[state.range(0)];
  char* odata = new char[state.range(0)];
  for (auto _ : state){
    bobj.process(idata, odata, state.range(0));
  }
  state.counters["rate"] = benchmark::Counter(state.range(0), benchmark::Counter::kIsIterationInvariantRate, benchmark::Counter::OneK::kIs1024);
  state.counters["byte-size"] = state.range(0);
  state.counters["nthreads"] = std::get<0>(args_tuple).nthreads;
  state.counters["npol"] = std::get<0>(args_tuple).npol;
}


BENCHMARK_CAPTURE(BM_PolMerger, pol_merge_benchmark, merge_conf({1, 1000, 2, 8000}))
    ->DenseRange(8000*2*1000, 8000*2*1000*8, 8000*2*1000);
BENCHMARK_CAPTURE(BM_PolMerger, pol_merge_benchmark, merge_conf({2, 1000, 4, 8000}))
    ->DenseRange(8000*2*1000, 8000*2*1000*8, 8000*2*1000);
BENCHMARK_CAPTURE(BM_PolMerger, pol_merge_benchmark, merge_conf({4, 1000, 1, 8000}))
    ->DenseRange(8000*2*1000, 8000*2*1000*8, 8000*2*1000);

BENCHMARK_MAIN();