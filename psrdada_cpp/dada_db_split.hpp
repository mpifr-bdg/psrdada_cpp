#ifndef PSRDADA_CPP_DADA_DB_SPLIT
#define PSRDADA_CPP_DADA_DB_SPLIT

#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/common.hpp"

#include <sys/types.h>
#include <iostream>
#include <string>
#include <ios>
#include <vector>
#include <fstream>

namespace psrdada_cpp
{
    class DbSplit
    {
    public:
        DbSplit(std::vector<key_t> out_keys, MultiLog& log, std::size_t chunk_size=0);
        ~DbSplit();


        void init(RawBytes &header_block);
        bool operator()(RawBytes &dada_block);

    public:
        std::vector<DadaWriteClient*> _writer;
        std::size_t _input_buffer_sz;
        std::size_t _dsize;
        std::size_t _hsize;
        std::size_t _dblock;
        std::size_t _hblock;
        std::size_t _chunk_size;
        std::size_t _n_writer;
        std::size_t _writes_per_slot;
    };
} //namespace psrdada_cpp

#include "psrdada_cpp/src/dada_db_split.cpp"

#endif //PSRDADA_CPP_DADA_DB_SPLIT
