#include "psrdada_cpp/dadaflow/io/sigproc/SigprocIO.hpp"

namespace psrdada_cpp {
namespace sigproc {

SigprocFile::SigprocFile(const fs::path& filename)
    : _filename(filename) 
{
    open();
    _header_size = _header.from_stream(static_cast<std::istream&>(*_stream));
    prepare();
}

bool SigprocFile::at_end() const {
    return static_cast<std::size_t>(_stream->tellg()) >= _filesize;     
}

char* SigprocFile::read_nsamples(char* ptr, std::size_t nsamples) {
    BOOST_LOG_TRIVIAL(debug) << "Reading " << nsamples << " samples from " << _filename;
    std::size_t bytes_to_read = nsamples * _bytes_per_sample;
    _stream->read(ptr, bytes_to_read);
    if (_stream->eof() || _stream->fail()) {
        std::size_t bytes_read = _stream->gcount();
        if (bytes_read < bytes_to_read) {
            ptr += bytes_read;
            throw std::runtime_error("Reached end of file or encountered read error while reading samples.");
        }
    } else {
        ptr += bytes_to_read;
    }
    return ptr;
}

std::size_t SigprocFile::remaining_samples() const {
    std::size_t current_position = static_cast<std::size_t>(_stream->tellg());
    return (_filesize > current_position) ? (_filesize - current_position) / _bytes_per_sample : 0;
}

void SigprocFile::seek_sample(std::size_t sample_idx) {
    BOOST_LOG_TRIVIAL(debug) << "Seeking to sample " << sample_idx;
    std::size_t target_position = _header_size + _bytes_per_sample * sample_idx;
    _stream->seekg(target_position, std::ios::beg);
    if (_stream->fail()) {
        throw std::ios_base::failure("Failed to seek to the specified sample index: " + std::to_string(sample_idx));
    }
}

void SigprocFile::open() {
    BOOST_LOG_TRIVIAL(debug) << "Opening " << _filename << " for reading";
    if (!fs::exists(_filename)) {
        throw std::runtime_error("File not found: " + _filename.string());
    }
    _stream = std::make_unique<std::ifstream>(_filename, std::ios::binary);
    if (!_stream->is_open()) {
        throw std::ios_base::failure("Failed to open file: " + _filename.string());
    }
}

void SigprocFile::prepare() {
    _stream->seekg(0, std::ios::end);
    _filesize = static_cast<std::size_t>(_stream->tellg());
    _bytes_per_sample = _header.nchans() * _header.nifs() * _header.nbits() / 8;
    _nsamples = (_filesize - _header_size) / _bytes_per_sample;
    _stream->seekg(_header_size, std::ios::beg);
    BOOST_LOG_TRIVIAL(debug) << "Total filesize: " << _filesize << " bytes";
    BOOST_LOG_TRIVIAL(debug) << "Bytes per sample: " << _bytes_per_sample << " bytes";
    BOOST_LOG_TRIVIAL(debug) << "Total samples: " << _nsamples;
}

void SigprocFile::close() {
    BOOST_LOG_TRIVIAL(debug) << "Closing " << _filename;
    if (_stream) {
        _stream->close();
        _stream.reset();
    }
}

SigprocInputFileStream::SigprocInputFileStream(std::vector<fs::path> const& files, bool assume_contiguous)
{
    BOOST_LOG_TRIVIAL(debug) << "Building SigprocInputFileStream";
    if (files.empty()) {
        throw std::runtime_error("At least one input file must be specified");
    }
    _files.reserve(files.size());
    for (auto const& fname: files) {
        _files.emplace_back(fname);
    }
    std::sort(_files.begin(), _files.end(),
        [](SigprocFile const& a, SigprocFile const& b) {
                return a.header().tstart() < b.header().tstart();
            });
    check_compatibility(); 
    if (!assume_contiguous){
        check_contiguity();
    }
    calculate_total_nsamples();
    set_generator(_files.front().header());
}

void SigprocInputFileStream::seek_sample(std::size_t sample_idx) {
    std::size_t start_sample = 0;
    for (std::size_t file_idx = 0; file_idx < _files.size(); ++file_idx) {
        auto& file = _files.at(file_idx);
        std::size_t end_sample = start_sample + file.nsamples();
        if (sample_idx >= start_sample && sample_idx < end_sample) {
            _curr_file_idx = file_idx;
            file.seek_sample(sample_idx - start_sample);
            return; // Stop once the correct file and position are found
        }
        start_sample = end_sample;
    }
    throw std::out_of_range("Sample index is out of range");
}

FilterbankVariant SigprocInputFileStream::read_block(std::size_t nsamples)
{
    BOOST_LOG_TRIVIAL(debug) << "Reading " << nsamples << " samples from stream";
    if (at_end()) {
        throw std::runtime_error("Stream is at end, cannot read.");
    }
    FilterbankVariant data = _generator->generate(nsamples);
    std::visit([this, nsamples](auto& filterbank_ptr){
        auto& filterbank = *filterbank_ptr;
        std::size_t nsamples_to_read = nsamples;
        std::size_t samples_read = 0;
        char* write_ptr = reinterpret_cast<char*>(filterbank.data());
        while (nsamples_to_read > 0)
        {
            BOOST_LOG_TRIVIAL(debug) << "samples to read = " << nsamples_to_read 
                                        << ", samples read = " << samples_read 
                                        << ", write ptr = " << static_cast<const void*>(write_ptr)
                                        << ", file idx = " << _curr_file_idx;
            auto& current_file = _files.at(_curr_file_idx);
            std::size_t remaining_samples = current_file.remaining_samples();
            BOOST_LOG_TRIVIAL(debug) << "remaining samples in file: " << remaining_samples;
            if (remaining_samples == 0) {
                if (_files.size() > (_curr_file_idx + 1)) {
                    ++_curr_file_idx;
                    continue;
                } else {
                    BOOST_LOG_TRIVIAL(debug) << "reached end of stream";
                    _end_of_stream = true;
                    break;
                }   
            }
            if (remaining_samples >= nsamples_to_read) {
                // Read nsamples_to_read from this file
                write_ptr = current_file.read_nsamples(write_ptr, nsamples_to_read);
                samples_read += nsamples_to_read;
                break;
            } else if (remaining_samples < nsamples_to_read) {
                // Read remaining_samples from file
                write_ptr = current_file.read_nsamples(write_ptr, remaining_samples);
                samples_read += remaining_samples;
                nsamples_to_read -= remaining_samples;
                continue;
            }
        }
        filterbank.resize({samples_read, _files.front().header().nchans()});
    }, data);
    return data;
}

void SigprocInputFileStream::check_contiguity() const {
    // tstart is in MJD
    // tsamp is in seconds
    BOOST_LOG_TRIVIAL(debug) << "Verifying that files are time contiguous";
    auto it = _files.begin();
    auto const& first_file = *it;
    double tstart = first_file.header().tstart();
    std::size_t nsamples = first_file.nsamples();
    double tsamp = first_file.header().tsamp();
    ++it;
    while (it != _files.end())
    {
        auto const& file = *it;
        double expected_next_tstart = tstart + (nsamples * tsamp) / 86400.0;
        double next_tstart = file.header().tstart();
        double diff_seconds = (next_tstart - expected_next_tstart) * 86400.0;
        if (diff_seconds > tsamp) {
            throw std::runtime_error("File is not contiguous with previous file: " + std::string{file.filename()});
        }
        tstart = next_tstart;
        nsamples = file.nsamples();
        ++it;
    }
}

void SigprocInputFileStream::check_compatibility() const {
    BOOST_LOG_TRIVIAL(debug) << "Checking all files are compatible";
    auto it = _files.begin();
    auto const& first_file = *it;
    double fch1 = first_file.header().fch1();
    double foff = first_file.header().foff();
    uint32_t nchans = first_file.header().nchans();
    uint32_t nifs = first_file.header().nifs();
    double tsamp = first_file.header().tsamp();
    if (nifs > 1) {
        throw std::runtime_error("NIFS > 1 is not supported");
    }
    ++it;
    while (it != _files.end()) {
        auto const& file = *it;
        std::string filename = file.filename();        
        if (file.header().fch1() != fch1) {
            throw std::runtime_error("Incompatible frequency channel (fch1) in file: " + filename);
        }
        if (file.header().foff() != foff) {
            throw std::runtime_error("Incompatible frequency offset (foff) in file: " + filename);
        }
        if (file.header().nchans() != nchans) {
            throw std::runtime_error("Incompatible number of channels (nchans) in file: " + filename);
        }
        if (file.header().nifs() != nifs) {
            throw std::runtime_error("Incompatible number of IFs (nifs) in file: " + filename);
        }
        if (file.header().tsamp() != tsamp) {
            throw std::runtime_error("Incompatible sample time (tsamp) in file: " + filename);
        }
        ++it;
    }
}

void SigprocInputFileStream::calculate_total_nsamples() {
    _total_nsamples = 0;
    for (auto const& file: _files) {
        _total_nsamples += file.nsamples();
    }
}

void SigprocInputFileStream::set_generator(SigprocHeader const& header) {
    int nbits = header.nbits();
    bool signed_ = false;
    if (header.is_signed().has_value()) {
        signed_ = bool(header.is_signed().value());
    }
    if (nbits == 1) {
        _generator = std::make_unique<SigprocGenerator<Filterbank1Bit>>(header, std::make_shared<ResourcePool<Filterbank1Bit>>(3));
    } else if (nbits == 2) {
        _generator = std::make_unique<SigprocGenerator<Filterbank2Bit>>(header, std::make_shared<ResourcePool<Filterbank2Bit>>(3));
    } else if (nbits == 4) {
        _generator = std::make_unique<SigprocGenerator<Filterbank4Bit>>(header, std::make_shared<ResourcePool<Filterbank4Bit>>(3));
    } else if (nbits == 8 && !signed_) {
        _generator = std::make_unique<SigprocGenerator<Filterbank8Bit>>(header, std::make_shared<ResourcePool<Filterbank8Bit>>(3));
    } else if (nbits == 8 && signed_) {
        _generator = std::make_unique<SigprocGenerator<Filterbank8BitSigned>>(header, std::make_shared<ResourcePool<Filterbank8BitSigned>>(3));
    } else if (nbits == 32 && !signed_) {
        _generator = std::make_unique<SigprocGenerator<Filterbank32Bit>>(header, std::make_shared<ResourcePool<Filterbank32Bit>>(3));
    } else if (nbits == 32 && signed_) {
        _generator = std::make_unique<SigprocGenerator<Filterbank32BitSigned>>(header, std::make_shared<ResourcePool<Filterbank32BitSigned>>(3));
    } else {
        throw std::runtime_error("Unsupported nbits/signed value");
    }
}


} // namespace sigproc
} // namespace psrdada_cpp