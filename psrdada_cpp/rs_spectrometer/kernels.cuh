#pragma once

#include <cuda.h>
#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/reduce.h>
#include <thrust/binary_search.h>
#include <thrust/adjacent_difference.h>
#include <thrust/transform_reduce.h>
#include <thrust/fill.h>

#define PASSTHROUGH_MODE_IQ_SCALING 1.0f/(1<<15)
#define PFB_MODE_IQ_SCALING 1.0f/(1<<(30 - 3))
#define FSW_IMPEDANCE 50.0f

namespace psrdada_cpp {
namespace rs_spectrometer {
namespace kernels {

struct short2_to_float2 : public thrust::unary_function<short2, float2>
{
    __host__ __device__ float2 operator()(short2 in)
    {
        char4 swap;
        char4* in_ptr = (char4*)(&in);
        swap.x = in_ptr->y;
        swap.y = in_ptr->x;
        swap.z = in_ptr->w;
        swap.w = in_ptr->z;
        short2* swap_as_short2 = (short2*)(&swap);
        float2 out;
        out.x = (float) swap_as_short2->x;
        out.y = (float) swap_as_short2->y;
        return out;
    }

};

struct detect_scale : public thrust::unary_function<float2, float>
{
    const float _scale_factor;

    detect_scale(float scale_factor=1)
        : _scale_factor(scale_factor){}

    __host__ __device__ float operator()(float2 voltage)
    {
        float x = voltage.x * _scale_factor;
        float y = voltage.y * _scale_factor;
        float power = x * x + y * y;
        return power;
    }
};

struct detect_magnitude
    : public thrust::unary_function<float2, float>
{
    const float _scale_factor;

    detect_magnitude(float scale_factor=1)
        : _scale_factor(scale_factor){}

    __device__ float operator()(float2 voltage)
    {
        float x = voltage.x * _scale_factor;
        float y = voltage.y * _scale_factor;
        float power = x * x + y * y;
        return sqrtf(power);
    }
};

struct detect_accumulate
    : public thrust::binary_function<float2, float, float>
{
    detect_accumulate(float scale_factor=1)
        : _scale_factor(scale_factor){}

    __host__ __device__ float operator()(float2 voltage, float power_accumulator)
    {
        float x = voltage.x * _scale_factor;
        float y = voltage.y * _scale_factor;
        float power = x * x + y * y;
        return power_accumulator + power;
    }

    const float _scale_factor;
};

struct convert_to_dBm
    : public thrust::unary_function<float, float>
{
    convert_to_dBm(float scale_factor=1, float offset=0)
        : _scale_factor(scale_factor), _offset(offset){}

    __device__ float operator()(float power)
    {
        // Typically _scale_factor here is 1000.0 / (50.0 * naccumulate);
        return 10 * __log10f(power * _scale_factor) + _offset;
    }

    const float _scale_factor;
    const float _offset;
};


}
}
}