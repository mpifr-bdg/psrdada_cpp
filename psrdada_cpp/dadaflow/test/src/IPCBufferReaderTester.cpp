#include "psrdada_cpp/dadaflow/processors/IPCBufferReader.hpp"
#include "psrdada_cpp/dadaflow/IPCBuffer.hpp"
#include "psrdada_cpp/dadaflow/Semaphore.hpp"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <gtest/gtest.h>
#include <chrono>
#include <cstdint>

namespace psrdada_cpp {
namespace test {

struct IPCTesterHeader {
    int expected_nsamps;
};

TEST(IPCBufferReaderTester, test_read) 
{
    using IPCTesterData = DescribedData<std::vector<float>, TimeDimension>;
    namespace bip = boost::interprocess;
    std::string key{"test_shm_segment"};

    IPCBufferReader<IPCTesterHeader, IPCTesterData> reader {
        key, {100}, [](IPCTesterHeader const& header, IPCTesterData& data){
            data.timestamp(Timestamp(0.0));
            if (header.expected_nsamps != 100) {
                throw std::invalid_argument("Expected 100 samples");
            }
        }, 0s};
    
    auto shared_memory = bip::shared_memory_object(
        bip::open_only, key.c_str(), bip::read_write);
    auto region = bip::mapped_region(shared_memory, bip::read_write);
    
    // open existing semaphore
    Semaphore sem("test_shm_segment_update", 0, true);
    using ValueType = typename IPCTesterData::value_type;
    IPCTesterHeader* header = reinterpret_cast<IPCTesterHeader*>(region.get_address());
    ValueType* values_ptr = reinterpret_cast<ValueType*>(static_cast<char*>(region.get_address()) + sizeof(IPCTesterHeader));  

    // At this point the buffer has not been updated so the reader is expected to
    // return an empty optional.
    {
        auto empty_optional = reader();
        ASSERT_FALSE(empty_optional.has_value());
    }

    header->expected_nsamps = 100;
    sem.post();

    auto valued_optional = reader();
    ASSERT_TRUE(valued_optional.has_value());

    auto& data = *(valued_optional.value());
    ASSERT_EQ(data.timestamp().unix_epoch(), 0.0);

    std::size_t index = 0;
    for (auto& val: data) {
        ASSERT_EQ(val, values_ptr[index]);
        ++index;
    }
}

TEST(IPCBufferReaderTester, invalid_size) 
{
    using IPCTesterData = DescribedData<std::vector<float>, TimeDimension>;
    namespace bip = boost::interprocess;
    std::string key{"test_shm_segment"};

    IPCBufferReader<IPCTesterHeader, IPCTesterData> reader {
        key, {100}, [](IPCTesterHeader const& header, IPCTesterData& data){
            data.timestamp(Timestamp(0.0));
            if (header.expected_nsamps != 100) {
                throw std::invalid_argument("Expected 100 samples");
            }
        }, 0s};
    
    auto shared_memory = bip::shared_memory_object(
        bip::open_only, key.c_str(), bip::read_write);
    auto region = bip::mapped_region(shared_memory, bip::read_write);
    
    // open existing semaphore
    Semaphore sem("test_shm_segment_update", 0, true);
    using ValueType = typename IPCTesterData::value_type;
    IPCTesterHeader* header = reinterpret_cast<IPCTesterHeader*>(region.get_address());
    header->expected_nsamps = 1;
    sem.post();
    ASSERT_THROW(reader(), std::invalid_argument);
}


} // namespace test
} // namespace psrdada_cpp