#include "psrdada_cpp/test/dada_client_tester.cuh"


namespace psrdada_cpp {
namespace test {

DadaClientTester::DadaClientTester() 
    :   ::testing::TestWithParam<dada_buffer_config>(),
        wlog("DadaWriteClient"),
        rlog("DadaReadClient"),
        config(GetParam()){}


void DadaClientTester::SetUp()
{
    test_buffer = std::make_unique<DadaDB>(config.nbufs, config.bufsz, config.nhdrs, config.hdrsz);
    test_buffer->create();
    test_writer = std::make_unique<DadaWriteClient>(test_buffer->key(), wlog);
    test_reader = std::make_unique<DadaReadClient>(test_buffer->key(), rlog);
}

void DadaClientTester::TearDown()
{
    test_writer.reset();
    test_reader.reset();
    test_buffer->destroy();
}

void DadaClientTester::test_buffer_is_full()
{
    ASSERT_FALSE(test_writer->header_buffer_is_full());
    ASSERT_FALSE(test_reader->header_buffer_is_full());
    ASSERT_FALSE(test_reader->data_buffer_is_full());
    ASSERT_FALSE(test_writer->data_buffer_is_full());
    this->write_header_until_full();
    this->write_data_until_full();
    ASSERT_TRUE(test_writer->header_buffer_is_full());
    ASSERT_TRUE(test_reader->header_buffer_is_full());
    ASSERT_TRUE(test_writer->data_buffer_is_full());
    ASSERT_TRUE(test_reader->data_buffer_is_full());
    test_reader->header_stream().next();
    test_reader->header_stream().release();
    ASSERT_FALSE(test_writer->header_buffer_is_full());
    ASSERT_FALSE(test_reader->header_buffer_is_full());
    test_reader->data_stream().next();
    test_reader->data_stream().release();
    ASSERT_FALSE(test_writer->data_buffer_is_full());
    ASSERT_FALSE(test_reader->data_buffer_is_full());
}

void DadaClientTester::test_buffer_is_empty()
{

    ASSERT_TRUE(test_writer->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->data_buffer_is_empty());
    ASSERT_TRUE(test_writer->data_buffer_is_empty());
    this->write_header_until_full();
    this->write_data_until_full();
    ASSERT_FALSE(test_writer->header_buffer_is_empty());
    ASSERT_FALSE(test_reader->header_buffer_is_empty());
    ASSERT_FALSE(test_writer->data_buffer_is_empty());
    ASSERT_FALSE(test_reader->data_buffer_is_empty());
    // Read a single header and data slot
    read_data_slot();
    read_header_slot();
    ASSERT_FALSE(test_writer->header_buffer_is_empty());
    ASSERT_FALSE(test_reader->header_buffer_is_empty());
    ASSERT_FALSE(test_writer->data_buffer_is_empty());
    ASSERT_FALSE(test_reader->data_buffer_is_empty());
    // read until header and data buffer are empty
    this->read_header_until_empty();
    this->read_data_until_empty();
    ASSERT_TRUE(test_writer->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->header_buffer_is_empty());
    ASSERT_TRUE(test_reader->data_buffer_is_empty());
    ASSERT_TRUE(test_writer->data_buffer_is_empty());
}

void DadaClientTester::test_buffer_nfree()
{
    for(std::size_t i = config.nhdrs; i > 1; i--)
    {
        ASSERT_EQ(i, test_writer->header_buffer_free_slots());
        ASSERT_EQ(i, test_reader->header_buffer_free_slots());
        write_header_slot();
    }
    for(std::size_t i = config.nbufs; i > 1; i--)
    {
        ASSERT_EQ(i, test_writer->data_buffer_free_slots());
        ASSERT_EQ(i, test_reader->data_buffer_free_slots());
        write_data_slot();
    }
}

void DadaClientTester::test_buffer_nfull()
{

    auto& hstream = test_writer->header_stream();
    for(std::size_t i = 0; i > config.nhdrs; i++)
    {
        ASSERT_EQ(i, test_writer->header_buffer_full_slots());
        ASSERT_EQ(i, test_reader->header_buffer_full_slots());
        auto& hbuffer = hstream.next();
        hbuffer.used_bytes(config.hdrsz);
        hstream.release();
    }
    auto& dstream = test_writer->data_stream();
    for(std::size_t i = 0; i > config.nbufs; i++)
    {
        ASSERT_EQ(i, test_writer->data_buffer_full_slots());
        ASSERT_EQ(i, test_reader->data_buffer_full_slots());
        auto& dbuffer = dstream.next();
        dbuffer.used_bytes(config.bufsz);
        dstream.release();
    }
}

void DadaClientTester::test_buffer_size()
{
    ASSERT_EQ(config.hdrsz, test_writer->header_buffer_size());
    ASSERT_EQ(config.hdrsz, test_reader->header_buffer_size());
    ASSERT_EQ(config.bufsz, test_writer->data_buffer_size());
    ASSERT_EQ(config.bufsz, test_reader->data_buffer_size());
}

void DadaClientTester::test_buffer_count()
{
    ASSERT_EQ(config.nhdrs, test_writer->header_buffer_count());
    ASSERT_EQ(config.nhdrs, test_reader->header_buffer_count());
    ASSERT_EQ(config.nbufs, test_writer->data_buffer_count());
    ASSERT_EQ(config.nbufs, test_reader->data_buffer_count());
}

void DadaClientTester::test_reader_has_block()
{
    auto& hstream = test_reader->header_stream();
    auto& dstream = test_reader->data_stream();
    this->write_data_slot(); // Write one slot, so it can be read
    this->write_header_slot(); // Write one slot, so it can be read
    hstream.next(); // Acquire the header slot
    dstream.next(); // Acquire the data slot
    ASSERT_TRUE(dstream.has_block());
    ASSERT_TRUE(hstream.has_block());
    dstream.release(); // Release the read header slot
    hstream.release(); // Release the read data slot
    ASSERT_FALSE(dstream.has_block());
    ASSERT_FALSE(hstream.has_block());
}

void DadaClientTester::test_reader_stop_next()
{
    auto& hstream = test_reader->header_stream();
    auto& dstream = test_reader->data_stream();
    test_reader->stop_next();
    EXPECT_THROW(hstream.next(), DadaInterruptException);
    EXPECT_THROW(dstream.next(), DadaInterruptException);
}

void DadaClientTester::test_reader_stop_next_from_thread()
{
    auto& hstream = test_reader->header_stream();
    auto& dstream = test_reader->data_stream();
    std::thread hthread([&hstream]() {
        EXPECT_THROW(hstream.next(), DadaInterruptException);
    });
    test_reader->stop_next();
    hthread.join();
    std::thread dthread([&dstream]() {
        EXPECT_THROW(dstream.next(), DadaInterruptException);
    });
    test_reader->stop_next();
    dthread.join();
}

void DadaClientTester::test_reader_connect()
{
    ASSERT_TRUE(test_reader->is_connected());
    test_reader->disconnect();
    ASSERT_FALSE(test_reader->is_connected());
    test_reader->connect();
    ASSERT_TRUE(test_reader->is_connected());
}

void DadaClientTester::test_reader_disconnect_while_acquired()
{
    this->write_data_slot(); // Write one slot, so it can be read
    test_reader->data_stream().next(); // Acquire the slot
    ASSERT_TRUE(test_reader->data_stream().has_block());
    test_reader->disconnect(); // Disconnect while acquired
    ASSERT_FALSE(test_reader->is_connected());
    this->write_data_slot();
    test_reader->connect();
    test_reader->data_stream().next();
    ASSERT_TRUE(test_reader->data_stream().has_block());
}

void DadaClientTester::test_writer_connect()
{
    ASSERT_TRUE(test_writer->is_connected());
    test_writer->disconnect();
    ASSERT_FALSE(test_writer->is_connected());
    test_writer->connect();
    ASSERT_TRUE(test_writer->is_connected());
}

void DadaClientTester::test_writer_disconnect_while_acquired()
{
    test_writer->data_stream().next(); // Acquire the slot
    test_writer->header_stream().next(); // Acquire the slot
    ASSERT_TRUE(test_writer->data_stream().has_block());
    ASSERT_TRUE(test_writer->header_stream().has_block());
    test_writer->disconnect(); // Disconnect while acquired
    ASSERT_EQ(test_reader->data_buffer_full_slots(), 1); // Use reader to get full slots as writer is disconnected
    ASSERT_EQ(test_reader->header_buffer_full_slots(), 1); // Use reader to get full slots as writer is disconnected
    ASSERT_FALSE(test_writer->is_connected());
    test_writer->connect();
}

void DadaClientTester::test_writer_stop_next()
{
    auto& hstream = test_writer->header_stream();
    auto& dstream = test_writer->data_stream();
    test_writer->stop_next(); // Set before next call
    EXPECT_THROW(hstream.next(), DadaInterruptException);
    EXPECT_THROW(dstream.next(), DadaInterruptException);
}

void DadaClientTester::test_writer_stop_next_from_thread()
{
    auto& hstream = test_writer->header_stream();
    auto& dstream = test_writer->data_stream();
    std::thread hthread([&hstream]() {
        EXPECT_THROW(hstream.next(), DadaInterruptException);
    });
    test_writer->stop_next();
    hthread.join();
    std::thread dthread([&dstream]() {
        EXPECT_THROW(dstream.next(), DadaInterruptException);
    });
    test_writer->stop_next();
    dthread.join();
}


void DadaClientTester::test_cuda_register_unregister_memory()
{
#ifdef ENABLE_CUDA
    cudaStream_t d2h_copy;
    cudaStream_t h2d_copy;
    CUDA_ERROR_CHECK(cudaStreamCreate(&d2h_copy));
    CUDA_ERROR_CHECK(cudaStreamCreate(&h2d_copy));
    ASSERT_TRUE(test_writer->has_registered());
    ASSERT_TRUE(test_reader->has_registered());
    size_t buffer_size = test_buffer->data_buffer_size();
    thrust::device_vector<char> idata(buffer_size, 42);
    thrust::device_vector<char> odata(buffer_size, 0);
    
    // Writer client: Copy data from the GOU and write it to DADA memory 
    auto copy_to_buffer = test_writer->data_stream().next();
    CUDA_ERROR_CHECK(cudaMemcpyAsync(copy_to_buffer.ptr(), 
        thrust::raw_pointer_cast(idata.data()), buffer_size, cudaMemcpyDeviceToHost, d2h_copy));
    CUDA_ERROR_CHECK(cudaStreamSynchronize(d2h_copy));
    test_writer->data_stream().release();

    // Reader client: read data from DADA memory and copy it to the GPU
    auto copy_from_buffer = test_reader->data_stream().next();
    CUDA_ERROR_CHECK(cudaMemcpyAsync(thrust::raw_pointer_cast(odata.data()), 
        copy_from_buffer.ptr(), buffer_size, cudaMemcpyHostToDevice, h2d_copy));
    CUDA_ERROR_CHECK(cudaStreamSynchronize(h2d_copy));
    test_reader->data_stream().release();

    ASSERT_TRUE(thrust::equal(idata.begin(), idata.end(), odata.begin(), 
        [] __device__ (char a, char b) { return a == b; }));

    test_writer->cuda_unregister_memory();
    test_reader->cuda_unregister_memory();
    ASSERT_FALSE(test_writer->has_registered());
    ASSERT_FALSE(test_reader->has_registered());
#endif
}

// ----------------
// HELPER FUNCTIONS 
// ----------------
void DadaClientTester::write_header_slot()
{
    auto& hstream = test_writer->header_stream();
    auto& hbuffer = hstream.next();
    hbuffer.used_bytes(config.hdrsz);
    hstream.release();
}


void DadaClientTester::write_data_slot()
{
    auto& dstream = test_writer->data_stream();
    auto& dbuffer = dstream.next();
    dbuffer.used_bytes(config.bufsz);
    dstream.release();
}

void DadaClientTester::read_header_slot()
{
    auto& hstream = test_reader->header_stream();
    hstream.next();
    hstream.release();
}


void DadaClientTester::read_data_slot()
{
    auto& dstream = test_reader->data_stream();
    dstream.next();
    dstream.release();
}

void DadaClientTester::write_header_until_full()
{
    while(test_writer->header_buffer_free_slots())
    {
        write_header_slot();
    }
}

void DadaClientTester::write_data_until_full()
{
    while(test_writer->data_buffer_free_slots())
    {
        write_data_slot();
    }
}

void DadaClientTester::read_header_until_empty()
{
    while(test_reader->header_buffer_full_slots())
    {
        read_header_slot();
    }
}

void DadaClientTester::read_data_until_empty()
{
    while(test_reader->data_buffer_full_slots())
    {
        read_data_slot();
    }
}

TEST_P(DadaClientTester, test_buffer_is_full){ test_buffer_is_full(); }
TEST_P(DadaClientTester, test_buffer_is_empty){ test_buffer_is_empty(); }
TEST_P(DadaClientTester, test_buffer_nfree){ test_buffer_nfree(); }
TEST_P(DadaClientTester, test_buffer_nfull){ test_buffer_nfull(); }
TEST_P(DadaClientTester, test_buffer_size){ test_buffer_size(); }
TEST_P(DadaClientTester, test_buffer_count){ test_buffer_count(); }
TEST_P(DadaClientTester, test_cuda_register_unregister_memory){ test_cuda_register_unregister_memory(); }

TEST_P(DadaClientTester, test_reader_has_block){ test_reader_has_block(); }
TEST_P(DadaClientTester, test_reader_connect){ test_reader_connect(); }
TEST_P(DadaClientTester, test_reader_disconnect_while_acquired){ test_reader_disconnect_while_acquired(); }
TEST_P(DadaClientTester, test_reader_interrupt_when_acquire){ test_reader_stop_next(); }
TEST_P(DadaClientTester, test_reader_stop_next_from_thread){ test_reader_stop_next_from_thread(); }

TEST_P(DadaClientTester, test_writer_connect){ test_writer_connect(); }
TEST_P(DadaClientTester, test_writer_disconnect_while_acquired){ test_writer_disconnect_while_acquired(); }
TEST_P(DadaClientTester, test_writer_stop_next){ test_writer_stop_next(); }
TEST_P(DadaClientTester, test_writer_stop_next_from_thread){ test_writer_stop_next_from_thread(); }

INSTANTIATE_TEST_SUITE_P(DadaClientBaseTesterInstantiation, DadaClientTester, ::testing::Values(
    //          in_key | out_key | device_id | heap_size | n_sample | n_channel | n_element | n_pol | n_acc | n_bit
    dada_buffer_config{16, 2048, 8, 4096},
    dada_buffer_config{11, 2345, 12, 1111},
    dada_buffer_config{123, 24, 2, 1337},
    dada_buffer_config{9, 111222, 19, 32}
));

}
}