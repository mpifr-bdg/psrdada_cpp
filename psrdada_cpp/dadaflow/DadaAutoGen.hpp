#pragma once
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/DadaHeader.hpp"
#include "psrdada_cpp/dadaflow/units.hpp"
#include <string>
#include <memory>
#include <functional>
#include <optional>
#include <numeric>


/**
 * Purpose of this file is to provide tools for automatically parsing 
 * DADA headers in order to build DescribedData objects that contain
 * the contents of each buffer.
 */

namespace psrdada_cpp {


struct DefaultKeyFormatter {

    /**
     * Create a range from the first to the last vector item avoiding long string in header file.
     * ToDo: The function is very stupid and is not analysing the range, it just takes the first and last element.
     *      Write some parsing here which parses the stepwidth and the actual first and last vector.
     */
    static std::string range(std::vector<std::string> const& vec) 
    {
        return vec.front() + ":" + vec.back();
    }

    static std::string format(std::string const& key) {
        return key;
    }
};

template <int N>
struct SuffixKeyFormatter {
    static std::string format(std::string const& key) {
        return key + "_" + std::to_string(N);
    }
};

template <typename Dimension, typename KeyFormatter = DefaultKeyFormatter>
struct DimensionParser
{
    static void from_header(DadaHeader const& header, Dimension& dim){}
    static void to_header(Dimension const& dim, DadaHeader& header){}
};

template <typename KeyFormatter>
struct DimensionParser<ContiguousFrequencyDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, ContiguousFrequencyDimension& dim) {
        std::size_t nchans = header.get<std::size_t>(KeyFormatter::format("NCHANS"));
        dim.resize_dimension(nchans);
        const double cfreq = header.get<double>(KeyFormatter::format("FREQUENCY")); // Hz
        const double bandwidth  = header.get<double>(KeyFormatter::format("BANDWIDTH")); // Hz
        const double channel_bw = bandwidth / nchans;
        dim.channel_zero_freq((cfreq - bandwidth / 2 + channel_bw / 2) * hertz);
        dim.channel_bandwidth(channel_bw * hertz);
    }

    static void to_header(ContiguousFrequencyDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NCHANS"), dim.nchannels());
        header.set(KeyFormatter::format("FREQUENCY"), dim.centre_frequency().value());
        header.set(KeyFormatter::format("BANDWIDTH"), dim.bandwidth().value());
    }

};


template <typename KeyFormatter>
struct DimensionParser<NonContiguousFrequencyDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, NonContiguousFrequencyDimension& dim) {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NCHANS")));
        const auto frequencies = header.get<double>(KeyFormatter::format("FREQUENCIES"), ","); // Hz
        NonContiguousFrequencyDimension::FrequenciesType frequencies_hz;
        frequencies_hz.reserve(frequencies.size());
        for(auto const freq: frequencies) {
            frequencies_hz.emplace_back(freq * hertz);
        }
        dim.frequencies(frequencies_hz);
    }

    static void to_header(NonContiguousFrequencyDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NCHANS"), dim.nchannels());
        std::vector<double> frequencies;
        frequencies.reserve(dim.nchannels());
        for (auto const& freq: dim.frequencies()) {
            frequencies.emplace_back(freq.value());
        }
        header.set(KeyFormatter::format("FREQUENCIES"), frequencies);
    }
};


template <typename KeyFormatter>
struct DimensionParser<TimeDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, TimeDimension& dim)
    {
        if (auto nsamples = header.get_optional<std::size_t>(KeyFormatter::format("NSAMPLES"))) {
            dim.resize_dimension(*nsamples);
        }
        if (auto tsamp = header.get_optional<long double>(KeyFormatter::format("TSAMP"))) {
            dim.timestep(std::chrono::duration<long double, pico>(*tsamp * 1e12));
        }
    }

    static void to_header(TimeDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NSAMPLES"), dim.nsamples());
        header.set(KeyFormatter::format("TSAMP"), dim.timestep().count());
    }
};


template <typename KeyFormatter>
struct DimensionParser<BeamDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, BeamDimension& dim)
    {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NBEAMS")));
        if (auto beams = header.get_optional<std::string>(KeyFormatter::format("BEAMS"), ",")){
            dim.beams(*beams);
        }
    }

    static void to_header(BeamDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NBEAMS"), dim.nbeams());
        header.set(KeyFormatter::format("BEAMS"), dim.beams());
    }
};

template <typename KeyFormatter>
struct DimensionParser<AntennaDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, AntennaDimension& dim)
    {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NANTENNAS")));
        if (auto antennas = header.get_optional<std::string>(KeyFormatter::format("ANTENNAS"), ",")){
            dim.antennas(*antennas);
        }
    }

    static void to_header(AntennaDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NANTENNAS"), dim.nantennas());
        header.set(KeyFormatter::format("ANTENNAS"), dim.antennas());
    }
};


template <typename KeyFormatter>
struct DimensionParser<PolarisationDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, PolarisationDimension& dim)
    {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NPOL")));
        if (auto polarisations = header.get_optional<std::string>(KeyFormatter::format("POLARISATIONS"), ",")){
            dim.polarisations(*polarisations);
        }
    }

    static void to_header(PolarisationDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NPOL"), dim.npol());
        header.set(KeyFormatter::format("POLARISATIONS"), dim.polarisations());
    }
};


template <typename KeyFormatter>
struct DimensionParser<DispersionDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, DispersionDimension& dim)
    {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NDMS")));
        if (auto dms = header.get_optional<double>(KeyFormatter::format("DMS"), ",")){
            using DMVector = typename DispersionDimension::DispersionMeasuresType;
            DMVector dms_pcccm;
            dms_pcccm.reserve((*dms).size());
            for (auto dm: *dms) {
                dms_pcccm.emplace_back(dm * pc/cm3);
            }
            dim.dms(dms_pcccm);
        }
    }

    static void to_header(DispersionDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NDMS"), dim.ndms());
        std::vector<double> dms;
        dms.reserve(dim.ndms());
        for (auto const& dm: dim.dms()) {
            dms.emplace_back(dm.value());
        }
        header.set(KeyFormatter::format("DMS"), dms);
    }
};

template <typename KeyFormatter>
struct DimensionParser<GateDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, GateDimension& dim)
    {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NGATES")));
        if (auto gates = header.get_optional<std::string>(KeyFormatter::format("GATES"), ",")){
            dim.gates(*gates);
        }
    }

    static void to_header(GateDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NGATES"), dim.ngates());
        header.set(KeyFormatter::format("GATES"), dim.gates());
    }
};



template <typename KeyFormatter>
struct DimensionParser<BaselineDimension, KeyFormatter>
{
    static void from_header(DadaHeader const& header, BaselineDimension& dim) {
        dim.resize_dimension(header.get<std::size_t>(KeyFormatter::format("NBASELINES")));
        const auto baselines = header.get<std::string>(KeyFormatter::format("BASELINES"), ",");
        BaselineDimension::BaselinesType baseline_map;
        baseline_map.reserve(baselines.size());
        for(auto const baseline: baselines) {
            baseline_map.emplace_back(baseline);
        }
        dim.baselines(baseline_map);
    }

    static void to_header(BaselineDimension const& dim, DadaHeader& header) {
        header.set(KeyFormatter::format("NBASELINES"), dim.nbaselines());
        header.set(KeyFormatter::format("BASELINES"), KeyFormatter::range(dim.baselines()));
    }
};


template <template <int> class XDimensionN, int N>
struct DimensionParser<XDimensionN<N>> {
private:
    using BaseType = typename XDimensionN<N>::BaseType;
    using Formatter = SuffixKeyFormatter<N>;

public:
    static void from_header(DadaHeader const& header, XDimensionN<N>& dim) {
        DimensionParser<BaseType, Formatter>::from_header(header, dim);
    }

    static void to_header(XDimensionN<N> const& dim, DadaHeader& header) {
        DimensionParser<BaseType, Formatter>::to_header(dim, header);
    }
};


template <typename ContainerType, typename... Dimensions>
void dimensions_to_header(DescribedData<ContainerType, Dimensions...> const& data, DadaHeader& header)
{
    (DimensionParser<Dimensions>::to_header(data, header), ...);
};




class InvalidBlockSize: public std::runtime_error
{
  public:
    explicit InvalidBlockSize(const std::string& message)
        : std::runtime_error(message)
    {
    }
};


template <typename T>
class DadaAutoGen;

template <typename T, typename... Dimensions>
class DadaAutoGen<DescribedData<std::span<T>, Dimensions...>>
{
public:
    using Container = std::span<T>;
    using DimensionsTuple = std::tuple<Dimensions...>; 
    using DataType = DescribedData<Container, Dimensions...>;
    using DataPtr = std::shared_ptr<DataType>;
    using ReleaseLifetime  = std::shared_ptr<void>;
    using MetadataType = MetadataMap;
    using MetadataPtrType = std::shared_ptr<MetadataType>;
    using MetadataCallback = std::function<void(MetadataType&, DadaHeader const&)>;
    using TimestepType = typename BaseTimeDimension::TimestepType;

    DadaAutoGen(){}

    explicit DadaAutoGen(MetadataCallback&& callback)
        :_metadata_callback(callback) {}

    void parse_header(RawBytes& header_bytes)
    {
        DadaHeader header;
        header.purge();
        header.read_from(header_bytes);
        
        std::vector<TimestepType> timesteps;
        std::apply(
            [&](auto&... dims){
                (DimensionParser<Dimensions>::from_header(header, dims), ...);
                (_extents.push_back(dims.extent()), ...);
                (append_timestep<Dimensions>(timesteps, dims), ...);
            }, _dimensions
        );

        if (timesteps.size() == 0) {
            throw std::runtime_error("No timesteps specified");
        }
        _innermost_timestep = timesteps.back();

        validate_extents();

        _timestamp = parse_timestamp(header);

        _metadata = std::make_shared<MetadataType>();
        if (_metadata_callback) {
            _metadata_callback(*_metadata, header);
        }
    }

    DataPtr parse_data(RawBytes& bytes, ReleaseLifetime releaser)
    {
        const std::size_t nelements = bytes.used_bytes() / sizeof(T);

        if ((nelements % _minimum_size != 0) || (nelements == 0)) {
            std::stringstream msg;
            msg << "Invalid number of " << sizeof(T) << "-byte elements in block: " 
                << nelements << " is not a multiple of " << _minimum_size;
            throw InvalidBlockSize(msg.str());
        }
        
        if (_unspecified_idx.has_value()) {
            _extents[*_unspecified_idx] = nelements / _minimum_size;
        } else {
            if (nelements != _minimum_size) {
                throw std::runtime_error("Block is a multiple of expected size but"
                                         " has no unspecified dimensions");
            }
        }

        std::span<T> container(reinterpret_cast<T*>(bytes.ptr()), nelements);
        DataPtr data_ptr = 
            DataPtr(new DataType{container, _extents},
                    [releaser](DataType* ptr) { delete ptr; });

        std::apply(
            [&](auto&... dims){
                (data_ptr->Dimensions::from(dims), ...);
            }, _dimensions
        );

        data_ptr->timestamp(_timestamp + total_samples * _innermost_timestep);
        total_samples += data_ptr->total_nsamples();
        data_ptr->metadata_ptr(_metadata);
        return data_ptr;
    }   


private:
    void validate_extents() {
        std::size_t index = 0;
        for (auto extent: _extents)
        {
            if (extent == 0) {
                if (_unspecified_idx.has_value()) {
                    throw std::runtime_error("Only one extent may be unspecified");
                } else {
                    _unspecified_idx = index;
                    _extents[index] = 1;
                }
            }
            ++index;
        }
        _minimum_size = std::accumulate(_extents.begin(), _extents.end(), 1, std::multiplies<>());
    }

    template <typename Dimension>
    void append_timestep(std::vector<TimestepType>& timesteps, Dimension const& dim)
    {
        if constexpr(std::is_base_of_v<BaseTimeDimension, Dimension>) {
            try {
                timesteps.push_back(dim.timestep());
            } catch (std::runtime_error&) {
                // The timestamp might not be set here and that is ok.
            }
        }
    } 

    Timestamp parse_timestamp(DadaHeader const& header) const {
        // ? Should this fallback to UTC_START/PICOSECONDS and MJD_START?
        const std::size_t sample_clock_start =
            header.get<std::size_t>("SAMPLE_CLOCK_START");
        const long double sample_clock =
            header.get<long double>("SAMPLE_CLOCK");
        const long double sync_time = header.get<long double>("SYNC_TIME");
        long double unix_epoch = sync_time + sample_clock_start / sample_clock;
        return {unix_epoch};
    }

    MetadataCallback _metadata_callback;
    DimensionsTuple _dimensions;
    MetadataPtrType _metadata;
    Timestamp _timestamp;
    TimestepType _innermost_timestep{};
    std::vector<std::size_t> _extents;
    std::optional<std::size_t> _unspecified_idx;
    std::size_t _minimum_size = 0;
    std::size_t total_samples = 0;
};


}