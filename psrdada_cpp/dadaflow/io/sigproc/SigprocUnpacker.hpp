#pragma once

#include "psrdada_cpp/dadaflow/io/sigproc/SigprocIO.hpp"
#include "psrdada_cpp/dadaflow/ResourceAllocator.hpp"
#include <algorithm>

namespace psrdada_cpp {
namespace sigproc {

/**
 * @brief Unpack a FilterbankVariant to a given trivial type
 * 
 * @tparam TargetType Target type, must be a Filterbank<?> type.
 * @tparam AllocatorType The type of allocator for the outputs
 */
template <typename TargetType, typename AllocatorType = DefaultAllocator<TargetType>>
class SigprocUnpacker {
public:
    using OutputPtr = std::shared_ptr<TargetType>;

    /**
     * @brief Construct a new Sigproc Unpacker object
     * 
     * @param args The constructor arguments for the chosen allocator type
     */
    SigprocUnpacker(auto&&... args)
    : _allocator(std::make_unique<AllocatorType>(std::forward<decltype(args)>(args)...))
    {}
    ~SigprocUnpacker() = default;
    SigprocUnpacker(SigprocUnpacker const&) = delete;
    SigprocUnpacker& operator=(SigprocUnpacker const&) = delete;
    SigprocUnpacker(SigprocUnpacker&&) = default;
    SigprocUnpacker& operator=(SigprocUnpacker&&) = default;

    /**
     * @brief Unpack a FilterbankVariant to the target type
     * 
     * @param input_variant An instance of a FilterbankVariant
     * @return OutputPtr A shared pointer to an instance of the target type
     */
    OutputPtr operator()(const FilterbankVariant& input_variant) {
        // Obtain a TargetType instance from the resource pool
        OutputPtr output_ptr = _allocator->get();
        TargetType& output = *output_ptr;

        // Use std::visit to process the FilterbankVariant and call unpack_impl
        std::visit([&output, this](auto&& arg) {
            using T = typename std::decay_t<decltype(arg)>::element_type::value_type;
            BOOST_LOG_TRIVIAL(trace) << "Unpacking from " << typeid(T).name() 
                  << " to " << typeid(typename TargetType::value_type).name() << "\n";
            auto const& input = *arg;
            output.like(input);
            this->unpack_impl(input, output);
        }, input_variant);
        return output_ptr;
    }

private:
    /**
     * @brief Generic unpack implementation that implicitly casts to the target type
     */
    template <typename SourceType>
    void unpack_impl(const SourceType& input, TargetType& output) {
        std::copy(input.begin(), input.end(), output.begin());
    }

    void unpack_impl(Filterbank1Bit const& input, TargetType& output) {
        unpack<Filterbank1Bit, 1>(input, output);
    }

    void unpack_impl(Filterbank2Bit const& input, TargetType& output) {
        unpack<Filterbank2Bit, 2>(input, output);
    }

    void unpack_impl(Filterbank4Bit const& input, TargetType& output) {
        unpack<Filterbank4Bit, 4>(input, output);
    }

    template <typename T, uint8_t Nbits>
    void unpack(T const& input, TargetType& output) {
        uint8_t mask = (1 << Nbits) - 1;
        auto it = output.begin();
        std::size_t n = output.size();
        for (auto const& byte : input.container().container()) {
            std::size_t remaining = std::min(std::size_t{8}, Nbits * n);
            for (std::size_t ii = 0; ii < remaining; ii += Nbits) {
                *(it++) = static_cast<typename TargetType::value_type>((byte >> ii) & mask);  
            }
            n -= (8 / Nbits);
        }
    }

    // Keep this as a ptr to maintain move semantics
    std::unique_ptr<AllocatorType> _allocator;
};

} // namespace sigproc
} // namespace psrdada_cpp