#pragma once
#include <memory>
#include <thread>
#include <gtest/gtest.h>
#ifdef ENABLE_CUDA
#include <thrust/device_vector.h>
#include <thrust/equal.h>
#include "psrdada_cpp/cuda_utils.hpp"
#endif
#include "psrdada_cpp/dada_db.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/dada_read_client.hpp"


namespace psrdada_cpp {
namespace test {


struct dada_buffer_config
{
    uint64_t nbufs;
    uint64_t bufsz;
    uint64_t nhdrs;
    uint64_t hdrsz;
};

class DadaClientTester : public ::testing::TestWithParam<dada_buffer_config>
{
public:
    DadaClientTester();

    ~DadaClientTester(){}

    void SetUp() override;

    void TearDown() override;

    void test_buffer_is_full();

    void test_buffer_is_empty();

    void test_buffer_nfree();

    void test_buffer_nfull();

    void test_buffer_size();

    void test_buffer_count();
    
    void test_cuda_register_unregister_memory();

    void test_reader_has_block();

    void test_reader_connect();

    void test_reader_disconnect_while_acquired();

    void test_reader_stop_next();
    
    void test_reader_stop_next_from_thread();

    void test_writer_connect();

    void test_writer_disconnect_while_acquired();

    void test_writer_stop_next();

    void test_writer_stop_next_from_thread();

private:
    void write_header_slot();
    void write_data_slot();
    void read_header_slot();
    void read_data_slot();
    void write_header_until_full();
    void write_data_until_full();
    void read_header_until_empty();
    void read_data_until_empty();

private:
    MultiLog wlog;
    MultiLog rlog;
    dada_buffer_config config;
    std::unique_ptr<DadaWriteClient> test_writer;
    std::unique_ptr<DadaReadClient> test_reader;
    std::unique_ptr<DadaDB> test_buffer;
};

}
}