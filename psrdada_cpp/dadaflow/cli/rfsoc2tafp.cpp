/**
 * This is the FFT detector and accumulator pipeline intended for use
 * with the EDD transient detection pipeline. Its input is data from
 * a polyphase filterbank in TFTP order and its output is sigproc
 * filterbank order data in floating point.
 */

#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/dadaflow/pipelines/RFSoC2TAFP.hpp"

#include <boost/program_options.hpp>

using namespace std::chrono;
using namespace boost::units;
using namespace boost::units::si;
using namespace psrdada_cpp;

namespace
{
const size_t ERROR_IN_COMMAND_LINE     = 1;
const size_t SUCCESS                   = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;
} // namespace

struct RFSoC2TAFPArgs {
    key_t input_dada_key;
    key_t output_dada_key;
    std::size_t nthreads;
};

int main(int argc, char** argv)
{
    try {
        RFSoC2TAFPArgs pipeline_args;
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()("help,h", "Print help messages")
            
            ("input-key,i",
            po::value<std::string>()->default_value("dada")->notifier(
                [&pipeline_args](std::string in) {
                    pipeline_args.input_dada_key = string_to_key(in);
                }),
            "The key for the input DADA buffer (hex key as string without "
            "leading 0x)")
            
            ("output-key,i",
            po::value<std::string>()->default_value("dadc")->notifier(
                [&pipeline_args](std::string in) {
                    pipeline_args.output_dada_key = string_to_key(in);
                }),
            "The key for the output DADA buffer (hex key as string without "
            "leading 0x)")
            
            ("nthreads,n",
            po::value<std::size_t>()->default_value(8)->notifier(
                [&pipeline_args](std::size_t nthreads) { pipeline_args.nthreads = nthreads; }),
            "The number of threads to use in the transpose.")
            
            ("log_level",
            po::value<std::string>()->default_value("info")->notifier(
                [](std::string level) { set_log_level(level); }),
                   "The logging level to use (debug, info, warning, error)");
        po::variables_map vm;
        try {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if(vm.count("help")) {
                std::cout << "rfsoc2tafp -- Convert TAPFT data from the ARGOS RFSoC channeliser to TAFP\n"
                          << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        } catch(po::error& e) {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        /**
         * All the application code goes here
         */
        RFSoC2TAFPPipeline pipeline(pipeline_args.input_dada_key, 
            pipeline_args.output_dada_key, pipeline_args.nthreads);
        Graph& graph = pipeline.graph();
        graph.show_dependencies();
        graph.show_topological_order();
        graph.make_dot_graph();
        GraphRunner::run(&graph);
        graph.show_statistics();
        /**
         * End of application code
         */
    } catch(std::exception& e) {
        std::cerr << "Unhandled Exception reached the top of main: " << e.what()
                  << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;
}