#ifndef PSRDADA_CPP_DADA_CLIENT_BASE_HPP
#define PSRDADA_CPP_DADA_CLIENT_BASE_HPP

#include <exception>
#include <psrdada/dada_hdu.h>
#include <psrdada/dada_def.h>
#ifdef ENABLE_CUDA
#include <psrdada/dada_cuda.h>
#endif //ENABLE_CUDA
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/common.hpp"

namespace psrdada_cpp {

    class DadaResourceInUseException : public std::exception {
        private:
            std::string _message;
        public:
            DadaResourceInUseException(const std::string& msg)
                : _message(std::move(msg)) {}
        
            const char* what() const noexcept override 
            {
                return _message.c_str();
            }
    };

    class DadaInterruptException : public std::exception
    {
    private:
        std::string _message;
    public: 
        DadaInterruptException(const std::string& msg) 
            : _message(std::move(msg)) {}
        
        const char* what() const noexcept override 
        {
            return _message.c_str();
        }
    };

    class DadaClientBase
    {
    protected:
        key_t _key;
        dada_hdu_t* _hdu;
        bool _connected;
        bool _cuda_registered_memory = false;
        MultiLog& _log;
        std::string _id;
        std::atomic<bool> _stop{false};

    public:
        /**
         * @brief      Create a new basic DADA client instance
         *
         * @param[in]  key   The hexidecimal shared memory key
         * @param      log   A MultiLog instance for logging buffer transactions
         */
        DadaClientBase(key_t key, MultiLog& log);
        DadaClientBase(DadaClientBase const&) = delete;
        virtual ~DadaClientBase();

        /**
         * @brief      Get the sizes of each data block in the ring buffer
         */
        std::size_t data_buffer_size() const;

        /**
         * @brief      Get the sizes of each header block in the ring buffer
         */
        std::size_t header_buffer_size() const;

        /**
         * @brief      Get the number of data blocks in the ring buffer
         */
        std::size_t data_buffer_count() const;

        /**
         * @brief      Get the number of header blocks in the ring buffer
         */
        std::size_t header_buffer_count() const;

        /**
         * @brief      Return the number of full data slots
         */
        std::size_t data_buffer_full_slots() const;

        /**
         * @brief      Return the number of free data slots
         */
        std::size_t data_buffer_free_slots() const;

        /**
         * @brief      Returns true if the data block is full otherwise false
         */
        bool data_buffer_is_full() const;

        /**
         * @brief      Returns true if the data block is empty otherwise false
         */
        bool data_buffer_is_empty() const;

        /**
         * @brief      Return the number of full header slots
         */
        std::size_t header_buffer_full_slots() const;

        /**
         * @brief      Return the number of free header slots
         */
        std::size_t header_buffer_free_slots() const;

        /**
         * @brief      Returns true if the header block is full otherwise false
         */
        bool header_buffer_is_full() const;
        
        /**
         * @brief      Returns true if the header block is empty otherwise false
         */
        bool header_buffer_is_empty() const;

        /**
         * @brief      Connect to ring buffer
         */
        virtual void connect();

        /**
         * @brief      Disconnect from ring buffer
         */
        virtual void disconnect();

        /**
         * @brief      Reconnect to the ring buffer
         */
        void reconnect();

        /**
         * @brief     Pin memory with CUDA API
         */
        void cuda_register_memory();

        /**
         * @brief     Unpin memory with CUDA API
         */
        void cuda_unregister_memory();

        /**
         * @brief     Stops the client while it is acquiring the next data or header block.
         *              This function forces the client to interrupt the next block acquisition 
         */
        void stop_next();

        /**
         * @brief     Returns true if the client is connected to a buffer
         */
        bool is_connected();

        /**
         * @brief     Returns true if the client has registered the memory as pinned
         */
        bool has_registered();

        /**
         * @brief      Return a string identifier based on the buffer key and log name
         */
        std::string const& id() const;
};

} //namespace psrdada_cpp

#endif //PSRDADA_CPP_DADA_CLIENT_BASE_HPP
