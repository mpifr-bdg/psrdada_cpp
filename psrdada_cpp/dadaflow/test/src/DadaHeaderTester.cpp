#include "psrdada_cpp/dadaflow/DadaHeader.hpp"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

namespace psrdada_cpp
{
namespace test
{
namespace
{
char header_bytes[4096] = R""""(
    VALID_ULL_KEY 1024
    INVALID_ULL_KEY -1

    VALID_LL_KEY -100
    INVALID_LL_KEY 123123123123123123123

    # Comment Line
    KEY_WITH_COMMENT HELLO # This is a comment

    KEY_WITHOUT_VALUE # This should not raise an error unless gotten

    VALID_FIXED_LD_KEY 1231231.12312313
    VALID_SCI_LD_KEY 1231.2342e12

    VALID_STRING_VEC_KEY STRING0 STRING1 STRING2
    VALID_FLOAT_VEC_KEY 1.0,2.0,3.0,4.0
    VALID_ULL_VEC_KEY 1,1,2,3,5,8

    INVALID_NUMERIC_KEY = 1,3.0,4,test,8
    )"""";
}

TEST(DadaHeaderTester, valid_reads)
{
    DadaHeader header;
    header.read_from(header_bytes, 4096);
    ASSERT_EQ(header.get<std::size_t>("VALID_ULL_KEY"), std::size_t{1024});
    ASSERT_EQ(header.get<long long>("VALID_LL_KEY"),
              static_cast<long long>(-100));
    ASSERT_EQ(header.get<std::string>("KEY_WITH_COMMENT"),
              std::string{"HELLO"});
    ASSERT_DOUBLE_EQ(header.get<long double>("VALID_FIXED_LD_KEY"),
                     static_cast<long double>(1231231.12312313));
    ASSERT_DOUBLE_EQ(header.get<long double>("VALID_SCI_LD_KEY"),
                     static_cast<long double>(1231.2342e12));
    ASSERT_EQ(header.get<std::string>("VALID_STRING_VEC_KEY", " "),
              (std::vector<std::string>{"STRING0", "STRING1", "STRING2"}));
    ASSERT_EQ(header.get<float>("VALID_FLOAT_VEC_KEY", ","),
              (std::vector<float>{1.0, 2.0, 3.0, 4.0}));
    ASSERT_EQ(header.get<std::size_t>("VALID_ULL_VEC_KEY", ","),
              (std::vector<std::size_t>{1, 1, 2, 3, 5, 8}));
}

TEST(DadaHeaderTester, invalid_reads)
{
    DadaHeader header;
    header.read_from(header_bytes, 4096);
    ASSERT_THROW(static_cast<void>(header.get<std::size_t>("INVALID_ULL_KEY")),
                 std::runtime_error);
    ASSERT_THROW(static_cast<void>(header.get<long long>("INVALID_LL_KEY")),
                 std::runtime_error);
    ASSERT_THROW(
        static_cast<void>(header.get<std::string>("KEY_WITHOUT_VALUE")),
        std::runtime_error);
    ASSERT_THROW(
        static_cast<void>(header.get<std::size_t>("INVALID_NUMERIC_KEY", ",")),
        std::runtime_error);
}

TEST(DadaHeaderTester, overwrites)
{
    DadaHeader header;
    header.read_from(header_bytes, 4096);
    header.set("KEY_WITH_COMMENT", "OTHER_VALUE");

    DadaHeader header_validator;
    header_validator.read_from(header.to_vector());
    ASSERT_EQ(header_validator.get<std::string>("KEY_WITH_COMMENT"),
              std::string{"OTHER_VALUE"});
}

TEST(DadaHeaderTester, setters)
{
    DadaHeader header;
    std::vector<float> data{1.0, 2.0, 3.0};
    header.set("FLOAT_VEC", data);
    header.set("FLOAT_VAL", 1e3);

    DadaHeader header_validator;
    header_validator.read_from(header.to_vector());
    ASSERT_EQ(header_validator.get<float>("FLOAT_VEC", ","), data);
    ASSERT_EQ(header_validator.get<float>("FLOAT_VAL"), 1e3);
}

} // namespace test
} // namespace psrdada_cpp