#pragma once
#include <fcntl.h>
#include <semaphore.h>
#include <stdexcept>
#include <string>
#include <system_error>
#include <unistd.h>

namespace psrdada_cpp
{

/**
 * @brief A class for wrapping POSIX semaphores
 * 
 */
class Semaphore
{
  public:
    /**
     * @brief Construct a new Semaphore object
     * 
     * @param name The name of the semaphore
     * @param initial_value The initial semaphore value
     * @param open_existing Expect the semaphore to already exist
     * 
     * @details If open_existing is set to False (the default), then any 
     *          semaphore that matches the name will be deleted and a new 
     *          one created.
     * 
     */
    Semaphore(std::string_view name,
              unsigned int initial_value = 1,
              bool open_existing         = false)
        : _name(name)
    {
        if(open_existing) {
            _sem = sem_open(_name.c_str(), 0);
        } else {
            sem_unlink(_name.c_str());
            _sem =
                sem_open(_name.c_str(), O_CREAT | O_EXCL, 0644, initial_value);
        }

        if(_sem == SEM_FAILED) {
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Failed to create/open semaphore");
        }
    }

    Semaphore(const Semaphore&)            = delete;
    Semaphore& operator=(const Semaphore&) = delete;

    /**
     * @brief Move construct a new Semaphore object
     * 
     * @param other 
     */
    Semaphore(Semaphore&& other) noexcept
        : _sem(other._sem), _name(std::move(other._name))
    {
        other._sem = nullptr;
    }

    /**
     * @brief Move assign this Semaphore object
     * 
     * @param other 
     * @return Semaphore& 
     */
    Semaphore& operator=(Semaphore&& other) noexcept
    {
        if(this != &other) {
            close();
            _sem       = other._sem;
            _name      = std::move(other._name);
            other._sem = nullptr;
        }
        return *this;
    }

    /**
     * @brief Destroy the Semaphore object
     * 
     */
    ~Semaphore() { close(); }

    /**
     * @brief Acquire (decrement) the semaphore
     * 
     * @details If the semaphore is initially at zero, this call will block
     *          until the semephore value is incremented.
     */
    void wait()
    {
        if(sem_wait(_sem) == -1) {
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Failed to wait on semaphore");
        }
    }

    /**
     * @brief Release (increment) the semaphore
     * 
     * @details This call is non blocking
     */
    void post()
    {
        if(sem_post(_sem) == -1) {
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Failed to post semaphore");
        }
    }

    /**
     * @brief Check the value of the semaphore
     * 
     * @return int the current semaphore value
     */
    int count()
    {
        int sval;
        if(sem_getvalue(_sem, &sval) == -1) {
            throw std::system_error(errno,
                                    std::generic_category(),
                                    "Failed to get semaphore value");
        }
        return sval;
    }

    /**
     * @brief Get the handle to the underlying semaphore
     * 
     * @return sem_t* 
     */
    sem_t* get() const { return _sem; }

  private:
    sem_t* _sem;       // POSIX semaphore handle
    std::string _name; // Semaphore name

    // Close the semaphore
    void close()
    {
        if(_sem != nullptr) {
            sem_close(_sem);
            sem_unlink(_name.c_str());
            _sem = nullptr;
        }
    }
};

/**
 * @brief An RAII locking helper for semaphores
 * 
 * @code{.cpp}
 * Semaphore sem("some_semaphore")
 * {
 *     SemaphoreLock lock(sem);
 *     ...here do some safe work...
 * }
 * @endcode
 * 
 */
struct SemaphoreLock {
    explicit SemaphoreLock(Semaphore& sem): _sem(sem) { _sem.wait(); }

    ~SemaphoreLock() { _sem.post(); }

  private:
    Semaphore& _sem;
};

} // namespace psrdada_cpp