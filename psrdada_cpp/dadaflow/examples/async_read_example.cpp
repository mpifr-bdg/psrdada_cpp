#include "psrdada_cpp/dada_db.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/dadaflow/DadaHeader.hpp"
#include "psrdada_cpp/dadaflow/processors/AsyncDadaSource.hpp"
#include "psrdada_cpp/raw_bytes.hpp"

#include <atomic>
#include <chrono>
#include <cmath>
#include <condition_variable>
#include <functional>
#include <thread>
#include <type_traits>

namespace psrdada_cpp
{

struct SimpleData {
    std::vector<char> header;
    char* data; // Here this will be directly the DADA memory
    std::size_t size;
};

class ExampleParser
{
  public:
    using OutputType      = SimpleData;
    using OutputPtr       = std::shared_ptr<SimpleData>;
    using OutputTuple     = std::tuple<OutputPtr>;
    using ReleaseLifetime = std::shared_ptr<void>;

    ExampleParser()                                = default;
    ExampleParser(ExampleParser const&)            = delete;
    ExampleParser& operator=(ExampleParser const&) = delete;
    ExampleParser(ExampleParser&&)                 = default;

    void parse_header(RawBytes& header_bytes)
    {
        _header.purge();
        _header.read_from(header_bytes);
    }

    OutputTuple parse_data(RawBytes& data_bytes, ReleaseLifetime releaser)
    {
        OutputPtr output_ptr =
            OutputPtr(new OutputType, [releaser](OutputType* ptr) {
                delete ptr;
                // Here the releaser doesn't need to be called. It is captured
                // in the lambda and when the lambda is called it will go out of
                // scope, releasing the buffer
            });
        output_ptr->header = _header.to_vector();
        output_ptr->data   = data_bytes.ptr();
        output_ptr->size   = data_bytes.used_bytes();
        return std::tie(output_ptr);
    }

  private:
    DadaHeader _header;
};

} // namespace psrdada_cpp

int main()
{
    using namespace psrdada_cpp;
    MultiLog log("AsyncDadaSource");

    const std::size_t header_size = 4096;
    const std::size_t data_size   = 16384;

    DadaDB db(16, data_size, 4, header_size);
    db.create();
    char header[header_size];
    RawBytes header_bytes(header, header_size, header_size);

    char data[data_size];
    RawBytes data_bytes(data, data_size, data_size);

    DadaOutputStream writer(db.key(), log);
    writer.init(header_bytes);
    std::thread writer_thread([&]() {
        while(true) { writer(data_bytes); }
    });

    AsyncDadaSource source(db.key(), log, ExampleParser());

    std::thread reader_thread([&]() {
        while(true) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            auto result = source();
        }
    });

    writer_thread.join();
    reader_thread.join();
    db.destroy();
}