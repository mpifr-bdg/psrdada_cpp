#pragma once
#include "psrdada_cpp/dadaflow/utils.hpp"

#include <bit>
#include <iostream>
#include <type_traits>

#define BITS_PER_BYTE 8

namespace psrdada_cpp
{

// Concept to check if a type is an unsigned integral type
template <typename T>
concept UnsignedIntegral = std::integral<T> && std::is_unsigned_v<T>;

template <typename T>
constexpr T gcd(T a, T b)
{
    while(b != 0) {
        T temp = b;
        b      = a % b;
        a      = temp;
    }
    return a;
}

template <typename T>
constexpr T lcm(T a, T b)
{
    return (a / gcd(a, b)) * b;
}

template <uint8_t Nbits, UnsignedIntegral PackingWord_, std::endian Endianness>
struct FixedWidthTraits {
    using PackingWord                        = PackingWord_;
    static constexpr uint32_t bits_per_datum = Nbits;
    static constexpr uint32_t bits_per_word =
        sizeof(PackingWord) * BITS_PER_BYTE;
    static constexpr uint32_t ndatum_min =
        lcm(bits_per_datum, bits_per_word) / bits_per_datum;

    static constexpr std::size_t packing_size(std::size_t n)
    {
        return n * bits_per_datum / bits_per_word + (n % ndatum_min != 0);
    }
};

template <typename ContainerType,
          uint8_t Nbits,
          std::endian Endianness,
          uint8_t Ndim_ = 1>
class FixedWidth
{
  public:
    using value_type = typename ContainerType::value_type;
    using Traits     = FixedWidthTraits<Nbits, value_type, Endianness>;
    static constexpr uint8_t Ndim = Ndim_;

    FixedWidth()
        requires Resizeable<ContainerType>
    {
    }

    FixedWidth(FixedWidth const& other)
        requires(!Resizeable<ContainerType>)
    {
        _container = other._container;
        _nelements = other._nelements;
    };

    FixedWidth& operator=(FixedWidth const& other)
        requires(!Resizeable<ContainerType>)
    {
        _container = other._container;
        _nelements = other._nelements;
    };

    FixedWidth(ContainerType const& container, std::size_t n)
        requires(!Resizeable<ContainerType>)
    {
        if(container.size() < Traits::packing_size(n * Ndim)) {
            throw std::runtime_error(
                "Container is too small for specified extents");
        }
        _nelements = n;
    }

    void resize(std::size_t n)
        requires Resizeable<ContainerType>
    {
        _container.resize(Traits::packing_size(n * Ndim));
        _nelements = n;
    }

    // ! WARNING
    // This is a virtual size not the true size
    // of the underlying container. If somebody
    // naïvely uses size() * sizeof(value_type)
    // to get the number of bytes in this thing
    // they are going to have a bad time.
    // ! Plan to change this to return _container.size() but
    // ! need to check the consequences.
    std::size_t size() const noexcept { return _nelements; }

    ContainerType& container() noexcept { return _container; }

    ContainerType const& container() const noexcept { return _container; }

    auto data() noexcept { return _container.data(); }

    auto data() const noexcept { return _container.data(); }

  private:
    ContainerType _container;
    std::size_t _nelements = 0;
};

} // namespace psrdada_cpp
