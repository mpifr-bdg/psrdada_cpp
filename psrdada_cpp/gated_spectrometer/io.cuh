#pragma once

#include <iomanip>
#include <psrdada/dada_hdu.h>

#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/double_host_buffer.cuh"
#include "psrdada_cpp/double_device_buffer.cuh"
#include "psrdada_cpp/mk_buffer_view.hpp"
#include "psrdada_cpp/gated_spectrometer/types.hpp"

namespace psrdada_cpp {
namespace gated_spectrometer{

/**
 @class PolarizationData
 @brief Device data arrays for raw voltage input and intermediate processing data for one polarization
 */
struct PolarizationData
{
    size_t _nbits;
    /**
    * @brief      Constructor
    *
    * @param      nbits Bit-depth of the input data.
    */
    PolarizationData(size_t nbits): _nbits(nbits) {};
    /// Raw ADC Voltage
    DoubleDeviceBuffer<RawVoltageType> _raw_voltage;
    /// Side channel data
    DoubleDeviceBuffer<uint64_t> _sideChannelData;
    DoubleHostBuffer<uint64_t> _sideChannelData_h;

    /// Baseline in gate 0 state
    thrust::device_vector<UnpackedVoltageType> _baseLineG0;
    /// Baseline in gate 1 state
    thrust::device_vector<UnpackedVoltageType> _baseLineG1;

    /// Baseline in gate 0 state after update
    thrust::device_vector<UnpackedVoltageType> _baseLineG0_update;
    /// Baseline in gate 1 state after update
    thrust::device_vector<UnpackedVoltageType> _baseLineG1_update;

    /// Channelized voltage in gate 0 state
    thrust::device_vector<ChannelisedVoltageType> _channelised_voltage_G0;
    /// Channelized voltage in gate 1 state
    thrust::device_vector<ChannelisedVoltageType> _channelised_voltage_G1;

    /// Swaps input buffers
    void swap();

    ///resize the internal buffers
    void resize(size_t rawVolttageBufferBytes, size_t nsidechannelitems, size_t channelized_samples);
};


/**
 @class SinglePolarizationInput
 @brief Input data for a buffer containing one polarization
 */
class SinglePolarizationInput : public PolarizationData
{
    MKDadaBufferView _dadaBufferLayout;
    size_t _fft_length;

public:

    /**
    * @brief      Constructor
    *
    * @param      fft_length length of the fft.
    * @param      nbits bit-depth of the input data.
    * @param      dadaBufferLayout layout of the input dada buffer
    */
    SinglePolarizationInput(size_t fft_length, size_t nbits,
            const MKDadaBufferView &dadaBufferLayout);

    /// Copy data from input block to input dubble buffer
    void getFromBlock(RawBytes &block, cudaStream_t &_h2d_stream);

    /// Number of samples per input polarization
    size_t getSamplesPerInputPolarization();
};


/**
 @class SinglePolarizationInput
 @brief Input data for a buffer containing two polarizations
 */
class DualPolarizationInput
{
    MKDadaBufferView _dadaBufferLayout;
    size_t _fft_length;

    public:
    PolarizationData polarization0, polarization1;

    /**
    * @brief      Constructor
    *
    * @param      fft_length length of the fft.
    * @param      nbits bit-depth of the input data.
    * @param      dadaBufferLayout layout of the input dada buffer
    */
    DualPolarizationInput(size_t fft_length, size_t nbits, const MKDadaBufferView
            &dadaBufferLayout);

    /// Swaps input buffers for both polarizations
    void swap();

    /// Copy data from input block to input dubble buffer
    void getFromBlock(RawBytes &block, cudaStream_t &_h2d_stream);

    /// Number of samples per input polarization
    size_t getSamplesPerInputPolarization();
};




/**
 @class PowerSpectrumOutput
 @brief Output data for one gate, single power spectrum
 */
struct PowerSpectrumOutput
{
    /**
    * @brief      Constructor
    *
    * @param      size size of the output, i.e. number of channels.
    * @param      blocks number of blocks in the output.
    */
    PowerSpectrumOutput(size_t size, size_t blocks);

    /// spectrum data
    DoubleDeviceBuffer<IntegratedPowerType> data;

    /// Number of samples integrated in this output block
    DoubleDeviceBuffer<uint64_cu> _noOfBitSets;

    /// Number of samples overflowed
    DoubleHostBuffer<uint64_t> _noOfOverflowed;


    /// Swap output buffers and reset the buffer in given stream for new integration
    void swap(cudaStream_t &_proc_stream);
};


/**
 @class OutputDataStream
 @brief Interface for the processed output data stream
 */
struct OutputDataStream
{
    size_t _nchans;
    size_t _blocks;

    /**
    * @brief      Constructor
    *
    * @param      nchans number of channels.
    * @param      blocks number of blocks in the output.
    */
    OutputDataStream(size_t nchans, size_t blocks) : _nchans(nchans), _blocks(blocks)
    {
    }

    virtual ~OutputDataStream() {}

    /// Swap output buffers
    virtual void swap(cudaStream_t &_proc_stream) = 0;

    // output buffer on the host
    DoublePinnedHostBuffer<char> _host_power;

    // copy data from internal buffers of the implementation to the host output
    // buffer
    virtual void data2Host(cudaStream_t &_d2h_stream) = 0;
};


/**
 @class GatedPowerSpectrumOutput
 @brief Output Stream for power spectrum output
 */
struct GatedPowerSpectrumOutput : public OutputDataStream
{
    GatedPowerSpectrumOutput(size_t nchans, size_t blocks);

    /// Power spectrum for off and on gate
    PowerSpectrumOutput G0, G1;

    void swap(cudaStream_t &_proc_stream);

    void data2Host(cudaStream_t &_d2h_stream);
};


/**
 @class FullStokesOutput
 @brief Output data for one gate full stokes
 */
struct FullStokesOutput
{
    /**
    * @brief      Constructor
    *
    * @param      size size of the output, i.e. number of channels.
    * @param      blocks number of blocks in the output.
    */
    FullStokesOutput(size_t size, size_t blocks);

    /// Buffer for Stokes Parameters
    DoubleDeviceBuffer<IntegratedPowerType> I, Q, U, V;

    /// Number of samples integrated in this output block
    DoubleDeviceBuffer<uint64_cu> _noOfBitSets;

    /// Number of samples overflowed
    DoubleHostBuffer<uint64_t> _noOfOverflowed;

    /// Swap output buffers
    void swap(cudaStream_t &_proc_stream);
};


/**
 @class GatedFullStokesOutput
 @brief Output Stream for power spectrum output
 */
struct GatedFullStokesOutput: public OutputDataStream
{
    /// stokes output for on/off gate
    FullStokesOutput G0, G1;

    GatedFullStokesOutput(size_t nchans, size_t blocks);

    /// Swap output buffers
    void swap(cudaStream_t &_proc_stream);

    void data2Host(cudaStream_t &_d2h_stream);
};

} // edd
} // gated_spectrometer
