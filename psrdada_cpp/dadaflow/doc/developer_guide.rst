=========================
Dadaflow Developer Guide
=========================

Introduction
=============

Dadaflow is an extension to the psrdada_cpp library with the following aims:

- Enable developers to write complex processing pipelines faster
- Provide strong types for handling common data layouts and their metadata
- Provide tools for representing processing piplines as asynchronous directed
  acycling graphs (DAGs)

This page contains information targeted at developers who wish to develop
applications based on dadaflow or who wish to extend the library itself.

.. contents::
   :local:

Data Types
===========

Dadaflow provides a generic base type for handling all multidimensional data
arrays: `DescribedData`. 

.. code-block:: cpp

    template <typename ContainerType, typename... Dimensions>
    class DescribedData;

As can be seen from the signature above, the `DescribedData` class is a template
taking a `ContainerType` and a variadic list of `Dimensions`. 

Containers
----------

The container type can be any type that behaves like a standard template library
(STL) container with the caveat that certain behaviours such as iterability,
resizabiltiy and indexibility are optional. Depending on the traits of the
`ContainerType` member functions of the `DescribedData` class will be enabled or
disabled. Typically we use STL or thrust vectors for the container but
developers are free to create new container types that satisfy the required
interface. Type traits are implemented for the following container types:

- `std::vector`
- `std::span`
- `cuda::std::span`
- `thrust::host_vector`
- `thrust::device_vector`

The `::span` variants noted above are a C++20 non-owning memory view to a raw
pointer. In dadaflow they are typically used to provide safe access to the raw
pointers returned by the `psrdada` library.

.. note::

   Even though `DescribedData` represents a multidimensional array the
   underlying `ContainerType` contains a flat (i.e. one dimensional and
   contiguous) data array. 

Dimensions
----------

The `Dimensions` template argument specifies the type of the dimensions of the
underlying data. Dimensions provide logical named access to the size of a given
dimension of the data and to any specific metadata required to describe the
properties of that dimension. The simplest example is a pure time series of some
arbitrary physical type (here a `float` type that may represent intensity).

.. code-block:: cpp

    using Timeseries = DescribedData<std::vector<float>, TimeDimension>;
    Timeseries t;

Here we specify a `DescribedData` instance wrapping a container of type
`std::vector<float>` with only one dimension: `TimeDimension`. The
`TimeDimension` class provides the following interface to the `Timeseries` type:

.. code-block:: cpp
    
    const char* short_name() const; // Gives a short form name for the dimension, e.g. "T"
    const char* long_name() const;  // Gives a long form name for the dimension, e.g. "Time"
    std::size_t nsamples() const;   // Alias to the size/extent of the dimension
    void timestep(TimestepType const& dt); // Set the time interval between samples in this dimension
    TimestepType const& timestep();        // Get the time interval between samples in this dimension
    std::string describe() const;   // Provide a human readable description of this dimension
    void from(BaseTimeDimension const& other); // Update optional metadata to match another instance of this dimension

As such can query the `Timeseries t` like:

.. code-block:: cpp
    
    auto nsamples = t.nsamples();
    auto timestep = t.timestep();
    // etc...

The underlying data in the container of our timeseries may also be accessed via
indexing or iterators:

.. code-block:: cpp
    
    auto value0 = t[0];
    auto begin = t.begin();

Note that in this case the call to `operator[]` on timeseries takes the linear
index to the data in the underlying container and is not aware of the data
dimensions. A helper method exists to index multidimensional data types:

.. code-block:: cpp
    auto value0 = t[t.index({0})];
    auto begin = t.begin();

In this case as the data is only one dimensional the call to `t.index({0})` will
simply return `0`. In the case of multidimensional types we can call
`t.index({a, b, c, d, ...})` to create a linear index from a multidimensional
one.

Using Multiple Dimensions
-------------------------

As seen above, the `Dimensions` template argument to `DescribedData` is a
variadic list. Under the hood, `DescribedData` will publically (but, critically,
not virtually) inherit from all passed `Dimensions`. This is an example of a
variadic interface in C++ where the template arguments determine the interface
of the final concrete type. An inheritance diagram is shown below for an
arbitrary set of dimensions. Note that even though `Dimension0`, `Dimension1`
and `DimensionN` all inherit from some `BaseDimension` the base dimension is
itself instantiated multiple times on construction (this would not have been the
case with virtual inheritance). As we will see later this is a critical for
allowing the specifcation of repeated dimensions.

.. code-block:: cpp
    
    template <typename ContainerType_, typename... Dimensions>
    class DescribedData: public Dimensions...

::

           BaseDimension   BaseDimension    BaseDimension
                    |            |            |
                Dimension0   Dimension1   DimensionN
                     \           |           /
                      +----------+----------+
                                 |
    DescribedData<ContainerType, Dimension0, Dimension1, DimensionN>

An example of a multidimensional DescribedData instance would be a type for
representing time-frequency intensity data. Here we will also place the
contained data onto GPU memory using a `thrust::device_vector`

.. code-block:: cpp
    
    using TimeFreqData = DescribedData<thrust::device_vector<int8_t>, TimeDimension, FrequencyDimension>;
    TimeFreqData tf;
    tf.resize({100, 16}); // Resize to 100 spectra composed of 16 channels each

We have now created a two dimensional time frequency type. The ordering
dimensions in the template spefication determines the ordering of the data in
memory. From left to right we go from outermost (slowest) to innermost (fastest)
dimension (in row-major order). Note that both `TimeDimension` and
`FrequencyDimension` have similar interfaces and in some cases methods on these
clases may shadow each other. An example here is the `short_name()` method
implemented in both dimension clases. As a reminder, C++ allows for (and
requires) the disambiguation of such methods. This means that calling
`tf.short_name()` will result in a compile time error. To correctly call the
corresponding method from the desired baseclass we can do:

.. code-block:: cpp
    
    std::cout << tf.TimeDimension::short_name(); // Outputs "T"
    std::cout << tf.FrequencyDimension::short_name(); // Outputs "F"
    // For non-shadowed methods, disambiguation is not required, e.g.
    std::cout << tf.nsamples(); // Outputs "100";
    std::cout << tf.nchannels(); // Outputs "16";

Using Repeated Dimensions
-------------------------

In the case of multidimensional data with repeated dimensions it is necessary to
use derivative type aliases to disambiguate base classes. The macro
`CREATE_REPEATABLE_DIMENSION` can be used to easily generate repeatable
dimensions from a given dimension type. This is already done for us in the
`Dimensions.hpp` file for both `TimeDimension` and `FrequencyDimension`,
providing the template types `template<int> TimeDimensionN;` and `template<int>
FrequencyDimensionN;`. Below we use these for a more complex 5 dimensional type
that is storing complex voltages in a `char2` vector.

.. code-block:: cpp
    
    using TAFTPData = DescribedData<
        thrust::device_vector<char2>,
        TimeDimensionN<0>,
        AntennaDimension,
        FrequencyDimension,
        TimeDimensionN<1>,
        PolarisationDimension
    >;
    TAFTPData taftp;

Here `TimeDimensionN<0>` and `TimeDimensionN<1>` are used to differentiate
between the two time dimensions. As these are both subclasses of `TimeDimension`
they have identical interfaces, hence it is required to disambiguate when making
calls to this object.

.. code-block:: cpp
    
   taftp.resize({10, 16, 16, 1024, 2});
   std::cout << taftp.TimeDimensionN<0>::nsamples(); // Outputs "10"
   std::cout << taftp.TimeDimensionN<1>::nsamples(); // Outputs "1024"

Currently the following dimensions are provided in the library:

- `TimeDimension`
- `FrequencyDimension` (both a non-contiguous and a contiguous frequency
  version)
- `AntennaDimension`
- `BeamDimension` 
- `PolarisationDimension`
- `DispersionDimension`
- `GateDimension` 

Custom Dimensions
-----------------

Custom Dimension types are straightforward to add and can be defined in
downstream application code. An example can be found in the tests for
`DescribedData` where the following is used:

.. code-block:: cpp

    class CustomDimension: protected BaseDimension
    {
    public:
        using BaseDimension::BaseDimension;
    
        const char* short_name() const override { return "C"; }
        const char* long_name() const override { return "Custom"; }
    
        std::size_t ncustom() const {return extent();}
        float custom() const {return _custom.get();}
        void custom(float const& val) {return _custom.set(val);}
    
        std::string describe() const override{
            std::stringstream stream;
            stream << long_name() << "\n";
            stream << "  Ncustom: " << ncustom() << "\n";
            stream << std::setprecision(15);
            stream << "  Custom: " << _custom << "\n";
            return stream.str();
        }
    
        void from(CustomDimension const& other) { _custom = other._custom; }
    
    private:
        MetaDataWrapper<float> _custom;
    };

The critical parts here are:

- The custom dimension must inherit from `BaseDimension`.
- It must provide, `short_name()`, `long_name()`, and `from()` member functions
- It can optionally provide an overridden `describe()` member function.
- It can optionally provide a named alias to access its `extent()` (in this case
  `ncustom()`).
- If specific behavior (e.g. metadata invalidation) is required on resize then
  the `resize_dimension()` member function can be overridden.
- It can optionally contain relevant metadata setter/getter member functions
  (see e.g. `custom()`).
- Dimension metadata should be wrapped in a MetaDataWrapper instance (see
  below).

.. note::

    There is a difference between general metadata and the type of descriptive
    data that should be included in a dimension. The metadata on a dimension
    should be only that which is required to tell a user of the data what that
    dimension contains. For general purpose metadata like the name of the
    telescope or the source being observed (unless there are multiple sources in
    the DescribedData in which case that could become a SourceDimension or
    BeamDimension).

Metadata
--------

There are two means of carrying metadata with a DescribedData instance, on the
`Dimensions` themselves or in the `_metadata` member of the `DescribedData`
class. The `_metadata` member is a shared pointer to a map of arbitrary values.
It can be used as follows:

.. code-block:: cpp

    auto const& metdata = taftp.metadata(); 
    std::cout << std::any_cast<std::string>(metadata.at("telescope"));
    std::cout << std::any_cast<std::string>(metadata.at("source_name"));

Note that the contents of the metadata are stored as a `std::any` instance which
can have arbitrary type. To return this to the original type the user must use
`std::any_cast`. It is therefore strongly recommended to be explicit about the types
being passed to the metadata map.

The metadata on the DescribedData instances has a read-only accessor. This is due to
the underlying type not being threadsafe and so updates to the shared pointer are
dangerous. To add metadata to a given instance, it is necessary to copy the existing
metadata and create a new shared_ptr wrapper, as so:

.. code-block:: cpp

    // First make the new pointer and copy the metadata from the old
    auto new_metadata_ptr = std::shared_ptr<std::remove_const<decltype(taftp)::MetadataType>>(taftp.metadata());
    auto& metadata = *new_metadata_ptr;
    // Then add some new parameters
    metadata["SomeNewParameterKey"] = std::string{"SomeNewParameterValue"};
    // Usually you will not update the metadata on the current instance but on some
    // output instance from the current process
    output.metadata_ptr(new_metadata_ptr);


.. code-block:: cpp

    metadata["number"] = 1.0;
    // std::any_cast<float>(metadata.at("number")) <-- will fail!
    std::any_cast<double>(metadata.at("number")) // Ok

As noted above, descriptive metadata data stored on the dimensions base classes
of `DescribedData` should be wrapped in a `MetaDataWrapper` object. The
`MetaDataWrapper` class provides runtime safety for dimension metadata. As
setting the metadata on a given dimension is not always required by a given
application, the specification of such metadata is optional. As we cannot assume
resonable defaults for metadata and we do not want to have applications
accessing uninitialised values at runtime, we hide such metadata in a
`MetaDataWrapper` that tracks whether a value has been initilised or is
considered stale.

.. code-block:: cpp

    MetaDataWrapper<float> value;
    // value.get() <-- will raise a runtime error as the value was not initilised
    value.is_stale(); // = true
    value.set(10.0f);
    value.is_stale(); // = false
    value.get(); // = 10.0f
    value.mark_stale(); 
    value.is_stale(); // = true
    // value.get() <-- will raise a runtime error as the value is now stale

To ease debugging, passing a MetaDataWrapper object to a `std::ostream` will
print `(stale)` rather than raising an error.

As dimension metadata will often be in the form of a vector of values with one
entry for each element in that dimension, the `MetaDataWrapper` class provides a
safe setter for vector types of the form:

.. code-block:: cpp

    template <typename T = MetadataType> typename std::enable_if<
        std::is_same<T, std::vector<typename T::value_type>>::value>::type
    set(MetadataType const& metadata, std::size_t expected_size){...}

It is on the developer to write code to ensure that the length of any metadata
vector matches the extent of the dimension it is associated with. Several
examples of such code are available in the `Dimensions.hpp` header file.

The one exception to the above discussion of metadata is a that the `DescribedData` 
class itself provides a `timestamp()` interface for setting and getting a  
timestamp associated with given data.

Metadata Units
--------------

Throughout dadaflow we use strongly typed units. Namely we use `boost::units` for physical 
quantities like frequency and dispersion measure. For temporal units we use the 
`std::chrono` library. A helper class for handling timestamps is provided: `Timestamp`. 
Internally this class stores the time as an whole second epoch and an offset from that 
epoch in picoseconds. It supports some limited arithmetic functions but care should be taken
by the developer to ensure that rounding errors do not cause issues.

.. code-block:: cpp

    auto t0 = Timestamp{11232344242.1201321} // Construction from a unix timestamp

    auto now   = std::chrono::system_clock::now();
    auto epoch = std::chrono::time_point_cast<std::chrono::seconds>(now);
    std::chrono::duration<long double, pico> offset(0.0);
    auto t1 = Timestamp(epoch, offset); // Construction from std::chrono types

    // Comparison of timestamps is valid to a 1 picosecond tolerance.
    t0 == t1

    // Advance the timestamp by 4 seconds
    t1 += std::chrono::seconds(4);

Copying and Liking
------------------

Two methods are provided on `DescribedData` to allow for dimension and metadata
cloning: `like()` and `metalike()`. The `like()` function will resize a
`DescribedData` instance to match the extents of another `DescribedData`
instance. Additionally it will clone the other instance's metadata, timestamp
and dimensional metadata. The `metalike()` function will only clone the
metadata, timestamp and dimensional metadata but has the advantage that it can
clone from objects that do not share its exact dimensions.

.. code-block:: cpp

    TAFTPData original;
    original.resize({1,1,1,1,1});

    TAFTPData clone;
    clone.extents(); // {0,0,0,0,0}
    clone.like(original);
    clone.extents(); // {1,1,1,1,1}

As an example of metalike, we can see that the call is compatible with any type that is a valid transpose of itself.

.. code-block:: cpp

    using T = TimeDimension;
    using F = FrequencyDimension;
    using TFData = DescribedData<std::vector<float>, T, F>;
    using FTData = DescribedData<std::vector<int>, F, T>;

    TFData original;
    // ... populate metadata ...
    FTData metaclone;
    metaclone.metalike(original); // valid call

Fixed Width and Packed Data
---------------------------

Dadaflow provides a wrapper container for fixed width or binary packed data
called `FixedWidth`. Below is an example useage of `FixedWidth` to wrap a vector
of backed 10-bit data. Here the packing word type for the data is a uint64_t and
the byte order is big endian. 

.. code-block:: cpp

    using FixedVecT = FixedWidth<std::vector<uint64_t>, 10, std::endian::big>;
    DescribedData<FixedVecT, T, F> fixed_vec;
    fixed_vec.resize({4, 13});

The `resize()` member function here takes the logical size of the dimensions
(i.e. the number of 10-bit values). The bitwidth of the data and the packing
word type will determine the actual size of the underlying container on resize. 

CUDA Vectors and Stokes Parameters
----------------------------------

As class for wrapping CUDA vector types as stokes vector types also exists. 

.. code-block:: cpp

    #include "psrdada_cpp/dadaflow/StokesVector.hpp"
    //...
    using StokesType = StokesVector<float, I, Q, U, V>;
    using TStokesIQUVH = DescribedData<std::vector<StokesType>, TimeDimension>;
    TStokesIQUVH data;
    data.resize({1000});

Under the hood this works because in the `vector_traits.cuh` header file we
implement a set of rich type traits for all CUDA vector types as well as
enabling basic element-wise arithmetic operations for those types. This means
that the following snippets are all valid code.

.. code-block:: cpp

    #include "psrdada_cpp/dadaflow/StokesVector.hpp"
    #include "psrdada_cpp/dadaflow/vector_traits.cuh"
    //...
    using StokesType = StokesVector<float, I, Q, U, V>;
    StokesType a{1.0f, 2.0f, 3.0f, 4.0f};
    StokesType b{2.0f, 3.0f, 4.0f, 5.0f};
    a * b;
    b / a;
    b - a;
    a * 3.0f;
    b / 4;

    float2 c{1.0f, 2.0f};
    char2 d{1, 2};
    c -= d;
    c % d
    // etc. etc.


Processing Graphs
==================

Dadaflow provides the means to build asynchronous DAGs for data processing.
While the type saftey of these graphs is guaranteed as compile time, the
validity of the graph can only be checked at runtime. As a reminder, a DAG
is a graph with the following characteristics:

- Directed edges: Each connection (edge) between two nodes has a direction,
  indicating the flow from one node to another.
- Acyclic: It contains no cycles, meaning it is impossible to start at one node
  and return to it by following the directed edges.
- Topological order: Nodes are arranged in a sequence such that every edge
  points from an earlier node to a later node.

In dadaflow we use a slightly different terminology. We refer to a node as a
process and an edge as a queue. All processes are wrapped in an executor
that allows them to be embedded in a graph. In practice this results in  
graphs that look like:

::

    Executor<Process0>                                Executor<Process3>
                      \                              /
                       \                            /
                        + -- Executor<Process2> -- + 
                       /                            \
                      /                              \ 
    Executor<Process1>                                Executor<Process4>

In this examples we have 5 processes. Processes 0 and 1 are known as sources
processes. The have no inputs interal to the graph producing only output data.
An example of such a process might be a one-time file reader. Processes 3 and 4
are known as sink processes. The have inputs but no outputs internal to the
graph. An example might be a file writer or a terminal display. Process 2 takes
two inputs and also produces two outputs. All edges in the graph are instances
of some thread-safe queue type or other thread-safe exchange mechanism like a 
cache.

Key Concepts
------------

Graph processing in dadaflow relies on some key practices and concepts in order
to maintain performance and avoid possible runtime bugs.

- Shared pointers: Objects passed between processes in the graph should be
  wrapped in shared pointers. These pointers must be passed between processes
  and queues by value to assure copy/move semantics.
- Memory management: All processes are responsible for allocating the memory
  needed to store their outputs. In practice this is done with resource pools
  (see below) that can be passed to the processes on construction.
- Concurrency: All processes in the graph are asynchronous and run in their own
  dedicated thread.
- Back pressure: To avoid excess memory allocation we use back pressure to
  impose limits on what can be produced by upstream components in the graph.
  This is done through two methods: 1.) restricted resource pools which prevent
  a processes from over allocating output buffers and 2.) finite queue depths
  which cause upstream processes to block if downstream processes cannot clear
  their queues.
- Metadata completeness: All processes are responsible for ensuring that any
  metadata they receive on their inputs is, where appropriate, forwarded to
  their outputs with any changes based on the algorithms run in the process.
- Single consumer, single producer: All queues in the graph are pushed to by
  only one process and read from by only one process. Merging or splitting
  queues is performed via aggregating or mutliplexing.

Processes (Basics)
------------------

Processes are the nodes in the dadaflow graphs. They must obey certain rules:

- They must be callable.
- If they have multiple return values they must return them in a tuple.
- Return types can be wrapped in `std::optional` for processes not guaranteed
  to produce an output on every call.
- Arguments can be wrapped in `std::optional` to allow them to work with missing
  input queues.
- Processes may return Generator objects to allow for mutliple return values per
  input call.
- Processes must implement safe move semantics and ideally follow Resource
  Acquisition Is Initialisation (RAII) concepts.

The simplest process is a lambda function:

.. code-block:: cpp

    auto noop = [](T){};

Above is a no-op sink process that receives an argument of type T by
value and does nothing. This is the equivalent of writing to `/dev/null`.

An example of processes with more responsibilitites would be a source process
that generates vectors.

.. code-block:: cpp

    using T = std::vector<int>;
    auto source = []() -> std::shared_ptr<T> {
        auto data_ptr = std::make_shared<T>();
        data_ptr->resize(10);
        return data_ptr;
    };

Here we specify a lambda with no arguments that will return a shared pointer to
a vector of ints (`std::shared_ptr<T>`). Note that the trailing
return-type specification is critical for such lambda processes to avoid
mistakes in type deduction.

A more complex example would be a process that takes arguments and modifies them.

.. code-block:: cpp

    auto add = [](std::shared_ptr<T> input_ptr) -> std::shared_ptr<T> {
        auto& data = input_ptr.get();
        for (auto& val: data) {
            val += 1;
        }
        return input_ptr;
    };

In this case the process takes an input modifies it an then pases it to its
output. In general such in-plance modification is not recommended as it is not
thread safe (although if this is the only process using this shared pointer
there is no risk). Instead it is recommended to make a new output when modifying 
data, such that:

.. code-block:: cpp

    auto add_new = [](std::shared_ptr<T> input_ptr) -> std::shared_ptr<T> {
        auto const& data = input_ptr.get();
        auto output_ptr = std::make_shared<T>(data.size());
        auto& odata = output_ptr.get();
        std::copy(data.begin(), data.end(), odata.begin());
        for (auto& val: odata) {
            val += 1;
        }
        return output_ptr;
    };

The use of lambdas as nodes is particularly useful for writing simple graph
manipulation functions. As noted above, queues are single producer, single
consumer as such to route the output of a queue to two downstream nodes it is
necessary to multiplex. This can be trivally done with a lambda:

.. code-block:: cpp

    auto multiplex = [](std::shared_ptr<T> input_ptr) -> std::tuple<std::shared_ptr<T>, std::shared_ptr<T>> {
        return {input_ptr, input_ptr};
    };

    // Alternatively a helper class exists for generating 
    // multiplexers of arbitrary size and type. In this case
    // the behaviour is identical to the above lambda.
    Multiplexer<2, std::vector<int>> multiplex;

Here, the incoming shared pointer will be copied and returned as two-tuple
which, as we will see later, will be routed to two output queues.

In many realistic use cases it will be necessary for a process to have state. Hence they 
are often implemented as classes with overloaded `operator()`s: 

.. code-block:: cpp
    
    template <typename InputType1, typename InputType2, typename OutputType>
    class ScaledAdder {
    public:
      using OutputPtr = std::shared_ptr<OutputType>;
      using InputPtr1 = std::shared_ptr<InputType1>;
      using InputPtr2 = std::shared_ptr<InputType2>;
    
      OutputPtr operator()(InputPtr1 a, InputPtr2 b) {
        auto const &vec1 = a.get();
        auto const &vec2 = b.get();
        if (vec1.size() != vec2.size()) {
          throw std::runtime_error("Vector lengths do not match");
        }
        auto output_ptr = std::make_shared<OutputType>(vec1.size());
        auto& odata = output_ptr.get();
        for (auto ii = 0; ii < vec1.size(); ++ii) {
            odata[ii] = vec1[ii] + vec2[ii] * _scale;
        }
        _scale += 1.0f;
        return output_ptr;
      }

    private:
      float _scale = 1.0f;
    };

In this somewhat nonsensical example we have a ScaledAdder process which takes
two vectors of equal length and adds one to the other with some scaling factor.
The scaling factor increments for every call to the process.

Resource Pools
--------------

A critical component of dadaflow is that processes must be responsible for
memory allocation of their outputs. This can introduce a significant performance
penalty as processes may allocate on every call. A developer could use caching
allocators to mitigate this penalty but as this is a common operation, dadaflow
provides a thread-safe helper class, `ResourcePool`, for exactly this purpose.

The `ResourcePool` is a a wrapper around a pool of a finite number of instances 
of a user-provided type. For example the code below is a resource pool of vectors
of integers:

.. code-block:: cpp

    #include <psrdada_cpp/dadaflow/ResourcePool.hpp>
    //...
    // Create a pool of 3 vectors. Each will be initialised
    // to 10 elements all set to the value 1.
    ResourcePool<std::vector<int>> vector_pool(3, 10, 1);
    
Under the hood `ResourcePool` us using perfect forwarding to allow inplace construction
of 3 instances of `std::vector<int>`. Note that the resource pool does not limit 
the resources used by these objects only the number of objects that can be created.

Resource pools return their instances as shared pointers as can be seen below:

.. code-block:: cpp

    std::shared_ptr<std::vector<int>> ptr = vector_pool.get();
    // or more commonly
    auto ptr = vector_pool.get();

If a resource pool is empty, calls to `.get()` will block until a pool resource
becomes free. Pool resources are free'd when they are destructed. The logic to
return the contents of shared pointer back to the resource pool is hidden inside
a custom destructor on the shared pointer returned by `.get()`. As such the
resource pool is "fire and forget" with the resource being protected until all
copies have gone out of scope or been manually deleted.

A common pattern when using `ResourcePool` is to pass an instance to a process on 
construction or to a lambda by reference:  

.. code-block:: cpp

    using ContainerType = std::vector<float>;
    using Timeseries = DescribedData<ContainerType, TimeDimension>;
    ResourcePool<Timeseries> pool(4);

    // If memory pre-allocation is required for performance then 
    // resizing on construction can be used:
    // ResourcePool<Timeseries> pool(4, {n});

    using TimeseriesPtr = std::shared_ptr<Timeseries>;

    auto node = [&](TimeseriesPtr const input_timeseries_ptr) -> TimeseriesPtr {
        TimeseriesPtr output_timeseries_ptr = pool.get();
        // Do some processing...
        return output_timeseries_ptr;
    };


Queues
------

Processes in dadaflow are connected by queues. Queues are primarily hidden from the 
developer being instantiated by calls to `Graph::connect`. Users may specify the type
of queue to use on a connect call, but generally it is preferable to use the existing 
`ThreadSafeQueue` which can be found in `psrdada_cpp/dadaflow/ThreadSafeQueue.hpp`. 
Queue lifetime is managed by shared_ptrs and is hidden from the developer.

While queues can carry any object type, they cannot and should not carry
references and as such must use either copies or moves. This is another 
argument in favour of using shared pointers as a wrapper around types in 
dadaflow graphs.

Graphs
------

With data types, processes, resource pools and queues now covered we have enough
information to build a complete graph. Graphs can be instantiatied with the `Graph`
class.

.. code-block:: cpp

    #include <psrdada_cpp/dadaflow/Graph.hpp>
    //...
    Graph graph;

The graph itself can be built up by adding nodes to the instance. The
`add_node(...)` method can be used to add any node. As a simple example we can 
build type conversion graph that produces some data
copies it to a resouce allocated from a pool and passes that data to a null sink.

.. code-block:: cpp

    using InputT = std::vector<int>;
    using InputTPtr = std::shared_ptr<InputT>;
    using OutputT = std::vector<float>;
    using OutputTPtr = std::shared_ptr<OutputT>;

    Graph graph;
    ResourcePool<InputT> source_pool(3);
    ResourcePool<OutputT> converter_pool(3);

    // Add a data source called "source node"
    auto& source_node = graph.add_node(
        []() -> InputTPtr {
            auto ptr = source_pool.get();
            ptr->resize(10, 4);
            return ptr;
        },
        "source node"
    );

    // Add a node to convert from int to float
    auto& converter_node = graph.add_node(
        [](InputTPtr input) -> OutputTPtr {
            auto output = converter_pool.get();
            output->resize(input->size());
            std::copy(input->begin(), input->end(), output->begin());
            return output;
        },
        "converter node"
    );

    // Add a node to convert from int to float
    auto& sink_node = graph.add_node(
        [](OutputTPtr input) -> void {
            std::cout << input.get() << "\n";
        },
        "printer node"
    );

    // Here we connect nodes with queues, specifying the name 
    // and the max size of the queue between the nodes.
    // The template arguments <N,N> specify that we are connecting the 
    // Nth output of the first node to the Nth input of the second 
    // node.
    graph.connect<0,0>(source_node, converter_node, "src->converter", 3);
    graph.connect<0,0>(converter_node, sink_node, "converter->sink", 3);

    // Validate the graph and prepare for execution 
    graph.prepare();

At this stage the graph is built and checks have been made for doubly consumed
queues, hanging queues and loops in the graph. Assuming all checks pass, the graph 
is now ready for execution. 

The example above provides a trivial use of the Graph class. From the developers 
perspective, much of the complexity of the type management is hidding. Below is a 
brief explanation of what is going on under the hood in the graph.

To build the graph, calls are made to add nodes, e.g.:

.. code-block:: cpp

    auto& source_node = graph.add_node({some callable}, {some name});

The first argument to `add_node` is any callable object, e.g. function, member function,
functor or lambda. At compile time the arguments to this callable and its return types 
are deduced and a tuple of typed queues in generated in the executor for the callable.
An unwrapper is used to convert the return type to a compatible variant (i.e. a std::tuple
of types with no Generators or std::optionals) for generating the types of the executors
output queues.

With the node added to the graph. It become possible to connect nodes via:

.. code-block:: cpp

    graph.connect<{src_output_queue_index}, {dest_input_queue_index}>({src_node}, {dest_node}, {some name}, {queue size});

Here the types of the specified queues are checked (at compile time) if they are 
compatible a queue will be constructed joining the nodes. 

The graph requires that the result of the invocation of the callable is known during
the adding of nodes. As such care must be taken when adding callables with overloads.
For example an overloaded lambda with the following signature will cause compilation
errors:

.. code-block:: cpp

    auto callable = [](auto arg) -> decltype(auto) {return arg;};
    
To use the graph with callable types it is necessary to specify how they will be invoked.
This can be done by passing an invocation trait when adding the node:

.. code-block:: cpp

    auto callable = [](auto arg) -> decltype(auto) {return arg;};
    graph.add_node<decltype(callable), invocation_traits<decltype(callable), float>>(std::move(callable), "some node");

Here we are specifying that when this callable is invoked it will resolve to a `float(float)` 
function type. A typical pattern here would be to pass the type of the expected output of an 
upstream node in the graph to the `invocation_traits`. Such an example may look like:

.. code-block:: cpp

    auto& upstream_node = _graph.add_node(...);

    Callable callable;
    auto& some_node = _graph.add_node<
        decltype(callable), // Type of the callable
        invocation_traits<
            decltype(callable), typename std::remove_reference_t<decltype(upstream_node)>::OutputValueType<0> 
        >
    > (std::move(callable), "Some callable");

Here we use `OutputValueType<0>` of the type of the upstream node to get the type of the elements
in its first output queue.

Graphs can be introspected to show their dependency chains and the topological
ordering of the nodes. Additionaly a graphviz compatible dot graph can be dumped
to visualise the graph.

.. code-block:: cpp

    graph.show_dependencies();
    graph.show_topological_order();
    graph.make_dot_graph();

Currently there are two methods of executing the graph. The first is to directly call
the graph's `run_async()` member function. This will spawn threads for all nodes in the 
graph start each node's executor before returning control to the calling thread. It is 
incumbent on the developer to then manage stopping and cleanup of the graph, which can 
be done with the `stop()` and `await()`.

.. code-block:: cpp

    graph.run_async(); // start execution
    std::this_thread::sleep_for(10s); // run for 10 seconds
    graph.stop(); // start the shutdown process for the graph
    graph.await(); // block until the graph has come to a stop

For CLI applications a runner with hooks for gracefully shutting down the graph
based on receiving signals is provided. This replaces the method shown above with 
the following:

.. code-block:: cpp

    GraphRunner::run(&graph);

After execution is complete, the statistics of the graph can be displayed with:

.. code-block:: cpp
    
    graph.show_statistics();

If exceptions are raised in the graph they are caught at the node level and the
stop function is called on all executors. At this point sentinel values are
pushed into downstream queues. A `nullptr` is used a sentinel. If an executor
detects a `nullptr` on any input queue it assumes a general graph stop has been
called and stops itself (after also pushing sentinels to its outputs). In any
exception has been detected, `await()` call will throw a general exception after 
the graph has stopped.

Processes (Advanced)
--------------------

Any callable added to the graph with the `add_node` member function will be wrapped
in an instanciation of the `ProcessExecutor` template. The `ProcessesExecutor` will
determine the traits of the callable at compile time and generate input and output
ports for each argument and return value of the callable. Like ports on a network
switch, ports here provide the means by which queues may be connected to the
`ProcessExecutor` instance (the node) at runtime. The behaviour of ports is 
controllable at runtime via two primary methods: enabling/disabling and setting 
strategies. 

Ports are enabled by default. An enabled port can consume from or push to a queue, or 
(if it is an input port) can invoke a strategy to enable some custom behaviour. Ports
can be accessed via public member functions of the `ProcessExecutor`:

.. code-block:: cpp

    using T = std::shared_ptr<int>;
    auto& some_node = _graph.add_node([](T a, T b) -> T {
        return std::make_shared<int>(*a + *b);
    }, "SomeCallable");
    // Here some_node will have:
    // - two input ports (one per argument)
    // - one output port (as there is only one return value)
    auto& iport0 = some_node.input_port<0>();
    auto& iport1 = some_node.input_port<1>();
    auto& oport0 = some_node.output_port<0>();

    // Disable a port
    iport1.disable();

    // Check if a port has a queue
    bool has_queue = oport0.has_queue();

    // Check if the port is valid and can be used
    bool is_valid = oport0.is_valid();

Input and output ports have different behaviours when disabled. Disabled input ports
cannot be used to get data. Requesting data from a disabled input port will cause a 
runtime error. The exception to this rule is for optional arguments. If the callable
has arguments of the form `std::optional<T>` then the type of the port will be 
`InputPort<std::optional<T>>`. A call to get on a disabled optional port will return 
and empty option. 

.. code-block:: cpp

    using T = std::shared_ptr<int>;
    // Let b be an optional argument
    auto& some_node = _graph.add_node([](T a, std::optional<T> b) -> T {
        if (b.has_value()){
            return std::make_shared<int>(*a + *(*b));
        } else {
            return std::make_shared<int>(*a * 2);
        }
    }, "SomeCallable");

    // ... connect both input ports to some upstream nodes ...

    some_node.input_port<0>().enable();
    // While enabled, some_node will calculate a + b

    some_node.input_port<0>().disable();
    // While disabled, some_node will calculate a * 2

Disable output ports have simpler behaviour. If the port is enabled then data will be
passed to the output queue. If the port is disabled, data will be discarded.

The callables wrapped by `ProcessExecutor` do not (should not) have access to their ports
or to the graph they are embedded in. Ideally they are simple processes that execute with 
no side effects beyond internal state changes (if they are functors). However, it may be 
necessary to implement more complex queue or port handling logic in the graph to deal with 
problems like stream synchronisation or filtering. To handle such cases a strategy can be 
set on each input port. By default the strategy is set to a blocking queue consumer. Several
simple strategies are implemented as examples. Below is the implementation of a constant 
value strategy that does not pull from a queue but instead returns a constant value every 
time data is requested from the port.

.. code-block:: cpp

    template <typename T>
    class ConstantValueStrategy : public InputStrategyInterface<T> {
    public:
        ConstantValueStrategy(T const& value): _value(value) {}

        T get(InputPort<T>& port) override {
            return _value;
        }

        bool is_valid(InputPort<T> const& port) const override {
            return true;
        };

    private:
        T _value;
    };

Input port strategies must implement both a `get` and `is_valid` method. These both take as arguments
references to the port on which the strategy acts. As such they have access to any queue that is 
attached to the port. Below is a simple non-blocking queue consumer that will return some default
value if the queue is empty.

.. code-block:: cpp

    T get(InputPort<T>& port) override {
        auto& queue = port.queue(); // Will throw if no queue present
        if (queue.empty()) {
            return /* some default value */;
        } else {
            return queue.pop();
        }
    }

If the strategy requires a queue, then the `is_valid` method should check for the presence of a queue:

.. code-block:: cpp

    bool is_valid(InputPort<T> const& port) const override {
        return port.has_queue();
    };

Strategies can be set on input ports using the `set_strategy` function:

.. code-block:: cpp

    T default_value = /* something */;
    some_node.input_port<0>().set_strategy<ConstantValueStrategy>(default_value);


Flow control
------------

The combination of optional arguments and port enabling/disabling allows for control over the flow
of data through the graph. Developers may write their own conditional nodes, but a helper node is 
provided for mutli-input multi-output selection: `InputOutputSelector`. Additionally the `Graph` class
provides a helper function for creating such a switch-like node with up to 5 inputs and a definiable 
number of outputs.

.. code-block:: cpp

    // Create a switch with 3 inputs and 5 outputs for passint data of type T
    auto& switch_ = _graph.add_switch<3, 5, T>("my switch");

Such switches or I/O selectors allow for the reception of data on one or more ports and the forwarding
of that data to any set of output ports.

.. code-block:: cpp

    // Get data from the 1st and 3nd inputs
    switch_.select_inputs<0,2>();

    // Route the data to the 1st, 3rd and 5th outputs
    switch_.select_outputs<0,2,4>();

Swtiching inputs is thread-safe as it will block access to the ports before enabling/disabling. This does not
mean it is "safe" in the sense of preserving the logic of the graph. It is on the developer to determine what 
the valid set of switch states should be for each instance in their specific application.


External control
----------------

Dadaflow provides a message queue-based means for external control of dadaflow applications. The `MessageQueue`
class takes two message queues, a sending queue and a receiving queue. In the example below, we make use of 
the `PosixMessageQueue` class which provides an RAII wrapper for POSIX message queues.

.. code-block:: cpp

    // Make a requests and responses queue
    PosixMessageQueue request_queue("/app_requests");
    PosixMessageQueue response_queue("/app_responses");

    // Start the server with request and response queues
    MessageServer server(request_queue, response_queue);

The server instance will listen for messages on the request queue and dispatch callbacks based on the content
of the message. Messages arriving in the queue should be JSON formatted

.. code-block:: json

    {
        "command": "<some command name>",
        "arguments": {
            "arg1": "some arg",
            "arg2": "some other arg"
        }
    }

The arguments required for specific commands can be queried from the server using the "help" command:

.. code-block:: json

    {
        "command": "help",
        "arguments": {}
    }

Internally the dispatching is based on callbacks that are registered in the application. 

.. code-block:: cpp

    server.register_callback(
    "say_hello",
    {"repeats"},
    "Says hello, repeatedly.",
    [&](js::object const& args) -> js::object {
        js::object response;
        int nrepeats = 0;
        for(auto ii = 0; ii < args.at("repeats").as_int64(); ++ii) {
            BOOST_LOG_TRIVIAL(info) << "HELLO!";
            ++nrepeats;
        }
        response["ntimes_repeated"] = nrepeats;
        return response;
    });

Here we are registering a command called `"say_hello"` that takes one argument called `"repeats"``. The callback
for this command simply prints the word "HELLO!" to the log n times then exits.

Detailed examples of how to register callbacks and write Python clients to these servers can be found in
`examples/message_server_example.cpp` and `examples/message_server_example.py`.


Monitoring and parameter control
--------------------------------

Dadaflow provides a second method of interprocess communication via the `SharedStruct` class. This class
provides a self-describing shared memory buffer containing a structure that is (optionally) synchronised 
for all clients. To create a SharedStruct it is necessary to create an instance of the `Descriptor` class
which is used to build self-describing structures. To enable struct fields to be self describing, they 
must be created using two special preprocessor macros: `FIELD` and `ARRAYFIELD`. Below is an example of
the creation of some fields using these macros. It is good practice to call these in a dedicated or anonymous
namespace to avoid polluting the global or project namespace.

.. code-block:: cpp

    namespace fields
    {
        // Create an array field with the name "weights"
        // of type float2 and with the description "Antenna weights"
        ARRAYFIELD(weights, float2, "Antenna weights")

        // Create a scalar field with the name "lo_frequency"
        // of type float and with the description "LO frequency (Hz)"
        FIELD(lo_frequency, float, "LO frequency (Hz)")

        // Create an array field with the name "state"
        // of type char and with the description "State"
        ARRAYFIELD(state, char, "State")
    } 

The types passed to these macros must be satisfy the constraints of being both trivial and having standard
memory layout.

With our fields defined we can now create a `Descriptor` that can be shared:

.. code-block:: cpp

    using MonitoringData = Descriptor<fields::weights<8>, 
                                      fields::lo_frequency, 
                                      fields::state<64>>;

Under this hood this creates a struct equivalent to:

.. code-block:: cpp

    struct MonitoringDataEquivalent 
    {
        std::array<float2, 8> weights;
        float lo_frequency;
        std::array<char, 64> state;
    };

However the `Descriptor` version of the struct also contains static methods for reflecting the properties
of each field. As such this can now be passed to the `SharedStruct` class:

.. code-block:: cpp

    SharedStruct<MonitoringData> buffer("shared_struct_example");

Here the string argument passed is the name of the POSIX shared memory buffer that will be used as the underlying
memory for the `MonitoringData` struct. At this stage the shared memory buffer is now ready to be used. Clients can
connect from the outside and discover the contents of the buffer. An example of how this is done is provided in 
`examples/shared_struct_example.py` with the corresponding c++ code in `examples/shared_struct_example.cpp`.

To manipulate the contents of the buffer from inside the c++ application, we can get a reference to the `MonitoringData`
struct managed by the `SharedStruct` instace:

.. code-block:: cpp

    auto& data = buffer.data();

Here the `data` variable behaves as if it were of type `MonitoringDataEquivalent`. This means it can be used as follows:

.. code-block:: cpp

    data.lo_frequency = 1324234.4324234f;
    float2 antenna0_weight = data.weights[0];
    // etc. etc.

Direct manipulation of the struct like this is non-blocking but also means that changes to the buffer may be made during
a read by a client. Thread/process-safe access to the buffer is managed via locked callbacks. 

.. code-block:: cpp

    // Perform thread-safe update of the LO frequency
    buffer.with_write_lock([&](auto& data) {
        data.lo_frequency = 0.3f;
    });

    // Perform thread-safe read of the LO frequency
    float lo_frequency;
    buffer.with_read_lock([&](auto& data) {
        lo_frequency = data.lo_frequency;
    });

The `SharedStruct` memory will be automatically cleaned up at the end of program execution.






