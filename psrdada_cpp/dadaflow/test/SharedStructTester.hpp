#pragma once
#include "psrdada_cpp/dadaflow/SharedStruct.hpp"
#include <gtest/gtest.h>
#include <string>

namespace bip = boost::interprocess;

namespace psrdada_cpp {
namespace test {

class SharedStructTester : public ::testing::Test
{
public:
    SharedStructTester()
    : shared_memory_name("test_monitoring")
    {}

    ~SharedStructTester(){}

    void SetUp() override {
        bip::shared_memory_object::remove(shared_memory_name.c_str());
    }

    void TearDown() override {
        bip::shared_memory_object::remove(shared_memory_name.c_str());
    }

    std::string shared_memory_name;
};

} // namespace test
} // namespace psrdada_cpp