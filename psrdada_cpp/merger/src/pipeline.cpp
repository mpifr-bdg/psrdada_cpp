namespace psrdada_cpp {
namespace merger {

template<class MergerType, class Handler>
Pipeline<MergerType, Handler>::Pipeline(MergerType& merger, Handler& handler)
    : _merger(merger), _handler(handler){
}

template<class MergerType, class Handler>
Pipeline<MergerType, Handler>::~Pipeline(){
}

template<class MergerType, class Handler>
void Pipeline<MergerType, Handler>::init(RawBytes& block)
{
    RawBytes& oblock = _handler.header_stream().next();
    if (block.used_bytes() > oblock.total_bytes())
    {
        _handler.header_stream().release();
        throw std::runtime_error("Output DADA buffer does not have enough space for header");
    }
    std::memcpy(oblock.ptr(), block.ptr(), block.used_bytes());
    char buffer[1024];
    ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%s", buffer);
    std::size_t sample_clock_start = std::strtoul(buffer, NULL, 0);
    ascii_header_get(block.ptr(), "CLOCK_SAMPLE", "%s", buffer);
    long double sample_clock = std::strtold(buffer, NULL);
    ascii_header_get(block.ptr(), "SYNC_TIME", "%s", buffer);
    long double sync_time = std::strtold(buffer, NULL);
    long double unix_time = sync_time + (sample_clock_start / sample_clock);
    long double mjd_time = (unix_time / 86400.0 ) + 40587;
    std::ostringstream mjd_start;
    mjd_start << std::fixed;
    mjd_start << std::setprecision(12);
    mjd_start << mjd_time;
    ascii_header_set(oblock.ptr(), "MJD_START", "%s", mjd_start.str().c_str());
    ascii_header_set(oblock.ptr(), "UNIX_TIME", "%Lf", unix_time);
    oblock.used_bytes(oblock.total_bytes());
    _handler.header_stream().release();
}

template<class MergerType, class Handler>
bool Pipeline<MergerType, Handler>::operator()(RawBytes& block)
{
    RawBytes& oblock = _handler.data_stream().next();
    BOOST_LOG_TRIVIAL(debug) << "block.used_bytes: " << block.used_bytes()
        << "  oblock.used_bytes " << oblock.used_bytes();
    if (block.used_bytes() > oblock.total_bytes())
    {
        _handler.data_stream().release(); // <- Does the release makes sense here?
        throw std::runtime_error("Output DADA buffer does not match with the input dada buffer");
    }

    _merger.process(block.ptr(), oblock.ptr(), block.used_bytes());

    oblock.used_bytes(block.used_bytes());
    _handler.data_stream().release();
    return false;
}

}//merger
}//psrdada_cpp

