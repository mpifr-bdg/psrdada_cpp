#include "psrdada_cpp/merger/test/merge_tester.hpp"


namespace psrdada_cpp {
namespace merger {
namespace test {

// ------------------------------- //
//   Implementation of PFBMerger   //
// ------------------------------- //

void PfbMergeTester::test_process()
{
    std::size_t heap_size = 4 * params.time;
    std::vector<char> i_vector = generateTestVector(); // testing_tools::random_vector<int8_t>();
    std::vector<char> o_actual = std::vector<char>(i_vector.size());
    std::vector<char> o_expect = generateExpectedOutput();
    PFBMerger merger(params.freq_chunk, params.nthreads, heap_size);
    merger.process(i_vector.data(), o_actual.data(), i_vector.size());
    for (std::size_t i = 0; i < i_vector.size(); i++) {
        ASSERT_EQ(o_actual[i], o_expect[i]) << " Position " << i;;
    }
}


std::size_t PfbMergeTester::getBufferSize() {
    return params.byte_per_sample * params.freq_chunk * params.time;
}


std::vector<char> PfbMergeTester::generateTestVector()
{
    std::vector<char> test_vector(params.byte_per_sample * params.time * params.freq_chunk);
    for (std::size_t j = 0; j < params.freq_chunk; j++) {
        for (std::size_t i = 0; i < params.time; i++) {
            std::size_t index = params.byte_per_sample * i + j * params.byte_per_sample * params.time;
            char value = static_cast<char>(i % 128);
            std::fill(
                test_vector.begin() + index,
                test_vector.begin() + index + params.byte_per_sample,
                value
            );
        }
    }
    return test_vector;
}


std::vector<char> PfbMergeTester::generateExpectedOutput()
{
    std::vector<char> expected_output(params.byte_per_sample * params.time * params.freq_chunk, 'X');
    for (std::size_t j = 0; j < params.time; j++) {
        for (std::size_t i = 0; i < params.freq_chunk; i++) {
            std::size_t index = j * params.freq_chunk * params.byte_per_sample + i * params.byte_per_sample;
            char value = static_cast<char>(j % 128);
            std::fill(
                expected_output.begin() + index,
                expected_output.begin() + index + params.byte_per_sample,
                value
            );
        }
    }
    return expected_output;
}




// ------------------------------- //
//   Implementation of PolnMerger  //
// ------------------------------- //
void PolnMergeTester::test_process()
{
    std::vector<char> idata = generateTestVector(); // testing_tools::random_vector<int8_t>();
    std::vector<char> odata = std::vector<char>(idata.size());
    PolnMerger merger(params.npol, params.nthreads);
    merger.process(idata.data(), odata.data(), idata.size());
    for (std::size_t ii = 0; ii < params.nheap_groups; ii++)
    {
        for (std::size_t jj = 0; jj < params.nsamps_per_heaps; jj++)
        {
            for (std::size_t kk = 0; kk < params.npol; kk++)
            {
                std::size_t idx = ii * params.npol * params.nsamps_per_heaps
                    + jj * params.npol + kk;
                ASSERT_EQ(odata[idx], static_cast<char>((ii * 2 + kk) % 128));
            }
        }
    }
}


std::size_t PolnMergeTester::getBufferSize() {
    return params.npol * params.nsamps_per_heaps * params.nheap_groups;
}


std::vector<char> PolnMergeTester::generateTestVector()
{
	std::vector<char> test_vector(getBufferSize());
	for (std::size_t ii = 0; ii < params.nheap_groups; ii++)
	{
		for (std::size_t jj = 0; jj < params.npol; jj++)
		{
			for (std::size_t kk = 0; kk < params.nsamps_per_heaps; kk++)
			{
                std::size_t idx = ii * params.npol * params.nsamps_per_heaps
                    + jj * params.nsamps_per_heaps + kk;
				test_vector[idx] = (ii * 2 + jj) % 128;
			}
		}
	}
    return test_vector;
}


TEST_P(PfbMergeTester, test_process) {
    test_process();
}

INSTANTIATE_TEST_SUITE_P(ParameterizedTest, PfbMergeTester, ::testing::Values(
    TestParamsPfb({4, 2, 4000, 3}),
    TestParamsPfb({4, 64, 16000, 3}),
    TestParamsPfb({4, 32, 16000, 3})
));

TEST_P(PolnMergeTester, test_process) {
    test_process();
}

INSTANTIATE_TEST_SUITE_P(ParameterizedTest, PolnMergeTester, ::testing::Values(
    TestParamsPoln({2, 4096, 4, 2}),
    TestParamsPoln({2, 4096, 8, 4}),
    TestParamsPoln({2, 4096, 16, 8})
));


} // namespace test
} // namespace merger
} // namespace psrdada_cpp


