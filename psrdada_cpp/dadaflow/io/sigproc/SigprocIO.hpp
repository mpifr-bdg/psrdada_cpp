#pragma once
#include "psrdada_cpp/dadaflow/io/sigproc/SigprocHeader.hpp"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Dimensions.hpp"
#include "psrdada_cpp/dadaflow/FixedWidth.hpp"
#include "psrdada_cpp/dadaflow/ResourcePool.hpp"
#include "psrdada_cpp/dadaflow/units.hpp"
#include <variant>
#include <memory>
#include <fstream>
#include <filesystem>

namespace psrdada_cpp {
namespace sigproc {

namespace fs = std::filesystem;

template <typename ContainerType>
using Filterbank = DescribedData<ContainerType, 
                                 TimeDimension,
                                 FrequencyDimension>;
using Filterbank1Bit        = Filterbank<FixedWidth<std::vector<uint8_t>, 1, std::endian::little>>;
using Filterbank2Bit        = Filterbank<FixedWidth<std::vector<uint8_t>, 2, std::endian::little>>;
using Filterbank4Bit        = Filterbank<FixedWidth<std::vector<uint8_t>, 4, std::endian::little>>;
using Filterbank8Bit        = Filterbank<std::vector<uint8_t>>;
using Filterbank8BitSigned  = Filterbank<std::vector<int8_t>>;
using Filterbank32Bit       = Filterbank<std::vector<uint32_t>>;
using Filterbank32BitSigned = Filterbank<std::vector<int32_t>>;
using FilterbankVariant     = std::variant<std::shared_ptr<Filterbank1Bit>,
                                           std::shared_ptr<Filterbank2Bit>,
                                           std::shared_ptr<Filterbank4Bit>,
                                           std::shared_ptr<Filterbank8Bit>,
                                           std::shared_ptr<Filterbank8BitSigned>,
                                           std::shared_ptr<Filterbank32Bit>,
                                           std::shared_ptr<Filterbank32BitSigned>>;

//--------------------------------------

/**
 * @brief Helper class for reading sigproc files of a specific type
 * 
 */
class SigprocGeneratorInterface
{
public:
    virtual ~SigprocGeneratorInterface() = default;
    virtual FilterbankVariant generate(std::size_t nsamples) = 0;
};

template <typename FilterbankType>
class SigprocGenerator: public SigprocGeneratorInterface
{
public:
    using Allocator = std::shared_ptr<ResourcePool<FilterbankType>>;
    using OutputPtr = std::shared_ptr<FilterbankType>;

    SigprocGenerator(SigprocHeader const& header, Allocator allocator)
    : _header(header)
    , _metadata_ptr(std::make_shared<MetadataMap>())
    , _allocator(allocator) {
        (*_metadata_ptr)["SIGPROC_HEADER"] = header;
    }
    FilterbankVariant generate(std::size_t nsamples) override;

private:
    SigprocHeader _header;
    std::shared_ptr<MetadataMap> _metadata_ptr;
    Allocator _allocator;
};

//--------------------------------------

/**
 * @brief Wrapper for a single sigproc file of arbitrary type
 * 
 * @note This class is generally not intended to be used in isolation
 *       but instread it is used by the more general SigprocInputFileStream
 *       class below to provide access to one or multiple sigproc files with
 *       appropriate bit-width handling.
 */
class SigprocFile 
{
public:
    explicit SigprocFile(const fs::path& filename);
    SigprocFile(SigprocFile&& other) noexcept = default;
    SigprocFile& operator=(SigprocFile&& other) noexcept = default;
    SigprocFile(const SigprocFile&) = delete;
    SigprocFile& operator=(const SigprocFile&) = delete;
    ~SigprocFile() = default;
    const SigprocHeader& header() const { return _header; }
    std::size_t nsamples() const { return _nsamples; }
    const fs::path& filename() const { return _filename; }
    bool at_end() const;
    char* read_nsamples(char* ptr, std::size_t nsamples);
    std::size_t remaining_samples() const;
    void seek_sample(std::size_t sample_idx);

private:
    void open();
    void prepare();
    void close();

    fs::path _filename;
    SigprocHeader _header;
    std::size_t _header_size = 0;
    std::size_t _nsamples = 0;
    std::size_t _bytes_per_sample = 0;
    std::size_t _filesize = 0;
    std::unique_ptr<std::ifstream> _stream; 
};

//--------------------------------------

/**
 * @brief A wrapper for accessing a set of sigproc files as a stream
 * 
 */
class SigprocInputFileStream
{
public:

    /**
     * @brief Construct a new Sigproc Input File Stream object
     * 
     * @param files A vector of paths to sigproc files
     * @param assume_contiguous Assume the files are time contigous when sorted by their tstart values
     * 
     * @note the assume_contiguous option is often useful for instruments that have poor rounding behaviour
     *       on timestamps written to the files. 
     */
    SigprocInputFileStream(std::vector<fs::path> const& files, bool assume_contiguous=false);

    /**
     * @brief Read N time samples of data from the stream
     * 
     * @param nsamples The number of samples to read from the stream
     * @return FilterbankVariant A variant with a value that depends on the nbits of the file
     * 
     * @note This method will only read complete spectra from the file. So for a 16 channel file, reading 2 samples will
     *       return an object with 32 elements.
     */
    FilterbankVariant read_block(std::size_t nsamples);

    /**
     * @brief Check if the end of the stream has been reached
     * 
     * @return true At end of stream
     * @return false Not at end of stream
     */
    bool at_end() const { return _end_of_stream; }

    /**
     * @brief Seek to a specific sample in the stream
     * 
     * @param sample_idx Sample index as measured from the first sample of the first file in the stream
     */
    void seek_sample(std::size_t sample_idx);

private:
    void check_contiguity() const;
    void check_compatibility() const;
    void calculate_total_nsamples(); 
    void set_generator(SigprocHeader const& header);
    
    std::vector<SigprocFile> _files;
    std::unique_ptr<SigprocGeneratorInterface> _generator;
    std::size_t _curr_file_idx = 0;
    std::size_t _total_nsamples = 0;
    bool _end_of_stream = false;
};

} // namespace sigproc
} // namespace psrdada_cpp

#include "psrdada_cpp/dadaflow/io/sigproc/detail/SigprocIO.cpp"