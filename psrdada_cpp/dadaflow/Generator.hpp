/**
 * ONLY until C++23 supported std::generator becomes available in Clang
 */

#pragma once
#include <coroutine>
#include <exception>
#include <iostream>
#include <memory>
#include <tuple>
#include <vector>

namespace psrdada_cpp
{

template <typename T>
class Generator
{
  public:
    struct promise_type;
    using handle_type = std::coroutine_handle<promise_type>;
    using yield_type = T;

    Generator(handle_type h): coro(h) {}
    ~Generator()
    {
        if(coro)
            coro.destroy();
    }

    T get_value() { return coro.promise().value; }

    bool next()
    {
        coro.resume();
        return not coro.done();
    }

    struct promise_type {
        T value;

        auto get_return_object()
        {
            return Generator{handle_type::from_promise(*this)};
        }

        auto initial_suspend() { return std::suspend_always(); }
        auto final_suspend() noexcept { return std::suspend_always(); }
        void return_void() {}

        auto yield_value(T v)
        {
            value = v;
            return std::suspend_always();
        }

        void unhandled_exception() { std::terminate(); }
    };

  private:
    handle_type coro;
};

template <typename T>
struct is_generator: std::false_type {
};

// Specialization for Generator
template <typename T>
struct is_generator<Generator<T>>: std::true_type {
};

// Helper variable template
template <typename T>
inline constexpr bool is_generator_v = is_generator<T>::value;

} //  namespace psrdada_cpp