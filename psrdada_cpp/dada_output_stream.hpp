#ifndef PSRDADA_CPP_DADA_OUTPUT_STREAM_HPP
#define PSRDADA_CPP_DADA_OUTPUT_STREAM_HPP

#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/common.hpp"

namespace psrdada_cpp
{

    class AbstractCopyEngine
    {
        virtual void operator()(void* dst, void* src, std::size_t nbytes) = 0;
        virtual void sync() = 0;
    };

    class DadaOutputStream
    {
    public:
        DadaOutputStream(key_t key, MultiLog& log);
        ~DadaOutputStream();
        void init(RawBytes&);
        void operator()(RawBytes&);

        template<typename CopyEngine>
        void operator()(RawBytes& in, CopyEngine engine)
        {
            engine.sync();
            if(!_first){
                _writer.data_stream().release();
            }
            auto& dst = _writer.data_stream().next();
            engine(
                static_cast<void*>(dst.ptr()),
                static_cast<void*>(in.ptr()),
                in.used_bytes());
            dst.used_bytes(in.used_bytes());
            _first = false;
        }


        DadaWriteClient const& client() const;

    protected:
        bool _first = true;
        DadaWriteClient _writer;
    };
}

#endif //PSRDADA_CPP_DADA_OUTPUT_STREAM_HPP