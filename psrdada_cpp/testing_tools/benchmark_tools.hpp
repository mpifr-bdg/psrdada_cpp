#pragma once

#include <benchmark/benchmark.h>
#include <numeric>

namespace psrdada_cpp {
namespace testing_tools {

void max(const std::vector<double>& v, benchmark::State& state){
    state.counters["B/s max"] = *(std::max_element(v.begin(), v.end()));
}
void min(const std::vector<double>& v, benchmark::State& state){
    state.counters["B/s min"] = *(std::min_element(v.begin(), v.end()));
}
void mean(const std::vector<double>& v, benchmark::State& state){
    state.counters["B/s mean"] = std::accumulate(v.begin(), v.end(), 0.0) / v.size();
}
void stddev(const std::vector<double>& v, benchmark::State& state){
    double mean = std::accumulate(std::begin(v), std::end(v), 0.0) / v.size();
    std::vector<double> diff(v.size());
    std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return (x - mean) * (x - mean);});
    state.counters["B/s stddev"] = std::sqrt(std::accumulate(diff.begin(), diff.end(), 0.0) / diff.size());
}
void getStats(const std::vector<double>& v, benchmark::State& state){
    max(v, state); min(v, state); mean(v, state); stddev(v, state);
}

}
}