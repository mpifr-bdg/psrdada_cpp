
#pragma once
#include "psrdada_cpp/common.hpp"

#include <memory>
#include <tuple>

namespace psrdada_cpp
{

template <typename OutputType, typename OutputAllocator>
class JunkSource
{
  public:
    using OutputPtr         = std::shared_ptr<OutputType>;
    //using Output            = std::tuple<OutputPtr>;
    using PopulatorCallback = std::function<void(OutputPtr&)>;

    explicit JunkSource(OutputAllocator& allocator,
                        PopulatorCallback&& populator)
        : _allocator(allocator), _populator(std::move(populator))
    {
    }

    OutputPtr operator()()
    {
        OutputPtr output = _allocator.get();
        _populator(output);
        return output;
    }

  private:
    OutputAllocator& _allocator;
    PopulatorCallback _populator;
};

/** Example usage
int main()
{
    using T    = std::vector<float>;
    using Tptr = std::shared_ptr<T>;
    ResourcePool pool(3, 20);
    JunkSource<decltype(prototype), decltype(pool)> source(pool, [](Tptr ptr) {
        T& data = *ptr;
        for(auto& datum: data) { datum = 1.0f; }
    });
    auto data = source();
}
*/

} // namespace psrdada_cpp