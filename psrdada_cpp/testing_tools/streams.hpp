#pragma once

#include <random>
#include <cmath>
#include <cassert>
#include <vector>

#include <iostream>
#include <fstream>

#include <gmock/gmock.h>
#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/testing_tools/clients.hpp>


namespace psrdada_cpp{
namespace testing_tools{

class AbstractStream
{
public:
    virtual ~AbstractStream() {}
    virtual void init(RawBytes &block) = 0;
    virtual bool operator()(RawBytes &block) = 0;
};

class MockDadaOutputStream : public AbstractStream
{
public:
    MockDadaOutputStream(){}
    MOCK_METHOD(void, init, (RawBytes&));
    MOCK_METHOD(bool, op_parenthesis, (RawBytes&));
    MOCK_METHOD(MockDadaWriteClient const&, client, ());
    bool operator()(RawBytes& block) override {return op_parenthesis(block);} // Wrapper - GMock does not suppoert operator overloading
};



template<typename T>
class DummyStream : public AbstractStream
{
public:
    DummyStream(bool copy=true)
        : _call_count(0), _copy(copy)
    {
        _header.resize(0);
        _data.resize(0);
    };

    ~DummyStream()
    {
        BOOST_LOG_TRIVIAL(debug) << "Destroyed DummyStream";
    };

    void init(psrdada_cpp::RawBytes &block) override
    {
        if(_header.size() != block.used_bytes()){_header.resize(block.used_bytes());}
        std::copy(block.ptr(), block.ptr() + block.used_bytes(), _header.data());
    };

    bool operator()(psrdada_cpp::RawBytes &block) override
    {
        _call_count++;
        BOOST_LOG_TRIVIAL(debug) << "DummyStream got new data block";

        if(!_copy){return false;}

        if(_data.size() * sizeof(T) != block.used_bytes()){
            _data.resize(block.used_bytes() / sizeof(T));
        }
        std::memcpy(_data.data(), block.ptr(), block.used_bytes());
        return false;
    };


    template<typename CopyEngine>
    void operator()(RawBytes& block, CopyEngine engine)
    {
        engine.sync();
        _call_count++;

        if(!_copy){return;}

        if(_data.size() * sizeof(T) != block.used_bytes()){
            _data.resize(block.used_bytes() / sizeof(T));
        }
        engine(
            static_cast<void*>(_data.data()),
            static_cast<void*>(block.ptr()),
            block.used_bytes());
    }

    bool save_data(std::string fname="data.bin")
    {
        std::ofstream file(fname, std::ios::binary);
        if (!file.is_open())
        {
            std::cerr << "Failed to open file for writing" << std::endl;
            return false;
        }
        std::cout << "Writing " << _data.size() * sizeof(T) / 1024 << " KB to " << fname << std::endl;
        file.write(reinterpret_cast<const char*>(_data.data()), _data.size() * sizeof(T));
        file.close();
        return true;
    }

    T* data_ptr(std::size_t position=0)
    {
        return &_data.data()[position];
    }
    std::vector<T>& data()
    {
        return _data;
    }
    char* header_ptr(std::size_t position=0)
    {
        return &_header.data()[position];
    }
    std::vector<char>& header()
    {
        return _header;
    }
    T item(std::size_t pos = 0)
    {
        return _data[pos];
    }
    std::size_t data_size()
    {
        return _data.size();
    }
    std::size_t header_size()
    {
        return _header.size();
    }
    std::size_t call_count()
    {
        return _call_count;
    }

private:
    bool _copy;
    std::size_t _header_bytes;
    std::size_t _payload_bytes;
    std::size_t _call_count;
    std::vector<char> _header;
    std::vector<T> _data;
};

}
}