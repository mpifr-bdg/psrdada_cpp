#include "psrdada_cpp/common/test/detector_accumulator_tester.cuh"

namespace psrdada_cpp {
namespace test {

DetectorAccumulatorTester::DetectorAccumulatorTester()
    : ::testing::Test()
    , _stream(0)
{

}

DetectorAccumulatorTester::~DetectorAccumulatorTester()
{

}

void DetectorAccumulatorTester::SetUp()
{
    CUDA_ERROR_CHECK(cudaStreamCreate(&_stream));
}

void DetectorAccumulatorTester::TearDown()
{
    CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
}

void DetectorAccumulatorTester::detect_c_reference(
    InputType const& input,
    OutputType& output,
    int nchans,
    int tscrunch,
    float scale,
    float offset)
{
    int nsamples = input.size() / nchans;
    int nsamples_out = nsamples / tscrunch;
    output.resize(nsamples_out * nchans);
    for (int chan_idx = 0; chan_idx < nchans; ++chan_idx)
    {
        for (int output_sample_idx = 0;
            output_sample_idx < nsamples_out;
            ++output_sample_idx)
        {
            double value = 0.0f;
            for (int input_sample_offset=0;
                input_sample_offset < tscrunch;
                ++input_sample_offset)
            {
                int input_sample_idx = output_sample_idx * tscrunch + input_sample_offset;
                float2 x = input[input_sample_idx * nchans + chan_idx];
                value += ((x.x * x.x) + (x.y * x.y));
            }
            output[output_sample_idx * nchans + chan_idx] = (int8_t)((value - offset) / scale);
        }
    }
}

void DetectorAccumulatorTester::compare_against_host(
    DetectorAccumulator<int8_t>::OutputType const& gpu_output,
    OutputType const& host_output)
{
    OutputType copy_from_gpu = gpu_output;
    ASSERT_EQ(host_output.size(), copy_from_gpu.size());
    for (std::size_t ii = 0; ii < host_output.size(); ++ii)
    {
	    ASSERT_EQ(host_output[ii], copy_from_gpu[ii]);
    }
}

TEST_F(DetectorAccumulatorTester, noise_test)
{
    int nchans = 512;
    int tscrunch = 16;
    float stdev = 15.0;
    float scale = std::sqrt(stdev * tscrunch);
    int n = nchans * tscrunch * 16;
    std::default_random_engine generator;
    std::normal_distribution<float> distribution(0.0, stdev);
    InputType host_input(n);
    for (int ii = 0; ii < n; ++ii)
    {
        host_input[ii].x = distribution(generator);
	host_input[ii].y = distribution(generator);
    }
    DetectorAccumulator<int8_t>::InputType gpu_input = host_input;
    DetectorAccumulator<int8_t>::OutputType gpu_output;
    gpu_output.resize(gpu_input.size() / tscrunch );
    OutputType host_output;
    DetectorAccumulator<int8_t> detector(nchans, tscrunch, scale, 0.0, _stream);
    detector.detect(gpu_input, gpu_output);
    detect_c_reference(host_input, host_output, nchans, tscrunch, scale, 0.0);
    CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
    compare_against_host(gpu_output, host_output);
}


// parameters for the detector accumulator test
struct detect_accumulate_params
{
    size_t nchan;       // umber of channels
    size_t nspectra;    // number of output spectra
    size_t naccumulate; // number of spectra to accumulate
};


class detect_and_accumulate32bit: public testing::TestWithParam<detect_accumulate_params> {
// Test that the kernel executes in certain parameter
// settings
};

// These tests fail in DEBUG mode
TEST_P(detect_and_accumulate32bit, no_stride)
{
    thrust::device_vector<float2> input;
    thrust::device_vector<float> output;

    detect_accumulate_params params = GetParam();

    input.resize(params.nspectra * params.nchan * params.naccumulate);
    output.resize(params.nspectra * params.nchan);
    float2 v;
    v.x = 1.0;
    v.y = 1.0;
    thrust::fill(input.begin(), input.end(), v);
    thrust::fill(output.begin(), output.end(), 0.);

    kernels::detect_and_accumulate<float> <<<1024, 1024>>>(
            thrust::raw_pointer_cast(input.data()),
            thrust::raw_pointer_cast(output.data()),
            params.nchan,
            input.size(),
            params.naccumulate,
            1, 0., 1, 0);
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());

    thrust::host_vector<float> output_host = output;
    for (size_t i =0; i < output.size(); i++)
    {
        ASSERT_FLOAT_EQ(output_host[i], (v.x * v.x + v.y * v.y) * params.naccumulate) 
            << "i = " << i << " for nchan = " << params.nchan << ", nspectra = " 
            << params.nspectra << ", naccumulate = " << params.naccumulate;
    }
}

// These tests fail in DEBUG mode
TEST_P(detect_and_accumulate32bit, w_stride)
{

    thrust::device_vector<float2> input;
    thrust::device_vector<float> output;

    detect_accumulate_params params = GetParam();

    input.resize(params.nspectra * params.nchan * params.naccumulate);
    output.resize(params.nspectra * params.nchan * 2);
    float2 v;
    v.x = 1.0;
    v.y = 1.0;
    thrust::fill(input.begin(), input.end(), v);
    thrust::fill(output.begin(), output.end(), 0.);
    kernels::detect_and_accumulate<float> <<<1024, 1024>>>(
            thrust::raw_pointer_cast(input.data()),
            thrust::raw_pointer_cast(output.data()),
            params.nchan,
            input.size(),
            params.naccumulate,
            1, 0., 2, 0);
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());

    thrust::host_vector<float> output_host = output;
    for (size_t i =0; i < params.nchan * params.nspectra; i++)
    {
        size_t current_spectrum = i / params.nchan;
        ASSERT_FLOAT_EQ(output_host[i + current_spectrum * params.nchan], (v.x * v.x + v.y * v.y) * params.naccumulate) 
            << "i = " << i << " for nchan = " << params.nchan << ", nspectra = " 
            << params.nspectra << ", naccumulate = " << params.naccumulate;
    }
}






INSTANTIATE_TEST_SUITE_P (DetectorAccumulatorTester,
        detect_and_accumulate32bit,
        testing::Values(
          // nchan; nspectra; naccumulate;
          detect_accumulate_params({1024, 1,  128}),
          detect_accumulate_params({1024, 1,  1024}),
          detect_accumulate_params({1024, 1,  2048}),
          detect_accumulate_params({1024, 1,  16384}),
          detect_accumulate_params({1024, 16,  1024}),
          detect_accumulate_params({131072, 2,  16}),
          detect_accumulate_params({131072, 1,  32})
            )
        );



} //namespace test
} //namespace psrdada_cpp
