#pragma once

#include <cuda_profiler_api.h>
#include <cuda.h>
#include <cufft.h>
#include <thrust/device_vector.h>

#include "psrdada_cpp/common/unpacker.cuh"
#include "psrdada_cpp/common/detector_accumulator.cuh"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/double_device_buffer.cuh"
#include "psrdada_cpp/double_host_buffer.cuh"

namespace psrdada_cpp {
namespace fft_spectrometer {

template <class HandlerType>
class Pipeline
{
public:
    typedef uint64_t RawVoltageType;
    typedef float UnpackedVoltageType;
    typedef float2 ChannelisedVoltageType;
    typedef float IntegratedPowerType;

public:
    Pipeline(
        std::size_t buffer_bytes,
        std::size_t fft_length,
        std::size_t naccumulate,
        std::size_t nbits,

        HandlerType& handler);
    ~Pipeline();

    /**
     * @brief      A callback to be called on connection
     *             to a ring buffer.
     *
     * @details     The first available header block in the
     *             in the ring buffer is provided as an argument.
     *             It is here that header parameters could be read
     *             if desired.
     *
     * @param      block  A RawBytes object wrapping a DADA header buffer
     */
    void init(RawBytes& block);

    /**
     * @brief      A callback to be called on acqusition of a new
     *             data block.
     *
     * @param      block  A RawBytes object wrapping a DADA data buffer
     */
    bool operator()(RawBytes& block);

private:
    void process(thrust::device_vector<RawVoltageType> const& digitiser_raw,
        thrust::device_vector<IntegratedPowerType>& detected);

private:
    std::size_t _buffer_bytes;
    std::size_t _fft_length;
    std::size_t _naccumulate;
    std::size_t _nbits;
    float _scaling;
    float _offset;
    HandlerType& _handler;
    cufftHandle _fft_plan;
    int _nchans;
    int _call_count;
    std::unique_ptr<Unpacker> _unpacker;
    std::unique_ptr<DetectorAccumulator<IntegratedPowerType> > _detector;
    DoubleDeviceBuffer<RawVoltageType> _raw_voltage_db;
    DoubleDeviceBuffer<IntegratedPowerType> _power_db;
    thrust::device_vector<UnpackedVoltageType> _unpacked_voltage;
//    thrust::device_vector<ChannelisedVoltageType> _channelised_voltage;
    DoublePinnedHostBuffer<IntegratedPowerType> _host_power_db;
    cudaStream_t _h2d_stream;
    cudaStream_t _proc_stream;
    cudaStream_t _d2h_stream;
};


} // fft_spectrometer
} //psrdada_cpp

#include "psrdada_cpp/fft_spectrometer/src/pipeline.cu"
