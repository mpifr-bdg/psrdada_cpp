#pragma once

#include <thrust/host_vector.h>
#include <arpa/inet.h>
#include <random>
#include <cmath>
#include <complex>
#include <fstream>
#include <iomanip>
#include <gtest/gtest.h>

#include "psrdada_cpp/rs_spectrometer/pipeline.cuh"
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cuda_utils.hpp"

namespace psrdada_cpp {
namespace rs_spectrometer {
namespace test {


struct test_config{
    std::size_t input_nchans;
    std::size_t fft_length;
    std::size_t naccumulate;
    std::size_t nskip;
    std::size_t nspec_per_block;
    std::size_t nitems(){
        return input_nchans * fft_length * nspec_per_block;
    }
    std::size_t buffer_size(){
        return nitems() * sizeof(short2);
    }
    std::size_t nblocks(){
        return naccumulate / nspec_per_block + 1;
    }
};

class PipelineTester: public ::testing::TestWithParam<test_config>
{
protected:
    void SetUp() override;
    void TearDown() override;

public:
    PipelineTester();
    ~PipelineTester();

    void run_dc_power_test();
private:
    test_config _config;
    std::unique_ptr<Pipeline> test_object;
    std::unique_ptr<RawBytes> raw_header;
    std::unique_ptr<RawBytes> raw_data;
    std::vector<char> header;
    std::vector<short2> idata;
};

} //namespace test
} //namespace rs_spectrometer
} //namespace psrdada_cpp

