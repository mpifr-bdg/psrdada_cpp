#pragma once

#include <gtest/gtest.h>
#include <vector>
#include "psrdada_cpp/merger/merger.hpp"

namespace psrdada_cpp {
namespace merger {
namespace test {


/**
 * Struct containing testing params for the PFBMerger
*/
struct TestParamsPfb {
    std::size_t byte_per_sample;
    std::size_t freq_chunk;
    std::size_t time;
    int nthreads;
};


class PfbMergeTester : public testing::TestWithParam<TestParamsPfb>
{
public:
    PfbMergeTester() : ::testing::TestWithParam<TestParamsPfb>(),
        params(GetParam()){};
    void test_process();
    std::size_t getBufferSize();
private:
    std::vector<char> generateTestVector();
    std::vector<char> generateExpectedOutput();
private:
    TestParamsPfb params;
};


/**
 * Struct containing testing params for the PolnMerger
*/
struct TestParamsPoln {
    std::size_t npol;
    std::size_t nsamps_per_heaps;
    std::size_t nheap_groups;
    int nthreads;
};


class PolnMergeTester : public testing::TestWithParam<TestParamsPoln>
{
public:
    PolnMergeTester() : ::testing::TestWithParam<TestParamsPoln>(),
        params(GetParam()){};
    void test_process();
    std::size_t getBufferSize();
private:
    std::vector<char> generateTestVector();
private:
    TestParamsPoln params;
};


} //namespace test
} //namespace merger
} //namespace psrdada_cpp

