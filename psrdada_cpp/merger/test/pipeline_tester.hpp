#pragma once

#include <gtest/gtest.h>
#include <psrdada/ascii_header.h>
#include <vector>

#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/testing_tools/clients.hpp"
#include "psrdada_cpp/merger/merger.hpp"
#include "psrdada_cpp/merger/pipeline.hpp"


namespace psrdada_cpp {
namespace merger {
namespace test {


template<typename MergeType>
class PipelineTester
{
public:
    PipelineTester(merge_conf conf)
        : _conf(conf){

    }

    void test_operator_bad_size(){
        testing_tools::MockDadaWriteClient mock_client;

        std::vector<char> idata(8192);
        std::vector<char> odata(8191);

        RawBytes iraw(idata.data(), idata.size(), idata.size());
        RawBytes oraw(odata.data(), odata.size(), odata.size());
        EXPECT_CALL(mock_client.data_stream(), next()).WillOnce(testing::ReturnRef(oraw));
        EXPECT_CALL(mock_client.data_stream(), release()).Times(testing::AtMost(1));

        MergeType merger(_conf);
        Pipeline<decltype(merger), decltype(mock_client)> pipeline(merger, mock_client);
        EXPECT_THROW(pipeline(iraw), std::runtime_error);
    }

    void test_operator_good_size(){
        testing_tools::MockDadaWriteClient mock_client;

        std::vector<char> idata(_conf.hgroup_size());
        std::vector<char> odata(_conf.hgroup_size());

        RawBytes iraw(idata.data(), idata.size(), idata.size());
        RawBytes oraw(odata.data(), odata.size(), odata.size());
        EXPECT_CALL(mock_client.data_stream(), next()).WillOnce(testing::ReturnRef(oraw));
        EXPECT_CALL(mock_client.data_stream(), release()).Times(testing::AtMost(1));

        MergeType merger(_conf);
        Pipeline<decltype(merger), decltype(mock_client)> pipeline(merger, mock_client);
        EXPECT_FALSE(pipeline(iraw));
        EXPECT_EQ(oraw.used_bytes(), iraw.used_bytes());
        EXPECT_EQ(oraw.total_bytes(), odata.size());
        EXPECT_EQ(iraw.total_bytes(), idata.size());
    }

private:
    merge_conf _conf;
    // testing_tools::MockDadaWriteClient mock_client;
    // MergeType merger;
    // Pipeline<MergeType, testing_tools::MockDadaWriteClient> pipeline;
};

}
}
}