# Gated Spectrometer

The Gated Spectrometer receives time domain data and transforms it to the frequency domain. In contrast to the FFT spectrometer, two gates are created. The first gate generates spectra that are superimposed with the signal of a noise diode. The second gate generates spectra when the noise diode is not active.

## Input Data Format

EDD Packetizer single and dual

## Output Data Format

...

## Modes

- Full Stokes
- Dual Polarization