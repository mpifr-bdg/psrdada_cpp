if(ENABLE_CUDA)

  set(PSRDADA_CPP_DADAFLOW_SRC
      ${CMAKE_CURRENT_SOURCE_DIR}/src/AsyncDadaReadClient.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/src/DadaHeader.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/src/Graph.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/io/sigproc/src/SigprocHeader.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/io/sigproc/src/SigprocIO.cpp
  )

  # Install all include files and exclude unnecessary folders
  install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} 
      DESTINATION include/psrdada_cpp
      PATTERN "test" EXCLUDE
      PATTERN "examples" EXCLUDE
      PATTERN "cli" EXCLUDE
      PATTERN "doc" EXCLUDE)
      # PATTERN "src" EXCLUDE)

  # Add the sources to the psrdada_cpp library
  target_sources(${CMAKE_PROJECT_NAME} PRIVATE ${PSRDADA_CPP_DADAFLOW_SRC})


  # ----------------------------- #
  # -- Build DADAFLOW examples -- #
  # ----------------------------- #
  set(DADAFLOW_EXAMPLE_CLI
      async_read_example
      demo
      realistic_demo
      shared_struct_example
      message_server_example
      relay_example
      switch_example
      node_selector_example
  )
  # Install the dadaflow examples using the DADAFLOW_EXAMPLE_CLI list
  foreach(EXEC_NAME ${DADAFLOW_EXAMPLE_CLI})
      set(EXEC_SRC "examples/${EXEC_NAME}.cpp")
      add_executable(${EXEC_NAME} ${EXEC_SRC})
      target_link_libraries(${EXEC_NAME} PUBLIC ${PSRDADA_CPP_LIBRARIES})
  endforeach()

  # ----------------------------- #
  # --   Build DADAFLOW CLIs   -- #
  # ----------------------------- #
  add_executable(rfsoc2tafp cli/rfsoc2tafp.cpp)
  target_link_libraries (rfsoc2tafp PUBLIC ${PSRDADA_CPP_LIBRARIES})
  install (TARGETS rfsoc2tafp DESTINATION bin)

  
  # ----------------------------- #
  # --        Add tests        -- #
  # ----------------------------- #
  if (ENABLE_TESTING)
    add_subdirectory(test)
    add_subdirectory(io/sigproc/test)
  endif()

endif(ENABLE_CUDA)
