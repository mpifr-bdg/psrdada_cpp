#pragma once
#include "psrdada_cpp/common.hpp"
#include "psrdada_cpp/cuda_utils.hpp"
#include "psrdada_cpp/dadaflow/DescribedData.hpp"
#include "psrdada_cpp/dadaflow/Generator.hpp"
#include "psrdada_cpp/dadaflow/StokesVector.cuh"

#include <functional>
#include <thrust/fill.h>

namespace psrdada_cpp
{
namespace
{
#define TILE_SIZE             32
#define NTHREADS              1024
#define NBLOCKS_WARNING_LEVEL 16
} // namespace

template <typename T, typename... StokesParameters>
struct DetAccStokesPolicy {
    using AccumulatorType = StokesVector<T, StokesParameters...>;

    template <typename DataType>
    static void set_polarisations(DataType& data)
    {
        data.polarisations(get_stokes_names<StokesParameters...>());
    }

    static constexpr std::size_t size = sizeof...(StokesParameters);

    __host__ __device__ static inline AccumulatorType detect(float2 const& p0,
                                                             float2 const& p1)
    {
        return calculate_stokes<T, StokesParameters...>(p0, p1);
    }

    __host__ __device__ static inline void store(float* __restrict__ out,
                                                 AccumulatorType const& value,
                                                 int nchannels,
                                                 int channel_idx)
    {
        atomicAdd(&out[channel_idx], value.x);
        atomicAdd(&out[nchannels + channel_idx], value.y);
        atomicAdd(&out[2 * nchannels + channel_idx], value.z);
        atomicAdd(&out[3 * nchannels + channel_idx], value.w);
    }
};

template <typename T>
struct DetAccDualPolnPolicy {
    using AccumulatorType = vector_type_t<T, 2>;

    template <typename DataType>
    static void set_polarisations(DataType& data)
    {
        data.polarisations({"XX", "YY"});
    }

    static constexpr std::size_t size = 2;

    __host__ __device__ static inline AccumulatorType detect(float2 const& p0,
                                                             float2 const& p1)
    {
        return {cuCmagf(p0), cuCmagf(p1)};
    }

    __host__ __device__ static inline void store(float* __restrict__ out,
                                                 AccumulatorType const& value,
                                                 int nchannels,
                                                 int channel_idx)
    {
        atomicAdd(&out[channel_idx], value.x);
        atomicAdd(&out[nchannels + channel_idx], value.y);
    }
};

namespace kernels
{
template <typename Policy, int tile_width = 32>
__global__ void detect_and_accumulate(float2 const* __restrict__ p0in,
                                      float2 const* __restrict__ p1in,
                                      float* __restrict__ out,
                                      int nchannels,
                                      int nsamples)
{
    // Each block is handling TILE_SIZE samples for a single frequency
    // channel. Each execution of this kernel only produces one spectrum!
    const int channel_idx      = blockIdx.x * blockDim.x + threadIdx.x;
    const int start_sample_idx = blockIdx.y * tile_width;
    if(channel_idx >= nchannels) {
        return;
    }
    const int end_sample_idx = min((start_sample_idx + tile_width), nsamples);
    typename Policy::AccumulatorType sum{};
    for(int sample_idx = start_sample_idx; sample_idx < end_sample_idx;
        ++sample_idx) {
        const int idx = sample_idx * nchannels + channel_idx;
        sum += Policy::detect(p0in[idx], p1in[idx]);
    }
    Policy::store(out, sum, nchannels, channel_idx);
}

} // namespace kernels

// For the GS pipeline we go
// TPT4096 (host) -[H2D w/transpose]-> PTTT(nchan*2*naccum) (device)
// PTTT (device) -[Gating]-> GPTTT (device)
// GPTTT (device) -[FFT]-> GPTTF (device)
// GPTTF (device) -[Detect]-> T-GPF (device)
// TGPF (device) -[D2H]-> TGPF (host)
// Forget about gates for the moment here and
// implement for one gate

template <typename InputType,
          typename OutputType,
          typename Allocator,
          typename Policy>
class SpectralDetectorAccumulator;

template <typename InputContainer,
          typename OutputContainer,
          typename Allocator,
          typename Policy>
    requires(Vec2<typename InputContainer::value_type> &&
             is_device_container_v<InputContainer> &&
             is_device_container_v<OutputContainer>)
class SpectralDetectorAccumulator<DescribedData<InputContainer,
                                                PolarisationDimension,
                                                TimeDimension,
                                                ContiguousFrequencyDimension>,
                                  DescribedData<OutputContainer,
                                                PolarisationDimension,
                                                TimeDimension,
                                                ContiguousFrequencyDimension>,
                                  Allocator,
                                  Policy>
{
  public:
    using T           = TimeDimension;
    using P           = PolarisationDimension;
    using F           = FrequencyDimension;
    using InputType   = DescribedData<InputContainer, P, T, F>;
    using OutputType  = DescribedData<OutputContainer, P, T, F>;
    using InputPtr    = std::shared_ptr<InputType>;
    using OutputPtr   = std::shared_ptr<OutputType>;
    using OutputTuple = std::tuple<OutputPtr>;

    SpectralDetectorAccumulator(std::size_t naccumulate, Allocator& allocator)
        : _naccumulate(naccumulate), _allocator(allocator), _first_call(true)
    {
        BOOST_LOG_TRIVIAL(debug)
            << "Constructing SpectralDetectorAccumulator instance";
        CUDA_ERROR_CHECK(cudaStreamCreate(&_stream));
    }

    ~SpectralDetectorAccumulator()
    {
        CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
    }

    SpectralDetectorAccumulator(SpectralDetectorAccumulator const&) = delete;
    SpectralDetectorAccumulator&
    operator=(SpectralDetectorAccumulator const&) = delete;

    Generator<std::optional<OutputTuple>> operator()(InputPtr const& input_ptr)
    {
        InputType const& input = *input_ptr;
        if(input.npol() != 2) {
            throw std::runtime_error(
                "SpectralDetectorAccumulator requires dual polarisation input");
        }

        BOOST_LOG_TRIVIAL(debug)
            << "SpectralDetectorAccumulator received input data: "
            << input.describe();
        if(_first_call) {
            BOOST_LOG_TRIVIAL(debug) << "In first call, initializing.";
            initialize(input);
            _first_call = false;
        }

        std::size_t input_samples = input.nsamples();
        std::size_t input_offset  = 0;

        while(input_samples > 0) {
            BOOST_LOG_TRIVIAL(debug)
                << "input_samples = " << input_samples
                << ", input_offset = " << input_offset
                << ", remaining_to_accumulate = " << _remaining;
            if(input_samples >= _remaining) {
                // Do whole remaining
                BOOST_LOG_TRIVIAL(debug) << "Performing complete accumulation";
                invoke_kernel(input, input_offset, _remaining);
                input_samples -= _remaining;
                input_offset += _remaining;
                _remaining = 0;
            } else /* input_samples < _remaining */ {
                // Do remaining input (from input_offset with count of
                // input_samples)
                BOOST_LOG_TRIVIAL(debug) << "Performing partial accumulation";
                invoke_kernel(input, input_offset, input_samples);
                _remaining -= input_samples;
                input_offset += input_samples;
                input_samples = 0;
            }
            if(_remaining == 0) {
                BOOST_LOG_TRIVIAL(debug) << "Releasing output block";
                co_yield std::optional<OutputTuple>{std::tie(_current_block)};
                next_block(input);
            }
        }
        co_return;
    }

  private:
    void initialize(InputType const& input)
    {
        // Get first timestamp
        _timestamp = input.timestamp();
        next_block(input);
    }

    void next_block(InputType const& input)
    {
        _current_block = _allocator.get();
        std::array<std::size_t, 3> extents{Policy::size, 1, input.nchannels()};
        if constexpr(Resizeable<OutputContainer>) {
            _current_block->resize(extents);
        } else {
            if(_current_block->extents() != extents) {
                throw std::runtime_error("Non-resizeable container of invalid "
                                         "extents provided by allocator");
            }
        }
        _current_block->timestamp(_timestamp);
        _current_block->timestep(input.timestep() *
                                 static_cast<double>(_naccumulate));
        _current_block->channel_bandwidth(input.channel_bandwidth());
        _current_block->channel_zero_freq(input.channel_zero_freq());
        Policy::set_polarisations(*_current_block);
        _remaining = _naccumulate;
        // Update timestamp to be correct for next block
        _timestamp += input.timestep() * _naccumulate;
        thrust::fill(_current_block->begin(),
                     _current_block->end(),
                     typename OutputType::value_type{0.0f});
    }

    void invoke_kernel(InputType const& input,
                       std::size_t input_offset,
                       std::size_t naccumulate)
    {
        // threads x -- channels
        // blocks x -- samples -- sime cap on nsamples to accumulate

        int nblocks_x = input.nchannels() / NTHREADS + 1;
        int nblocks_y = naccumulate / TILE_SIZE + 1;
        if(nblocks_x * nblocks_y < NBLOCKS_WARNING_LEVEL) {
            BOOST_LOG_TRIVIAL(warning)
                << "Small number of blocks on kernel launch, expect "
                   "performance degradation.";
        }
        dim3 grid(nblocks_x, nblocks_y);
        BOOST_LOG_TRIVIAL(debug)
            << "Invoking kernel with TILE_SIZE " << TILE_SIZE << ", grid "
            << grid << ", nthreads " << NTHREADS;
        kernels::detect_and_accumulate<Policy, TILE_SIZE>
            <<<grid, NTHREADS, 0, _stream>>>(
                thrust::raw_pointer_cast(input.data()) +
                    input_offset * input.nchannels(),
                thrust::raw_pointer_cast(input.data()) +
                    (input.nsamples() + input_offset) * input.nchannels(),
                thrust::raw_pointer_cast(_current_block->data()),
                input.nchannels(),
                naccumulate);
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
    }

    std::size_t _naccumulate;
    Allocator& _allocator;
    bool _first_call;
    cudaStream_t _stream;
    Timestamp _timestamp;
    OutputPtr _current_block;
    std::size_t _remaining;
};

/**
namespace kernels {

// cufftDx kernel
// TFTP input
// Each block loads TTP data in PT oder
// TT length = nchans * naccumulate (typically 16 and 150 to 200)

template <typename FFT, typename InputType>
requires Vec2<InputType>
__global__ void fft_detector_accumulator(InputType* input, typename
FFT::workspace_type workspace)
{
    using complex_type = FFT::value_type;
    extern __shared__ __align__(alignof(float4)) complex_type shared_mem[];
    complex_type* cufftdx_shared_memory = shared_mem;


    extern __shared__ int shared_memory[];
    InputType *integerData = s;                        // nI ints
    float *floatData = (float*)&integerData[nI]; // nF floats
    char *charData = (char*)&floatData[nF];      // nC chars


}


} // namespace kernels




template <typename Container> requires (std::is_same_v<typename
Container::value_type, char2>) using PFBData = DescribedData<Container,
TimeDimensionN<0>, FrequencyDimension, TimeDimensionN<1>,
PolarisationDimension>;

template <typename Container>
using FilterbankData = DescribedData<Container, TimeDimension,
FrequencyDimension>;

template <typename InputType, typename OutputType>
class FFTDetectorAccumulator;

template <typename InputContainer, typename OutputContainer, typename Allocator,
typename Policy> requires(Vec2<typename InputContainer::value_type> &&
             is_device_container_v<InputContainer> &&
             is_device_container_v<OutputContainer>)
class FFTDetectorAccumulator <
    DescribedData<InputContainer,
                  PolarisationDimension,
                  TimeDimension,
                  ContiguousFrequencyDimension>,
    DescribedData<OutputContainer,
                  PolarisationDimension,
                  TimeDimension,
                  ContiguousFrequencyDimension>>
{


};
*/
} // namespace psrdada_cpp