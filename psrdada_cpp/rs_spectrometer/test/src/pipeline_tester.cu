#include "psrdada_cpp/rs_spectrometer/test/pipeline_tester.cuh"

namespace psrdada_cpp {
namespace rs_spectrometer {
namespace test {

PipelineTester::PipelineTester()
    : ::testing::TestWithParam<test_config>(),
    _config(GetParam())
{
    BOOST_LOG_TRIVIAL(debug) << "Constructing PipelineTester object";
    header.resize(4096);
    idata.resize(_config.nitems());
    raw_data.reset(new RawBytes(
        reinterpret_cast<char *>(idata.data()), idata.size(), idata.size()));
    raw_header.reset(new RawBytes(
        reinterpret_cast<char *>(header.data()), header.size(), header.size()));
    test_object.reset(new Pipeline(
        _config.input_nchans, _config.fft_length, _config.naccumulate,
        _config.nskip, "/tmp/dc_power_test.bin", 0.0f));
}

PipelineTester::~PipelineTester()
{


}

void PipelineTester::SetUp()
{

}

void PipelineTester::TearDown()
{

}

void PipelineTester::run_dc_power_test()
{
    test_object->init(*raw_header);
    // Fill the buffer
    for (std::size_t ii = 0; ii < idata.size(); ii++)
    {
            idata[ii].x = htons(static_cast<unsigned short>(1));
            idata[ii].y = 0;
    }
    // Process the buffers
    for (size_t ii=0; ii < _config.nblocks(); ++ii)
    {
        if (test_object->operator()(*raw_data))
        {
            break;
        }
    }

    std::vector<float> acc_spec(_config.input_nchans * _config.fft_length);
    std::ifstream infile;
    infile.open("/tmp/dc_power_test.bin", std::ifstream::in | std::ifstream::binary);
    if (!infile.is_open())
    {
        std::stringstream stream;
        stream << "Could not open file " << "/tmp/dc_power_test.bin";
        throw std::runtime_error(stream.str().c_str());
    }
    infile.read(reinterpret_cast<char*>(acc_spec.data()), _config.input_nchans * _config.fft_length * sizeof(float));
    infile.close();


    for (std::size_t chan_idx = 0; chan_idx < acc_spec.size(); chan_idx+=_config.fft_length)
    {
        float expected_peak = 10 * log10f(1000.0f
            / (FSW_IMPEDANCE * _config.fft_length * _config.fft_length / 4));
        for (std::size_t fft_idx = 0; fft_idx < _config.fft_length; fft_idx++)
        {
            if (fft_idx == _config.fft_length-1/2)
            {
                ASSERT_NEAR(acc_spec[chan_idx + fft_idx], expected_peak, expected_peak * 0.00001f);
            }
            else if(fft_idx < _config.fft_length/2)
            {
                ASSERT_TRUE(std::isinf(acc_spec[chan_idx + fft_idx]));
            }
        }
    }
    EXPECT_EQ(0, remove("/tmp/dc_power_test.bin")) << "Error deleting file '/tmp/dc_power_test.bin'";
}

TEST_P(PipelineTester, test_dc_offset)
{
    run_dc_power_test();
}

INSTANTIATE_TEST_SUITE_P (ParameterizedTest, PipelineTester, testing::Values(
    // input_nchans | fft_length | naccumulate | nskip | nspec_per_block
    test_config({1, 16384, 4, 0, 16}),
    // test_config({1, 32000, 256, 0, 1}), ToDo: Fails do to not written file. It seems the pipeline is not processing
    test_config({1, 4096, 8, 0, 64})
));

} //namespace test
} //namespace rs_spectrometer
} //namespace psrdada_cpp

