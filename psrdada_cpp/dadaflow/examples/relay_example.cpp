#include "psrdada_cpp/dadaflow/Graph.hpp"
#include "psrdada_cpp/dadaflow/processors/Multiplexer.hpp"
#include "psrdada_cpp/dadaflow/processors/Relay.hpp"
#include <iostream>
#include <thread>
#include <memory>
#include <chrono>
#include <mutex>
#include <optional>

using namespace psrdada_cpp;
using namespace std::chrono_literals;

int main()
{
    using T = int;
    using Tptr = std::shared_ptr<T>;
    std::mutex writer_mutex;
    
    Graph graph;
    
    auto& counter = graph.add_node([val=0]() mutable ->Tptr {
        std::this_thread::sleep_for(100ms);
        return std::make_shared<T>(val++);
    }, "counter");
    
    auto& multiplexer = graph.add_node(Multiplexer<2, T>(), "multiplexer");
    
    auto& relayA = graph.add_node(Relay<Tptr>(), "relay");
    
    auto& relayB = graph.add_node(Relay<Tptr>(), "relay");
    
    auto& receiverA = graph.add_node([&](Tptr input) ->void {
        std::unique_lock<std::mutex> lock(writer_mutex);
        std::cout << "Received " << *input << " in A\n";
    }, "receiver A");
    
    auto& receiverB = graph.add_node([&](Tptr input) ->void {
        std::unique_lock<std::mutex> lock(writer_mutex);
        std::cout << "Received " << *input << " in B\n";
    }, "receiver B");
    
    graph.connect<0,0>(counter, multiplexer, "c->m", 3);
    graph.connect<0,0>(multiplexer, relayA, "m->rA", 3);
    graph.connect<1,0>(multiplexer, relayB, "m->rB", 3);
    graph.connect<0,0>(relayA, receiverA, "rA-recvA", 3);
    graph.connect<0,0>(relayB, receiverB, "rB-recvB", 3);
    graph.prepare();

    std::jthread toggle([&](){
        std::this_thread::sleep_for(2s);
        relayA.get_callable().disable();
        std::this_thread::sleep_for(2s);
        relayB.get_callable().disable();
        relayA.get_callable().enable();
        std::this_thread::sleep_for(2s);
        relayB.get_callable().enable();
        graph.stop();
    });
    
    graph.run_async();
    graph.await();
    return 0;
}
