if(ENABLE_BENCHMARK)
    if(NOT FORCE_DOWNLOAD)
        find_package(benchmark QUIET)
        if(benchmark_FOUND)
            message(STATUS "Googlebenchmark found! Version: ${benchmark_VERSION}!")
        endif()
    endif()
    if(FORCE_DOWNLOAD OR NOT benchmark_FOUND)
        message(STATUS "Fetching content for googlebenchmark from remote..")
        include(FetchContent)
        FetchContent_Declare(
            googlebenchmark
            GIT_REPOSITORY https://github.com/google/benchmark.git
            GIT_TAG v1.8.3
            GIT_PROGRESS TRUE
            INSTALL_COMMAND "")
        # The flags are set to OFF to avoid dependency to googletest
        set(BENCHMARK_USE_BUNDLED_GTEST, OFF CACHE BOOL "Disable testing" FORCE)
        set(BENCHMARK_ENABLE_GTEST_TESTS OFF CACHE BOOL "Disable testing" FORCE)
        set(BENCHMARK_ENABLE_TESTING OFF CACHE BOOL "Disable testing" FORCE)
        # Ensure the content is downloaded and built
        FetchContent_MakeAvailable(googlebenchmark)
        set(BENCHMARK_INCLUDE_DIR ${googlebenchmark_SOURCE_DIR}/include)
    endif()
    set(BENCHMARK_LIBRARIES benchmark::benchmark benchmark::benchmark_main)
endif()
